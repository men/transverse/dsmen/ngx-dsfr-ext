(N'hésitez pas à supprimer les paragraphes dont vous n'avez pas besoin)

### Décrire le bug

Une description précise du bug avec captures d'écrans ou Stackblitz si possible.

### Comportement attendu

Une description claire et concise de ce qui devrait se produire.

### Configuration et système utilisé

- **Version du ngx-dsfr-ext : **
- **Appareil (mobile, tablette, desktop) : **
- **Système d’exploitation : **
- **Navigateur (et version si possible) : **

### Informations complémentaires

Ajouter toute autre information concernant le problème.
