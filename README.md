# ngx-dsfr-ext - Readme

NgxDsfrExt (`@edugouvfr/ngx-dsfr-ext`) est une extension au package [`@edugouvfr/ngx-dsfr`](https://www.npmjs.com/package/@edugouvfr/ngx-dsfr).

Ce package a pour but de couvrir les besoins fonctionnels qui ne sont pas encore couverts par le DSFR. Elle a été construite en cohérence avec les composants DSFR existants, dans le respect des fondamentaux techniques et des principes de design du DSFR.

🔥 Attention : ces composants ne sont pas des éléments <b> officiels </b> du [DSFR](https://www.systeme-de-design.gouv.fr/).

Cette bibliothèque est développée, maintenue et gérée par une équipe de développement du Ministère de l'Éducation
Nationale basée à Grenoble et pilotée par les sous-directions "socle numérique" (SOCLE1) et "services numériques" (SN2)
de la Direction du Numérique pour l'Éducation (DNE).

## Dépendances

Ce package dépend de `@edugouvfr/ngx-dsfr` version 1.13.0 ou ultérieure.
( cf. Installation de [@edugouv/ngx-dsfr](https://foad.phm.education.gouv.fr/edugouvfr/ngx-dsfr/) )

Nous développons avec Angular 17, mais notre bibliothèque est compatible de la 17 à 19.
Le package `@angular/cdk` correspondant à votre version d'Angular est également requis à partir de la version 0.6.

Pour utiliser certains composants, des dépendances supplémentaires peuvent être nécessaires :

- pour le composant de grid `dsfr-ext-ag-grid` : **ag-grid-community ⩾ 32** et **ag-grid-angular ⩾ 32**
- pour le composant éditeur `dsfr-ext-editor` : **quill ⩾ 2**

| ngx-dsfr-ext | ngx-dsfr | Angular | ag-grid |  node  | quill |
| :----------: | :------: | :-----: | :-----: | :----: | :---: |
|    1.1.0     | >=1.13 |  >=17   |   ^32   | >18.19 |  ^2   |

## Licence et droit d'utilisation

Le contenu de ce projet est placé sous licence [EUPL 1.2.](https://eupl.eu/1.2/fr/).

## Installation

- Installer les dépendances vers les modules NPM obligatoires

  ```
  $ npm install --save @edugouvfr/ngx-dsfr
  $ npm install --save @angular/cdk
  ```

- Installer la dépendance vers le module NPM dans votre projet Angular

  ```bash
  $ npm install --save @edugouvfr/ngx-dsfr-ext
  ```

- Installer les dépendances optionnelles (cf. _Dépendances_)

## Utilisation

- Dans votre module ou composant standalone, importer le composant que vous souhaitez utiliser :

  ```typescript
  import { DsfrMultiselectComponent } from 'edugouvfr/ngx-dsfr-ext';
  import { DsfrDatePickerComponent } from '@edugouvfr/ngx-dsfr-ext';

  @Component({
  imports: [
    DsfrDatePickerComponent,
    DsfrMultiselectComponent
  ],
  })
  export class MonComposant {}
  ```

- Puis utiliser le composant dans votre template html :

  ```html
  <dsfrx-multiselect>...</dsfrx-multiselect>
  ```

## Documentation

Se référer au Storybook de la version que vous utilisez :

https://foad.phm.education.gouv.fr/edugouvfr/ngx-dsfr-ext/

Icônes de la documentation :

- 📌 Note
- 🔥 Point d'attention
- 👓 Accessibilité

## Code source

Le code source de cette bibliothèque de composants est publié sur mim-libre :

https://gitlab.mim-libre.fr/men/transverse/dsmen/ngx-dsfr-ext

## Composants disponibles

- Grille de données (datagrid)

  Le composant `dsfrx-ag-grid` permet de représenter des tableaux complexes de données notamment pour les applications de gestion.
  Il s'appuie sur la librairie [ag-grid](https://www.ag-grid.com/angular-data-grid/) en version `Community` auquel s'applique l'aspect du tableau du composant table DSFR.

  Il permet une pagination (utilisation du composant de pagination de ngx-dsfr), des filtres, et du tri côté serveur ou client.
  Les autres fonctionnalités prévues par AgGrid Community sont également applicables au travers du composant.

- Autocomplete: composant d'autocomplétion intégrant un `dsfr-form-input`
- Carrousel
- Calendar
- Datepicker: sélecteur de date avec le composant `calendar`
- DropdownMenu: composant de menu déroulant (pattern menu button)
- Editeur : composant wysiwig d'édition basé sur la librairie `quilljs`
- Multiselect: liste déroulante avec multi-sélection et champ de recherche
- Spinner: indicateur de chargement
- Panel: composant de container simple
- Progressbar: indicateur d'état de processus
- Timeline: mise en page d'évènements en utilisant les cartes du DSFR
- Toaster: service `DsfrToastService` pour afficher des notifications de type toasts
- Tree: affichage d'informations sous forme d'arborescence
- Treeselect: liste déroulante sous forme d'arborescence avec multi-sélection et champ de recherche

## Feuille de route 

### Chore

- Compatibilité `ag-grid` v33

### Formulaires

- Amélioration du composant multiselect 
- Amélioration du composant éditeur

### Agencement

- Composant `en-tête connecté` pour le header **_en cours_**
- Section
- Toolbar

### Accompagnement utilisateur

- Préfiguration de chargement (skeleton)
- BlockUI

## Demander une évolution ou signaler une anomalie

### Équipes internes Éduc. Nat.

- Passer par les [issues du Gitlab Forge](https://gitlab.forge.education.gouv.fr/dsmen/components/ngx-dsfr-ext/-/issues)

### Autres ministères ou délégations de service public

- Passer par les [issues Gitlab Mim-Libre](https://gitlab.mim-libre.fr/men/transverse/dsmen/ngx-dsfr-ext/-/issues)

## Contact

- Liste de diffusion de l'équipe de développement [dsmen-core@ldiff.forge.education.gouv.fr](mailto:dsmen-core@ldiff.forge.education.gouv.fr)
- [Canal Mattermost](https://mattermost.forge.education.gouv.fr/tyforge/channels/ngx-dsfr-components) (interne forge Éduc. Nat.)
- [Discord](https://discord.gg/3Yq486xa)