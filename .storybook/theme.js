import { create } from '@storybook/theming';
import { version } from '../projects/ngx-dsfr-ext/package.json';


export default create({
  base: 'light',
  brandTitle: `
  <style>
  .link-brand {
    height: 100%;
    margin: -3px -4px;
    padding: 2px 3px;
    border: 1px solid transparent;
    border-radius: 3px;
    color: inherit;
    text-decoration: none;
  }
  .link-discord {
    display: inline-block;
    //border: 1px solid #d6e0e7;
    color: grey;
    margin-top: 8px;
  }
  .link-discord:hover {
    color: rgb(2, 156, 253);
    background: rgba(2, 156, 253, 0.14);
  }
  .link-discord:focus {
    box-shadow: rgb(2, 156, 253) 0px 0px 0px 1px inset;
    outline: none;
  }
  .link-discord img {
    padding-top: 3px;
    vertical-align: sub;
  }
  .link-discord span {
    margin-left: 4px;
    margin-right: 8px;
  }  
  </style>
     <a href=".." target="_blank" class="link-brand css-mq3wcb">
      <div>
        <img src="img/marianne.svg" height="16px"/>
      </div>
      <div style="font-size:1.1rem">ngx-dsfr</div>
      <div style="font-size:0.8rem;color:grey">${version}</div>
    </a>
    <a title="Ouvrir discord - nouvelle fenêtre" class="link-brand link-discord" href="https://discord.gg/3Yq486xa" target="_blank"> 
      <img src="img/discord-fill.svg" height="18px"/> 
      <span> Discord </span>
    </a>
  `,
  brandUrl: null,
  // brandImage: "img/marianne.svg",
});
