import { EventEmitter } from '@angular/core';
import { componentWrapperDecorator } from '@storybook/angular';

// -- Constantes -------------------------------------------------------------------------------------------------------
export const controlEmitter = { control: EventEmitter };
export const controlDisabled = { table: { disable: true } };

export function dsfrDecorators(title: string, info?: string): any {
  return [titleDecorator(title, info)];
}

export function titleDecorator(title: string, info?: string, id?: string): any {
  const infoPart = info ? `<div class="sb-message">${info}</div>` : ``;
  const idPart = id ? `id="${id}" ` : ``;

  return componentWrapperDecorator((story) => `<div ${idPart}class="sb-title">${title}${infoPart}</div>${story}`);
}

// export function titleDecorator(title: string, info?: string, id?: string): any {
//   if (!info) return componentWrapperDecorator((story) => `<div class="sb-title">${title}</div>${story}`);
//   return componentWrapperDecorator(
//     (story) => `<div class="sb-title">${title}</div><div class="sb-message">${info}</div>${story}`,
//   );
// }

export const gridDecoratorLG = gridDecorator('LG');
export const gridDecoratorMD = gridDecorator('MD');
export const gridDecoratorSM = gridDecorator('SM');

function gridDecorator(size: string): any {
  let gridClass: string;
  if (size === 'SM') gridClass = 'fr-col-6 fr-col-md-3 fr-col-lg-3';
  else if (size === 'LG') gridClass = 'fr-col-12 fr-col-md-8 fr-col-lg-6';
  else gridClass = 'fr-col-9 fr-col-md-4 fr-col-lg-4';
  return componentWrapperDecorator(
    (story) => `<div class="fr-mb-6v">
    <div class="fr-grid-row fr-grid-row--gutters">
        <div class="${gridClass}">
      ${story}
    </div>
  </div>
</div>
`,
  );
}
