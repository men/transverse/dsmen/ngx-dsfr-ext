import { setCompodocJson } from '@storybook/addon-docs/angular';
import { withThemeByDataAttribute } from '@storybook/addon-themes';
import docJson from '../documentation.json';

/**
 * Ajout de documentation pour les propriétés dépréciées, et déplacer en bas de la liste.
 *
 * A supprimer quand SB prendra en charge les annotations 'deprecated'.
 *
 * @see https://github.com/storybookjs/storybook/issues/9721
 */
function displayDeprecated(list) {
  return list
    .map((prop) => ({
      ...prop,
      rawdescription: prop.deprecated
        ? `<p>
            <p>${prop.rawdescription ?? ''}</p>
            <p class="deprecated">
              <span class="flag">deprecated</span>
              <span class="message">${prop.deprecationMessage ?? ''}</span>
            </p>
           </p>`
        : prop.rawdescription ?? '',
    }))
    .sort((a, b) => {
      return a.deprecated && !b.deprecated ? 1 : -1;
    });
}

const additionalCompoDoc = (doc) => ({
  ...doc,
  components: doc.components.map((component) => ({
    ...component,
    inputsClass: displayDeprecated(component.inputsClass),
    propertiesClass: displayDeprecated(component.propertiesClass),
    outputsClass: displayDeprecated(component.outputsClass),
  })),
});

setCompodocJson(additionalCompoDoc(docJson));

const preview = {
  parameters: {
    controls: {},
    docs: {
      toc: {
        disable: false,
        headingSelector: 'h2, h3',
        ignoreSelector: '.toc-ignore, .edu-grid__title, .fr-accordion__title, .fr-card__title, .month-year',
      },
      story: {
        inline: true,
      },
    },
    options: {
      storySort: {
        method: 'alphabetical',
        locales: 'fr-FR',
        order: ['Introduction', ['Readme', 'Changelog', 'Migration'], 'Components'],
      },
    },
  },
};

export default preview;

export const decorators = [
  withThemeByDataAttribute({
    themes: {
      light: 'light',
      dark: 'dark',
    },
    defaultTheme: 'light',
    attributeName: 'data-fr-scheme',
  }),
];
