import type { StorybookConfig } from '@storybook/angular';

const config: StorybookConfig = {
  stories: ['../projects/ngx-dsfr-ext/**/*.mdx', '../projects/ngx-dsfr-ext/**/*.stories.@(js|jsx|ts|tsx)'],
  addons: [
    '@storybook/addon-essentials',
    '@storybook/addon-themes',
    '@storybook/addon-links',
    '@storybook/addon-actions',
    '@storybook/addon-a11y',
  ],
  staticDirs: ['./assets', '../projects/ngx-dsfr-ext/src/docs/assets', '../node_modules/@gouvfr/dsfr/dist'],
  babel: async (options: any) => ({
    ...options,
    compact: true,
  }),
  framework: {
    name: '@storybook/angular',
    options: {},
  },
  docs: {},
};

export default config;
