module.exports = {
  preset: 'jest-preset-angular',
  setupFilesAfterEnv: ['<rootDir>/setup-jest.ts'],
  modulePathIgnorePatterns: ['<rootDir>/dist/'],
  coverageReporters: ['json', 'html'],
  transformIgnorePatterns: ['node_modules/(?!(@angular|@edugouvfr|quill|lodash-es|parchment))'],
  // moduleNameMapper: {
  //   // Force module uuid to resolve with the CJS entry point, because Jest does not support package.json.exports. See https://github.com/uuidjs/uuid/issues/451
  //   uuid: require.resolve('uuid'),
  // },
};
