import { controlEmitter, titleDecorator } from '.storybook/storybook-utils';
import { FormsModule } from '@angular/forms';
import { DsfrSeverityConst } from '@edugouvfr/ngx-dsfr';
import { Meta, moduleMetadata, StoryObj } from '@storybook/angular';
import { DsfrEditorComponent } from './editor.component';

const meta: Meta<DsfrEditorComponent> = {
  title: 'Components/Editor',
  component: DsfrEditorComponent,
  decorators: [moduleMetadata({ imports: [FormsModule] })],
  argTypes: {
    messageSeverity: { control: 'inline-radio', options: ['info', 'success', 'error'] },
    valueChange: { control: controlEmitter },
  },
};
export default meta;
type Story = StoryObj<DsfrEditorComponent>;

export const Default: Story = {
  decorators: titleDecorator('Defaut'),
  args: {
    editorId: 'myDefaultEditor',
    label: 'Libellé champ de saisie',
    labelSrOnly: false,
  },
};

export const Labelled: Story = {
  decorators: titleDecorator('Labelled'),
  args: {
    editorId: 'myLabelledEditor',
    ariaLabelledBy: 'editorLabel',
  },
  render: (args) => ({
    props: { ...args },
    template: `
     <span id="editorLabel" class="fr-label" style="margin-bottom:0.5rem">Libellé champ de saisie</span>
      <dsfrx-editor [editorId]="editorId" 
                    [ariaLabelledBy]="ariaLabelledBy"
                    [showEditorHtmlDebugPanel]="showEditorHtmlDebugPanel"
                    [showSemanticHtmlDebugPanel]="showSemanticHtmlDebugPanel">
      </dsfrx-editor>
    `,
  }),
};

export const Message: Story = {
  decorators: titleDecorator('Message', undefined, 'myAccessibleLabel'),
  args: {
    editorId: 'myMessageEditor',
    ariaLabelledBy: 'myAccessibleLabel',
    message: `Message associé au champ de saisie`,
    messageSeverity: DsfrSeverityConst.ERROR,
    messagesGroupId: 'myMessageGroupForRichEditor',
  },
};

const myHtmlModel1 = {
  value: `
    <strong>Hello</strong>&nbsp;<u>world</u>
  `,
};

export const Output: Story = {
  decorators: titleDecorator('Using valueChange Output', undefined, 'myAccessibleLabel'),
  args: {
    editorId: 'myOutputEditor',
    ariaLabelledBy: 'myAccessibleLabel',
  },
  render: (args) => ({
    props: { ...args, myHtmlModel1, onValueChanged },
    template: `
      <dsfrx-editor [editorId]="editorId"
                    [value]="myHtmlModel1.value"
                    [ariaLabelledBy]="ariaLabelledBy"
                    (valueChange)="onValueChanged($event)"></dsfrx-editor>

      <code style="display:inline-block;margin-top:20px">model : {{ myHtmlModel1 | json }}</code>
    `,
  }),
};

function onValueChanged(value: string) {
  console.log('onValueChanged', value);
  //FIXME: je voudrais tracer une action dans le panel SB
}

const myHtmlModel2 = {
  value: `
    <h2>Hello world</h2>
  `,
};

export const TwoWayBinding: Story = {
  name: 'Two-Way Binding',
  decorators: titleDecorator('Using two-way binding', undefined, 'myAccessibleLabel'),
  args: {
    editorId: 'myTwoWayBindingEditorr',
    ariaLabelledBy: 'myAccessibleLabel2qd',
    showEditorHtmlDebugPanel: true,
    showSemanticHtmlDebugPanel: true,
  },
  render: (args) => ({
    props: { ...args, myHtmlModel2 },
    template: `
      <dsfrx-editor [editorId]="editorId"  
                    [(value)]="myHtmlModel2.value"
                    [ariaLabelledBy]="ariaLabelledBy"
                    [showEditorHtmlDebugPanel]="showEditorHtmlDebugPanel"
                    [showSemanticHtmlDebugPanel]="showSemanticHtmlDebugPanel"></dsfrx-editor>

      <code style="display:inline-block;margin-top:20px">model : {{ myHtmlModel2 | json }}</code>
    `,
  }),
};

const myHtmlModel3 = {
  value: `
    <p class="foobar">Une liste d'<span style="color:tomato">éléments</span> :</p>
    <ul>
      <li style="background-color:thistle">&Eacute;l&eacute;ment #1</li>
      <li style="background-color:darkkhaki">&Eacute;l&eacute;ment #2</li>
      <li style="text-decoration: line-through; background-color:lightsteelblue">&Eacute;l&eacute;ment #3</li>
    </ul>
  `,
};

export const NgModel: Story = {
  name: 'NgModel',
  decorators: titleDecorator('Usage avec NgModel'),
  args: {
    editorId: 'myNgModelEditor',
    showEditorHtmlDebugPanel: true,
    showSemanticHtmlDebugPanel: false,
  },
  render: (args) => ({
    props: { ...args, myHtmlModel3 },
    template: `
      <dsfrx-editor [editorId]="editorId" 
                    [(ngModel)]="myHtmlModel3.value" 
                    [ariaLabelledBy]="ariaLabelledBy"
                    [showEditorHtmlDebugPanel]="showEditorHtmlDebugPanel"
                    [showSemanticHtmlDebugPanel]="showSemanticHtmlDebugPanel">
      </dsfrx-editor>

      <code style="display:inline-block;margin-top:20px">model : {{ myHtmlModel3 | json }}</code>
    `,
  }),
};
