import {
  Component,
  CUSTOM_ELEMENTS_SCHEMA,
  EventEmitter,
  forwardRef,
  Input,
  OnDestroy,
  OnInit,
  Output,
  ViewEncapsulation,
} from '@angular/core';
import { FormsModule, NG_VALUE_ACCESSOR, ReactiveFormsModule } from '@angular/forms';
import {
  DefaultValueAccessorComponent,
  DsfrButtonModule,
  DsfrFormSelectModule,
  DsfrI18nService,
  DsfrSeverity,
  DsfrSkiplinksModule,
  Language,
  LoggerService,
} from '@edugouvfr/ngx-dsfr';

import { MESSAGES_EN } from './i18n/messages.en';
import { MESSAGES_FR } from './i18n/messages.fr';

import { Subscription } from 'rxjs';
import { EduEditorContentComponent } from './components/content/editor-content.component';
import { DEFAULT_TOOLBAR_OPTIONS } from './components/toolbar/editor-toolbar-default-options';
import { EduEditorToolbarComponent } from './components/toolbar/editor-toolbar.component';
import { ToolbarControl } from './components/toolbar/editor-toolbar.model';
import { DsfrEditorService } from './editor.service';

// Basé sur https://github.com/KillerCodeMonkey/ngx-quill
// Demo sur https://killercodemonkey.github.io/ngx-quill-example/

@Component({
  selector: 'dsfr-ext-editor, dsfrx-editor',
  encapsulation: ViewEncapsulation.None,
  templateUrl: './editor.component.html',
  styleUrls: ['./editor.component.scss'],
  standalone: true,
  imports: [
    FormsModule,
    ReactiveFormsModule,
    DsfrSkiplinksModule,
    DsfrFormSelectModule,
    DsfrButtonModule,
    EduEditorToolbarComponent,
    EduEditorContentComponent,
  ],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => DsfrEditorComponent),
      multi: true,
    },
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
})
export class DsfrEditorComponent extends DefaultValueAccessorComponent<string> implements OnInit, OnDestroy {
  /**
   * Options de la toolbar.
   */
  @Input() toolbarOptions: ToolbarControl[] = DEFAULT_TOOLBAR_OPTIONS;

  /**
   * Permet de spécifier l'identifiant à utiliser pour le composant d'édition.
   * C'est utile si vous souhaitez disposer de plusieurs éditeurs sur un même écran.
   *
   * Attention : cet input est statique et doit être connu à l'initialisation du composant.
   */
  @Input() editorId = 'editor';

  /**
   * Libellé du champ de saisie riche.
   *
   * Obligatoire si vous ne gérez pas un libellé accessible via aria-Labelledby.
   *
   * Il est possible de masquer visuellement ce libellé via la propriété `labelSrOnly`.
   */
  @Input() label: string;

  /**
   * Permet de masquer visuellement le libellé en le réservant aux dispositifs de restitution.
   */
  @Input() labelSrOnly = false;

  /**
   * Renseigner cette propriété si vous souhaitez référencer un élément dénotant le champ d'édition au niveau de
   * l'attribut aria-labelledby positionné au niveau de l'élément hôte de l'éditeur HTML.
   */
  @Input() ariaLabelledBy: string;

  /**
   * Active la lecture seule.
   */
  //TODO: @Input() readOnly = false;

  /** Ajoute une validation comme étant un champs required */
  //TODO: @Input() required = false;

  /** Utilise DomSanitizer. Uniquement pour le format `html`. */
  //TODO: @Input() sanitize = false;

  /** Ajoute une valeur maximale de caractère pour validation. */
  //TODO: @Input() maxLength: number;

  /** Ajoute une valeur minimal de caractère pour validation. */
  //TODO: @Input() minLength: number;

  /**
   * Message d'information associé au composant.
   */
  @Input() message: string;

  /**
   * Représente la sévérité du message. 🔥 `WARNING` n'est pas géré dans cette version.
   */
  @Input() messageSeverity: DsfrSeverity;

  /**
   * Id de l'élément affichant les messages associé à l'éditeur. A renseigner si vous souhaitez positionner un attribut
   * `aria-describedby` référençant votre message au niveau de l'élément hôte de l'éditeur.
   *
   * Attention : cet input est statique et doit être connu à l'initialisation du composant.
   */
  @Input() messagesGroupId: string;

  /**
   * Dev/debug : permet d'afficher le code HTML interne de l'éditeur (i.e. celui qui sert pour l'affichage WYSIWYG).
   */
  @Input() showEditorHtmlDebugPanel = false;

  /**
   * Dev/debug : permet d'afficher le code HTML qui sera récupéré en sortie.
   */
  @Input() showSemanticHtmlDebugPanel = false;

  /**
   * Signale les changements sur le contenu HTML.
   */
  @Output() valueChange: EventEmitter<string> = new EventEmitter<string>();

  /**
   * Gestion de l'identifiant du libellé accessible.
   */
  protected labelId: string;

  private _sub: Subscription;

  constructor(
    private readonly i18n: DsfrI18nService,
    private readonly logger: LoggerService,
    private readonly editorService: DsfrEditorService,
  ) {
    super();
    this.labelId = 'label-' + this.editorId;
    if (!this.ariaLabelledBy) {
      this.ariaLabelledBy = this.labelId;
    }
    this.i18n.extendsLabelsBundle(Language.EN, MESSAGES_EN);
    this.i18n.extendsLabelsBundle(Language.FR, MESSAGES_FR);
  }

  ngOnInit() {
    this._sub = this.editorService.editorChange$.subscribe((value: string) => {
      if (this.value !== value) {
        this.value = value && value.length ? value : '';
        this.valueChange.emit(this.value);
      }
    });
    // Si la valeur est non-nulle, on émet un changement de modèle pour forcer la mise à jour le contenu de l'éditeur
    // car si l'utilisateur passe par une syntaxe [value]="<s>foo</s>" et non par la directive [ngModel]
    // le writeValue ne sera pas appelé, donc l'éditeur ne sera pas notifié et ne se mettra pas à jour
    if (this.value) {
      this.editorService.emitModelChange(this.value);
    }
  }

  ngOnDestroy(): void {
    this._sub?.unsubscribe();
  }

  /** @internal */
  override writeValue(value: string | undefined): void {
    super.writeValue(value);
    if (value) {
      this.editorService.emitModelChange(value);
    }
  }
}
