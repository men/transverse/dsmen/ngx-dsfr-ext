import { expect, test } from '@playwright/test';
import { getSbFrame } from 'projects/ngx-dsfr-ext/shared/e2e.utils';

test('editor.default', async ({ page }) => {
  const frame = getSbFrame(page);
  const editorElt = frame.locator('#myDefaultEditor');
  const toolbarElt = frame.locator('.editor-toolbar');
  const qlEditorElt = editorElt.locator('.ql-editor');
  const boldElt = frame.locator('[data-testid="bold"] button');
  const selectHeadingLevelElt = toolbarElt.locator('[data-testid="header"] select');
  const selectFontSizeElt = toolbarElt.locator('[data-testid="size"] select');

  await page.goto('?path=/story/components-editor--default');

  await expect(editorElt).toHaveId('myDefaultEditor');

  await expect(toolbarElt).toHaveAttribute('tabindex', '0');
  await expect(toolbarElt).toHaveAttribute('aria-controls', 'myDefaultEditor');

  await qlEditorElt.focus();

  await qlEditorElt.pressSequentially('Hello World');
  await qlEditorElt.selectText();
  await boldElt.click();
  await expect(qlEditorElt.locator('p > strong')).toHaveText('Hello World');
  // on défait la mise en gras via un raccourci clavier
  await qlEditorElt.press('Control+b');
  await expect(qlEditorElt.locator('p > strong')).toHaveCount(0);

  await toolbarElt.focus();
  await expect(selectHeadingLevelElt).toBeFocused();
  await toolbarElt.press('Tab');
  // aucun élément de la toolbar n'est focusable
  await expect(selectFontSizeElt).not.toBeFocused();
  // de manière préserver la prise de focus directe vers l'éditeur
  await expect(qlEditorElt).toBeFocused();
  // on reprend le focus sur la toolbar
  await toolbarElt.focus();
  // mais cette fois on navigue en utilisant la flêche droite
  await toolbarElt.press('ArrowRight');
  // et cette fois le second select doit récupérer le focus
  await expect(selectFontSizeElt).toBeFocused();
  // si l'utilisateur navigue ainsi jusqu'à la fin de la toolbar, s'assurer qu'il boucle et se retrouve sur le select
  // de niveau de titre au début de la toolbar
  // on récupère le nombre de contrôles de formatage, sachant qu'on est actuellement sur le deuxième select
  // on avance de manière à se reboucler sur le premier select de la toolbar
  const controlsCount = await toolbarElt.locator('button, select').count();
  for (let index = 0; index < controlsCount - 1; index++) {
    await toolbarElt.press('ArrowRight');
  }
  await expect(selectHeadingLevelElt).toBeFocused();
  // même chose en sens inverse :
  await toolbarElt.press('ArrowLeft');
  // s'assurer qu'on repart bien vers le dernier élément de la toolbar
  await expect(toolbarElt.locator('[data-testid="clean"] button')).toBeFocused();
});

test('editor.labelled', async ({ page }) => {
  const frame = getSbFrame(page);
  const editorElt = frame.locator('#myLabelledEditor');
  const labelElt = frame.locator('#editorLabel');

  await page.goto('?path=/story/components-editor--labelled');

  await expect(editorElt).toHaveId('myLabelledEditor');

  await expect(labelElt).toContainText('Libellé champ de saisie');
});

test('editor.message', async ({ page }) => {
  const frame = getSbFrame(page);
  const messageElt = frame.locator('p.fr-message.fr-message--error');

  await page.goto('?path=/story/components-editor--message');

  await expect(messageElt).toContainText('Message associé au champ de saisie');
});

test('editor.ngmodel', async ({ page }) => {
  const frame = getSbFrame(page);
  const editorContentElt = frame.locator('.ql-editor');
  const p = editorContentElt.locator('p');
  const li = editorContentElt.locator('ol > li');

  await page.goto('?path=/story/components-editor--ng-model');

  await expect(editorContentElt).toHaveCount(1);

  // on teste que la valeur du modèle a bien été restranscrite dans l'éditeur
  await expect(p).toContainText(`Une liste d'éléments`);

  await expect(li).toHaveCount(3);
});
