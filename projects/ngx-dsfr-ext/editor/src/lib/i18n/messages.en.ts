export const MESSAGES_EN = {
  editor: {
    placeholder: 'Veuillez éditer votre contenu',
    controls: {
      align: { left: 'Left', center: 'Center', right: 'Right', justify: 'Justify' },
      background: `Background color`,
      blockquote: 'Quote',
      clean: 'Reset format',
      bold: 'Bold',
      code: 'Code block',
      color: 'Text color',
      direction: { normal: 'Left to right', rtl: 'Right to left' },
      header: {
        'label': 'Heading level',
        '0': 'Normal',
        '1': 'Heading 1',
        '2': 'Heading 2',
        '3': 'Heading 3',
        '4': 'Heading 4',
        '5': 'Heading 5',
        '6': 'Heading 6',
      },
      image: { label: 'Image', prompt: `Image path:` },
      indent: { '-1': `Decrease indentation`, '+1': `Increase indentation` },
      italic: 'Italic',
      link: { label: 'Link', prompt: `Link URL:` },
      list: { label: 'List', ordered: 'Ordered', bullet: 'Unordered', checkbox: 'Checklist' },
      script: { sub: 'Subscript', super: 'Superscript' },
      size: { label: 'Text size', small: 'Small', normal: 'Normal', large: 'Large', huge: 'Huge' },
      strike: 'Strike',
      underline: 'Underline',
    },
  },
};
