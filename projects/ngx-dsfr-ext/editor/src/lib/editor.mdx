import { Canvas, Controls, Meta } from '@storybook/blocks';
import * as Stories from './editor.stories';

<Meta of={Stories} title="Editor" />

# Éditeur (editor)

Le composant editor permet d'éditer du contenu HTML en mode WYSIWYG. Une barre d'outils permet de formater le texte
préalablement saisi. Cette barre d'outils est paramétrable, vous pouvez retirer (ou ajouter) des actions selon vos
besoins.

- _Composant_ : `DsfrEditorComponent`
- _Tag_ : `dsfr-ext-editor`, `dsfrx-editor`

## Fonctionnement

### Barre d'outils

Le composant `editor` vient avec une barre d'outils par défaut dont les actions de formatage ont été pré-configurées.

Vous pouvez utiliser l'input `toolbarOptions` pour supprimer, ré-ordonner voire ajouter des actions de formatage.

Le type `ToolbarControl` vous permet de spécifier une série de contrôles de formatage. Un contrôle de formatage est un
object qui expose a minima deux propriétés : un identifiant d'action (`name:Format`) et une modalité de présentation
(`mode:ControlMode`).

Par exemple :

```typescript
{
  name: Format.BOLD,
  mode: ControlMode.TOGGLE
}
```

Les classes utilitaires décrites ci-dessous vous permettent de décrire les différents contrôles dont vous souhaitez
disposer dans votre barre d'outils :

- `DsfrToggleFormatControl` : à utiliser pour les actions de formatage binaires (on/off) (ex. gras, italic, etc.)
- `SelectFormatControl`: à utiliser pour les contrôles associés à plusieurs valeurs possibles présentées sous forme de
  liste sélectionnable (ex. niveaux de titre, taille du texte)
- `RadioFormatControl`: à utiliser pour les contrôles associés à plusieurs valeurs possibles présentées un groupe de
  boutons en ligne (ex. alignement, indentation, etc.)
- `DsfrPromptFormatControl` : à utiliser pour les actions de formatage qui nécessitent la capture d'un paramètre
  utilisateur (image, lien, couleur, etc.)

Vous pouvez vous inspirer de la configuration par défaut de la toolbar pour ajuster selon vos besoins :

#### Supprimer des actions de formatage

> Ex. : supprimer le groupe d'actions relatives à la couleur de fond et de la police

```typescript
import { DEFAULT_TOOLBAR_OPTIONS } from from '@edugouvfr/ngx-dsfr-ext';
const colorActionsArrayIndex = 4; // just look in default options to find right index
let options = DEFAULT_TOOLBAR_OPTIONS.splice(colorActionsArrayIndex, 1);

```

#### Supprimer certaines valeurs d'une action multi-valuées

> Ex. : n'autoriser que les titres de niveau H3 et H4

```typescript
import { DEFAULT_TOOLBAR_OPTIONS, DsfrMultivaluedFormatControl, HeaderValue } from '@edugouvfr/ngx-dsfr-ext';

const header: DsfrMultivaluedFormatControl = DEFAULT_TOOLBAR_OPTIONS[0] as DsfrMultivaluedFormatControl;

header.values = header.values.filter((v) => v.value === HeaderValue.H3 || v.value === HeaderValue.H4);
```

#### Changer l'ordre des actions de formatage

> Ex. : positionner les contrôles d'indentation juste avant les contrôles subscript/superscript

```typescript
import { DEFAULT_TOOLBAR_OPTIONS, FormatConst, FormatControl } from '@edugouvfr/ngx-dsfr-ext';

const indent: any = DEFAULT_TOOLBAR_OPTIONS.find(
  (g) => !Array.isArray(g) && (g as FormatControl).name === FormatConst.INDENT,
);

DEFAULT_TOOLBAR_OPTIONS.splice(DEFAULT_TOOLBAR_OPTIONS.indexOf(indent), 1);
DEFAULT_TOOLBAR_OPTIONS.splice(3, 0, indent);
```

#### Ajouter une action custom

Il n'est pas encore possible d'utiliser la classe `DsfrCustomFormatControl` pour gérer des actions spécifiques.

↪ Étude en cours 🚧

### Contenu HTML

Pour initialiser et récupérer le contenu HTML du composant, l'approche recommandée est de passer par une directive
`ngModel` ou par un `FormControl`.

Cela vous assure en effet de récupérer automatiquement le contenu HTML au sein de votre propriété.

Il est également possible de ne pas utiliser `ngModel` ou `FormControl` et de vous mettre à l'écoute de l'output
`valueChange`pour réagir aux changements de valeur :

```html
<dsfrx-editor [value]="myHtml" (valueChange)="onValueChanged($event)"></dsfrx-editor>
```

👆 A noter : vous disposez de deux panneaux pouvant éventuellement vous aider au debug, pour les afficher vous devez
passer par les inputs suivants :

- `showEditorHtmlDebugPanel` : affiche le code HTML interne de l'éditeur (i.e. celui qui sert pour l'affichage WYSIWYG),
- `showSemanticHtmlDebugPanel` : affiche le code HTML qui sera récupéré en sortie de l'éditeur (via `ngModel` par exemple)

## Accessibilité

La barre d'outils peut être utilisée entièrement via la navigation au clavier. Le modèle de navigation est inspiré
du pattern W3C [Toolbar](https://www.w3.org/WAI/ARIA/apg/patterns/toolbar/).

Lorsque la barre d'outils reçoit le focus, ce dernier est automatiquement transféré sur le premier élément de contrôle
de la barre d'outils. La navigation vers les autres contrôles de formatage se fait ensuite via les touches "flêches"
droite et gauche. Un nouvel appui sur la touche `Tab` bascule le focus sur le contenu HTML.

La barre d'outils conserve la mémoire du dernier contrôle ayant reçu le focus de manière à restaurer automatiquement
le focus sur cet élément lorsque la toolbar reprendra le focus depuis le contenu éditable (via Shift-Tab par exemple).

Des tooltips (title) internationalisés sont positionnés sur chaque action.

Des attributs `aria-pressed` ou `aria-selected` (selon les types de contrôle) sont positionnées sur les contrôles
actifs pour la sélection courante.

Enfin, les combinaisons de touches standards telles que `Ctrl+B`, `Ctrl+U`, `Ctrl+S` ou `Ctrl+I` sont supportées.

## Limitations

### Inputs non réactifs

Du fait de l'usage sous-jacent de la biblothèque Quill, certains inputs ne peuvent être évalués qu'à l'initialisation
et ne seront pas mis à jour en cas de modification dynamique :

- `editorId`
- `ariaLabelledBy`
- `messagesGroupId`

### Bugs connus

🐛 Bug reproduit sous Chrome v129

Comme indiqué, l'implémentation de l'éditeur WYSIWYG s'appuie sur le composant `Quill.js`, nous avons repéré le bug
suivant : lorsque l'utilisateur sélectionne à la souris la totalité du texte présent dans l'éditeur en procédant de
droite à gauche puis que celui-ci utilise le sélecteur de taille du texte et sélectionne une nouvelle taille,
alors :

- la sélection est perdue,
- le curseur se trouve à la fin du texte,
- la nouvelle taille de texte n'a pas été appliquée sur le texte mais "après" celui-ci.

A noter, ce bug ne se produit pas si la sélection du texte est réalisée :

- via un double-clic (en cas de texte contigu),
- à la souris mais en procédant de gauche à droite,
- en utilisant seulement la navigation au clavier.

## Améliorations prévues

- 🔲 Remplacer les attributs `title` des actions par des vrais tooltips DSFR
- 🔲 Améliorer l'ergonomie des actions requérant la capture d'un paramètre utilisateur (link, image, color)
- 🔲 Gérer un mode `readOnly`
- 🔲 Prendre en charge `required`
- 🔲 Permettre de `sanitize` la valeur HTML
- 🔲 Prendre en charge une `maxLength`/`minLength` (?)

## Aperçu

<Canvas of={Stories.Default} />

## API

<Controls />

## Exemples

### Ajout d'un libellé

Pour des raisons d'acccessibilité ou pour utiliser l'éditeur au sein d'un formulaire, il peut être utile d'ajouter
un libellé dénotant le champ d'édition. Cette story propose un exemple de mise en oeuvre avec un rendu conforme à ce le
DSFR propose pour les labels des champs de saisie.

<Canvas of={Stories.Labelled} />

### Affichage d'un message

<Canvas of={Stories.Message} />

### Usage avec `NgModel`

<Canvas of={Stories.NgModel} />
