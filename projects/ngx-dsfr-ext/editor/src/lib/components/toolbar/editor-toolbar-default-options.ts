import {
  AlignValue,
  ControlMode,
  DsfrPromptFormatControl,
  DsfrToggleFormatControl,
  DsfrValueFormatControl,
  FormatConst,
  HeaderValue,
  IndentValue,
  ListValue,
  RadioFormatControl,
  ScriptValue,
  SelectFormatControl,
  SizeValue,
  ToolbarControl,
} from './editor-toolbar.model';

/**
 * Options par défaut de la toolbar.
 */
export const DEFAULT_TOOLBAR_OPTIONS: ToolbarControl[] = [
  // niveau de titre
  new SelectFormatControl(FormatConst.HEADER, [
    new DsfrValueFormatControl(HeaderValue.NORMAL),
    new DsfrValueFormatControl(HeaderValue.H1),
    new DsfrValueFormatControl(HeaderValue.H2),
    new DsfrValueFormatControl(HeaderValue.H3),
    new DsfrValueFormatControl(HeaderValue.H4),
    new DsfrValueFormatControl(HeaderValue.H5),
    new DsfrValueFormatControl(HeaderValue.H6),
  ]),
  // taille du texte
  new SelectFormatControl(FormatConst.SIZE, [
    new DsfrValueFormatControl(SizeValue.SMALL),
    new DsfrValueFormatControl(SizeValue.NORMAL),
    new DsfrValueFormatControl(SizeValue.LARGE),
    new DsfrValueFormatControl(SizeValue.HUGE),
  ]),
  // mise en forme du texte (gras, italique, etc.)
  [
    new DsfrToggleFormatControl(FormatConst.BOLD),
    new DsfrToggleFormatControl(FormatConst.ITALIC),
    new DsfrToggleFormatControl(FormatConst.UNDERLINE),
    new DsfrToggleFormatControl(FormatConst.STRIKE),
  ],
  new RadioFormatControl(
    FormatConst.SCRIPT,
    [new DsfrValueFormatControl(ScriptValue.SUPERSCRIPT), new DsfrValueFormatControl(ScriptValue.SUBSCRIPT)],
    true,
  ),
  // couleurs
  [new DsfrPromptFormatControl(FormatConst.COLOR), new DsfrPromptFormatControl(FormatConst.BACKGROUND)],
  // mise en forme avancée
  [
    new DsfrToggleFormatControl(FormatConst.BLOCKQUOTE),
    new DsfrToggleFormatControl(FormatConst.CODE_BLOCK),
    new DsfrPromptFormatControl(FormatConst.LINK),
    new DsfrPromptFormatControl(FormatConst.IMAGE),
  ],
  // listes
  new RadioFormatControl(
    FormatConst.LIST,
    [
      new DsfrValueFormatControl(ListValue.ORDERED),
      new DsfrValueFormatControl(ListValue.BULLET),
      // new ValueFormatControl(ListValue.CHECKBOX), //RPA: pas proposé par défaut car non standard au sens html
    ],
    true,
  ),
  // indentation des lignes de texte
  new RadioFormatControl(
    FormatConst.INDENT,
    [new DsfrValueFormatControl(IndentValue.DECREASE), new DsfrValueFormatControl(IndentValue.INCREASE)],
    true,
  ),

  // alignement des lignes de texte
  new RadioFormatControl(
    FormatConst.ALIGN,
    [
      new DsfrValueFormatControl(AlignValue.LEFT), // même effet que clean, faut-il le conserver ? ça fait un peu bizarre sans
      new DsfrValueFormatControl(AlignValue.CENTER),
      new DsfrValueFormatControl(AlignValue.RIGHT),
      new DsfrValueFormatControl(AlignValue.JUSTIFY),
    ],
    true,
  ),
  // reset du format
  {
    name: FormatConst.CLEAN,
    mode: ControlMode.CLEAN,
  },
];
