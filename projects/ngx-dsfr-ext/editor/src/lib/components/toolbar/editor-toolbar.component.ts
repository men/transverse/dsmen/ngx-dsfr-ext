import { CommonModule } from '@angular/common';
import {
  Component,
  CUSTOM_ELEMENTS_SCHEMA,
  ElementRef,
  Input,
  NgZone,
  OnDestroy,
  OnInit,
  Renderer2,
  ViewChild,
  ViewEncapsulation,
} from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterTestingModule } from '@angular/router/testing';
import {
  DsfrButtonModule,
  DsfrFormSelectModule,
  DsfrI18nPipe,
  DsfrI18nService,
  DsfrSelectOption,
  DsfrSkiplinksModule,
} from '@edugouvfr/ngx-dsfr';

import { Subscription } from 'rxjs';
import { UnfocusableDirective } from '../../directives/unfocusable.directive';
import { DsfrEditorService, Formatting } from '../../editor.service';
import { DEFAULT_TOOLBAR_OPTIONS } from './editor-toolbar-default-options';
import {
  ControlMode,
  DsfrBaseFormatControl,
  DsfrMultivaluedFormatControl,
  DsfrValueFormatControl,
  Format,
  FORMAT_I18N_KEYS,
  FORMAT_ICON,
  FormatConst,
  FormatControl,
  ToolbarControl,
} from './editor-toolbar.model';

/**
 * Les quatre modalités de présentation supportées par la toolbar pour afficher les contrôles de format.
 */
type ControlViewType = 'btn' | 'group' | 'radio' | 'select';

@Component({
  selector: 'edu-editor-toolbar',
  encapsulation: ViewEncapsulation.None,
  templateUrl: './editor-toolbar.component.html',
  styleUrls: ['./editor-toolbar.component.scss'],
  standalone: true,
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    DsfrSkiplinksModule,
    RouterTestingModule,
    DsfrFormSelectModule,
    DsfrButtonModule,
    UnfocusableDirective,
    DsfrI18nPipe,
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
})
export class EduEditorToolbarComponent implements OnInit, OnDestroy {
  /**
   * L'identifiant du tag hôte pour le composant éditeur.
   */
  @Input() editorId = 'editor';

  /**
   * Active la lecure seule.
   */
  //TODO: @Input() readOnly = false;

  /**
   * Options de la toolbar.
   */
  @Input() toolbarOptions: ToolbarControl[] = DEFAULT_TOOLBAR_OPTIONS;

  /** @internal */
  @ViewChild('controlsParent') controlsParent: ElementRef;

  /**
   * Modèle de présentation spécifique pour les select (header+size).
   * RPA: je passe par un modèle de présentation spécifique et je n'utilise pas directement un binding sur
   * currentSelectionFormats car j'ai besoin de reflêter l'état de sélection utilisateur sinon voici ce qui ce passe :
   * l'utilisateur sélectionne une portion de texte et sélectionnz un niveau de titre H2, puis il place le curseur dans
   * un paragraphe quelconque, le select reste sur H2 car la propriété interne 'value' n'est pas mise à jour lors de
   * la sélection utilisateur (binding non bi-directionnel). C'est pourquoi il est nécessaire de passer par un modèle
   * de présentation dédié à la gestion des select.
   */
  protected selectModel: any = { header: null, size: null };

  /**
   * Permet de gérer l'affichage d'un outline custom lorsque la toolbar prend le focus.
   */
  protected toolbarFocused = false;

  /**
   * Pointeur sur le dernier élément de contrôle actif, permet de restaurer le focus directement sur celui-ci au
   * SHIFT+TAB.
   */
  protected lastActiveControlElement?: HTMLElement;

  /**
   * Modèle de présentation interne exposant les formats de la sélection courante.
   */
  private _currentSelectionFormats: Formatting | undefined;

  private _unlistenFn: Function;

  private _sub: Subscription;

  constructor(
    private readonly ngZone: NgZone,
    private renderer2: Renderer2,
    private readonly elementRef: ElementRef,
    public readonly editorService: DsfrEditorService,
    private readonly i18n: DsfrI18nService,
  ) {}

  ngOnInit(): void {
    this._sub = this.editorService.currentFormatting$.subscribe((formatting: Formatting) => {
      this._updateToolbar(formatting);
    });
    // Hors zone Angular pour des raisons de perf
    this.ngZone.runOutsideAngular(() => {
      this._unlistenFn = this.renderer2.listen('document', 'click', (e) => {
        this.clickOutside(e);
      });
    });
  }

  ngOnDestroy(): void {
    this._sub?.unsubscribe();
    this._unlistenFn();
  }

  /**
   * Handler sur l'input file associé à une inserion d'image.
   */
  //FIXME : erreur dans la console : ParchmentError: [Parchment] Cannot wrap imag
  protected onImageInputFileChange(event: Event) {
    this._uploadImage(event);
  }

  /**
   * Permet de conditionner la structure HTML selon le type contrôle.
   */
  protected getButtonType(control: FormatControl | FormatControl[]): ControlViewType {
    let result: ControlViewType = 'btn';
    if (Array.isArray(control) && control.length) {
      result = 'group';
    } else if ('values' in control && control.values && control.values.length) {
      switch ((control as DsfrBaseFormatControl).mode) {
        case ControlMode.RADIO:
          result = 'radio';
          break;

        case ControlMode.SELECT:
          result = 'select';
          break;

        default:
          break;
      }
    }

    return result;
  }

  /**
   * Retourne l'icône correspondant au format passé en paramètre.
   *
   * @param format le format à considérer
   * @param value la vaaleur à prendre en compte le cas échéant
   * @returns la classe css correspondant à l'icône
   */
  protected getIcon(format: Format, value = undefined): string | undefined {
    let icon: any = FORMAT_ICON[format];
    // la valeur peut être le booléen false d'où le test explicite sur not(undefined)
    if (icon && value !== undefined) {
      // on doit aller chercher l'icône en utilisant la valeur comme clé
      const key: string = value === false ? 'normal' : value;
      icon = icon[key];
    }

    return icon;
  }

  /**
   * Retourne la clé du libellé internationalisé.
   * @param control  le contôle de formatage
   */
  protected getI18nKey(format: Format, value: string | boolean | undefined = undefined): string {
    let key = FORMAT_I18N_KEYS[format];
    if (value !== undefined) {
      key = key as Record<string, string>;
      key = value === false ? key['normal'] : key[String(value)];
    }

    return key as string;
  }

  /**
   * Détermine si l'option de format doit être activée ou pas.
   *
   * @param format le nom de contrôle de format
   * @param value la valeur de l'option considérée
   * @returns vrai si le couple format/value fait partie des formats de la sélection courante, faux sinon
   */
  protected isValueActive(format: Format, value: string): boolean {
    // @ts-ignore (RPA) le test sur currentSelectionFormats est effectué à l'intérieur de la fonction isFormatActive
    return this.isFormatActive(format) ? this._currentSelectionFormats[format] === value : false;
  }

  /**
   * Retourne la liste des optiosn à affichier au sein du contrôle multivalué (select).
   *
   * @param control le contrôle multivalué à considéré
   */
  protected getSelectOptions(control: DsfrMultivaluedFormatControl): DsfrSelectOption[] {
    return control.values.map((v: DsfrValueFormatControl) => this._buildSelectOption(control.name, v));
  }

  /**
   * Permet d'indiquer si le contrôle de ce format doit être activé au niveau de la barre d'outils.
   *
   * @param format le format à considérer vis-à-vis des formats de la sélection courante
   * @returns vrai si ce format doit être actif, faux sinon
   */
  protected isFormatActive(format: Format): boolean {
    return this._currentSelectionFormats ? format in this._currentSelectionFormats : false;
  }

  /**
   * Permet de réagir aux actions utilisateur sur la toolbar.
   */
  protected onToolbarControlPerform(event: Event, format: FormatControl, value?: string) {
    event.preventDefault();
    this._applyFormat(format, value);
  }

  /**
   * Permet d'écouter la capture d'une couleur par l'utilisateur.
   */
  protected onColorPickerChange(event: Event, action: string) {
    const target = <HTMLInputElement>event.target;
    const value = target.value ?? undefined;
    if (value) {
      this.performAction(action, value);
    }
  }

  /**
   * Permet d'écouter les changement sur le format "list".
   *
   * @param action
   * @param event
   */
  protected onListSelect(action: string, event: Event) {
    this.performAction(action, event);
  }

  /**
   * Permet de forcer la prise de focus sur le premier élément focusable de la barre d'outil lorsque le conteneur de
   * la toolbar reçoit le focus.
   */
  protected onToolbarFocus() {
    this.toolbarFocused = true;
    if (this.lastActiveControlElement) {
      this.lastActiveControlElement.focus();
    } else {
      const nextElem: HTMLElement = this.elementRef.nativeElement.querySelector('.item-editor, .fr-select');
      nextElem?.focus();
    }
  }

  /**
   * Permet de gérer la sortie de focus
   */
  protected onToolbarBlur(event: any) {
    // si le focus est pris par un élément de contrôle, on conserve l'outline custom
    if (!event.currentTarget?.contains(event.relatedTarget)) {
      this.toolbarFocused = false;
    }
  }

  /**
   * Permey de se mettre à l'écoute des actions utilisateur sur la barre d'outils.
   */
  protected onToolbarKeydown(event: KeyboardEvent) {
    const key = event.key;
    switch (key) {
      case 'Tab':
        event.preventDefault(); // évite la perte de focus asynchrone sur l'éditeur due à la propagation de l'event
        this.editorService.requestFocusOnEditorContent();
        this.toolbarFocused = false;
        break;

      case 'ArrowRight':
      case 'ArrowLeft':
        event.preventDefault(); // évite la prise de focus sur l'éditeur suite propagation event
        let focusableElt = key === 'ArrowRight' ? this.findNextSiblingControl() : this.findPrevSiblingControl();
        if (focusableElt) {
          focusableElt.focus();
          this.lastActiveControlElement = focusableElt;
        }
        break;

      default:
        break;
    }
  }

  private performAction(action: any, value?: any) {
    if (action) {
      this.editorService.requestFormatting({ action: action, value: value });
    } else {
      this.editorService.requestFormatting(undefined);
    }

    this.toolbarFocused = false;
  }

  /**
   * Détecte le clic en dehors du composant pour retirer le marqueur de focus custom sur la toolbar.
   */
  private clickOutside(event: any) {
    // comme on s'est mis à l'écoute de l'event hors Angular pour des raisons de perf, on doit mettre à jour l'input
    // en s'exécutant dans la zone Angular
    this.ngZone.run(() => {
      if (!this.elementRef.nativeElement.contains(event.target)) {
        this.toolbarFocused = false;
      }
    });
  }

  private _buildSelectOption(format: Format, control: DsfrValueFormatControl) {
    return { label: this._getOptionLabel(format, control.value), value: control.value };
  }

  private _getOptionLabel(format: Format, value: any): string {
    const record: Record<string, string> = FORMAT_I18N_KEYS[format] as Record<string, string>;
    const resolvedValue = value === false ? 'normal' : value;
    const key = record[String(resolvedValue)];
    return this.i18n.t(key);
  }

  private findNextSiblingControl(): HTMLElement | undefined {
    return this.findSiblingControl('nextElementSibling', () => this.getFirstToolbarControl());
  }

  private findPrevSiblingControl(): HTMLElement | undefined {
    return this.findSiblingControl('previousElementSibling', () => this.getLastToolbarControl());
  }

  /**
   * Recherche l'élément de contrôle focusable adjaçant à l'élément de contrôle actif.
   *
   * @param siblingProperty La propriété permettant de récupérer le voisin suivant ou précédent (selon)
   * @param failoverFunction La fonction permettant de récupérer un élément focusable lorsqu'il n'y plus de suivant
   * @returns Une élément HTML sur lequel il sera possible de prendre le focus
   */
  private findSiblingControl(
    siblingProperty: 'nextElementSibling' | 'previousElementSibling',
    failoverFunction: Function,
  ): HTMLElement | undefined {
    let result: HTMLElement | undefined = undefined;
    let activeElt: Element | null = document.activeElement;
    //TODO: imbrication conditionnelle complexe > à mettre sous harnais de tests pour ensuite tenter une simplification
    if (activeElt) {
      if (activeElt.tagName === 'BUTTON') {
        if (activeElt.closest('dsfr-buttons-group')) {
          const dsfrButtonsGroup = activeElt.parentElement;
          if (dsfrButtonsGroup && dsfrButtonsGroup[siblingProperty])
            // directly try to get next sibling
            result = dsfrButtonsGroup[siblingProperty] as HTMLElement;
        } else if (activeElt.closest('span.group')) {
          const spanControl = activeElt.closest('span.control');
          if (spanControl && spanControl[siblingProperty]) {
            // directly try to get parent span to get next sibling
            result = spanControl[siblingProperty] as HTMLElement;
          }
        }
      }
      if (result === undefined) {
        while (activeElt.parentElement !== null && !activeElt.parentElement.classList.contains('editor-toolbar')) {
          activeElt = activeElt.parentElement;
        }
        if (activeElt.parentElement?.classList.contains('editor-toolbar')) {
          result = activeElt[siblingProperty] as HTMLElement;
        }
      }
    }

    return result ? this.getFocusableControl(result, siblingProperty === 'previousElementSibling') : failoverFunction();
  }

  private getFirstToolbarControl(): HTMLElement | undefined {
    return this.getFocusableControl(this.controlsParent.nativeElement);
  }

  private getLastToolbarControl(): HTMLElement | undefined {
    let lastOne = this.controlsParent.nativeElement.lastElementChild;
    return this.getFocusableControl(lastOne, true);
  }

  /**
   * Search focusable control (depth first traversal).
   */
  private getFocusableControl(elt: HTMLElement, reverse = false): HTMLElement | undefined {
    if (elt.tagName === 'BUTTON' || elt.tagName === 'SELECT') {
      return elt;
    } else if (elt.children.length !== 0) {
      let children = Array.from(elt.children);
      if (reverse) {
        children = children.reverse();
      }
      for (const c of children) {
        const focusable = this.getFocusableControl(c as HTMLElement);
        if (focusable) {
          return focusable;
        }
      }
    }
    return undefined;
  }

  /**
   * Répercute les formats actifs de la sélection courante vers la toolbar.
   */
  private _updateToolbar(formats: Formatting) {
    // on force l'exécution dans zonejs car l'événement provient de Quill et n'est pas géré par Angular
    this.ngZone.run(() => {
      this._currentSelectionFormats = formats;
      this.selectModel.header = this._getSelectedOption(FormatConst.HEADER);
      this.selectModel.size = this._getSelectedOption(FormatConst.SIZE);
    });
    // on met à jour les états aria-pressed sur les boutons
    //TODO: à remplacer par un input ariaPressed sur dsfr-button cf. ngx-dsfr#
    const dsfrButtons: NodeList = this.elementRef.nativeElement.querySelectorAll('dsfr-button');
    dsfrButtons.forEach((n) => {
      const e = n as HTMLElement;
      const controlInfo = e.getAttribute('data-testid')?.split('-');
      if (controlInfo) {
        const pressed =
          controlInfo.length === 1
            ? this.isFormatActive(controlInfo[0] as Format)
            : this.isValueActive(controlInfo[0] as Format, controlInfo[1] as string);

        if (pressed) {
          e.querySelector('button')?.setAttribute('aria-pressed', 'true');
        } else {
          e.querySelector('button')?.removeAttribute('aria-pressed');
        }
      }
    });
  }

  /**
   * Retourne la valeur à sélectionner dans le cas de contôle multivalué (select).
   *
   * @internal
   */
  private _getSelectedOption(format: Format): unknown | null {
    const debug =
      this._currentSelectionFormats && this._currentSelectionFormats[format]
        ? this._currentSelectionFormats[format]
        : null;
    return debug;
  }

  /**
   * Supprime toutes les mises en forme sur la sélection courante.
   */
  private _removeSelectionFormats() {
    this.performAction(undefined);
  }

  /**
   * Applique le format défini par l'utilisateur via la toolbar sur la sélection courante de l'éditeur.
   */
  private _applyFormat(control: FormatControl, value?: any) {
    // Cas particulier du clean
    if (control.name === FormatConst.CLEAN) {
      this._removeSelectionFormats();
    } else {
      const baseControl: DsfrBaseFormatControl = control as DsfrBaseFormatControl;
      switch (baseControl.mode) {
        case ControlMode.TOGGLE:
          // cas d'une toggle action, on détermine la valeur à transmettre à l'éditeur
          value = this._currentSelectionFormats && control.name in this._currentSelectionFormats ? false : true;
          break;

        case ControlMode.RADIO:
          // si l'action envoyée est déjà présente dans la sélection il faut envoyer "false" comme valeur
          value =
            this._currentSelectionFormats &&
            control.name in this._currentSelectionFormats &&
            this._currentSelectionFormats[control.name] === value
              ? false
              : value;
          break;

        case ControlMode.PROMPT:
          switch (baseControl.name) {
            case FormatConst.IMAGE:
              //TODO: à remplacer par quelque chose de plus sexy
              this._displayPromptElement('#prompt-' + FormatConst.IMAGE);
              break;

            case FormatConst.LINK:
              //TODO: à remplacer par quelque chose de plus sexy
              value = window.prompt(this.i18n.t('editor.controls.link.prompt'));
              break;

            case FormatConst.COLOR:
              this._displayPromptElement('#picker-' + FormatConst.COLOR);
              break;

            case FormatConst.BACKGROUND:
              this._displayPromptElement('#picker-' + FormatConst.BACKGROUND);
              break;

            default:
              break;
          }
          break;

        default:
          break;
      }

      // si on a pu déterminer une valeur, on demande l'application du format à l'éditeur
      if (value !== undefined) {
        this.performAction(control.name, value);
      }
    }
  }

  /**
   * Permet de capturer une couleur depuis un color-picker natif.
   *
   * @param selector le sélecteur permettant de requêter l'élément de contrôle complémentaire
   */
  private _displayPromptElement(selector: string) {
    const elem: HTMLElement = document.querySelector(selector) as HTMLElement;
    if (elem) elem.click();
  }

  /**
   * Positionne l'image Base64 dans l'editeur.
   */
  private _uploadImage = async (event: Event) => {
    const target = <HTMLInputElement>event.target;
    if (target?.files) {
      this.performAction('image', target.files);
    }
  };
}
