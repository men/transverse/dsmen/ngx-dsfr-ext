/**
 * Constantes des identifiants d'actions de formatage supportés.
 */
export namespace FormatConst {
  export const ALIGN = 'align';
  export const BACKGROUND = 'background';
  export const BLOCKQUOTE = 'blockquote';
  export const BOLD = 'bold';
  export const CLEAN = 'clean';
  export const CODE_BLOCK = 'code-block';
  export const COLOR = 'color';
  export const DIRECTION = 'direction';
  export const HEADER = 'header';
  export const IMAGE = 'image';
  export const INDENT = 'indent';
  export const ITALIC = 'italic';
  export const LINK = 'link';
  export const LIST = 'list';
  export const SCRIPT = 'script';
  export const SIZE = 'size';
  export const STRIKE = 'strike';
  export const UNDERLINE = 'underline';
}

/**
 * Type correspondant aux identifiants d'actions de formatage supportés.
 */
export type Format = (typeof FormatConst)[keyof typeof FormatConst];

/**
 * Mapping des clés d'internationalisation des libellés des actions de formatage.
 *
 * Selon le contexte, le libellé sera utilisé comme tooltip, libellé de champ ou transcription accessible.
 */
export const FORMAT_I18N_KEYS: Record<Format, string | Record<string, string>> = {
  'align': {
    normal: 'editor.controls.align.left',
    center: 'editor.controls.align.center',
    right: 'editor.controls.align.right',
    justify: 'editor.controls.align.justify',
  },
  'background': 'editor.controls.background',
  'blockquote': 'editor.controls.blockquote',
  'clean': 'editor.controls.clean',
  'bold': 'editor.controls.bold',
  'code-block': 'editor.controls.code',
  'color': 'editor.controls.color',
  'direction': { normal: 'editor.controls.direction.normal', rtl: 'editor.controls.direction.rtl' },
  'header': {
    'label': 'editor.controls.header.label',
    '0': 'editor.controls.header.0',
    '1': 'editor.controls.header.1',
    '2': 'editor.controls.header.2',
    '3': 'editor.controls.header.3',
    '4': 'editor.controls.header.4',
    '5': 'editor.controls.header.5',
    '6': 'editor.controls.header.6',
  },
  'image': 'editor.controls.image.label',
  'indent': { '-1': 'editor.controls.indent.-1', '+1': 'editor.controls.indent.+1' },
  'italic': 'editor.controls.italic',
  'link': 'editor.controls.link.label',
  'list': {
    ordered: 'editor.controls.list.ordered',
    bullet: 'editor.controls.list.bullet',
    unchecked: 'editor.controls.list.checkbox',
  },
  'script': { sub: 'editor.controls.script.sub', super: 'editor.controls.script.super' },
  'size': {
    label: 'editor.controls.size.label',
    small: 'editor.controls.size.small',
    normal: 'editor.controls.size.normal',
    large: 'editor.controls.size.large',
    huge: 'editor.controls.size.huge',
  },
  'strike': 'editor.controls.strike',
  'underline': 'editor.controls.underline',
};

/**
 * Mapping des icônes par défaut correspondant aux actions de formatage supportées.
 *
 * Note de conception (RPA+KGE) pour l'instant on décide de maintenir les icônes dans une structure à part afin de
 * verrouiller le modèle. Si demain on souhaite donner à l'utilisateur la possibilité de surcharger les icônes des
 * actions de formatage alors on pourra réintégrer les icônes au modèle en augmentant l'interface `BaseFormatControl`
 * d'une propriété `icon` (rétrocompatible car la propriété aura une valeur par défaut dans le modèle par défaut)
 */
export const FORMAT_ICON: Record<Format, string | Record<string, string> | undefined> = {
  'align': {
    normal: 'fr-icon-align-left',
    center: 'fr-icon-align-center',
    right: 'fr-icon-align-right',
    justify: 'fr-icon-align-justify',
  },
  'background': 'fr-icon-brush-3-line',
  'blockquote': 'fr-icon-quote-line',
  'clean': 'fr-icon-clean',
  'bold': 'fr-icon-bold',
  'code-block': 'fr-icon-code-view',
  'color': 'fr-icon-font-color',
  'direction': 'fr-icon-direction-rtl',
  'header': undefined,
  'image': 'fr-icon-image-line',
  'indent': { '-1': 'fr-icon-indent-decrease', '+1': 'fr-icon-indent-increase' },
  'italic': 'fr-icon-italic',
  'link': 'fr-icon-link',
  'list': { ordered: 'fr-icon-list-ordered', bullet: 'fr-icon-list-unordered', unchecked: 'fr-icon-list-check' },
  'script': { sub: 'fr-icon-subscript', super: 'fr-icon-superscript' },
  'size': undefined,
  'strike': 'fr-icon-strike',
  'underline': 'fr-icon-underline',
};

/**
 * Les différents mode supportés par les contrôles de formatage à valeur unique.
 */
export enum ControlMode {
  /**
   * Active/désactive le format.
   */
  TOGGLE = 'toggle',
  /**
   * Le format dépend de la capture d'un paramètre utilisateur.
   */
  PROMPT = 'prompt',
  /**
   * Action issue d'une des options d'une action multivaluée érigée en action unique autonome.
   * Ex. je veux un bouton permettant de formater en Titre de niveau 1 (H1)
   */
  CUSTOM = 'custom',
  /**
   * Permet de représenter l'une des options d'une action multivaluée.
   */
  VALUE = 'valued',
  /**
   * Dénote une action de formatage multivaluée à afficher sous forme de selecteur.
   */
  SELECT = 'select',
  /**
   * Dénote une action de formatage multivaluée à afficher sous de groupe de bouton exclusif.
   */
  RADIO = 'radio',
  /**
   * Mode spécial dénotant le contrôle de réinitialisation.
   */
  CLEAN = 'clean',
}

/**
 * Les valeurs possibles pour le format 'size'.
 */
export namespace SizeValue {
  export const SMALL = 'small';
  export const NORMAL = false;
  export const LARGE = 'large';
  export const HUGE = 'huge';
}

/**
 * Les valeurs possibles pour le format 'header'.
 */
export namespace HeaderValue {
  export const NORMAL = 0;
  export const H1 = 1;
  export const H2 = 2;
  export const H3 = 3;
  export const H4 = 4;
  export const H5 = 5;
  export const H6 = 6;
}

/**
 * Les valeurs possibles pour le format 'align'.
 */
export namespace AlignValue {
  export const LEFT = false;
  export const CENTER = 'center';
  export const RIGHT = 'right';
  export const JUSTIFY = 'justify';
}

/**
 * Les valeurs possibles pour le format 'list'.
 */
export namespace ListValue {
  export const ORDERED = 'ordered';
  export const BULLET = 'bullet';
  export const CHECKBOX = 'unchecked';
}

/**
 * Les valeurs possibles pour le format 'script'.
 */
export namespace ScriptValue {
  /**
   * Subscript.
   */
  export const SUBSCRIPT = 'sub';
  /**
   * Superscript.
   */
  export const SUPERSCRIPT = 'super';
}

/**
 * Les valeurs possibles pour le format 'indent'.
 */
export namespace IndentValue {
  /**
   * Decrease indent.
   */
  export const DECREASE = '-1';
  /**
   * Increase indent.
   */
  export const INCREASE = '+1';
}

/**
 * Les valeurs possibles pour le format 'direction'.
 */
export namespace DirectionValue {
  /**
   * De gauche à droite.
   */
  export const NORMAL = false;
  /**
   * De droite à gauche.
   */
  export const RTL = 'rtl';
}

/**
 * Représente une tâche de formatage de base.
 */
export interface DsfrBaseFormatControl {
  /**
   * L'identifiant Quill du format.
   */
  name: Format;

  /**
   * Le mode du contrôle de format.
   */
  mode: ControlMode;
}

/**
 * Représente une action simple (bouton).
 */
export interface DsfrSingleFormatControl extends DsfrBaseFormatControl {
  /**
   * Optionnel : permet de surcharger l'icône illustrative.
   * Doit être une classe CSS Remix wrappée DSFR. Ex. 'fr-icon-bold'.
   */
  icon?: string;
}

/**
 * Permet de sortir une des options de formatage d'une action multivaluée et d el'ériger en action unique autonome.
 */
//TODO: pas encore opérationnel, à re-travailler notamment sur la fourniture icône, libellés i18n, etc.
export class DsfrCustomFormatControl implements DsfrSingleFormatControl {
  /**
   * Le mode d'une action avec valeur prédéfinie est forcément 'valued'.
   */
  mode = ControlMode.CUSTOM;

  /**
   * Construit une action de formatage valuée.
   *
   * @param name le format de cette action
   * @param value la valeur spécifique de cette action de formatage
   */
  constructor(
    public name: Format,
    public value: string | number | false,
  ) {}
}

/**
 *
 */
export class DsfrToggleFormatControl implements DsfrSingleFormatControl {
  /**
   * Le mode d'une action avec valeur prédéfinie est forcément 'toggle'.
   */
  mode = ControlMode.TOGGLE;

  constructor(public name: Format) {}
}

/**
 *
 */
export class DsfrPromptFormatControl implements DsfrSingleFormatControl {
  /**
   * Le mode d'une action avec valeur prédéfinie est forcément 'prompt'.
   */
  mode = ControlMode.PROMPT;

  constructor(public name: Format) {}
}

/**
 * Représente une des actions possibles d'une méta-action multi-valuée.
 */
export class DsfrValueFormatControl implements Omit<DsfrSingleFormatControl, 'name'> {
  /**
   * Le mode d'une action avec valeur prédéfinie est forcément 'valued'.
   */
  mode = ControlMode.VALUE;

  /**
   * Construit une action de formatage valuée.
   *
   * @param value la valeur spécifique de cette action de formatage
   */
  constructor(public value: string | number | false) {}
}

/**
 * Représente une méta-tâche qui s'applique à travers la sélection d'une valeur concrète au sein d'une liste de valeurs.
 */
export interface DsfrMultivaluedFormatControl extends DsfrBaseFormatControl {
  /**
   * Permet d'indiquer si le contrôle multivalué doit s'afficher au sein d'un groupe dans la toolbar.
   */
  group: boolean;

  /**
   * Valeurs possibles pour cette action de formatage.
   */
  values: DsfrValueFormatControl[];
}

/**
 * Représente une action multi-valuée en mode radio.
 */
export class RadioFormatControl implements DsfrMultivaluedFormatControl {
  mode = ControlMode.RADIO;

  /**
   * Construit un control de type radio.
   *
   * @param values les différentes valeurs possibles pour le control de type radio
   */
  constructor(
    public name: Format,
    public values: DsfrValueFormatControl[],
    public group: boolean,
  ) {}
}

/**
 * Représente une action multi-valuée en mode select.
 */
export class SelectFormatControl implements DsfrMultivaluedFormatControl {
  mode = ControlMode.SELECT;
  group = false;

  /**
   * Construit un control de type select.
   *
   * @param values les différentes valeurs possibles pour le control de type select.
   */
  constructor(
    public name: Format,
    public values: DsfrValueFormatControl[],
  ) {}
}

/**
 * Type générique encapsulant toutes les types de tâches de formatage.
 */
export type FormatControl = DsfrSingleFormatControl | DsfrMultivaluedFormatControl;

/**
 * Type décrivant un élément de la barre d'outil qui peut être soit une tâche unitaire soit un regroupement de tâches.
 */
export type ToolbarControl = FormatControl | FormatControl[];
