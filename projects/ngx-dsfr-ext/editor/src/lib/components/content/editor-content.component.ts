import { CommonModule } from '@angular/common';
import {
  AfterViewInit,
  Component,
  CUSTOM_ELEMENTS_SCHEMA,
  Input,
  OnChanges,
  OnDestroy,
  SimpleChanges,
  ViewEncapsulation,
} from '@angular/core';

// eslint-disable-next-line @typescript-eslint/naming-convention
import Quill, { QuillOptions, Range } from 'quill';

import { DsfrI18nService, DsfrSeverity, DsfrSeverityConst } from '@edugouvfr/ngx-dsfr';
import { Delta } from 'quill/core';
// eslint-disable-next-line @typescript-eslint/naming-convention
import Emitter from 'quill/core/emitter';
import { Subscription } from 'rxjs';
import { DsfrEditorService, FormattingRequest } from '../../editor.service';

declare type Formatting = { [format: string]: unknown };

@Component({
  selector: 'edu-editor-content',
  encapsulation: ViewEncapsulation.None,
  templateUrl: './editor-content.component.html',
  styleUrls: ['./editor-content.component.scss'],
  standalone: true,
  imports: [CommonModule],

  schemas: [CUSTOM_ELEMENTS_SCHEMA],
})
export class EduEditorContentComponent implements AfterViewInit, OnDestroy, OnChanges {
  /**
   * L'identifiant du tag hôte pour le composant éditeur.
   *
   * Attention : cet input est statique et doit être connu à l'initialisation du composant.
   */
  @Input() editorId = 'editor';

  /**
   * Renseigner cette proprité positionne un attribut aria-labelledby sur l'élément hôte de l'éditeur.
   */
  @Input() ariaLabelledBy: string;

  /**
   * Active la lecure seule.
   */
  @Input() readOnly = false;

  /**
   * Message d'information associé au composant.
   */
  @Input() message: string;

  /**
   * Représente la sévérité du message.
   */
  @Input() messageSeverity: DsfrSeverity;

  /**
   * Id de l'élément affichant les messages associé à l'éditeur. A renseigner si vous souhaitez positionner un attribut
   * `aria-describedby` référençant votre message au niveau de l'élément hôte de l'éditeur.
   */
  @Input() messagesGroupId: string;

  /**
   * Dev/debug : permet d'afficher le code HTML interne de l'éditeur (i.e. celui qui sert pour l'affichage WYSIWYG).
   */
  @Input() showEditorHtmlDebugPanel = false;

  /**
   * Dev/debug : permet d'afficher le code HTML qui sera récupéré en sortie.
   */
  @Input() showSemanticHtmlDebugPanel = false;

  /**
   * Instance Quill.
   */
  protected _quill: Quill;

  /**
   * Pour pouvoir utiliser DsfrSeverityConst côté template.
   */
  protected readonly severityConst = DsfrSeverityConst;

  /**
   * Options de configuration de l'éditeur Quill.
   */
  private _options: QuillOptions = {
    readOnly: this.readOnly,
    modules: {
      toolbar: false,
    },
    placeholder: this.i18n.t('editor.placeholder'),
    theme: 'snow',
  };

  private _subscriptions: Subscription[] = [];

  private _editorSelectionHandler: Emitter;

  private _editorChangeHandler: Emitter;

  constructor(
    public i18n: DsfrI18nService,
    public editorService: DsfrEditorService,
  ) {}

  ngAfterViewInit(): void {
    this._quill = new Quill('#' + this.editorId, this._options);
    this._quill.root.setAttribute('role', 'textbox');
    this._quill.root.setAttribute('aria-multiline', 'true');
    this._quill.root.setAttribute('spellcheck', 'false');
    // fix a11y : par défaut, le key-binding Quill sur la touche Tab empêche de sortir de l'éditeur en utilisant Tab
    delete (this._quill.getModule('keyboard') as any)?.bindings['Tab'];

    if (this.ariaLabelledBy && this.ariaLabelledBy.length) {
      this._quill.root.setAttribute('aria-labelledby', this.ariaLabelledBy);
    }
    if (this.message && this.message.length && this.messagesGroupId && this.messagesGroupId.length) {
      this._quill.root.setAttribute('aria-describedby', this.messagesGroupId);
    }
    // On se met à l'écoute de la sélection utilisateur
    this._editorSelectionHandler = this._quill.on(Emitter.events.SELECTION_CHANGE, (range) =>
      this._onEditorSelectionChange(range),
    );
    // Détecte le changement de contenu de l'éditeur.
    this._editorChangeHandler = this._quill.on(
      Emitter.events.EDITOR_CHANGE,
      (eventName: any, delta: Delta, oldContents: Delta, source: string) =>
        this._onEditorChange(eventName, delta, oldContents, source),
    );

    this._subscriptions.push(
      this.editorService.formattingRequest$.subscribe((req: FormattingRequest) => {
        const range = this._quill.getSelection();
        if (req === undefined) {
          if (range) {
            // remove formatting
            this._quill.removeFormat(range.index, range.length);
          }
        } else if ('image' === req.action) {
          // perform special image format
          if (range) {
            this._quill.uploader.upload(range, req.value);
          }
        } else {
          // perform standart formatting
          this._quill.format(req.action, req.value);
        }
        this.editorService.setCurrentFormatting(this._getCurrentFormatting());
      }),
    );
    // Écoute les mises à jour du modèle externe.
    this._subscriptions.push(
      this.editorService.modelChange$.subscribe((newValue: string) => {
        this.setEditorContent(newValue);
      }),
    );
    // Écoute les demandes de prise de focus
    this._subscriptions.push(
      this.editorService.focusOnEditorRequest$.subscribe(() => {
        this._quill.focus();
      }),
    );
  }

  ngOnDestroy(): void {
    this._subscriptions.forEach((s) => s.unsubscribe());
    this._quill.off(Emitter.events.SELECTION_CHANGE, this._editorSelectionHandler.off);
    this._quill.off(Emitter.events.EDITOR_CHANGE, this._editorChangeHandler.off);
  }

  ngOnChanges({ ariaLabelledBy, message, messagesGroupId }: SimpleChanges): void {
    if (ariaLabelledBy) {
      if (this.ariaLabelledBy && this.ariaLabelledBy.length) {
        this._quill?.root.setAttribute('aria-labelledby', this.ariaLabelledBy);
      } else {
        this._quill?.root.removeAttribute('aria-labelledby');
      }
    }
    if (message || messagesGroupId) {
      if (this.message && this.message.length && this.messagesGroupId && this.messagesGroupId.length) {
        this._quill?.root.setAttribute('aria-describedby', this.messagesGroupId);
      } else {
        this._quill?.root.removeAttribute('aria-describedby');
      }
    }
  }

  protected hasMessage(severity: DsfrSeverity): boolean {
    return !this.isStringEmptyOrNull(this.message) && severity === this.messageSeverity;
  }

  private setEditorContent(newValue: string) {
    this._quill.setContents(this._quill.clipboard.convert({ html: newValue }), 'silent');
  }

  //FIXME: utiliser les utils de la lib ngx-dsfr
  private isStringEmptyOrNull(s: string | null | undefined): boolean {
    return !s || s.trim() === '';
  }

  private _onEditorSelectionChange(range: { index: number; length: number }) {
    // RPA: vu avec SAB le 25/09/2024 : on considère que la toolbar reflète l'état courant relatif à la position du
    // curseur et/ou la sélection active dans l'éditeur
    // si on change d'avis et qu'on on veut conserver l'état de la toolbar correspondant à la dernière position du
    // curseur avant une perte de focus de l'éditeur alors il ne faut pas émettre d'event si range==null
    this.editorService.setCurrentFormatting(this._getCurrentFormatting());
  }

  /**
   * Permet de se mettre à l'écoute du contenu de l'éditeur afin de reset la toolbar si l'éditeur est vidé.
   */
  private _onEditorChange(eventName: string, delta: Delta, oldContents: Delta, source: string) {
    if (eventName === 'text-change') {
      // Note even when Quill is empty, there is still a blank line represented by '\n', so getLength will return 1.
      if (this._quill.getLength() === 1) {
        this.editorService.setCurrentFormatting(undefined);
      } else {
        this.editorService.setCurrentFormatting(this._getCurrentFormatting());
      }
      // on signale le changement à l'extérieur
      this.editorService.emitEditorChange(this._quill.getSemanticHTML());
    }
  }

  private _getCurrentFormatting(): Formatting | undefined {
    const range: Range | null = this._quill.getSelection();
    return range ? this._quill.getFormat(range.index, range.length) : undefined;
  }
}
