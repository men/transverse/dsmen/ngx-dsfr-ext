import { AfterViewInit, Directive, ElementRef, Input, Renderer2 } from '@angular/core';

/**
 * Permet de rendre l'élément déonoté par le sélecteur explicitement non-focusable. Cela sert au niveau de la toolbar
 * sur les dsfr-bouton et les dsfr-form-select qui embarquent des éléments nativement focusable tels que button et
 * select.
 */
//TODO: (RPA) à réfléchir s'il ne faudrait pas plutôt prévoir d'ajouter un input `'unfocusable' permettant de gérer ça
// directement  aux niveaux des composants eux-même (?)
@Directive({
  selector: '[unfocusable]',
  standalone: true,
})
export class UnfocusableDirective implements AfterViewInit {
  /**
   * Le sélecteur à utiliser pour récupérer l'élément à rendre non focusable.
   */
  // RPA: j'utilise l'alias du même nom que la directive pour pouvoir paramétrer directement la directive elle-même
  // tout en disposant d'un nom plus explicite (selector) en interne.
  @Input('unfocusable')
  public selector: string;

  constructor(
    private _elementRef: ElementRef,
    private _renderer: Renderer2,
  ) {}

  // RPA: Je me mets sur AfterViewInit car sur OnInit je n'ai pas encore le select lorsque je requête le DOM
  // (pour le bouton par contre cela fonctionnait avec OnInit)
  ngAfterViewInit(): void {
    let el: HTMLElement = this._elementRef.nativeElement.querySelector(this.selector);
    if (el) {
      this._renderer.setAttribute(el, 'tabindex', '-1');
    } else {
      //FIXME: (RPA) utiliser LoggerService lorsque celui-ci sera exporté depuis ngx-dsfr
      console.warn('UnfocusableDirective > aucun élément remontée par le sélecteur', this.selector);
    }
  }
}
