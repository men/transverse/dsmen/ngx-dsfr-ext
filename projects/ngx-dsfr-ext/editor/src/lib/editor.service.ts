import { Injectable } from '@angular/core';
import { Observable, ReplaySubject } from 'rxjs';

export declare type Formatting = { [format: string]: unknown } | undefined;

export declare type FormattingRequest = { action: string; value: any } | undefined;

/**
 * Service permettant de gérer la communication bidirectionnelle entre la toolbar et l'éditeur.
 */
@Injectable({
  providedIn: 'root',
})
export class DsfrEditorService {
  /**
   * Observable sur le formatage de la sélection courante.
   */
  readonly currentFormatting$: Observable<Formatting>;

  /**
   * Observable sur les demandes de mises en forme émise depuis la toolbar.
   */
  readonly formattingRequest$: Observable<FormattingRequest>;

  /**
   * Permet de notifier les changements opérés de l'extérieur sur le modèle HTML.
   */
  readonly modelChange$: Observable<string>;

  /**
   * Permet de notifier les changements au niveau du contenu de l'éditeur qui impacteront le modèle HTML.
   */
  readonly editorChange$: Observable<string>;

  /**
   * Permet d'émettre une demande de prise de focus sur le contenu de l'éditeur HTML.
   */
  readonly focusOnEditorRequest$: Observable<void>;

  /**
   * Subject sur les formats de la sélection courante.
   */
  private _currentFormattingSubject: ReplaySubject<Formatting>;

  /**
   * Subject sur les demandes de formatage.
   */
  private _formattingRequestSubject: ReplaySubject<FormattingRequest>;

  /**
   * Subject sur les changements externes sur le modèle HTML.
   */
  private _modelChangeSubject: ReplaySubject<string>;

  /**
   * Subject sur les changements de la valeur de l'éditeur HTML.
   */
  private _editorChangeSubject: ReplaySubject<string>;

  /**
   * Subject sur les demandes de prise de focus sur l'éditeur.
   */
  private _focusOnEditorRequestSubject: ReplaySubject<void>;

  constructor() {
    // RPA: j'utilise des ReplaySubject(1) pour ne pas être parasité par la première émission de la valeur initiale
    // du BehaviorSubject, par contre comme pour ce dernier les souscripteurs seront notifiés de la dernière valeur
    // émise _à partir du moment où une valeur a été explicitement émise_
    // source: https://stackoverflow.com/a/43119768/8737817
    this._currentFormattingSubject = new ReplaySubject<Formatting>(1);
    this._formattingRequestSubject = new ReplaySubject<FormattingRequest>(1);
    this._modelChangeSubject = new ReplaySubject<string>(1);
    this._editorChangeSubject = new ReplaySubject<string>(1);
    this._focusOnEditorRequestSubject = new ReplaySubject<void>(1);

    this.currentFormatting$ = this._currentFormattingSubject.asObservable();
    this.formattingRequest$ = this._formattingRequestSubject.asObservable();
    this.modelChange$ = this._modelChangeSubject.asObservable();
    this.editorChange$ = this._editorChangeSubject.asObservable();
    this.focusOnEditorRequest$ = this._focusOnEditorRequestSubject.asObservable();
  }

  /**
   * Permet de positionner la mise en forme de la sélection courante.
   */
  setCurrentFormatting(formats: Formatting) {
    this._currentFormattingSubject.next(formats);
  }

  /**
   * Permet de demander une nouvelle mise en forme sur la sélection courante.
   */
  requestFormatting(request: FormattingRequest) {
    this._formattingRequestSubject.next(request);
  }

  /**
   * Permet de demander la prise de focus sur l'éditeur.
   */
  requestFocusOnEditorContent() {
    this._focusOnEditorRequestSubject.next();
  }

  /**
   * Permet de notifier un changement externe sur le modèle HTML.
   */
  emitModelChange(value: string) {
    this._modelChangeSubject.next(value);
  }

  /**
   * Permet de notifier un changement interne du contenu de l'éditeur HTML.
   */
  emitEditorChange(value: string) {
    this._editorChangeSubject.next(value);
  }
}
