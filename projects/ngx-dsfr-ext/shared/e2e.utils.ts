import { FrameLocator, Locator, Page, Response, expect } from '@playwright/test';

export class BasePageObjectModel {
  page: Page;
  sbFrame: FrameLocator;
  componentName: string;

  constructor(page: Page, componentName: string) {
    this.page = page;
    this.sbFrame = this.frame;
    this.componentName = componentName;
  }

  get frame(): FrameLocator {
    return this.page.frameLocator('#storybook-preview-iframe');
  }

  async goToPage(
    variant: string = 'default',
    waitUntil: 'load' | 'domcontentloaded' | 'networkidle' | 'commit' | undefined = 'commit',
  ): Promise<Response | null> {
    return this.page.goto(`?path=/story/components-${this.componentName}--${variant}`, {
      waitUntil,
    });
  }
}

export async function goToComponentPage(
  componentName: string,
  page: Page,
  variant: string = 'default',
): Promise<Response | null> {
  return page.goto(`?path=/story/components-${componentName}--${variant}`);
}

export function getSbFrame(page: Page): FrameLocator {
  return page.frameLocator('#storybook-preview-iframe');
}

export async function testTabNavigation(
  tabNavigationSequence: Locator[],
  keyPressContext: Locator,
  withShift: boolean = false,
): Promise<void> {
  const keyToPress = withShift ? 'Shift+Tab' : 'Tab';

  for (let navigationStepIndex in tabNavigationSequence) {
    await keyPressContext.press(keyToPress);
    await expect(tabNavigationSequence[navigationStepIndex]).toBeFocused();
  }
}
