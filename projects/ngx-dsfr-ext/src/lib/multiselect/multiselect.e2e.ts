import { expect, FrameLocator, test } from '@playwright/test';
import { getSbFrame, goToComponentPage } from 'projects/ngx-dsfr-ext/shared/e2e.utils';

test.describe('Multiselect', () => {
  test.describe('focuses', () => {
    test.beforeEach(async ({ page }) => {
      return goToComponentPage('multiselect', page, 'with-search-field');
    });

    test('correct focus navigation pattern with searchbox', async ({ page }) => {
      const frame = getSbFrame(page);
      await openSelectDropdown(frame);

      const listContainer = frame.getByRole('listbox');
      const listOptions = listContainer.locator('li');
      const searchBox = frame.locator('.fr-input');

      // Expect focus on searchbox when multiselect opened and no option is selected
      await expect(searchBox).toBeFocused();

      // When arrow down is pressed on search box
      await searchBox.press('ArrowDown');

      // Expect the focus to move directly to the first list item
      await expect(listOptions.first()).toBeFocused();
      await listOptions.first().press('ArrowDown');
      await expect(listOptions.nth(1)).toBeFocused();
    });

    test('focus back to first item', async ({ page }) => {
      const frame = getSbFrame(page);
      await openSelectDropdown(frame);

      const listContainer = frame.getByRole('listbox');
      const listOptions = listContainer.locator('li');

      // When arrow down is pressed on last item
      await listOptions.last().press('ArrowDown');

      // Expect focus to loop back to first item
      await expect(listOptions.first()).toBeFocused();
    });
  });
});

test.describe('focuses', () => {
  test.beforeEach(async ({ page }) => {
    return page.goto(`?path=/story/components-multiselect--default`);
  });

  test('focus on selected item when opening dropdown', async ({ page }) => {
    const frame = getSbFrame(page);
    await openSelectDropdown(frame);

    const multiselect = frame.locator('.fr-select');
    const listContainer = frame.getByRole('listbox');
    const listOptions = listContainer.locator('li');

    // When the 5th list item is selected
    await listOptions.nth(4).click();
    await listOptions.nth(4).press('Tab');

    // Expect focus to return to the selected item (5th)
    await expect(multiselect).toBeFocused();
    await multiselect.click();
    await expect(listOptions.nth(4)).toBeFocused();
  });
});

async function openSelectDropdown(frame: FrameLocator): Promise<void> {
  const multiselect = frame.locator('.fr-select');
  return multiselect.click();
}
