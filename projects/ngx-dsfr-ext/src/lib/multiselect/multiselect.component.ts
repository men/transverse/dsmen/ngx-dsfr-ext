import { CdkVirtualScrollViewport, ScrollingModule } from '@angular/cdk/scrolling';
import { CommonModule } from '@angular/common';
import {
  ChangeDetectionStrategy,
  Component,
  Input,
  OnInit,
  ViewChild,
  ViewEncapsulation,
  forwardRef,
} from '@angular/core';
import { FormsModule, NG_VALUE_ACCESSOR, ReactiveFormsModule } from '@angular/forms';
import { DsfrButtonModule, DsfrI18nPipe, DsfrOption, DsfrTagModule } from '@edugouvfr/ngx-dsfr';
import { AbstractSelectComponent, DropdownContainerComponent, EduCheckboxComponent } from '../shared/components';

@Component({
  selector: 'dsfr-ext-multiselect, dsfrx-multiselect,',
  styleUrls: ['./multiselect.component.scss'],
  templateUrl: './multiselect.component.html',
  encapsulation: ViewEncapsulation.None,
  standalone: true,
  changeDetection: ChangeDetectionStrategy.OnPush,
  imports: [
    CommonModule,
    FormsModule,
    ScrollingModule,
    ReactiveFormsModule,
    DropdownContainerComponent,
    DsfrTagModule,
    EduCheckboxComponent,
    DsfrButtonModule,
    DsfrI18nPipe,
  ],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => DsfrMultiselectComponent),
      multi: true,
    },
  ],
})
export class DsfrMultiselectComponent extends AbstractSelectComponent<DsfrOption> implements OnInit {
  /** @internal */
  @ViewChild(CdkVirtualScrollViewport) viewport: CdkVirtualScrollViewport;

  /** Personnalisation de la fonction de recherche pour filtrer les options. Fonction à 2 arguments : option (DsfrOption) et texte cherché (string) */
  @Input()
  set searchFn(fn: (o: DsfrOption, text: string) => boolean) {
    if (typeof fn !== 'function') {
      throw Error('`searchFn` must be a function.');
    }
    this._searchFn = fn;

    this.searchText = '';
    this.optionsFilter = this.options ?? [];
  }

  /**
   * Mise a jour de la valeur de selectedOptions et value programmaticalement (ex. à l'init reactive form)
   * @param value
   * @internal
   */
  override writeValue(value: any) {
    if (value) {
      if (this.isSingleMode()) {
        this.selectedOptions =
          this.options && this.options.length ? [this.options?.find((o) => this._compareWith(value, o.value))] : [];
      } else if (!(value instanceof Array)) {
        throw Error('`value` must be an array.');
      } else {
        // Sélection des options en enlevant les doublons et valeurs nulles éventuels
        this.selectedOptions = Array.from(
          new Set(
            value
              .map((val: DsfrOption) => this.options?.find((o) => this._compareWith(val, o.value)))
              .filter((selected: DsfrOption) => !!selected),
          ),
        );
      }

      this.indexFirstSelected = this.getFirstIndexSelected();
    } else {
      this.selectedOptions = [];
      this.indexFirstSelected = null;
    }

    super.writeValue(value);
    this.cd.markForCheck();
  }

  /**
   * Sélection ou désélection d'une option
   * @internal */
  public onOptionClick(e: any, option: DsfrOption): void {
    if (this.isSingleMode()) {
      this.optionClickSingle(option);
    } else {
      this.optionClickMultiple(option);
    }

    this.indexFirstSelected = this.getFirstIndexSelected();
    this.selectionChange.emit(this.value);

    e.preventDefault();
  }

  /** @internal */
  public isSelected(option: DsfrOption) {
    if (this.isSingleMode()) {
      return this._compareWith(this.value, option.value);
    }
    if (this.value && !(this.value instanceof Array)) throw Error('`value` must be an array.');
    return !!this.value?.find((o: any) => this._compareWith(o, option.value));
  }

  /**
   * Interaction au clavier dans la liste déroulante
   * Les flêches permettent de naviguer d'une option à l'autre en modifiant le focus.
   * A la fin ou au début de la liste on boucle sur la première ou dernière option
   * @internal */
  public onOptionKeydown(event: KeyboardEvent, option: DsfrOption): void {
    //retour tab
    if (event.shiftKey && event.key === 'Tab') {
      if (this.showSearch && this.appendTo) {
        // manually place focus back to search container if appendTo is active
        this.searchBox?.nativeElement?.focus();
      }
      return;
    }

    switch (event.key) {
      case 'ArrowDown':
        const nextItem = (event.target as Element).nextElementSibling;
        if (nextItem) {
          this.setFocusElement(nextItem as HTMLElement);
        } else this.setFocusElementOnFirst(event);
        event.preventDefault();
        break;
      case 'ArrowUp':
        const prevItem = (event.target as Element).previousElementSibling;
        if (prevItem) {
          this.setFocusElement(prevItem as HTMLElement);
        } else this.setFocusElementOnLast(event);
        event.preventDefault();
        break;
      case 'End':
        this.setFocusElementOnLast(event);
        event.preventDefault();
        break;
      case 'Home':
        this.setFocusElementOnFirst(event);
        event.preventDefault();
        break;
      case 'Enter':
        this.onOptionClick(event, option);
        event.preventDefault();
        this.hide();
        break;
      case ' ':
        this.onOptionClick(event, option);
        event.preventDefault();
        break;
      case 'Escape':
      case 'Tab':
        this.hide();
        break;
    }
  }

  /**
   * Sélectionner ou déselectionner toutes les options
   * Si maxSelectedOptions est défini, sélection uniquement des (max) premières options
   @internal
  */
  public selectAll(): void {
    if (this.optionsFilter?.length === this.value?.length) {
      this.selectedOptions = [];
      this.value = [];
    } else {
      this.selectedOptions = [...this.optionsFilter];
      this.value = this.selectedOptions.map((o) => o.value);
    }

    this.selectionChange.emit(this.value);
  }

  /**
   * Filtrer les options selon la fonction de recherche
   * @internal */
  public onSearch(): void {
    if (this._searchFn && this.options && this.options.length > 0) {
      this.optionsFilter = this.options.filter((o) => this._searchFn(o, this.searchText));
    }
  }

  /** @internal */
  public hasisAllSelected(): boolean {
    return this.optionsFilter?.length > 0 && this.value?.length === this.optionsFilter.length;
  }

  /** @internal */
  public trackByIndex = (index: number): number => {
    return index;
  };

  /** @internal */
  public clearOption(event: Event, option: DsfrOption): void {
    event.preventDefault();
    event.stopPropagation();
    this.onOptionClick(event, option);
  }

  /** @internal */
  public clearAllOptions(): void {
    this.selectedOptions = [];
    this.value = [];

    this.selectionChange.emit(this.value);
  }

  protected handleListFocus(el: HTMLElement): void {
    if (this.virtualScroll) this.scrollForVirtualScroll(el);
  }

  /** Index du premier element sélectionné pour positionner le focus et aria */
  private getFirstIndexSelected(): number {
    if (this.isSingleMode()) {
      return this.optionsFilter?.findIndex((o) => o.value === this.value);
    }
    if (this.value && this.value[0]) {
      const firstOption = this.value ? this.value[0] : null;
      return this.optionsFilter?.findIndex((o) => o.value === firstOption);
    }

    return -1;
  }

  /** Sélection ou déselection d'une option en mode multiple/checkbox */
  private optionClickMultiple(option: DsfrOption): void {
    if (!this.isSelected(option)) {
      if (this.maxSelectedOptions && this.selectedOptions.length === this.maxSelectedOptions) {
        // nothing here
      } else {
        this.selectedOptions.push(option);
      }
    } else {
      // déselection
      const i = this.selectedOptions.findIndex((o) => o === option);
      this.selectedOptions.splice(i, 1);
    }
    this.selectedOptions = [...this.selectedOptions];
    this.value = this.selectedOptions.map((o) => o.value);
  }

  /** Sélection ou déselection d'une option en mode single */
  private optionClickSingle(option: DsfrOption): void {
    this.selectedOptions = this.isSelected(option) ? [] : [option];
    this.value = this.selectedOptions[0] ? this.selectedOptions[0].value : undefined;
    this.hide();
  }

  /** Fonction de recherche par défaut */
  private _searchFn: (o: DsfrOption, searchText: string) => boolean = function (o, searchText) {
    return o.label?.toLowerCase().includes(searchText.toLowerCase());
  };

  private setFocusElementOnFirst(event: KeyboardEvent) {
    const firstItem = (event.target as Element).parentNode?.firstElementChild;
    this.setFocusElement(firstItem as HTMLElement);
  }

  private setFocusElementOnLast(event: KeyboardEvent) {
    const lastItem = (event.target as Element).parentNode?.lastElementChild;
    this.setFocusElement(lastItem as HTMLElement);
  }

  /** Set focus on element and add aria-activedescendant attribute to parent */
  private setFocusElement(optionEl: HTMLElement) {
    if (optionEl) {
      optionEl.focus();
      const optionId: string | null = optionEl.getAttribute('id');
      if (optionId) optionEl.parentElement?.setAttribute('aria-activedescendant', optionId);

      if (this.virtualScroll) {
        this.scrollForVirtualScroll(optionEl);
      }
    }
  }

  /* *Gestion manuelle du scroll dans la liste dans le cas de la navigation clavier avec virtualScroll activé */
  private scrollForVirtualScroll(optionEl: HTMLElement) {
    const index: string = optionEl.getAttribute('data-index') ?? '0';

    this.viewport.scrollToIndex(Number(index), 'smooth');
  }
}
