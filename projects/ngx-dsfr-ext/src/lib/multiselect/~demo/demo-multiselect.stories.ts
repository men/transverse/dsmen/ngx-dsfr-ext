import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { DsfrButtonModule, DsfrFormInputModule, DsfrOption } from '@edugouvfr/ngx-dsfr';
import { Meta, StoryObj, moduleMetadata } from '@storybook/angular';
import { DemoMultiselectComponent } from './demo-multiselect.component';

const meta: Meta<DemoMultiselectComponent> = {
  title: 'Components/Multiselect',
  component: DemoMultiselectComponent,
  decorators: [moduleMetadata({ imports: [FormsModule, ReactiveFormsModule, DsfrFormInputModule, DsfrButtonModule] })],
  parameters: { actions: false },
};
export default meta;
type Story = StoryObj<DemoMultiselectComponent>;

const options: DsfrOption[] = [
  { value: { id: '1', libelle: 'Libellé 1' }, label: 'Option 1' },
  { value: { id: '2', libelle: 'Libellé 2' }, label: 'Option 2' },
  { value: { id: '3', libelle: 'Libellé 3' }, label: 'Option 3' },
  { value: { id: '4', libelle: 'Libellé 4' }, label: 'Option 4' },
  { value: { id: '5', libelle: 'Libellé 5' }, label: 'Option 5' },
  { value: { id: '6', libelle: 'Libellé 6' }, label: 'Option 6' },
  { value: { id: '7', libelle: 'Libellé 7' }, label: 'Option 7' },
  { value: { id: '8', libelle: 'Libellé 8' }, label: 'Option 8' },
  { value: { id: '9', libelle: 'Libellé 8' }, label: 'Option 9' },
  { value: { id: '10', libelle: 'Libellé 8' }, label: 'Option 10' },
];

/** Default */
export const ReactiveForm: Story = {
  args: {
    options: options,
    maxDisplayOptions: 2,
    appendTo: true,
    showSearch: true,
  },
};

/** Default */
export const InsideScrollableContainer: Story = {
  args: {
    options: options,
    maxDisplayOptions: 2,
    appendTo: true,
    showSearch: true,
    insideContainer: true,
  },
};
