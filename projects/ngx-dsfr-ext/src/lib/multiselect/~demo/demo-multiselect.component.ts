import { CommonModule } from '@angular/common';
import { Component, Input, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, FormsModule, ReactiveFormsModule, Validators } from '@angular/forms';
import { DsfrAccordionModule, DsfrButtonModule, DsfrFormInputModule, DsfrOption } from '@edugouvfr/ngx-dsfr';
import { DemoToolbarComponent } from '../../shared/demo/demo-toolbar.component';
import { DsfrMultiselectComponent } from '../multiselect.component';

@Component({
  selector: `demo-multiselect`,
  templateUrl: './demo-multiselect.component.html',
  standalone: true,
  styleUrls: ['./demo-multiselect.component.scss'],
  imports: [
    CommonModule,
    FormsModule,
    DsfrMultiselectComponent,
    DsfrFormInputModule,
    DsfrButtonModule,
    ReactiveFormsModule,
    DsfrAccordionModule,
    DemoToolbarComponent,
  ],
})
export class DemoMultiselectComponent implements OnInit {
  @Input() options: DsfrOption[];
  @Input() appendTo: boolean;
  @Input() maxDisplayOptions: number;
  @Input() showSearch: boolean;
  @Input() insideContainer: boolean;

  /** @internal */
  form: FormGroup;

  /** @internal */
  formGroup: FormGroup;

  /** @internal */
  value: any;

  /** @internal */
  load: boolean = false;

  /**@internal */
  options3: DsfrOption[] = [
    { label: 'Arts plastiques', value: '0304' },
    { label: 'Espagnol LV3', value: '0305' },
    { label: 'Musique', value: '0832;F' },
  ];
  /** @internal */
  options2: DsfrOption[];

  constructor(private fb: FormBuilder) {
    this.load = true;
    this.form = this.fb.group({
      test: [[{ id: '3', libelle: 'Libellé 4' }], Validators.required],
    });
    this.value = { id: '3', libelle: 'Libellé 3' };
  }

  /** @internal */
  get formControl(): FormControl {
    return this.form.get('formControl') as FormControl;
  }

  /** @internal */
  get formControl2(): FormControl {
    return this.form.get('formControl2') as FormControl;
  }

  /** @internal */
  get formControl3(): FormControl {
    return this.form.get('formControl3') as FormControl;
  }

  ngOnInit(): void {
    this.form.addControl('formControl', new FormControl([], Validators.required));
    this.form.addControl('formControl2', new FormControl([], Validators.required));
    this.form.addControl('formControl3', new FormControl([], Validators.required));

    this.formControl.setValue([{ id: '3', libelle: 'Libellé 3' }]);
    this.formControl2.setValue([{ id: '3', libelle: 'Libellé 3' }]);
    this.formControl3.setValue(this.value);

    setTimeout(() => {
      this.load = false;
      this.options2 = [
        { value: 1, label: 'Test 1' },
        { value: 2, label: 'Test 2' },
        { value: 3, label: 'Test 3' },
      ];
      this.formControl2.setValue([1, 2]);
    }, 0);
  }

  /**@internal */
  compareFn(item: any, selected: any) {
    return item?.id === selected.id;
  }

  /**@internal */
  searchFn(item: any, text: string) {
    return item.value.libelle.includes(text);
  }

  /**
   * @internal */
  reset() {
    this.formControl.reset();
  }

  /**
   * @internal */
  changeOptions() {
    this.options = [
      { value: { id: '9', libelle: 'Libellé 9' }, label: 'Option 9' },
      { value: { id: '10', libelle: 'Libellé 10' }, label: 'Option 10' },
      { value: { id: '11', libelle: 'Libellé 11' }, label: 'Option 11' },
      { value: { id: '12', libelle: 'Libellé 12' }, label: 'Option 12' },
    ];
    this.formControl.setValue([this.options[2].value]);
  }
}
