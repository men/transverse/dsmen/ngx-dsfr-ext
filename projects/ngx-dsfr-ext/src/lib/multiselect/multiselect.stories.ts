import { controlEmitter } from '.storybook/storybook-utils';
import { FormsModule } from '@angular/forms';
import { DsfrAccordionModule, DsfrButtonModule, DsfrFormInputModule } from '@edugouvfr/ngx-dsfr';
import { Meta, StoryObj, moduleMetadata } from '@storybook/angular';
import { DropdownContainerComponent } from '../shared/components';
import { DsfrSelectionModeConst } from '../shared/models/selection-mode.type';
import { DsfrMultiselectComponent } from './multiselect.component';

const meta: Meta<DsfrMultiselectComponent> = {
  title: 'Components/Multiselect',
  component: DsfrMultiselectComponent,
  decorators: [
    moduleMetadata({
      imports: [FormsModule, DsfrFormInputModule, DsfrButtonModule, DsfrAccordionModule, DropdownContainerComponent],
    }),
  ],
  parameters: {
    actions: false,
    docs: {
      toc: {
        disable: true, //  Disables the table of contents
      },
    },
  },
  argTypes: {
    selectionChange: { control: controlEmitter },
    maxSelectedOptions: { control: { type: 'number' } },
    compareWith: { control: { type: 'object' } },
    selectionMode: { control: { type: 'inline-radio' }, options: ['single', 'multiple', 'checkbox'] },
    messageSeverity: { control: { type: 'inline-radio' }, options: ['info', 'error', 'success'] },
  },
};
export default meta;
type Story = StoryObj<DsfrMultiselectComponent>;

const optionsDefault = [
  { label: 'Option 1', value: 1 },
  { label: 'Option 2', value: 2 },
  { label: 'Option 3', value: 3 },
  { label: 'Option 4', value: 4 },
  { label: 'Option 5', value: 5 },
  { label: 'Option 6', value: 6 },
  { label: 'Option 7', value: 7 },
  { label: 'Option 8', value: 8 },
];

const makeOptions = () => {
  const options: any = [];
  for (let i = 0; i <= 1000; i++) {
    options.push({ value: { id: i, libelle: 'Libellé 8' }, label: 'Option ' + i });
  }
  return options;
};

const optionsVirtualScroll: any[] = makeOptions();

const templateAccordion = `<dsfr-accordion heading="Test">
<ng-container content>
  <dsfr-ext-multiselect
  [label]="label"
  [options]="options"
  [appendTo]="appendTo"
  [zIndex]="zIndex"
  [showSearch]="showSearch"
  [hint]="hint"
  [(ngModel)]="value"></dsfr-ext-multiselect>
</ng-container>
</dsfr-accordion>`;

const templateBasic = `
<dsfr-ext-multiselect [label]="label" 
  [showSelectAll]="showSelectAll" 
  [disabled]="disabled"
  [message]="message"
  [messageSeverity]="messageSeverity"
  [maxDisplayOptions]="maxDisplayOptions"
  [options]="options" 
  [(ngModel)]="value"></dsfr-ext-multiselect>`;

const templateClearable = `
<dsfr-ext-multiselect  [label]="label" [clearableOptions]="true" [options]="options" [(ngModel)]="value"></dsfr-ext-multiselect>`;

/** Default */
export const Default: Story = {
  args: {
    label: 'Label',
    hint: 'Description additionnelle',
    options: optionsDefault,
    value: [],
    selectId: 'test',
  },
};

/** Error */
export const MessageError: Story = {
  args: {
    label: 'Label',
    message: "Message d'erreur",
    messageSeverity: 'error',
    options: optionsDefault,
    maxDisplayOptions: 4,
    value: [2],
  },
  render: (args) => ({
    props: args,
    template: templateBasic,
  }),
};

/** Valid */
export const MessageValid: Story = {
  args: {
    label: 'Label',
    message: 'Message de validation',
    messageSeverity: 'success',
    options: optionsDefault,
    maxDisplayOptions: 4,
    value: [2],
  },
  render: (args) => ({
    props: args,
    template: templateBasic,
  }),
};

/** Disabled */
export const Disabled: Story = {
  args: {
    label: 'Label',
    options: optionsDefault,
    disabled: true,
    value: [2],
  },
  render: (args) => ({
    props: args,
    template: templateBasic,
  }),
};

/** Single mode */
export const ModeSingle: Story = {
  args: {
    label: "Sélection d'un seul élément",
    options: optionsDefault,
    selectionMode: 'single',
    showSearch: true,
  },
};

/** Hide checkboxes */
export const ModeMultiple: Story = {
  args: {
    label: 'Sélection sans cases à cocher',
    options: optionsDefault,
    showSearch: false,
    selectionMode: DsfrSelectionModeConst.MULTIPLE,
    showSelectAll: false,
  },
};

export const WithSelectAll: Story = {
  args: {
    label: 'Label',
    options: optionsDefault,
    showSelectAll: true,
    value: [2],
  },
};

export const WithSearchField: Story = {
  args: {
    options: [
      { label: 'Paris', value: 1 },
      { label: 'Marseille', value: 2 },
      { label: 'Lyon', value: 3 },
      { label: 'Toulouse', value: 4 },
      { label: 'Nice', value: 5 },
      { label: 'Nantes', value: 6 },
      { label: 'Strasbourg', value: 7 },
      { label: 'Montpellier', value: 8 },
      { label: 'Bordeaux', value: 9 },
      { label: 'Lille', value: 10 },
      { label: 'Rennes', value: 11 },
      { label: 'Reims', value: 12 },
      { label: 'Le Havre', value: 13 },
      { label: 'Saint-Étienne', value: 14 },
      { label: 'Toulon', value: 15 },
      { label: 'Grenoble', value: 16 },
      { label: 'Angers', value: 17 },
      { label: 'Dijon', value: 18 },
    ],
    showSearch: true,
    label: 'With search field',
  },
};

export const WithClearSearch: Story = {
  args: {
    options: [
      { label: 'Paris', value: 1 },
      { label: 'Marseille', value: 2 },
      { label: 'Lyon', value: 3 },
      { label: 'Toulouse', value: 4 },
      { label: 'Nice', value: 5 },
      { label: 'Nantes', value: 6 },
      { label: 'Strasbourg', value: 7 },
      { label: 'Montpellier', value: 8 },
      { label: 'Bordeaux', value: 9 },
      { label: 'Lille', value: 10 },
      { label: 'Rennes', value: 11 },
      { label: 'Reims', value: 12 },
      { label: 'Le Havre', value: 13 },
      { label: 'Saint-Étienne', value: 14 },
      { label: 'Toulon', value: 15 },
      { label: 'Grenoble', value: 16 },
      { label: 'Angers', value: 17 },
      { label: 'Dijon', value: 18 },
    ],
    showSearch: true,
    showSelectAll: true,
    showClearSearch: true,
    label: 'With clear search',
  },
};

/** withMaxDisplayOptions */
export const MaxDisplayOptions: Story = {
  args: {
    label: 'Afficher max. un label pour les options sélectionnées',
    options: optionsDefault,
    showSelectAll: false,
    maxDisplayOptions: 1,
    value: [2, 3],
  },
  render: (args) => ({
    props: args,
    template: templateBasic,
  }),
};

/** With max selected options */
export const MaxSelectedOptions: Story = {
  args: {
    label: 'Sélection limitée à 3 options',
    options: optionsDefault,
    showSearch: true,
    showSelectAll: true,
    maxSelectedOptions: 3,
  },
};

/** WithClearableOptions */
export const ClearableOptions: Story = {
  args: {
    label: 'Afficher les options sous forme de tags supprimables',
    options: optionsDefault,
    clearableOptions: true,
    maxDisplayOptions: 4,
    value: [2, 3],
  },
  render: (args) => ({
    props: args,
    template: templateClearable,
  }),
};

/** With virtual scroll */
export const VirtualScroll: Story = {
  args: {
    label: 'Label',
    options: optionsVirtualScroll,
    virtualScroll: true,
    showSearch: false,
    value: [2],
  },
};

/** Inside container */
/*export const InsideContainer: Story = {
  args: {
    label: 'Multiselect avec appendToBody',
    hint: "appendToBody permet d'avoir la liste par-dessus le container (overlay) en l'ajoutant directement au body",
    options: optionsDefault,
    showSearch: true,
    zIndex: 9000,
    appendToBody: true,
    value: [],
  },
  render: (args) => ({
    props: args,
    template: templateAccordion,
  }),
};*/
