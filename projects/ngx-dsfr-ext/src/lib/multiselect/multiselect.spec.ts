import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';

import { Component, ViewChild } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { DsfrMultiselectComponent } from './multiselect.component';

@Component({
  selector: `edu-host-component`,
  template: `<dsfr-ext-multiselect
    [options]="options"
    [(ngModel)]="value"
    [selectionMode]="selectionMode"></dsfr-ext-multiselect>`,
})
class TestHostComponent {
  @ViewChild(DsfrMultiselectComponent)
  public multiselectComponent: DsfrMultiselectComponent;
  options: { label: string; value: any }[] = [
    { label: 'Option 1', value: 1 },
    { label: 'Option 2', value: 2 },
    { label: 'Option 3', value: 3 },
    { label: 'Option 4', value: 4 },
    { label: 'Option 5', value: 5 },
    { label: 'Option 6', value: 6 },
    { label: 'Option 7', value: 7 },
    { label: 'Option 8', value: 8 },
  ];

  value: any;
  selectionMode = 'checkbox';
}

describe('DsfrMultiselectComponent', () => {
  let testHostComponent: TestHostComponent;
  let fixture: ComponentFixture<TestHostComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [FormsModule, DsfrMultiselectComponent],
      declarations: [TestHostComponent],
    }).compileComponents();
    fixture = TestBed.createComponent(TestHostComponent);
    testHostComponent = fixture.componentInstance;
    fixture.detectChanges();
  });

  // Mock initialisation and opening of dropdown list
  function initAndOpenDropdown(): HTMLElement {
    testHostComponent.multiselectComponent.selectId = 'test';
    testHostComponent.multiselectComponent.showSearch = true;
    testHostComponent.multiselectComponent.ngOnInit();
    fixture.detectChanges();

    const selectEl: HTMLElement = fixture.nativeElement.querySelector('.fr-select');
    selectEl.click();
    tick();
    fixture.detectChanges();

    return selectEl;
  }

  it('should put id test ', () => {
    // GIVEN
    testHostComponent.multiselectComponent.selectId = 'test';

    // WHEN
    testHostComponent.multiselectComponent.ngOnInit();
    fixture.detectChanges();
    const selectEl = fixture.nativeElement.querySelector('.fr-select');

    // THEN
    expect(selectEl.getAttribute('role')).toEqual('combobox');
    expect(selectEl.getAttribute('id')).toEqual('test');
  });

  describe('DsfrMultiselectComponent dropdown manipulation', () => {
    it('should display 8 options ', fakeAsync(() => {
      // WHEN
      const selectEl = initAndOpenDropdown();
      const listEl = fixture.nativeElement.querySelector('#test-listbox');

      // THEN
      expect(testHostComponent.multiselectComponent.displayList).toBeTruthy();
      expect(listEl.children).toHaveLength(8);
      expect(selectEl.textContent).toContain('Sélectionner une option');
      expect(testHostComponent.multiselectComponent.value).toEqual(undefined);
    }));

    it('should add list outside the component with appendTo ', fakeAsync(() => {
      // GIVEN
      testHostComponent.multiselectComponent.appendTo = 'body';

      // WHEN
      initAndOpenDropdown();
      const listInside = fixture.nativeElement.querySelector('#test-listbox');
      const listOutside = document.querySelector('#test-listbox');

      // THEN
      expect(listInside).toBeFalsy();
      expect(listOutside).toBeTruthy();
    }));

    it('should select first option ', fakeAsync(() => {
      // GIVEN
      const selectEl = initAndOpenDropdown();
      const listEl = fixture.nativeElement.querySelector('#test-listbox');

      // WHEN
      listEl.children[0].click();
      tick();
      fixture.detectChanges();

      // THEN
      expect(testHostComponent.multiselectComponent.value).toEqual([1]);
      expect(listEl.children[0].getAttribute('aria-selected')).toEqual('true');
      expect(listEl.children[1].getAttribute('aria-selected')).toEqual('false');
      expect(selectEl.textContent).toContain('Option 1');
    }));

    it('should select all option with button select all ', fakeAsync(() => {
      // GIVEN
      testHostComponent.multiselectComponent.showSelectAll = true;
      const selectEl = initAndOpenDropdown();
      const listEl = fixture.nativeElement.querySelector('#test-listbox');
      const buttonSelectAll = fixture.nativeElement.querySelector('button');

      // WHEN
      buttonSelectAll.click();
      tick();
      fixture.detectChanges();

      // THEN
      expect(testHostComponent.multiselectComponent.value).toEqual([1, 2, 3, 4, 5, 6, 7, 8]);
      expect(selectEl.textContent).toContain('8 sélectionnés');
      expect(listEl.children[0].getAttribute('aria-selected')).toEqual('true');
      expect(buttonSelectAll.textContent).toContain('Tout désélectionner');
    }));

    it('should select multiple options ', fakeAsync(() => {
      // GIVEN
      const selectEl = initAndOpenDropdown();
      const listEl = fixture.nativeElement.querySelector('#test-listbox');

      // WHEN
      listEl.children[0].click();
      listEl.children[2].click();
      listEl.children[3].click();
      tick();
      fixture.detectChanges();

      // THEN
      expect(testHostComponent.multiselectComponent.value).toEqual([1, 3, 4]);
      expect(listEl.getAttribute('aria-multiselectable')).toEqual('true');
      expect(listEl.children[0].getAttribute('aria-selected')).toEqual('true');
      expect(listEl.children[1].getAttribute('aria-selected')).toEqual('false');
      expect(listEl.children[2].getAttribute('aria-selected')).toEqual('true');
      expect(selectEl.textContent).toContain('Option 1, Option 3, Option 4');
      expect(testHostComponent.multiselectComponent.displayList).toEqual(true);
    }));

    it('should select multiple options and display label max ', fakeAsync(() => {
      // GIVEN
      testHostComponent.multiselectComponent.maxDisplayOptions = 1;
      const selectEl = initAndOpenDropdown();
      const listEl = fixture.nativeElement.querySelector('#test-listbox');

      // WHEN
      listEl.children[0].click();
      listEl.children[2].click();
      listEl.children[3].click();
      listEl.children[0].click();
      tick();
      fixture.detectChanges();

      // THEN
      expect(testHostComponent.multiselectComponent.value).toEqual([3, 4]);
      expect(listEl.children[0].getAttribute('aria-selected')).toEqual('false');
      expect(listEl.children[1].getAttribute('aria-selected')).toEqual('false');
      expect(listEl.children[2].getAttribute('aria-selected')).toEqual('true');
      expect(listEl.children[3].getAttribute('aria-selected')).toEqual('true');
      expect(selectEl.textContent).toContain('2 sélectionnés');
    }));

    it('should select one option and hide list in single mode', fakeAsync(() => {
      // GIVEN
      testHostComponent.multiselectComponent.maxDisplayOptions = 1;
      testHostComponent.multiselectComponent.selectionMode = 'single';
      const selectEl = initAndOpenDropdown();
      const listEl = fixture.nativeElement.querySelector('#test-listbox');

      // WHEN
      listEl.children[3].click();
      tick();
      fixture.detectChanges();

      // THEN
      expect(listEl.getAttribute('aria-multiselectable')).toEqual('false');
      expect(listEl.children[3].getAttribute('aria-selected')).toEqual('true');
      expect(selectEl.textContent).toContain('Option 4');
      expect(testHostComponent.multiselectComponent.value).toEqual(4);
      expect(testHostComponent.multiselectComponent.displayList).toEqual(false);
    }));
  });

  it('should filter options when typing in the search box', fakeAsync(() => {
    // GIVEN
    testHostComponent.multiselectComponent.showSearch = true;
    initAndOpenDropdown();
    const listEl = fixture.nativeElement.querySelector('#test-listbox');
    const searchBox = fixture.nativeElement.querySelector('.fr-input');

    // WHEN
    searchBox.value = '8';
    searchBox.dispatchEvent(new Event('input'));
    tick();
    fixture.detectChanges();

    // THEN
    expect(listEl.children.length).toEqual(1);
  }));

  it('should init with correct value on checkbox mode ', fakeAsync(() => {
    // GIVEN
    testHostComponent.value = [1, 2];
    fixture.detectChanges();

    // WHEN
    initAndOpenDropdown();

    const listEl = fixture.nativeElement.querySelector('#test-listbox');

    // THEN
    expect(testHostComponent.multiselectComponent.selectedOptions).toEqual([
      { label: 'Option 1', value: 1 },
      { label: 'Option 2', value: 2 },
    ]);
    expect(listEl.children[0].getAttribute('aria-selected')).toEqual('true');
    expect(listEl.children[1].getAttribute('aria-selected')).toEqual('true');
    expect(listEl.children[2].getAttribute('aria-selected')).toEqual('false');
  }));

  it('should init with correct value on single mode ', fakeAsync(() => {
    // GIVEN
    testHostComponent.selectionMode = 'single';
    testHostComponent.value = 1;
    fixture.detectChanges();

    // WHEN
    initAndOpenDropdown();

    const listEl = fixture.nativeElement.querySelector('#test-listbox');

    // THEN
    expect(testHostComponent.multiselectComponent.selectedOptions).toEqual([{ label: 'Option 1', value: 1 }]);
    expect(listEl.children[0].getAttribute('aria-selected')).toEqual('true');
  }));

  it('should init with correct value on checkbox mode when values are objects', fakeAsync(() => {
    // GIVEN
    testHostComponent.options = [
      { label: 'Option 1', value: { id: 1, libelle: 'Test 1' } },
      { label: 'Option 2', value: { id: 2, libelle: 'Test 2 ' } },
      { label: 'Option 3', value: { id: 3, libelle: 'Test 3' } },
    ];

    testHostComponent.value = [testHostComponent.options[0].value];
    fixture.detectChanges();

    // WHEN
    initAndOpenDropdown();

    const listEl = fixture.nativeElement.querySelector('#test-listbox');

    // THEN
    expect(testHostComponent.multiselectComponent.selectedOptions).toEqual([
      { label: 'Option 1', value: { id: 1, libelle: 'Test 1' } },
    ]);
    expect(listEl.children[0].getAttribute('aria-selected')).toEqual('true');
  }));

  it('should init with correct value on single mode when values are objects', fakeAsync(() => {
    // GIVEN
    testHostComponent.selectionMode = 'single';
    testHostComponent.options = [
      { label: 'Option 1', value: { id: 1, libelle: 'Test 1' } },
      { label: 'Option 2', value: { id: 2, libelle: 'Test 2 ' } },
      { label: 'Option 3', value: { id: 3, libelle: 'Test 3' } },
    ];

    testHostComponent.value = testHostComponent.options[0].value;
    fixture.detectChanges();

    // WHEN
    initAndOpenDropdown();

    const listEl = fixture.nativeElement.querySelector('#test-listbox');

    // THEN
    expect(testHostComponent.multiselectComponent.selectedOptions).toEqual([
      { label: 'Option 1', value: { id: 1, libelle: 'Test 1' } },
    ]);
    expect(listEl.children[0].getAttribute('aria-selected')).toEqual('true');
  }));
});
