import { CommonModule } from '@angular/common';
import { Component, Input, OnDestroy, ViewEncapsulation } from '@angular/core';
import { DsfrI18nPipe, DsfrI18nService, Language, newUniqueId } from '@edugouvfr/ngx-dsfr';
import { Subscription } from 'rxjs';
import { MESSAGES_EN } from './i18n/messages.en';
import { MESSAGES_FR } from './i18n/messages.fr';

@Component({
  selector: 'dsfr-ext-progressbar, dsfrx-progressbar',
  templateUrl: './progressbar.component.html',
  styleUrls: ['./progressbar.component.scss'],
  encapsulation: ViewEncapsulation.None,
  standalone: true,
  imports: [CommonModule, DsfrI18nPipe],
})
export class DsfrProgressBarComponent implements OnDestroy {
  /**
   * Label de la barre de progression.
   */
  @Input({ required: true }) label: string;

  /**
   * Affiche ou masque la valeur de la barre de progression, vrai par défaut.
   */
  @Input() showValue = true;

  /**
   * Affiche ou masque le label de la barre de progression, vrai par défaut.
   */
  @Input() showLabel = true;

  /**
   * Determine la valeur minimale et la valeur maximale de la valeur représentée dans la progress bar.
   */
  @Input() range: DsfrProgressBarRange = { min: 0, max: 100 };

  /**
   * Signe d'unité ajouté à la valeur, '%' par défaut.
   */
  @Input() unit = '%';

  /**
   * Stoppe l'animation de la barre si la valeur de cette dernière est indéterminée.
   */
  @Input() stopIndeterminateAnimation = false;

  /**
   * 👓 Message permettant de contextualiser le processus décrit par la barre de progression en mode "indéterminé".
   *
   * Valeur par défaut : "Chargement de l'agenda en cours. Veuillez patienter."
   */
  @Input() indeterminateContextMessage: string | undefined;

  /**
   * Id du conteneur
   * @internal */
  protected id: string = newUniqueId();

  /** Id du label
   * @internal
   */
  protected labelId: string = newUniqueId();

  protected barLength: number = 0;

  protected valueText: string | null = null;

  private _value: number | undefined;
  private subscription = new Subscription();

  constructor(protected i18n: DsfrI18nService) {
    this.i18n.extendsLabelsBundle(Language.EN, MESSAGES_EN);
    this.i18n.extendsLabelsBundle(Language.FR, MESSAGES_FR);
  }

  get indeterminate(): boolean {
    return this.value === undefined;
  }

  get value(): number | undefined {
    return this._value;
  }

  /**
   * Valeur actuelle de la progression. La valeur `null` représente une valeur indéterminée.
   */
  @Input() set value(value: number | undefined) {
    if (typeof value !== 'number') {
      this._value = undefined;
    } else {
      this._value = Math.min(Math.max(this.range.min, value), this.range.max);
    }

    this.computeBarLength();
    this.computeValueText();
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

  private computeBarLength(): void {
    if (this.value !== undefined) {
      const fullBarLength = this.range.max - this.range.min;
      this.barLength = Math.round((this.value / fullBarLength) * 100);
    } else {
      this.barLength = 25;
    }
  }

  private computeValueText(): void {
    this.valueText = this.value !== null ? `${this.value} ${this.unit}` : null;
  }
}

export interface DsfrProgressBarRange {
  min: number;
  max: number;
}
