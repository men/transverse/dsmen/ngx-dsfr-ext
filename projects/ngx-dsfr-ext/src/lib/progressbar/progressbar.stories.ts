import { controlDisabled, dsfrDecorators } from '.storybook/storybook-utils';
import { Meta, StoryObj, moduleMetadata } from '@storybook/angular';
import { DsfrProgressBarComponent } from './progressbar.component';

const meta: Meta<DsfrProgressBarComponent> = {
  title: 'Components/ProgressBar',
  component: DsfrProgressBarComponent,
  decorators: [moduleMetadata({ imports: [] })],
  parameters: {
    docs: {
      toc: {
        disable: true, // Disables the table of contents
      },
    },
  },
};
export default meta;
type Story = StoryObj<DsfrProgressBarComponent>;

export const Default: Story = {
  decorators: dsfrDecorators('Barre de progression par défaut'),
  args: {
    label: 'Libellé barre de progression',
    value: 25,
    unit: '%',
    showValue: true,
  },
};

export const Indeterminate: Story = {
  decorators: dsfrDecorators('Barre de progression, valeur indéterminée'),
  args: {
    ...Default.args,
    value: undefined,
    stopIndeterminateAnimation: false,
  },
  argTypes: {
    showValue: controlDisabled,
  },
};

export const Jauge: Story = {
  decorators: dsfrDecorators('Barre de progression, unité métier'),
  args: {
    ...Default.args,
    range: { min: 0, max: 30 },
    value: 17,
    unit: 'élèves',
    showLabel: true,
    label: `Nombre d'élèves assignés (max: 30 élèves)`,
  },
  argTypes: {
    showValue: controlDisabled,
  },
};
