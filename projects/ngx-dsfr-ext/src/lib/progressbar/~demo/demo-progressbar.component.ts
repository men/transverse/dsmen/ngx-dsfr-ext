import { AfterViewInit, Component, Input, ViewEncapsulation } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { DemoToolbarComponent } from '../../shared/demo/demo-toolbar.component';
import { DsfrProgressBarComponent } from '../progressbar.component';

@Component({
  selector: 'demo-progressbar',
  templateUrl: './demo-progressbar.component.html',
  styleUrls: ['./demo-progressbar.component.scss'],
  standalone: true,
  encapsulation: ViewEncapsulation.None,
  imports: [FormsModule, DsfrProgressBarComponent, DemoToolbarComponent],
})
export class DemoProgressbarComponent implements AfterViewInit {
  @Input() value: number;

  /** @internal */
  ngAfterViewInit() {
    this.doProgressValue();
  }

  private doProgressValue() {
    const intervalId = setInterval(() => {
      this.value++;
      if (this.value === 100) clearInterval(intervalId);
    }, 1000);
  }
}
