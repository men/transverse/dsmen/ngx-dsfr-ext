import { Meta, moduleMetadata, StoryObj } from '@storybook/angular';
import { DemoProgressbarComponent } from './demo-progressbar.component';

const meta: Meta<DemoProgressbarComponent> = {
  title: 'Components/ProgressBar',
  component: DemoProgressbarComponent,
  decorators: [moduleMetadata({ imports: [] })],
};
export default meta;
type Story = StoryObj<DemoProgressbarComponent>;

export const Custom: Story = {
  args: {
    value: 10,
  },
};
