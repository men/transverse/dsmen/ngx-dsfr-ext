import { Component, SimpleChange, ViewChild } from '@angular/core';
import { DsfrNode, DsfrTreeComponent } from '../../public-api';
import { ComponentFixture, fakeAsync, TestBed, tick } from '@angular/core/testing';
import { FormsModule } from '@angular/forms';

@Component({
  selector: `edu-host-component`,
  template: `<dsfr-ext-tree [value]="files" [selectionMode]="selectionMode"></dsfr-ext-tree>`,
})
class TestHostComponent {
  @ViewChild(DsfrTreeComponent)
  public treeComponent: DsfrTreeComponent;

  files: DsfrNode[] = [];
  selectionMode = undefined;
}

describe('DsfrTreeselectComponent', () => {
  let testHostComponent: TestHostComponent;
  let fixture: ComponentFixture<TestHostComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [FormsModule, DsfrTreeComponent],
      declarations: [TestHostComponent],
    }).compileComponents();

    fixture = TestBed.createComponent(TestHostComponent);
    testHostComponent = fixture.componentInstance;
  });

  describe('has a getTreeState method that', () => {
    beforeEach(() => {
      testHostComponent.files = [
        {
          label: 'Documents',
          children: [
            {
              label: 'Work',
              children: [
                {
                  label: 'Expenses.doc',
                  children: [{ label: 'Insurance.doc' }, { label: 'Debt.doc' }],
                },
                { label: 'Resume.doc' },
              ],
            },
            { label: 'Home', children: [{ label: 'Invoices.txt' }] },
          ],
        },
        {
          label: 'Events',
          children: [{ label: 'Meeting' }, { label: 'Product Launch' }, { label: 'Report Review' }],
        },
      ];

      fixture.detectChanges();
    });

    it('returns identified nodes if the model did not provide them', fakeAsync(() => {
      const { treeComponent, files } = testHostComponent;

      treeComponent.ngAfterViewInit();

      fixture.detectChanges();

      expect(files[0].id).not.toBeDefined();
      expect(files[1].id).not.toBeDefined();

      const treeStateBefore = treeComponent.getTreeState();
      expect(treeStateBefore[0].id).toBeDefined();
      expect(treeStateBefore[1].id).toBeDefined();
    }));

    it('returns the current expansion model of the Tree', fakeAsync(() => {
      const { treeComponent } = testHostComponent;

      treeComponent.ngAfterViewInit();

      fixture.detectChanges();

      const arrowIcons = fixture.nativeElement.querySelectorAll('.fr-icon-arrow-right-s-line');

      const treeStateBefore = treeComponent.getTreeState();
      expect(treeStateBefore[0].expanded).toBe(undefined);
      expect(treeStateBefore[1].expanded).toBe(undefined);

      arrowIcons[0].click();
      tick(100);
      fixture.detectChanges();

      const treeStateAfter = treeComponent.getTreeState();
      expect(treeStateAfter[0].expanded).toBe(true);
      expect(treeStateAfter[1].expanded).toBe(undefined);

      arrowIcons[0].click();
      arrowIcons[1].click();
      tick(100);
      fixture.detectChanges();

      const treeStateAfter2 = treeComponent.getTreeState();
      expect(treeStateAfter2[0].expanded).toBe(false);
      expect(treeStateAfter2[1].expanded).toBe(true);
    }));

    it('returns the current selection model of the Tree', fakeAsync(() => {
      const { treeComponent } = testHostComponent;

      treeComponent.selectionMode = 'multiple';
      treeComponent.ngAfterViewInit();

      fixture.detectChanges();

      const labels = fixture.nativeElement.querySelectorAll('.tree-node-label .fr-label');

      const treeStateBefore = treeComponent.getTreeState();
      expect(treeStateBefore[0].selected).toBe(undefined);
      expect(treeStateBefore[1].selected).toBe(undefined);

      labels[0].click();
      tick(100);
      fixture.detectChanges();

      const treeStateAfter = treeComponent.getTreeState();
      expect(treeStateAfter[0].selected).toBe(true);
      expect(treeStateAfter[1].selected).toBe(undefined);

      labels[0].click();
      labels[1].click();
      tick(100);
      fixture.detectChanges();

      const treeStateAfter2 = treeComponent.getTreeState();
      expect(treeStateAfter2[0].selected).toBe(false);
      expect(treeStateAfter2[1].selected).toBe(true);
    }));
  });

  describe('initializes properly', () => {
    const INITIAL_VALUE = [
      {
        label: 'Documents',
        expanded: true,
        selected: false,
        children: [
          {
            label: 'Work',
            expanded: true,
            selected: false,
            children: [
              {
                label: 'Expenses.doc',
                expanded: true,
                selected: true,
                children: [
                  { label: 'Insurance.doc', selected: true },
                  { label: 'Debt.doc', selected: true },
                ],
              },
              { label: 'Resume.doc', selected: false },
            ],
          },
          { label: 'Home', expanded: true, selected: false, children: [{ label: 'Invoices.txt' }] },
        ],
      },
      {
        label: 'Events',
        expanded: true,
        selected: false,
        children: [{ label: 'Meeting' }, { label: 'Product Launch' }, { label: 'Report Review' }],
      },
    ];

    it('in single mode, by only keeping the first detected selected node', () => {
      fixture.detectChanges();
      const { treeComponent } = testHostComponent;

      treeComponent.selectionMode = 'single';
      testHostComponent.files = INITIAL_VALUE;
      treeComponent['cd'].detectChanges();

      treeComponent.ngAfterViewInit();
      fixture.detectChanges();

      const treeNodes = fixture.nativeElement.querySelectorAll('.tree-node-content');
      const documentsNode = treeNodes[0];
      const workNode = treeNodes[1];
      const expensesNode = treeNodes[2];
      const insuranceNode = treeNodes[3];
      const debtNode = treeNodes[4];
      const resumeNode = treeNodes[5];

      expect(documentsNode.getAttribute('aria-selected')).toBe('false');
      expect(workNode.getAttribute('aria-selected')).toBe('false');
      expect(expensesNode.getAttribute('aria-selected')).toBe('false');
      expect(insuranceNode.getAttribute('aria-selected')).toBe('true');
      expect(debtNode.getAttribute('aria-selected')).toBe('false');
      expect(resumeNode.getAttribute('aria-selected')).toBe('false');
    });

    it('in multiple mode, by keeping the selection model as-is', () => {
      fixture.detectChanges();
      const { treeComponent } = testHostComponent;

      treeComponent.selectionMode = 'multiple';
      testHostComponent.files = INITIAL_VALUE;
      treeComponent['cd'].detectChanges();

      treeComponent.ngAfterViewInit();
      fixture.detectChanges();

      const treeNodes = fixture.nativeElement.querySelectorAll('.tree-node-content');
      const documentsNode = treeNodes[0];
      const workNode = treeNodes[1];
      const expensesNode = treeNodes[2];
      const insuranceNode = treeNodes[3];
      const debtNode = treeNodes[4];
      const resumeNode = treeNodes[5];

      expect(documentsNode.getAttribute('aria-selected')).toBe('false');
      expect(workNode.getAttribute('aria-selected')).toBe('false');
      expect(expensesNode.getAttribute('aria-selected')).toBe('true');
      expect(insuranceNode.getAttribute('aria-selected')).toBe('true');
      expect(debtNode.getAttribute('aria-selected')).toBe('true');
      expect(resumeNode.getAttribute('aria-selected')).toBe('false');
    });

    it('in checkbox mode, by only keeping the selection model for its leaves', () => {
      fixture.detectChanges();
      const { treeComponent } = testHostComponent;

      treeComponent.selectionMode = 'checkbox';
      testHostComponent.files = INITIAL_VALUE;
      treeComponent['cd'].detectChanges();

      treeComponent.ngAfterViewInit();
      fixture.detectChanges();

      const treeNodes = fixture.nativeElement.querySelectorAll('.tree-node-content');
      const documentsNode = treeNodes[0];
      const workNode = treeNodes[1];
      const expensesNode = treeNodes[2];
      const insuranceNode = treeNodes[3];
      const debtNode = treeNodes[4];
      const resumeNode = treeNodes[5];

      expect(documentsNode.getAttribute('aria-selected')).toBe(null);
      expect(workNode.getAttribute('aria-selected')).toBe(null);
      expect(expensesNode.getAttribute('aria-selected')).toBe('true');
      expect(insuranceNode.getAttribute('aria-selected')).toBe('true');
      expect(debtNode.getAttribute('aria-selected')).toBe('true');
      expect(resumeNode.getAttribute('aria-selected')).toBe('false');
    });
  });
});
