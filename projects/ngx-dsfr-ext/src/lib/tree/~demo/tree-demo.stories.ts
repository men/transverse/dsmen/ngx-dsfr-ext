import { Meta, moduleMetadata, StoryObj } from '@storybook/angular';
import { DsfrNode } from '../../shared/utils/tree/tree.model';
import { DemoTreeComponent } from './demo-tree.component';

const meta: Meta<DemoTreeComponent> = {
  title: 'Components/Tree',
  component: DemoTreeComponent,
  decorators: [moduleMetadata({ imports: [] })],
  argTypes: {
    selectionMode: { control: { type: 'inline-radio' }, options: ['single', 'multiple', 'checkbox', undefined] },
  },
};
export default meta;
type Story = StoryObj<DemoTreeComponent>;

const exams: DsfrNode[] = [
  {
    label: 'Examens Nationaux',
    id: '1',
    data: {},
    children: [
      {
        label: 'Baccalauréat',
        id: '2',
        data: {},
        children: [
          {
            label: 'Général',
            id: '3',
            icon: 'fr-icon-calendar-line',
            data: { badge: { label: 'Essentiel', color: 'fr-badge--blue-ecume' } },
          },
          {
            label: 'Technologique',
            id: '4',
            data: { badge: { label: 'Essentiel', color: 'fr-badge--blue-ecume' } },
          },
          {
            label: 'Professionnel',
            id: '5',
            data: { badge: { label: 'Essentiel', color: 'fr-badge--blue-ecume' } },
          },
        ],
      },
      {
        label: 'Diplôme National du Brevet',
        id: '6',
        data: {},
        children: [
          {
            label: 'Série Générale',
            id: '7',
            data: { badge: { label: 'Essentiel', color: 'fr-badge--blue-ecume' } },
          },
          {
            label: 'Série Professionnelle',
            id: '8',
            data: { badge: { label: 'Spécialisé', color: 'fr-badge--green-menthe' } },
          },
        ],
      },
    ],
  },
  {
    label: 'Diplômes Techniques et Artistiques',
    id: '9',
    data: {},
    expanded: true,
    children: [
      {
        label: 'Brevet de Technicien (BT)',
        id: '10',
        data: {},
        children: [
          {
            label: 'BT Maintenance Industrielle',
            id: '11',
            data: { badge: { label: 'Spécialisé', color: 'fr-badge--green-menthe' } },
          },
          {
            label: 'BT Conception Mécanique',
            id: '12',
            data: { badge: { label: 'Spécialisé', color: 'fr-badge--green-menthe' } },
          },
        ],
      },
      {
        label: 'Brevet de Métiers d’Art (BMA)',
        id: '13',
        expanded: true,
        data: {},
        children: [
          {
            label: 'BMA Ébénisterie',
            id: '14',
            expanded: true,
            data: { badge: { label: 'Artisanat', color: 'fr-badge--brown-caramel' } },
          },
          {
            label: 'BMA Céramique',
            id: '15',
            data: { badge: { label: 'Artisanat', color: 'fr-badge--brown-caramel' } },
          },
        ],
      },
    ],
  },
  {
    label: 'Certifications Professionnelles',
    id: '16',
    icon: 'fr-icon-briefcase-line',
    data: {},
    children: [
      {
        label: 'CAP (Certificat d’Aptitude Professionnelle)',
        id: '17',
        data: {},
        children: [
          {
            label: 'CAP Cuisine',
            id: '18',
            data: { badge: { label: 'Spécialisé', color: 'fr-badge--green-menthe' } },
          },
          {
            label: 'CAP Électricité',
            id: '19',
            data: { badge: { label: 'Spécialisé', color: 'fr-badge--green-menthe' } },
          },
        ],
      },
      {
        label: 'BEP (Brevet d’Études Professionnelles)',
        id: '20',
        data: {},
        children: [
          {
            label: 'BEP Métiers de la Vente',
            id: '21',
            data: { badge: { label: 'Spécialisé', color: 'fr-badge--green-menthe' } },
          },
          {
            label: 'BEP Métiers de l’Hôtellerie',
            id: '22',
            data: { badge: { label: 'Spécialisé', color: 'fr-badge--green-menthe' } },
          },
        ],
      },
    ],
  },
];

export const Slot: Story = {
  args: {
    selectionMode: 'multiple',
    nodes: exams,
  },
};

export const SlotCheckbox: Story = {
  args: {
    selectionMode: 'checkbox',
    nodes: exams,
  },
};
