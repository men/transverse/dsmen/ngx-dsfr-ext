import { CommonModule } from '@angular/common';
import { Component, Input, ViewChild, ViewEncapsulation } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { DsfrBadgeModule, DsfrButtonModule, DsfrButtonsGroupModule } from '@edugouvfr/ngx-dsfr';
import { DemoToolbarComponent } from '../../shared/demo/demo-toolbar.component';
import { DsfrNode, DsfrTree } from '../../shared/utils/tree/tree.model';
import { DsfrTreeComponent } from '../tree.component';

// https://primeng.org/tree
@Component({
  selector: 'demo-tree',
  templateUrl: './demo-tree.component.html',
  encapsulation: ViewEncapsulation.None,
  styles: [
    `
      .my-tree .dsfrx-tree-component .tree-node-label {
        display: flex;
        align-items: center;
        gap: 0.5rem;
      }
    `,
  ],
  standalone: true,
  imports: [
    CommonModule,
    FormsModule,
    DemoToolbarComponent,
    DsfrTreeComponent,
    DsfrButtonsGroupModule,
    DsfrButtonModule,
    DsfrBadgeModule,
  ],
})
export class DemoTreeComponent {
  /** @internal */
  @Input() nodes: DsfrNode[];
  /** @internal */
  @Input() selectionMode: any;
  /** @internal */
  @ViewChild('tree')
  private treeComponent: DsfrTree;

  /** @internal */
  protected collapseAll: boolean;

  /** @internal */
  protected expandAll: boolean;

  protected hideBtbLabel: string;

  private number = 1;

  private labelHideNode = 'Masquer <Baccalauréat>';

  private labelShowNode = 'Afficher <Baccalauréat>';

  constructor() {
    this.hideBtbLabel = this.labelHideNode;
  }

  /** @internal */
  onCollapseAll() {
    this.treeComponent.collapseAll();
  }

  /** @internal */
  onExpandAll() {
    this.treeComponent.expandAll();
  }

  /** @internal */
  onSelectAll(select: boolean) {
    this.treeComponent.selectAll(select);
  }

  /** @internal */
  updateNodes() {
    this.nodes[2].label = 'Mise à jour du parent : ' + this.number;
    if (this.nodes[2].children && this.nodes[2].children[1] && this.nodes[2].children[1].children) {
      this.nodes[2].children[1].children[0].data = { badge: { label: 'Update' } };
      this.nodes[2].children[1].children.push({
        label: 'Nouvel Ajout',
        id: '52' + this.number,
        data: { badge: { label: 'Spécialisé', color: 'fr-badge--green-menthe' } },
        children: [
          {
            label: 'Ajout enfant 1',
            id: '53',
            data: { badge: { label: 'Spécialisé', color: 'fr-badge--green-menthe' } },
          },
          {
            label: 'Ajout enfant 2',
            id: '54',
            data: { badge: { label: 'Spécialisé', color: 'fr-badge--green-menthe' } },
          },
        ],
      });
    }
    this.treeComponent.updateContent(); // équivalent àthis.nodes = [...this.nodes];

    this.number++;
  }

  /** @internal */
  resetTree() {
    this.treeComponent.reinitializeState();
  }

  /** @internal */
  hideNode() {
    const nodes = this.nodes[0].children;
    if (nodes) {
      const nodeToHide = nodes[0];
      nodeToHide.hidden = !Boolean(nodeToHide.hidden);
      this.hideBtbLabel = nodeToHide.hidden ? this.labelShowNode : this.labelHideNode;
      this.treeComponent.updateContent();
    }
  }
}
