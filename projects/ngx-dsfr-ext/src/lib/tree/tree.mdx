import { Canvas, Controls, Meta } from '@storybook/blocks';
import * as Stories from './tree.stories';

<Meta of={Stories} title="Tree" />

# Tree

Le composant `Tree` permet d'organiser des données sous forme hiérarchique. Les nœuds de l'arbre peuvent être
sélectionnables avec la propriété `selectionMode`.

- _Module_ : _standalone_
- _Composant_ : `DsfrTreeComponent`
- _Tags_ : `dsfr-ext-tree, dsfrx-tree`

<h2 id="preview"> Aperçu </h2>

Les données sont décrites par la propriété `value: DsfrNode[]`.

<Canvas of={Stories.Default} />

<h2 id="api"> API </h2>

<Controls />

<h3 id="methodes"> Méthodes </h3>

Des méthodes sont disponibles pour effectuer des actions de manière programmatique sur le composant depuis l'interface `DsfrTree`.

<table class="interface-table">
  <caption class="interface-caption">DsfrTree </caption>
  <thead>
    <tr>
      <th> Nom </th>
      <th> Paramètre </th>
      <th> Description </th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td> updateContent() </td>
      <td> </td>
      <td class="css-o1d7ko"> Mise a jour du contenu des nœuds de l'arbre. Conserve l'état si l'id est renseigné.</td>
    </tr>
    <tr>
      <td> expandAll() </td>
      <td> </td>
      <td class="css-o1d7ko"> Ouvre tous les nœuds de l'arborescence</td>
    </tr>
    <tr>
      <td> collapseAll() </td>
      <td> </td>
      <td class="css-o1d7ko"> Ferme tous les nœuds de l'arborescence.</td>
    </tr>
    <tr>
      <td> selectAll() </td>
      <td> selected: boolean </td>
      <td class="css-o1d7ko"> Sélectionne / désélectionne tous les nœuds en mode multiple ou checkbox</td>
    </tr>
    <tr>
      <td> reinitializeState() </td>
      <td> </td>
      <td class="css-o1d7ko"> Réinitialise l'état de l'arbre (nœuds étendus et sélectionnés)</td>
    </tr>
  </tbody>
</table>

Exemple d'utilisation:

```typescript
  @ViewChild('tree') private tree: DsfrTree;

  onCollapseAll() {
    this.treeComponent.collapseAll();
  }

  onExpandAll() {
    this.treeComponent.expandAll();
  }
```

Cf. Story [Slot et boutons d'actions](/story/components-tree--slot)

<h4> Tout plier / replier </h4>

Un nœud de l'arbre peut posséder un sous-ensemble de nœud défini par la propriété `children`. Cette sous-arborescence n'est
initialement affichée que si la propriété `expanded` du nœud est `true`.

Pour ouvrir / fermer tous les nœuds, vous pouvez soit modifier les propriétés `expanded` de chaque nœud, soit appeler les
méthodes `expandAll()` / `collapseAll` du composant.

<h4> Masquer des noeuds </h4>

Il est possible de masquer des noeuds sans avoir à les supprimer complètement de votre structure de données en utilisant
la proprieté `DsfrNode#hidden=true`.

🔥 Attention, veuillez noter que nous utilisons alors le principe ergonomique suivant :

> L'utilisateur final ne modifie pas ce qu'il ne voit pas

Cela signifie que si l'arbre est en mode _checkbox_, la sélection d'un noeud parent n'affectera que ses noeuds enfants
_visibles_. L'état des sous-noeuds qui auraient été masqués resterait alors inchangé.

Cette fonctionalité peut vous permettre d'implémenter facilement une fonction de filtrage sur votre arbre.

Veuillez noter que ce comportement peut induire des états visuels incohérents en mode _checkbox_. Par exemple :

- L'utilisateur coche un noeud parent dont un des noeuds enfants est décoché et masqué
  - A l'issue de la sélection du noeud parent l'état de le _checkbox_ reste "mixte".

🔥 Attention à bien accompagner vos utilisateurs si vous combinez filtrage de noeuds et sélection multiple.

<h2 id="dsfr-node"> Interface DsfrNode </h2>

```typescript
/**
 * Description d'un item, nœud, d'un arbre.
 */
export interface DsfrNode {
  /**
   * Identifiant unique de l'élément. Obligatoire pour la mise à jour du contenu de l'arbre avec updateContent
   */
  id?: string;

  /** Ouvert ou non à l'initialisation, `false` par défaut  */
  expanded?: boolean;

  /** Icône du nœud de l'arbre, préfix `fr-icon-` */
  icon?: string;

  /** Libellé du nœud de l'arbre, obligatoire. */
  label: string;

  /** Sélection du nœud désactivée */
  disabled?: boolean;

  /** Nœud sélectionné à l'initialisation, `false` par défaut. */
  selected?: boolean;

  /** Indique si le noeud doit être masqué, utilisé pour filtrer le contenu de l'arbre. */
  hidden?: boolean;

  /** Données associées, à disposition de l'utilisateur. */
  data?: any;

  /** Arborescences imbriquées, `undefined` par défaut. */
  children?: DsfrNode[];
}
```

<h2 id="utilisation"> Utilisation </h2>

<h3 id="maj-data"> Mise à jour des données </h3>

Les données des nœuds de l'arbre (le label par exemple) ne seront pas mises à jour automatiquement après l'initialisation.
Ceci permet de limiter les cycles de rendus et donc d'améliorer les performances. Les nœuds passés en entrée sont également immutables.
Pour mettre à jour le contenu de l'arbre :

- on peut utiliser la fonction `updateContent` depuis le composant
- Ou metre à jour la référence ` this.nodes = [...this.nodes];`

L'état de l'abre, c'est à dire les nœuds pliés/repliés et sélectionnés, sera conservé.

🔥 Les noeuds dont l'attribut unique id n'est pas renseigné ne conserveront pas leur état après une mise à jour.

Si on souhaite réinitialiser entièrement l'arbre depuis l'état des nœuds passés en entrée, on peut utiliser la méthode `reinitializeState()`.

<h3 id="navigation"> Navigation clavier</h3>

Le composant Tree implémente le pattern de navigation Treeview défini ici [WAI Treeview](https://www.w3.org/WAI/ARIA/apg/patterns/treeview/).

Le premier nœud est sélectionné quand le composant reçoit le focus.

Actions clavier :

- `Flèche droite` : ouvre le nœud courant s'il y a lieu.
- `Flèche gauche` : ferme le nœud courant s'il y a lieu.
- `Flèche vers le bas` : déplace le focus vers le nœud suivant sans ouvrir ni fermer de nœud.
- `Flèche vers le haut` : déplace le focus vers le nœud précédent sans ouvrir ni fermer de nœud.
- `Home` (Début) : déplace le focus sur le premier nœud de l'arborescence sans ouvrir ni fermer de nœud.
- `End` (Fin) : déplace le focus vers le dernier nœud de l’arborescence qui peut être focalisé sans ouvrir de nœud.
- `Entrée`, `Espace` : sélectionne / désélectionne le nœud si les éléments peuvent être sélectionnés.
- `Tab` : le composant perd le focus au profit de l'élément suivant dans le navigateur.

<h2 id="selection">Sélection</h2>

Le composant possède plusieurs modes de sélection : `single`, `multiple` ou `checkbox`. Ce choix est statique et ne peux être modifié après initialisation du composant.
Pour toute modification ultérieur, veillez à détruire le précédent composant et à en créer un nouveau.

Par défaut, les nœuds ne sont pas sélectionnables, `selectionMode=undefined`.

<h3 id="selection-single"> Sélection simple </h3>

`selectionMode='single'`

- Seul un nœud (feuille ou parent) peut être sélectionné / désélectionné.
- La sélection d'un nœud désélectionne le nœud précédemment sélectionné.
- L'événement `nodeSelect` est émis pour le nœud lui-même et le nœud précédemment sélectionné s'il y a lieu.
- La valeur d'un événement est la valeur du nœud sans la propriété `children`, exemple :

```
nodeChange: {label: "Movies", icon: "fr-icon-star-line", expanded: false, selected: true, inputId: "cb0ef74792-75f8-48ce-98e0-560601439e4d"}
```

<Canvas of={Stories.Single} />

🔥 Dans ce mode de sélection, il ne doit y avoir, initialement qu'un seul nœud sélectionné au plus.

<h3 id="selection-multiple"> Sélection multiple </h3>

`selectionMode='multiple'`

- Chaque nœud (feuille ou parent) peut être sélectionné / désélectionné individuellement.
- Un événement `nodeSelect` est émis avec pour valeur le nœud lui-même sans la propriété `children`.
- Un événement `selectionChange` est émis avec le nœud sélectionné / désélectionné et la liste des nœuds couramment sélectionnés.

🔥 La sélection d'un noeud parent n'entraine pas la sélection automatique de ses descendants.

<Canvas of={Stories.Multiple} />

📌 La méthode `selectAll(...)` du composant permet de sélectionner / désélectionner tous les nœuds en mode `multiple`
ou `checkbox`.

<h3 id="selection-checkbox"> Sélection checkbox </h3>

`selectionMode='checkbox'`

- Une case à cocher précède le label de chaque nœud.
- Chaque nœud peut être sélectionné / désélectionné.
- La sélection / désélection d'un nœud met à jour la sélection des enfants et propage la sélection aux parents.
- Une case à cocher peut avoir un statut indéterminé (mixed), `selected=undefined`, si ses enfants n'ont pas le même statut.
- L'événement `nodeSelect` est émis avec pour valeur le nœud lui-même sans la propriété `children`.
- Un événement `selectionChange` est émis avec le nœud sélectionné / désélectionné et la liste des nœuds couramment sélectionnés.

<Canvas of={Stories.Checkbox} />

<h2 id="slot"> Slot </h2>

Le slot `nodeTemplate` permet de personnaliser l'affichage du label de chaque nœud.
Si le composant Tree est en mode `checkbox`, le template sera appliqué après le label. Dans les autres modes il remplacera entièrement le label.

Cf. Story [Slot](/story/components-tree--slot)

_Exemple d'utilisation :_

```html
<dsfr-ext-tree #tree [value]="nodes" selectionMode="multiple">
  <ng-template #nodeTemplate let-node>
    <div *ngIf="node.children">
      <span>{{ node.label }}&nbsp;</span>
    </div>
    <div *ngIf="!node.children">
      <!-- Noeud feuille -->
      <span>{{ node.label }}&nbsp;</span>
      <dsfr-badge
        *ngIf="node.data?.badge"
        size="SM"
        [label]="node.data.badge?.label"
        [customClass]="node.data.badge.color">
      </dsfr-badge>
    </div>
  </ng-template>
</dsfr-ext-tree>
```

🔥 👓 Il est de votre responsabilité de vous assurer que le contenu du slot projeté respecte les règles d'accessibilité.
Notamment dans le cas du Tree, le pattern appliqué est celui du `role=tree`, ce qui implique des règles particulières au niveau de
la navigation et des éléments disponibles. cf. [WAI Treeview](https://www.w3.org/WAI/ARIA/apg/patterns/treeview/)
