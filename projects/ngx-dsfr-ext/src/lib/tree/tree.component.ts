import { CommonModule } from '@angular/common';
import {
  AfterViewInit,
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  ContentChild,
  ElementRef,
  EventEmitter,
  Input,
  OnChanges,
  Output,
  SimpleChanges,
  TemplateRef,
  ViewEncapsulation,
  inject,
} from '@angular/core';
import { FormsModule } from '@angular/forms';
import { EduCheckboxComponent } from '../shared/components/checkbox/checkbox.component';
import { DsfrSelectionMode } from '../shared/models/selection-mode.type';
import { TreeFocusManager, TreeFocusable } from '../shared/utils/tree/tree-focus-manager';
import { TreeSelectManager, TreeSelectable } from '../shared/utils/tree/tree-select-manager';
import { DsfrNode, DsfrViewNode } from '../shared/utils/tree/tree.model';

/** Événement émis lors de la sélection d'un nœud. */
export interface DsfrNodeEvent extends Omit<DsfrNode, 'children'> {}
export interface DsfrTreeNodeEvent {
  node: DsfrNodeEvent | null;
  nodesSelected: DsfrNode[];
}

@Component({
  selector: 'dsfr-ext-tree, dsfrx-tree',
  templateUrl: './tree.component.html',
  styleUrls: ['./tree.component.scss'],
  encapsulation: ViewEncapsulation.None,
  standalone: true,
  changeDetection: ChangeDetectionStrategy.OnPush,
  imports: [CommonModule, FormsModule, EduCheckboxComponent],
})
export class DsfrTreeComponent implements TreeFocusable, TreeSelectable, AfterViewInit, OnChanges {
  /** Un tableau de nœuds d'arbre */
  @Input() value: DsfrNode[] = [];

  /** Template pour personnaliser le contenu d'un noeud.
   * Le slot s'ajoute après le label en mode checkbox et le remplace dans les autres modes.  */
  @ContentChild('nodeTemplate', { static: true }) nodeTemplate: TemplateRef<any>;

  /** Indique le changement d'état d'un nœud. */
  @Output() nodeSelect = new EventEmitter<DsfrNodeEvent>();

  /** Indique un changement sur la sélection. Renvoie le noeud concerné et liste des noeuds (feuilles) sélectionnés */
  @Output() selectionChange = new EventEmitter<DsfrTreeNodeEvent>();

  /** Indique si toutes les options sont sélectionnées ou non. */
  /** @internal */
  isAllSelected: boolean; // Géré par TreeSelectManager

  protected isModeSelectable: boolean = false;

  private cd = inject(ChangeDetectorRef);

  private focusManager = new TreeFocusManager(this);
  private selectManager = new TreeSelectManager(this);
  private _viewNodes: DsfrViewNode[] = []; // noeuds internes avec état (selected, expand)

  constructor(private elementRef: ElementRef) {}

  get selectionMode(): DsfrSelectionMode {
    return this.selectManager.selectionMode;
  }

  /** Synonyme de value, mais plus explicite. */
  get nodes(): DsfrNode[] {
    return this._viewNodes;
  }

  /** Mode de sélection, `undefined` par défaut. */
  @Input() set selectionMode(value: DsfrSelectionMode) {
    this.selectManager.selectionMode = value;
    this.isModeSelectable = this.selectManager.isSelectable();
  }

  /**
   * Permet la récupération du modèle interne de l'arbre.
   *
   * @returns Un tableau de DsfrNode[].
   */
  public getTreeState(): DsfrNode[] {
    return this.nodes;
  }

  /**
   * Permet la récupération de la sélection courante de l'arbre.
   *
   * @returns Un tableau de DsfrNode[].
   */
  public getSelection(): DsfrNode[] {
    return this.selectManager.selectedNodes;
  }

  /** @internal */
  ngAfterViewInit() {
    // Mise à jour des tabindex
    this.focusManager.initTabindex();
  }

  /** @internal */
  ngOnChanges({ value }: SimpleChanges) {
    if (value) {
      if (this._viewNodes.length === 0) {
        // première initialisation
        this._viewNodes = this.selectManager.init(this.value);
      } else {
        // mise à jour avec conservation des états d'expansion et de sélection (si noeuds fournis avec identifiants)
        this._viewNodes = this.selectManager.updateNodesContent(this.value, this._viewNodes, 1);
      }
    }
  }

  /** @internal */
  getCheckboxId(node: DsfrNode): string {
    return 'cb' + node.id;
  }

  /** @internal */
  isCheckboxMode(): boolean {
    return this.selectManager.isCheckboxMode();
  }

  /** @internal */
  isSelectable(): boolean {
    return this.selectManager.isSelectable();
  }

  /**
   * Ouvre tous les nœuds de l'arborescence.
   */
  /** @internal */
  onNodeButton(node: DsfrNode) {
    this.focusManager.doToggleExpand(node);
  }

  /**
   * Sélection du label d'un nœud lorsque que l'on a un label sans checkbox. Si le mode est `selectable` :
   * - on inverse la sélection, uniquement du nœud, on émet un événement.
   */
  /** @internal */
  onNodeSelect(node: DsfrNode, event: Event) {
    // const targetElt = <HTMLElement>event.target;
    // ne pas activer si la target est le bouton expand/collapse
    //if (!targetElt.classList?.contains('tree-button')) {
    const nodeElt = <HTMLElement>event.currentTarget;
    this.selectManager.doSelectNode(node, nodeElt);
    //}
  }

  /**
   * Gestion des touches de navigation.
   * On transfère la gestion au composant qui gère la liste.
   */
  /** @internal */
  onNodeKeydown(node: DsfrNode, event: KeyboardEvent) {
    this.focusManager.doKeyEvent(node, event, !this.selectManager.isSingleMode());
  }

  // -- TreeFocusable --------------------------------------------------------------------------------------------------

  /** Ouvre tous les nœuds de l'arborescence. */
  /** @internal */
  expandAll() {
    this.focusManager.expandAll(this.nodes);
    this.cd.markForCheck();
  }

  /** Ferme tous les nœuds de l'arborescence.  */
  /** @internal */
  collapseAll() {
    this.focusManager.collapseAll(this.nodes);
    this.cd.markForCheck();
  }

  /** @internal */
  doNodeDefaultAction(node: DsfrNode, event: Event) {
    const nodeElt = <HTMLElement>event.currentTarget;
    this.selectManager.doSelectNode(node, nodeElt);
  }

  /**
   * Trouver tous les noeuds <li> visibles (possèdent une hauteur minimale).
   *
   * @internal
   */
  findVisibleNodesElt(): HTMLElement[] {
    const nativeElt = this.elementRef.nativeElement;
    const nodeList = nativeElt.querySelectorAll('.tree-node-content');
    const nodesArr: HTMLElement[] = [];

    nodeList.forEach((n: HTMLElement) => nodesArr.push(n));

    return nodesArr;
  }

  /** Retourne l'élément avec focus ou undefined. */
  /** @internal */
  findFocusElement(): HTMLElement | undefined {
    return this.elementRef.nativeElement.querySelector('.tree-node-content:focus') || undefined;
  }

  /**
   * Mise a jour du contenu de l'arbre. Conserve l'état (nœuds étendus et sélectionnés) si l'id des nœuds est renseigné.
   * @internal
   */
  updateContent(): void {
    this._viewNodes = this.selectManager.updateNodesContent(this.value, this._viewNodes, 1);
    this.cd.markForCheck();
  }

  /**
   * Réinitialise l'état de l'arbre (nœuds étendus et sélectionnés)
   * @internal
   */
  reinitializeState(): void {
    this._viewNodes = this.selectManager.init(this.value);
    this.cd.markForCheck();
  }

  // -- TreeSelectable -------------------------------------------------------------------------------------------------

  /** Sélectionne / désélectionne tous les nœuds en mode multiple ou checkbox.
   * @internal
   */
  selectAll(selected = true): void {
    this.selectManager.selectAllNodes(this.nodes, selected);
    this.cd.markForCheck();
    this.selectionChange.emit({ node: null, nodesSelected: this.selectManager.selectedNodes });
  }

  /** Emission d'un événement pour un nœud.
   * @internal
   */
  notifySelectEvent(node: DsfrNode): void {
    // la variable 'nodeContent' correspond à node sans les enfants
    const { children, ...nodeContent } = node;
    this.nodeSelect.emit(<DsfrNodeEvent>nodeContent);
    this.selectionChange.emit({
      node: nodeContent,
      nodesSelected: this.selectManager.selectedNodes.map((a) => a as DsfrNode),
    });
  }

  /**
   * Met à jour la propriété `indeterminate` de la checkbox `node.inputId` en fonction de la valeur de 'node.selected'.
   * @param node Nœud de l'arbre
   */

  /** @internal */
  updateIndeterminate(node: DsfrNode) {
    const nativeElt = this.elementRef.nativeElement;
    const checkboxId = this.getCheckboxId(node);
    const inputElt = <HTMLInputElement>nativeElt?.querySelector('#' + checkboxId);

    if (inputElt) inputElt.indeterminate = node.selected === undefined;
  }

  /** @internal */
  trackById(index: number, item: DsfrNode): string | undefined {
    return item.id;
  }
}
