import { CommonModule } from '@angular/common';
import { Component, ViewEncapsulation } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { DsfrBadgeModule, DsfrButtonModule, DsfrButtonsGroupModule } from '@edugouvfr/ngx-dsfr';
import { DsfrSelectionMode } from '../../../shared/models/selection-mode.type';
import { DsfrNode } from '../../../shared/utils/tree/tree.model';
import { DsfrTreeComponent } from '../../tree.component';

const INITAL_MODEL = [
  {
    id: '0',
    label: 'Examens Nationaux',
    expanded: true,
    data: {},
    children: [
      {
        id: '1',
        label: 'Baccalauréat',
        expanded: true,
        data: {},
        children: [
          {
            id: '2',
            label: 'Général',
            icon: 'fr-icon-calendar-line',
            data: { badge: { id: '3', label: 'Essentiel', color: 'fr-badge--blue-ecume' } },
            selected: true,
          },
          {
            id: '4',
            label: 'Technologique',
            data: { badge: { id: '44', label: 'Essentiel', color: 'fr-badge--blue-ecume' } },
            selected: true,
          },
          {
            id: '444',
            label: 'Professionnel',
            data: { badge: { id: '4444', label: 'Essentiel', color: 'fr-badge--blue-ecume' } },
          },
        ],
      },
      {
        id: '22',
        label: 'Diplôme National du Brevet',
        expanded: true,
        selected: true,
        data: {},
        children: [
          {
            id: '222',
            label: 'Série Générale',
            data: { badge: { id: '2222', label: 'Essentiel', color: 'fr-badge--blue-ecume' } },
          },
          {
            id: '3333',
            label: 'Série Professionnelle',
            data: { badge: { id: '333', label: 'Spécialisé', color: 'fr-badge--green-menthe' } },
          },
        ],
      },
    ],
  },
  {
    id: '111',
    label: 'Diplômes Techniques et Artistiques',
    expanded: true,
    data: {},
    children: [
      {
        id: '11',
        label: 'Brevet de Technicien (BT)',
        expanded: true,
        data: {},
        children: [
          {
            id: '11111',
            label: 'BT Maintenance Industrielle',
            data: { badge: { id: '999', label: 'Spécialisé', color: 'fr-badge--green-menthe' } },
          },
          {
            id: '666',
            label: 'BT Conception Mécanique',
            data: { badge: { id: '6666', label: 'Spécialisé', color: 'fr-badge--green-menthe' } },
            selected: true,
          },
        ],
      },
      {
        id: '23',
        label: 'Brevet de Métiers d’Art (BMA)',
        expanded: true,
        data: {},
        children: [
          {
            id: '32',
            label: 'BMA Ébénisterie',
            expanded: true,
            data: { badge: { id: '134', label: 'Artisanat', color: 'fr-badge--brown-caramel' } },
          },
          {
            id: '567',
            label: 'BMA Céramique',
            data: { badge: { id: '929', label: 'Artisanat', color: 'fr-badge--brown-caramel' } },
          },
        ],
      },
    ],
  },
  {
    id: '2222222222',
    label: 'Certifications Professionnelles',
    icon: 'fr-icon-briefcase-line',
    expanded: true,
    data: {},
    children: [
      {
        id: '222222222222',
        label: 'CAP (Certificat d’Aptitude Professionnelle)',
        expanded: true,
        data: {},
        children: [
          {
            id: '89789',
            label: 'CAP Cuisine',
            data: { badge: { id: '0', label: 'Spécialisé', color: 'fr-badge--green-menthe' } },
          },
          {
            id: '57283',
            label: 'CAP Électricité',
            data: { badge: { id: '0', label: 'Spécialisé', color: 'fr-badge--green-menthe' } },
          },
        ],
      },
      {
        id: '987654',
        label: 'BEP (Brevet d’Études Professionnelles)',
        expanded: true,
        data: {},
        children: [
          {
            id: '1234567',
            label: 'BEP Métiers de la Vente',
            data: { badge: { id: '123453456', label: 'Spécialisé', color: 'fr-badge--green-menthe' } },
          },
          {
            id: '987658765',
            label: 'BEP Métiers de l’Hôtellerie',
            data: { badge: { id: '567865432', label: 'Spécialisé', color: 'fr-badge--green-menthe' } },
          },
        ],
      },
    ],
  },
];

// https://primeng.org/tree
@Component({
  selector: 'demo-tree-initialization',
  templateUrl: './demo-tree-initialization.component.html',
  encapsulation: ViewEncapsulation.None,
  styles: [
    `
      .my-tree .dsfrx-tree-component .tree-node-label {
        display: flex;
        align-items: center;
        gap: 0.5rem;
      }
    `,
  ],
  standalone: true,
  imports: [CommonModule, FormsModule, DsfrTreeComponent, DsfrButtonsGroupModule, DsfrButtonModule, DsfrBadgeModule],
})
export class DemoTreeInitializationComponent {
  loading = false;

  model: DsfrNode[] = INITAL_MODEL;

  selectionMode: DsfrSelectionMode = 'single';

  initMode(mode: DsfrSelectionMode): void {
    this.loading = true;

    setTimeout(() => {
      this.selectionMode = mode;
      this.loading = false;
    }, 100);
  }
}
