import { Meta, moduleMetadata, StoryObj } from '@storybook/angular';
import { DsfrNode } from '../../../shared/utils/tree/tree.model';
import { DemoTreeInitializationComponent } from './demo-tree-initialization.component';

const meta: Meta<DemoTreeInitializationComponent> = {
  tags: ['!dev'],
  title: 'Components/Tree',
  component: DemoTreeInitializationComponent,
  decorators: [moduleMetadata({ imports: [] })],
};
export default meta;
type Story = StoryObj<DemoTreeInitializationComponent>;

const exams: DsfrNode[] = [
  {
    label: 'Examens Nationaux',
    expanded: true,
    data: {},
    children: [
      {
        label: 'Baccalauréat',
        expanded: true,
        data: {},
        children: [
          {
            label: 'Général',
            icon: 'fr-icon-calendar-line',
            data: { badge: { label: 'Essentiel', color: 'fr-badge--blue-ecume' } },
          },
          {
            label: 'Technologique',
            data: { badge: { label: 'Essentiel', color: 'fr-badge--blue-ecume' } },
          },
          {
            label: 'Professionnel',
            data: { badge: { label: 'Essentiel', color: 'fr-badge--blue-ecume' } },
          },
        ],
      },
      {
        label: 'Diplôme National du Brevet',
        expanded: true,
        data: {},
        children: [
          {
            label: 'Série Générale',
            data: { badge: { label: 'Essentiel', color: 'fr-badge--blue-ecume' } },
          },
          {
            label: 'Série Professionnelle',
            data: { badge: { label: 'Spécialisé', color: 'fr-badge--green-menthe' } },
          },
        ],
      },
    ],
  },
  {
    label: 'Diplômes Techniques et Artistiques',
    expanded: true,
    data: {},
    children: [
      {
        label: 'Brevet de Technicien (BT)',
        expanded: true,
        data: {},
        children: [
          {
            label: 'BT Maintenance Industrielle',
            data: { badge: { label: 'Spécialisé', color: 'fr-badge--green-menthe' } },
          },
          {
            label: 'BT Conception Mécanique',
            data: { badge: { label: 'Spécialisé', color: 'fr-badge--green-menthe' } },
          },
        ],
      },
      {
        label: 'Brevet de Métiers d’Art (BMA)',
        expanded: true,
        data: {},
        children: [
          {
            label: 'BMA Ébénisterie',
            expanded: true,
            data: { badge: { label: 'Artisanat', color: 'fr-badge--brown-caramel' } },
          },
          {
            label: 'BMA Céramique',
            data: { badge: { label: 'Artisanat', color: 'fr-badge--brown-caramel' } },
          },
        ],
      },
    ],
  },
  {
    label: 'Certifications Professionnelles',
    icon: 'fr-icon-briefcase-line',
    expanded: true,
    data: {},
    children: [
      {
        label: 'CAP (Certificat d’Aptitude Professionnelle)',
        expanded: true,
        data: {},
        children: [
          {
            label: 'CAP Cuisine',
            data: { badge: { label: 'Spécialisé', color: 'fr-badge--green-menthe' } },
          },
          {
            label: 'CAP Électricité',
            data: { badge: { label: 'Spécialisé', color: 'fr-badge--green-menthe' } },
          },
        ],
      },
      {
        label: 'BEP (Brevet d’Études Professionnelles)',
        expanded: true,
        data: {},
        children: [
          {
            label: 'BEP Métiers de la Vente',
            data: { badge: { label: 'Spécialisé', color: 'fr-badge--green-menthe' } },
          },
          {
            label: 'BEP Métiers de l’Hôtellerie',
            data: { badge: { label: 'Spécialisé', color: 'fr-badge--green-menthe' } },
          },
        ],
      },
    ],
  },
];

export const Initialization: Story = {};
