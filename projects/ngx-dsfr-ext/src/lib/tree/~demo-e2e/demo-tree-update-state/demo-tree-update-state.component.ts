import { CommonModule } from '@angular/common';
import { AfterViewInit, Component, Input, ViewChild, ViewEncapsulation } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { DsfrBadgeModule, DsfrButtonModule, DsfrButtonsGroupModule } from '@edugouvfr/ngx-dsfr';
import { DsfrNode } from '../../../shared/utils/tree/tree.model';
import { DsfrTreeComponent } from '../../tree.component';

// https://primeng.org/tree
@Component({
  selector: 'demo-tree-update-state',
  templateUrl: './demo-tree-update-state.component.html',
  encapsulation: ViewEncapsulation.None,
  styles: [
    `
      .my-tree .dsfrx-tree-component .tree-node-label {
        display: flex;
        align-items: center;
        gap: 0.5rem;
      }
    `,
  ],
  standalone: true,
  imports: [CommonModule, FormsModule, DsfrTreeComponent, DsfrButtonsGroupModule, DsfrButtonModule, DsfrBadgeModule],
})
export class DemoTreeUpdateStateComponent implements AfterViewInit {
  @Input() model: DsfrNode[];
  @Input() selectionMode: any;

  /** @internal */
  @ViewChild('tree')
  private treeComponent: DsfrTreeComponent;

  hidden = true;

  ngAfterViewInit(): void {
    this.updateTreeState();
  }

  saveTreeState() {
    this.model = this.treeComponent.getTreeState();
  }

  updateTreeState() {
    this.treeComponent.reinitializeState();
  }

  updateModel() {
    const nodes = this.model[0].children;

    if (nodes) {
      const nodeToHide = nodes[0];
      nodeToHide.hidden = !Boolean(nodeToHide.hidden);
      this.hidden = !Boolean(nodeToHide.hidden);
    }
  }
}
