import test, { FrameLocator, Locator, expect } from '@playwright/test';
import { getSbFrame, goToComponentPage } from 'projects/ngx-dsfr-ext/shared/e2e.utils';

test.describe('Tree', () => {
  test.describe('has nodes', () => {
    test.describe('which by default', () => {
      test.beforeEach(async ({ page }) => {
        await goToComponentPage('tree', page);
      });

      test('expand and collapse on click if it is a branch', async ({ page }) => {
        const frame = getSbFrame(page);
        const tree = getTree(frame);
        const bibliothequeNode = getNode('Bibliothèque', tree);
        const litteratureClassiqueNode = getNode('Littérature Classique', tree);

        await expect(litteratureClassiqueNode).not.toBeVisible();

        await bibliothequeNode.click();
        await expect(litteratureClassiqueNode).toBeVisible();

        await bibliothequeNode.click();
        await expect(litteratureClassiqueNode).not.toBeVisible();
      });

      test('respects WAI Treeview specifications for navigation', async ({ page }) => {
        const frame = getSbFrame(page);
        const tree = getTree(frame);
        const bibliothequeNode = getNode('Bibliothèque', tree);
        const litteratureClassiqueNode = getNode('Littérature Classique', tree);
        const romansFrancaisNode = getNode('Romans Français', tree);
        const focusedElement = getCurrentFocus(frame);

        await expect(litteratureClassiqueNode).not.toBeVisible();
        await expect(romansFrancaisNode).not.toBeVisible();

        await bibliothequeNode.focus();
        await bibliothequeNode.press('ArrowRight');
        await expect(litteratureClassiqueNode).toBeVisible();
        await expect(romansFrancaisNode).not.toBeVisible();

        await bibliothequeNode.press('ArrowLeft');
        await expect(litteratureClassiqueNode).not.toBeVisible();
        await expect(romansFrancaisNode).not.toBeVisible();

        await bibliothequeNode.press('ArrowRight');
        await bibliothequeNode.press('ArrowDown');
        await expect(litteratureClassiqueNode).toBeVisible();
        await expect(focusedElement).toContainText('Littérature Classique');
        await expect(romansFrancaisNode).not.toBeVisible();

        await litteratureClassiqueNode.press('ArrowUp');
        await expect(focusedElement).toContainText('Bibliothèque');
        await expect(litteratureClassiqueNode).toBeVisible();

        await bibliothequeNode.press('ArrowDown');
        await bibliothequeNode.press('ArrowDown');
        await bibliothequeNode.press('ArrowDown');
        await expect(focusedElement).toContainText('Événements Littéraires');

        await bibliothequeNode.press('Home');
        await expect(focusedElement).toContainText('Bibliothèque');

        await bibliothequeNode.press('End');
        await expect(focusedElement).toContainText('Sélection littéraire');
      });

      test('keeps the collapse state of its children after it has been collapsed', async ({ page }) => {
        const frame = getSbFrame(page);
        const tree = getTree(frame);
        const selectionLitteraireNode = getNode('Sélection littéraire', tree);
        const poesieNode = getNode('Poésie', tree);
        const fleurDuMalNode = getNode('Les Fleurs du Mal', tree);

        await selectionLitteraireNode.click();
        await poesieNode.click();
        await expect(fleurDuMalNode).toBeVisible();

        await selectionLitteraireNode.click();
        await expect(poesieNode).not.toBeVisible();

        await selectionLitteraireNode.click();
        await expect(fleurDuMalNode).toBeVisible();
      });
    });

    test.describe(`which in 'single' mode`, () => {
      test.beforeEach(async ({ page }) => {
        await goToComponentPage('tree', page, 'single');
      });

      test('enable a selection/deselection on click', async ({ page }) => {
        const frame = getSbFrame(page);
        const tree = getTree(frame);
        const bibliothequeNode = getNode('Bibliothèque', tree);
        const litteratureClassiqueNode = getNode('Littérature Classique', tree);
        const selectedNodes = getSelectedElements(frame);

        await expect(litteratureClassiqueNode).not.toBeVisible();

        await bibliothequeNode.click();
        await expect(litteratureClassiqueNode).not.toBeVisible();
        expect((await selectedNodes.all()).length).toBe(1);

        await bibliothequeNode.click();
        await expect(litteratureClassiqueNode).not.toBeVisible();
        expect((await selectedNodes.all()).length).toBe(0);
      });

      test('enable the selection of a single item', async ({ page }) => {
        const frame = getSbFrame(page);
        const tree = getTree(frame);
        const bibliothequeNode = getNode('Bibliothèque', tree);
        const selectionLitteraireNode = getNode('Sélection littéraire', tree);
        const selectedNodes = getSelectedElements(frame);

        await bibliothequeNode.click();
        expect((await selectedNodes.all()).length).toBe(1);

        await selectionLitteraireNode.click();
        expect((await selectedNodes.all()).length).toBe(1);
      });
    });

    test.describe(`which in 'multiple' mode`, () => {
      test.beforeEach(async ({ page }) => {
        await goToComponentPage('tree', page, 'multiple');
      });

      test('enable multiple selections on click', async ({ page }) => {
        const frame = getSbFrame(page);
        const tree = getTree(frame);
        const bibliothequeNode = getNode('Bibliothèque', tree);
        const selectionLitteraireNode = getNode('Sélection littéraire', tree);
        const atelierEcritureNode = getNode(`Atelier d'Écriture`, tree);
        const salonDuLivreNode = getNode(`Salon du Livre`, tree);
        const selectedNodes = getSelectedElements(frame);

        // Déselection de Salon du Livre (valeur par défaut de l'exemple)
        await salonDuLivreNode.click();
        /* ---------------------------- */

        await bibliothequeNode.click();
        expect((await selectedNodes.all()).length).toBe(1);

        await selectionLitteraireNode.click();
        expect((await selectedNodes.all()).length).toBe(2);

        await atelierEcritureNode.click();
        expect((await selectedNodes.all()).length).toBe(3);
      });
    });

    test.describe('which in checkbox mode', () => {
      test('select all children when a parent is selected', async ({ page }) => {
        const frame = getSbFrame(page);
        const tree = getTree(frame);
        const toutDeplierBtn = frame.getByText('Tout déplier');
        const examensNationauxNode = getNode('Examens Nationaux', tree);
        const selectedNodes = getSelectedElements(frame);

        await goToComponentPage('tree', page, 'slot-checkbox');

        await toutDeplierBtn.click();
        await examensNationauxNode.click();
        expect((await selectedNodes.all()).length).toBe(8);
      });

      test('do not select the hidden nodes if they are direct children of a selected parent node', async ({ page }) => {
        const frame = getSbFrame(page);
        const tree = getTree(frame);
        const toutDeplierBtn = frame.getByText('Tout déplier');
        const examensNationauxNode = getNode('Examens Nationaux', tree);
        const hideBaccalaureatBtn = frame.getByText('Masquer <Baccalauréat>');
        const selectedNodes = getSelectedElements(frame);

        await goToComponentPage('tree', page, 'slot-checkbox');

        await toutDeplierBtn.click();
        await hideBaccalaureatBtn.click();
        await examensNationauxNode.click();

        expect((await selectedNodes.all()).length).toBe(3);
      });
    });

    test('which can be hidden', async ({ page }) => {
      const frame = getSbFrame(page);
      const tree = getTree(frame);
      const baccalaureatNode = getNode('Baccalauréat', tree);
      const examensNationauxNode = getNode('Examens Nationaux', tree);
      const generalNode = getNode('Général', tree);
      const hideBaccalaureatBtn = frame.getByText('Masquer <Baccalauréat>');
      const showBaccalaureatBtn = frame.getByText('Afficher <Baccalauréat>');

      await goToComponentPage('tree', page, 'slot-checkbox');

      await expect(baccalaureatNode).not.toBeVisible();
      await expect(generalNode).not.toBeVisible();

      await examensNationauxNode.focus();
      await examensNationauxNode.press('ArrowRight');
      await expect(baccalaureatNode).toBeVisible();
      await expect(generalNode).not.toBeVisible();

      await examensNationauxNode.press('ArrowDown');
      await examensNationauxNode.press('ArrowRight');
      await expect(generalNode).toBeVisible();

      await hideBaccalaureatBtn.click();
      await expect(baccalaureatNode).not.toBeVisible();
      await expect(generalNode).not.toBeVisible();

      await showBaccalaureatBtn.click();
      await expect(baccalaureatNode).toBeVisible();
      await expect(generalNode).toBeVisible();
    });
  });

  test.describe('properly initializes', () => {
    test.beforeEach(async ({ page }) => {
      await goToComponentPage('tree', page, 'initialization');
    });

    test('in single mode', async ({ page }) => {
      const frame = getSbFrame(page);
      const tree = getTree(frame);
      const selectedNodes = getSelectedElements(frame);

      await expect(tree).toBeVisible();
      expect((await selectedNodes.all()).length).toBe(1);
      expect((await selectedNodes.allInnerTexts())[0]).toContain('Général');
    });

    test('in multiple mode', async ({ page }) => {
      const frame = getSbFrame(page);
      const tree = getTree(frame);
      const selectedNodes = getSelectedElements(frame);
      const switchBtn = frame.getByText('Switch to Multiple');

      await switchBtn.click();
      await expect(tree).toBeVisible();
      expect((await selectedNodes.all()).length).toBe(4);
      expect((await selectedNodes.allInnerTexts())[0]).toContain('Général');
      expect((await selectedNodes.allInnerTexts())[1]).toContain('Technologique');
      expect((await selectedNodes.allInnerTexts())[2]).toContain('Diplôme National du Brevet');
      expect((await selectedNodes.allInnerTexts())[3]).toContain('BT Conception Mécanique');
    });

    test('in checkbox mode', async ({ page }) => {
      const frame = getSbFrame(page);
      const tree = getTree(frame);
      const selectedNodes = getSelectedElements(frame);
      const switchBtn = frame.getByText('Switch to Checkbox');
      const examsNationauxCbx = tree.getByLabel('Examens Nationaux');
      const baccalaureatCbx = tree.getByLabel('Baccalauréat');

      await switchBtn.click();
      await expect(tree).toBeVisible();
      expect((await selectedNodes.all()).length).toBe(3);
      expect((await selectedNodes.allInnerTexts())[0]).toContain('Général');
      expect((await selectedNodes.allInnerTexts())[1]).toContain('Technologique');
      expect((await selectedNodes.allInnerTexts())[2]).toContain('BT Conception Mécanique');
      await expect(examsNationauxCbx).toHaveJSProperty('indeterminate', true);
      await expect(baccalaureatCbx).toHaveJSProperty('indeterminate', true);
    });
  });
});

/** LOCATORS */
function getTree(sbFrame: FrameLocator): Locator {
  return sbFrame.getByRole('tree');
}

function getNode(nodeText: string, tree: Locator): Locator {
  return tree.getByText(nodeText);
}

function getSelectedElements(sbFrame: FrameLocator): Locator {
  return sbFrame.locator('[aria-selected="true"]');
}

function getCurrentFocus(sbFrame: FrameLocator): Locator {
  return sbFrame.locator('*:focus');
}
