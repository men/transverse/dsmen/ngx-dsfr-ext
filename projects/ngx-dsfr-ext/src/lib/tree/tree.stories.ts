import { controlDisabled, controlEmitter, dsfrDecorators } from '.storybook/storybook-utils';
import { FormsModule } from '@angular/forms';
import { DsfrButtonModule } from '@edugouvfr/ngx-dsfr';
import { Meta, StoryObj, moduleMetadata } from '@storybook/angular';
import { DsfrNode } from '../shared/utils/tree/tree.model';
import { DsfrTreeComponent } from './tree.component';

const meta: Meta<DsfrTreeComponent> = {
  title: 'Components/Tree',
  component: DsfrTreeComponent,
  decorators: [moduleMetadata({ imports: [FormsModule, DsfrButtonModule] })],
  parameters: {
    docs: {
      toc: {
        disable: true, // Disables the table of contents
      },
    },
  },
  argTypes: {
    nodeSelect: controlEmitter,
    collapseAll: controlDisabled,
    expandAll: controlDisabled,
    selectionMode: { control: { type: 'inline-radio' }, options: ['single', 'multiple', 'checkbox', undefined] },
  },
};
export default meta;
type Story = StoryObj<DsfrTreeComponent>;

const library: DsfrNode[] = [
  {
    label: 'Bibliothèque',
    icon: 'fr-icon-book-2-fill',
    children: [
      {
        label: 'Littérature Classique',
        icon: 'fr-icon-book-2-line',
        children: [
          {
            label: 'Romans Français',
            icon: 'fr-icon-file-fill',
            children: [
              { label: 'Les Misérables', icon: 'fr-icon-file-line' },
              {
                label: 'Le Comte de Monte-Cristo',
                icon: 'fr-icon-file-line',
              },
            ],
          },
          {
            label: 'Romans Étrangers',
            icon: 'fr-icon-file-fill',
            children: [
              { label: 'Orgueil et Préjugés', icon: 'fr-icon-file-line' },
              { label: 'Guerre et Paix', icon: 'fr-icon-file-line' },
            ],
          },
        ],
      },
      {
        label: 'Contes et Fables',
        icon: 'fr-icon-star-line',
        children: [
          {
            label: 'Contes de Perrault',
            icon: 'fr-icon-star-line',
            children: [
              {
                label: 'Le Petit Chaperon Rouge',
                icon: 'fr-icon-film-line',
              },
              {
                label: 'La Belle au Bois Dormant',
                icon: 'fr-icon-film-line',
              },
            ],
          },
          {
            label: 'Fables de La Fontaine',
            icon: 'fr-icon-star-line',
            children: [
              {
                label: 'Le Corbeau et le Renard',
                icon: 'fr-icon-film-line',
              },
              {
                label: 'La Cigale et la Fourmi',
                icon: 'fr-icon-film-line',
              },
            ],
          },
        ],
      },
    ],
  },
  {
    label: 'Événements Littéraires',
    expanded: true,
    icon: 'fr-icon-calendar-line',
    children: [
      {
        label: 'Salon du Livre',
        icon: 'fr-icon-todo-fill',
        selected: true,
      },
      { label: "Conférence d'un Auteur", icon: 'fr-icon-todo-fill' },
      { label: "Atelier d'Écriture", icon: 'fr-icon-todo-fill' },
    ],
  },
  {
    label: 'Sélection littéraire',
    icon: 'fr-icon-star-line',
    children: [
      {
        label: 'Poésie',
        icon: 'fr-icon-star-line',
        children: [
          { label: 'Les Fleurs du Mal', icon: 'fr-icon-film-line' },
          { label: 'Les Contemplations', icon: 'fr-icon-film-line' },
        ],
      },
      {
        label: 'Théâtre',
        icon: 'fr-icon-star-line',
        children: [
          { label: 'Hamlet', icon: 'fr-icon-film-line' },
          { label: 'Le Malade Imaginaire', icon: 'fr-icon-film-line' },
        ],
      },
    ],
  },
];

export const Default: Story = {
  decorators: dsfrDecorators('Affichage par défaut'),
  args: {
    selectionMode: undefined,
    value: library,
  },
};

export const Single: Story = {
  decorators: dsfrDecorators("Mode de sélection 'single'"),
  args: {
    ...Default.args,
    selectionMode: 'single',
  },
};

export const Multiple: Story = {
  decorators: dsfrDecorators("Mode de sélection 'multiple'"),
  args: {
    ...Default.args,
    selectionMode: 'multiple',
  },
};

export const Checkbox: Story = {
  decorators: dsfrDecorators("Mode de sélection 'checkbox'"),
  args: {
    ...Default.args,
    selectionMode: 'checkbox',
  },
};
