import { CdkVirtualScrollViewport, ScrollingModule } from '@angular/cdk/scrolling';
import { CommonModule } from '@angular/common';
import {
  AfterViewInit,
  Component,
  ContentChild,
  ElementRef,
  EventEmitter,
  forwardRef,
  inject,
  Input,
  OnDestroy,
  OnInit,
  Output,
  Renderer2,
  TemplateRef,
  ViewChild,
  ViewEncapsulation,
} from '@angular/core';
import { FormsModule, NG_VALUE_ACCESSOR } from '@angular/forms';
import {
  DefaultControlComponent,
  DsfrButtonModule,
  DsfrButtonType,
  DsfrButtonVariant,
  DsfrFormInputComponent,
  DsfrFormInputModule,
  DsfrI18nPipe,
  DsfrI18nService,
  DsfrSeverity,
  Language,
  newUniqueId,
} from '@edugouvfr/ngx-dsfr';
import { debounceTime, distinctUntilChanged, Subject, takeUntil } from 'rxjs';
import { DropdownContainerComponent } from '../shared/components/dropdown-container/dropdown-container.component';
import { DsfrSpinnerComponent } from '../spinner';
import { DsfrCompleteEvent } from './autocomplete.model';
import { MESSAGES_EN } from './i18n/messages.en';
import { MESSAGES_FR } from './i18n/messages.fr';

const OPTION_SELECTED_CLASS = 'selected';
const OPTION_SELECTED_SELECTOR = '.' + OPTION_SELECTED_CLASS;

@Component({
  selector: 'dsfr-ext-autocomplete, dsfrx-autocomplete',
  encapsulation: ViewEncapsulation.None,
  templateUrl: './autocomplete.component.html',
  standalone: true,
  imports: [
    CommonModule,
    FormsModule,
    ScrollingModule,
    DsfrFormInputModule,
    DsfrButtonModule,
    DropdownContainerComponent,
    DsfrSpinnerComponent,
    DsfrI18nPipe,
  ],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => DsfrAutocompleteComponent),
      multi: true,
    },
  ],
  styleUrls: ['./autocomplete.component.scss'],
})

/**
 * Plus d'héritage de DsfrFormInputComponent afin de pouvoir utiliser le type de value (générique)
 * fixme : éviter la copie des différents inputs de DsfrFormInputComponent
 */
export class DsfrAutocompleteComponent
  extends DefaultControlComponent<any>
  implements OnInit, AfterViewInit, OnDestroy
{
  /** Template des suggestions, option en paramètre  */
  @ContentChild('suggestionTemplate', { static: true }) suggestionTemplate: TemplateRef<any>;

  /** @internal */
  @ViewChild(CdkVirtualScrollViewport) viewport: CdkVirtualScrollViewport;

  /** @internal */
  // Le composant de saisie
  @ViewChild(DsfrFormInputComponent, { read: ElementRef }) inputComponent: ElementRef;

  /** @internal */
  // Le container Dropdown
  @ViewChild(DropdownContainerComponent, { read: ElementRef }) listBox: ElementRef;

  /** L'utilisateur doit traiter cet événement pour déclencher une méthode de recherche qui met à jour la propriété suggestions. */
  @Output() filterChange = new EventEmitter<DsfrCompleteEvent>();

  /** Emet la suggestion à la sélection de celle-ci */
  @Output() suggestionSelect = new EventEmitter<any>();

  /** Fermeture de la liste de suggestions */
  @Output() listClose = new EventEmitter<void>();

  /** Attacher l'élément de liste déroulante au 'body' ou a un container (réf. vers un élément #div ou nom de la classe du parent) */
  @Input() appendTo: 'body' | HTMLElement | string | undefined;

  /** Propriété CSS z-index de la liste, par défaut supérieur aux composants 'lifted' DSFR */
  @Input() zIndex: number | undefined;

  /** Activer le filtre automatique sur l'autocomplete. Faux par défaut */
  @Input() autoFilter = false;

  /** Supprime la valeur de l'input si elle ne provient pas directement de la liste de suggestion. */
  @Input() requireSelection = false;

  /** Activer le virtual scroll */
  @Input() virtualScroll = false;

  /** Hauteur maximale avec unité avant le scroll sur la liste déroulante (correspond à la propriété css max-height)*/
  @Input() scrollHeight: string = '224px';

  /** Surcharger la hauteur en px d'un item dans le cas du virtual scroll. Par défaut 24 (px). */
  @Input() virtualScrollItemHeight: number = 24;

  /** Nom de la propriété correspond à la valeur (string) dans l'objet suggestion */
  @Input() suggestionValueKey: string;

  /** Afficher le spinner indiquant le chargement des suggestions */
  @Input() loading = false;

  /**
   * Le délai (en millisecondes) pour déclencher l'événement de changement de saisie et de filtrage.
   * Par défaut la valeur est 0
   */
  @Input() delay: number = 0;

  /** minimum de caractères nécessaires au déclenchement des suggestions. 1 par défaut */
  @Input() minLength: number = 1;

  // Attributs pour le dsfr-form-input ----------------------------------------------------------- */
  /** (form-input) type de l'input, 'text' ou 'search'. 'text' par défaut */
  @Input() type: 'text' | 'search' = 'text';
  /** (form-input) champ obligatoire ou non, faux par défaut. */
  @Input() required = false;
  /** (form-input) placeholder de l'input. */
  @Input() placeholder: string;
  /** (form-input) attribut maxLength de l'input. */
  @Input() maxLength: number;
  /** (form-input) ajoute un icone à droite dans le champ de saisie. */
  @Input() icon: string;
  /** (form-input) pattern de l'input.*/
  @Input() pattern: string;
  /** (form-input) ajout d'un style spécifique, permet par exemple de la limiter la largeur d'un input. */
  @Input() customClass: string;
  /** (form-input) message d'information lié au composant */
  @Input() message: string | undefined;
  /** (form-input) représente la sévérité du message.  */
  @Input() messageSeverity: DsfrSeverity;

  // Attributs pour le combo du dsfr-form-input ---------------------------------------------------------------------- */

  /*** (form-input) permet de désactiver le bouton d'action. 'false' par défaut. */
  @Input() buttonDisabled = false;
  /*** (form-input) crée un combo champ + bouton si buttonIcon est renseigné */
  @Input() buttonIcon: string | undefined;
  /** (form-input) crée un combo champ + bouton si buttonLabel est renseigné */
  @Input() buttonLabel: string;
  /*** (form-input) tooltip message sur le bouton s'il y a lieu. */
  @Input() buttonTooltipMessage: string;
  /*** (form-input) type du button,'button' par défaut. */
  @Input() buttonType: DsfrButtonType;
  /** (form-input) style du bouton, 'primary' par défaut. */
  @Input() buttonVariant: DsfrButtonVariant = 'primary';
  /** (form-input) [accessibilité] Spécifie le libellé qui sera retranscrit par les narrateurs d'écran. */
  @Input() buttonAriaLabel: string;
  /** (form-input) Change la mise en page d'un input accompagné d'un bouton.  */
  @Input() inputWrapMode: 'addon' | 'action' = 'addon';
  /** (form-input) Emission de l'événement si le type du bouton est différent de `submit`. */
  @Output() buttonSelect = new EventEmitter<Event>();

  /** @internal */
  valueInput: string; // value interne du dsfr-form-input

  protected isExpanded: boolean = false;
  protected currentOptionIndex: number;
  private i18n = inject(DsfrI18nService);

  /** Les suggestions manipulées par la fonction de filtrage de l'utilisateur. */
  private _suggestions: any[] = [];

  /** Les suggestions originales utilisées lorsqu'on est en autoFilter. */
  private originalSuggestions: any[];

  private inputChange$ = new Subject<Event>(); // Subject pour capturer les changements d'input
  private notifyOnDestroy = new Subject<void>();

  /** Permet d'identifier si la dernière valeur de l'input provient d'une suggestion. */
  private isLastValueFromSuggestion = false;

  /** Indique si l'utilisateur a tapé au moins un caractère */
  private hasUserTyped = false;

  constructor(private readonly _renderer: Renderer2) {
    super();
    this.i18n.extendsLabelsBundle(Language.EN, MESSAGES_EN);
    this.i18n.extendsLabelsBundle(Language.FR, MESSAGES_FR);
  }

  get suggestions(): any[] {
    return this._suggestions;
  }

  /** Liste des suggestions */
  @Input() set suggestions(suggestions: any[]) {
    this.originalSuggestions = suggestions;
    this._suggestions = suggestions;
  }

  /** Fonction de formatage de la valeur affichée dans l'input à la sélection d'une suggestion */
  @Input() formatOnSelect: (selectedSuggestion: any) => string = (selectedSuggestion) =>
    this.suggestionValueKey ? selectedSuggestion[this.suggestionValueKey] : selectedSuggestion;

  /** @internal */
  override ngOnInit() {
    super.ngOnInit();

    // Initialise le debounce lié à l'event input change
    this.inputChange$
      .pipe(debounceTime(this.delay), distinctUntilChanged(), takeUntil(this.notifyOnDestroy))
      .subscribe((event: Event) => {
        this.handleInputChange(event);
      });

    this.minLength ??= 1;
    this.inputId ??= newUniqueId();
    this.currentOptionIndex = 0;
  }

  public ngAfterViewInit(): void {
    const inputElement = this.inputComponent.nativeElement?.querySelector('input');

    if (inputElement) {
      this._renderer.setAttribute(inputElement, 'aria-controls', `listbox-${this.inputId}`);
    }
  }

  public ngOnDestroy() {
    this.notifyOnDestroy.next();
    this.notifyOnDestroy.complete();
  }

  /** @internal */
  public onToogleDisplayList(display: boolean): void {
    this.isExpanded = display;
  }

  /** @internal */
  public onInput(event: Event) {
    if (!this.hasUserTyped) this.hasUserTyped = true;
    this.inputChange$.next(event);
  }

  /** @internal Ouverture de la liste des suggestions au focus si le minLength est de 0 */
  public onFocusInput() {
    if (this.minLength === 0) {
      this.isExpanded = true;
    }
  }

  /** Gestion des touches de navigation. */
  /** @internal */
  public doKeyEvent(event: KeyboardEvent, options: any[]) {
    switch (event.key) {
      case 'ArrowDown':
        const nextIndex = Math.min(this.currentOptionIndex + 1, options?.length);
        // si on est focus sur la dernire option, on va sur la premiere de la liste
        this.currentOptionIndex =
          nextIndex !== this.currentOptionIndex ? this.activateOption(nextIndex) : this.activateOption(1);
        event.preventDefault();
        break;

      case 'ArrowUp':
        const prevIndex = Math.max(1, this.currentOptionIndex - 1);
        // si on est focus sur la premiere option, on va sur la derniere de la liste
        this.currentOptionIndex =
          prevIndex !== this.currentOptionIndex ? this.activateOption(prevIndex) : this.activateOption(options?.length);
        event.preventDefault();
        break;

      case 'Enter':
      case 'Tab':
      case 'NumpadEnter':
        /* If the ENTER key is pressed, prevent the form from being submitted, */
        if (event.key !== 'Tab') event.preventDefault();
        if (this.currentOptionIndex > 0) {
          this.onOptionSelect(this.suggestions[this.currentOptionIndex - 1], this.currentOptionIndex);
        } else {
          this.closeAutocomplete();
        }
        break;

      case 'Escape':
        this.closeAutocomplete();
        event.preventDefault();
        break;

      default:
    }
  }

  /** On ferme la liste des suggestions quand on sort du champ. */
  /** @internal */
  public closeAutocomplete() {
    this.isExpanded = false;
    this.resetAriaActiveDescendant();

    if (this.requireSelection && !this.isLastValueFromSuggestion) {
      // Vérifie si la valeur saisie correspond à une suggestion existante
      const selectedSuggestion = this.suggestions?.find((o: any) => this.formatOnSelect(o) === this.valueInput);

      if (selectedSuggestion) {
        this.value = selectedSuggestion;
      } else if (this.hasUserTyped) {
        this.resetValueAndIndex();
      }
    }

    this.listClose.emit();
  }

  /** Sélection d'une suggestion */
  /** @internal */
  public onOptionSelect(suggestion: any, i: number) {
    if (suggestion) {
      this.activateOption(i);
      this.value = suggestion;
      /* insert the value for the autocomplete text field: */
      this.valueInput = this.formatOnSelect(this.value);
      this.currentOptionIndex = i;
    } else {
      this.currentOptionIndex = 0;
    }
    this.suggestionSelect.emit(suggestion);
    /* close the list of autocompleted values */
    this.closeAutocomplete();
  }

  /**
   * Ajout de l'attribut aria-activedescendant correspond à l'option qui a le focus
   * @internal */
  public onOptionFocus(optionId: string) {
    this._renderer.setAttribute(
      this.inputComponent.nativeElement?.querySelector('input'),
      'aria-activedescendant',
      optionId,
    );
  }

  /** Override writeValue pour setter la valeur de l'input programmatiquement à l'initialisation
   * @internal
   */
  override writeValue(value: any): void {
    super.writeValue(value);
    if (this.value) {
      this.valueInput = this.formatOnSelect(this.value);
    } else {
      this.valueInput = this.value;
    }
  }

  /** @internal */
  public isOptionSelected(i: number): boolean {
    return this.currentOptionIndex === i;
  }

  /** Emission de buttonSelect au niveau de form-input, seulement si le type du bouton n'est pas `submit` */
  protected onInputButtonSelect(e: Event) {
    this.buttonSelect.emit(e);
  }

  /** Filtrage apres input change+debounce */
  private handleInputChange(event: Event) {
    this.value = this.valueInput;
    this.isExpanded =
      this.valueInput !== null && this.valueInput !== undefined && this.valueInput.length >= this.minLength;

    if (!this.isExpanded) {
      this.closeAutocomplete();
      return;
    }

    if (this.isExpanded) {
      if (!this.autoFilter) {
        const completeEvent: DsfrCompleteEvent = { originalEvent: event, query: this.valueInput! };
        this.filterChange.emit(completeEvent);
        this.isLastValueFromSuggestion = false;
      } else {
        this.doAutoFilter();
      }
      this.resetAriaActiveDescendant();
    }
  }

  /**
   * Filtre la liste avec la valeur
   *
   * La recherche se fait toujours à partir de : originalSuggestions
   * On valorise directement la propriété privée _suggestions sans passer par le setter (réservé à l'utilisateur)
   * afin de garder intact originalSuggestions
   */
  private doAutoFilter(): void {
    // Ici, on est sûr que 'this.value' soit valorisée
    if (this.originalSuggestions?.length > 0) {
      const searchValue: string = this.valueInput!.toLowerCase();

      if (this.suggestionValueKey) {
        // originalSuggestions liste d'objets et suggestionValueKey définie
        this._suggestions = this.originalSuggestions.filter((item: any) => {
          return item[this.suggestionValueKey].toLowerCase().includes(searchValue);
        });
      } else if (typeof this.originalSuggestions[0] === 'string') {
        // originalSuggestions liste de string
        this._suggestions = this.originalSuggestions.filter((item: string) => item.toLowerCase().includes(searchValue));
      }
    }
  }

  private activateOption(index: number): number {
    const nativeElt = this.listBox?.nativeElement;
    let options = nativeElt?.querySelector('ul');

    // Selection de l'element de liste si ajouté au body
    if (this.appendTo) {
      options = document.getElementById(`listbox-${this.inputId}`);
    }

    // Dé-sélection de l'option active s'il y a lieu
    const previousOption: HTMLElement = options?.querySelector(OPTION_SELECTED_SELECTOR);
    previousOption?.classList.remove(OPTION_SELECTED_CLASS);

    // Sélection de l'indice courant
    const activeOption: HTMLElement = options?.querySelector(`li:nth-child(${index})`);

    if (activeOption) {
      this.onOptionFocus(activeOption.id);
      activeOption.classList.add(OPTION_SELECTED_CLASS); // hover sur l'élément
      // comme on ne déplace pas le focus, scroll manuel sur l'élément
      if (!this.virtualScroll) activeOption.scrollIntoView({ behavior: 'smooth', block: 'end' });
      else this.manageScrollForVirtualScroll(index);
    }

    return index;
  }

  /**
   * En mode virtualScroll le scroll doit se faire par la methode scrollToIndex de CDK virtualscroll
   * @param index index de l'option dans la liste gérée par cdkVirtualFor
   */
  private manageScrollForVirtualScroll(index: number) {
    const indexToScroll = index > this.suggestions.length ? 0 : index - 2; // decalage de 2 pour ne pas avoir le viewport collé à l'input
    this.viewport.scrollToIndex(indexToScroll, 'smooth');
  }

  // Effacement de aria-activedescendant lorsque la liste est cachée ou une lettre tapée
  private resetAriaActiveDescendant() {
    this._renderer.setAttribute(this.inputComponent.nativeElement?.querySelector('input'), 'aria-activedescendant', '');
  }

  private resetValueAndIndex() {
    this.valueInput = '';
    this.value = undefined;
    this.currentOptionIndex = 0;
  }
}
