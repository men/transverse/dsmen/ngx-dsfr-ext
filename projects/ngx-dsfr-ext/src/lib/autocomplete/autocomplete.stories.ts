import { controlEmitter } from '.storybook/storybook-utils';
import { FormsModule } from '@angular/forms';
import { DsfrSeverityConst } from '@edugouvfr/ngx-dsfr';
import { Meta, StoryObj, moduleMetadata } from '@storybook/angular';
import { DsfrAutocompleteComponent } from './autocomplete.component';

const meta: Meta<DsfrAutocompleteComponent> = {
  title: 'Components/Autocomplete',
  component: DsfrAutocompleteComponent,
  decorators: [moduleMetadata({ imports: [FormsModule] })],
  parameters: {
    actions: true,
    docs: {
      story: {
        height: '380px',
      },
    },
  },
  argTypes: {
    filterChange: controlEmitter,
    buttonSelect: controlEmitter,
    listClose: controlEmitter,
    suggestionSelect: controlEmitter,
    inputWrapMode: { control: { type: 'inline-radio' } },
    type: { control: { type: 'inline-radio' }, options: ['text', 'search'] },
    value: { control: { type: 'text' } },
    buttonVariant: {
      control: { type: 'select' },
      options: ['primary', 'secondary', 'tertiary', 'tertiary-no-outline'],
    },
    buttonType: { control: { type: 'inline-radio' }, options: ['button', 'submit', 'reset'] },
    messageSeverity: { control: { type: 'inline-radio' }, options: ['info', 'error', 'success'] },
  },
};
export default meta;
type Story = StoryObj<DsfrAutocompleteComponent>;

const FRUITS = [
  'Apricot',
  'Bell pepper',
  'Carolina reaper',
  'Chili pepper',
  'Corn kernel',
  'Cucumber',
  'Eggplant',
  'Grapefruit',
  'Jalapeño',
  'Mango',
  'Melon',
  'Olive',
  'Pea',
  'Pumpkin',
  'Squash',
  'Tomato',
  'Zucchini',
  'Apple',
  'Banana',
  'Blueberry',
  'Broccoli',
  'Cabbage',
  'Carrot',
  'Cauliflower',
  'Celery',
  'Cherry',
  'Cranberry',
  'Garlic',
  'Ginger',
  'Kale',
  'Kiwi',
  'Lemon',
  'Lettuce',
  'Onion',
  'Papaya',
  'Peach',
  'Pear',
  'Pineapple',
  'Radish',
];

const templateAutocomplete = `
  <dsfr-ext-autocomplete
  [label]="label"
  [suggestions]="suggestions"
  [autoFilter]="autoFilter"
  [appendTo]="appendTo"
  [virtualScroll]="virtualScroll"
  [required]="required"
  [type]="type"
  [message]="message"
  [requireSelection]="requireSelection"
  [messageSeverity]="messageSeverity"
  [zIndex]="zIndex"
  [disabled]="disabled">
  </dsfr-ext-autocomplete> `;

export const Default: Story = {
  args: {
    suggestions: FRUITS,
    autoFilter: false,
    label: 'Sélectionner un fruit',
    hint: 'Par défaut, aucun filtre implémenté',
    type: 'search',
  },
  render: (args) => ({
    props: args,
    template: templateAutocomplete,
  }),
};

export const AutoFilter: Story = {
  args: {
    ...Default.args,
    autoFilter: true,
    hint: '',
  },
  render: (args) => ({
    props: args,
    template: templateAutocomplete,
  }),
};

export const Disabled: Story = {
  args: {
    ...Default.args,
    disabled: true,
    hint: 'hint',
  },
  render: (args) => ({
    props: args,
    template: templateAutocomplete,
  }),
};

export const Error: Story = {
  args: {
    ...Default.args,
    message: "Mon message d'erreur",
    messageSeverity: DsfrSeverityConst.ERROR,
  },
  render: (args) => ({
    props: args,
    template: templateAutocomplete,
  }),
};

const templateAutocompleteCombo = `
  <dsfr-ext-autocomplete
  [label]="label"
  [suggestions]="suggestions"
  [autoFilter]="autoFilter"
  [appendTo]="appendTo"
  [virtualScroll]="virtualScroll"
  [buttonLabel]="buttonLabel"
  [buttonIcon]="buttonIcon"
  [buttonDisabled]="buttonDisabled"
  [inputWrapMode]="inputWrapMode"
  [type]="type"
  [buttonVariant]="buttonVariant"
  [message]="message"
  [messageSeverity]="messageSeverity"
  [zIndex]="zIndex"
  [disabled]="disabled">
  </dsfr-ext-autocomplete> `;

export const Combo: Story = {
  args: {
    ...Default.args,
    buttonIcon: 'fr-icon-close-line',
    inputWrapMode: 'addon',
    buttonDisabled: false,
    buttonVariant: 'secondary',
  },
  render: (args) => ({
    props: args,
    template: templateAutocompleteCombo,
  }),
};

export const VirtualScroll: Story = {
  args: {
    ...Default.args,
    virtualScroll: true,
    autoFilter: true,
    hint: 'hint',
  },
  render: (args) => ({
    props: args,
    template: templateAutocomplete,
  }),
};

const OBJECTS = [
  { value: 'Apricot', color: 'orange', category: 'Fruit' },
  { value: 'Bell pepper', color: 'green', category: 'Vegetable' },
  { value: 'Carolina reaper', color: 'red', category: 'Vegetable' },
  { value: 'Chili pepper', color: 'red', category: 'Vegetable' },
  { value: 'Corn kernel', color: 'yellow', category: 'Vegetable' },
  { value: 'Cucumber', color: 'green', category: 'Vegetable' },
  { value: 'Eggplant', color: 'purple', category: 'Vegetable' },
  { value: 'Grapefruit', color: 'yellow', category: 'Fruit' },
  { value: 'Jalapeño', color: 'green', category: 'Vegetable' },
  { value: 'Mango', color: 'orange', category: 'Fruit' },
  { value: 'Melon', color: 'green', category: 'Fruit' },
  { value: 'Olive', color: 'green', category: 'Fruit' },
  { value: 'Pea', color: 'green', category: 'Vegetable' },
  { value: 'Pumpkin', color: 'orange', category: 'Vegetable' },
  { value: 'Squash', color: 'yellow', category: 'Vegetable' },
  { value: 'Tomato', color: 'red', category: 'Fruit' },
  { value: 'Zucchini', color: 'green', category: 'Vegetable' },
  { value: 'Apple', color: 'red', category: 'Fruit' },
  { value: 'Banana', color: 'yellow', category: 'Fruit' },
  { value: 'Blueberry', color: 'blue', category: 'Fruit' },
  { value: 'Broccoli', color: 'green', category: 'Vegetable' },
  { value: 'Cabbage', color: 'green', category: 'Vegetable' },
  { value: 'Carrot', color: 'orange', category: 'Vegetable' },
];

const templateWithSlot = `
  <dsfr-ext-autocomplete
  [label]="label"
  [suggestions]="suggestions"
  [autoFilter]="autoFilter"
  [(ngModel)]="value"
  [appendTo]="appendTo"
  suggestionValueKey="value"
  [zIndex]="zIndex"
  [disabled]="disabled">
    <ng-template #suggestionTemplate let-suggestion> 
      <div [ngStyle]="{'padding': '3px'}">   <span [ngStyle]="{
      'background-color': suggestion.color,
      'width': '12px',
      'height': '12px',
      'border-radius': '50%',
      'display': 'inline-block',
      'margin-right': '10px'
    }"></span>
      <b>{{suggestion.category}} </b>: {{suggestion.value}}   </div>
    </ng-template>
  </dsfr-ext-autocomplete> `;

export const Slot: Story = {
  args: {
    ...Default.args,
    suggestions: OBJECTS,
    virtualScroll: true,
    autoFilter: true,
  },
  render: (args) => ({
    props: { ...args },
    template: templateWithSlot,
  }),
};

const templateForFormat = `
  <dsfr-ext-autocomplete
  [label]="label"
  [suggestions]="suggestions"
  [autoFilter]="autoFilter"
  [(ngModel)]="value"
  [formatOnSelect]="formatOnSelect">
    <ng-template #suggestionTemplate let-suggestion> 
      <b>{{suggestion.category}} </b>: {{suggestion.value}}
    </ng-template>
  </dsfr-ext-autocomplete> `;

const formatSelectionFn = (o: any) => {
  return o.category + ' - ' + o.value;
};

export const FormatSelection: Story = {
  args: {
    ...Default.args,
    suggestions: OBJECTS,
    formatOnSelect: formatSelectionFn,
    suggestionValueKey: 'category',
  },
  render: (args) => ({
    props: args,
    template: templateForFormat,
  }),
};

export const RequireSelection: Story = {
  args: {
    ...Default.args,
    requireSelection: true,
  },
  render: (args) => ({
    props: args,
    template: templateAutocomplete,
  }),
};
