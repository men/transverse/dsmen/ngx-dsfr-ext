import { CommonModule } from '@angular/common';
import { Component, Input } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, FormsModule, ReactiveFormsModule, Validators } from '@angular/forms';
import { DsfrAccordionModule, DsfrButtonModule, DsfrButtonsGroupModule } from '@edugouvfr/ngx-dsfr';
import { delay, finalize, Observable, of } from 'rxjs';
import { DsfrAutocompleteComponent } from '../autocomplete.component';
import { DsfrCompleteEvent } from '../autocomplete.model';

@Component({
  selector: `demo-autocomplete`,
  templateUrl: './demo-autocomplete.component.html',
  standalone: true,
  imports: [
    CommonModule,
    FormsModule,
    DsfrAutocompleteComponent,
    DsfrAccordionModule,
    ReactiveFormsModule,
    DsfrButtonsGroupModule,
    DsfrButtonModule,
  ],
})
export class DemoAutocompleteComponent {
  @Input() minLength: number;
  @Input() appendTo: 'body' | HTMLElement | string | undefined;

  /** @internal */ @Input() selectedItem1: string | undefined = 'Bolivia';
  /** @internal */ @Input() selectedItem2: string = 'Bolivia';
  /** @internal */ @Input() selectedItem3: string = '';
  /** @internal */ @Input() displayAccordion: boolean;
  /** @internal */ @Input() async: boolean;

  /** @internal */ @Input() suggestions1: any[];
  /** @internal */ @Input() suggestions2: string[];
  /** @internal */ @Input() suggestions3: Observable<string[]> = of([]);

  /** @internal */ loading = true;

  /** @internal */
  form: FormGroup;

  constructor(private fb: FormBuilder) {
    this.form = this.fb.group({
      fruits: [{ value: 'Apricot', color: 'orange', category: 'Fruit' }, { nonNullable: true }, Validators.required],
    });
  }

  /** @internal */
  get formControl(): FormControl {
    return this.form.get('fruits') as FormControl;
  }

  /** @internal */
  search1(event: DsfrCompleteEvent) {
    const prefix = event?.query;
    const value = prefix?.toLowerCase();
    this.suggestions1 = OBJECTS.filter((a) => a.value.toLowerCase().includes(value));
  }

  /** @internal */
  search2(event: DsfrCompleteEvent) {
    const prefix = event?.query;
    const value = prefix?.toLowerCase();
    this.suggestions2 = COUNTRIES.filter((country) => country.toLowerCase().startsWith(value));
    console.log(this.suggestions2);
  }

  /** @internal */
  onModelChange2(value: any) {
    this.selectedItem2 = value;
  }

  // simulation requête asynchrone
  searchAsync(event: DsfrCompleteEvent) {
    const value = event?.query?.toLowerCase();
    this.loading = true;
    this.suggestions3 = of(COUNTRIES.filter((country) => country.toLowerCase().startsWith(value))).pipe(
      delay(200),
      finalize(() => (this.loading = false)),
    );
  }

  onClosed() {
    console.log('Liste fermée');
  }

  onSuggestionSelect(s: any) {
    console.log('Suggestion sélectionnée : ' + s);
  }

  resetControl() {
    this.formControl.reset();
  }

  disable() {
    this.form.disable();
  }
}

const OBJECTS = [
  { value: 'Apricot', color: 'orange', category: 'Fruit' },
  { value: 'Bell pepper', color: 'green', category: 'Vegetable' },
  { value: 'Carolina reaper', color: 'red', category: 'Vegetable' },
  { value: 'Chili pepper', color: 'red', category: 'Vegetable' },
  { value: 'Corn kernel', color: 'yellow', category: 'Vegetable' },
  { value: 'Cucumber', color: 'green', category: 'Vegetable' },
  { value: 'Eggplant', color: 'purple', category: 'Vegetable' },
  { value: 'Grapefruit', color: 'yellow', category: 'Fruit' },
  { value: 'Jalapeño', color: 'green', category: 'Vegetable' },
  { value: 'Mango', color: 'orange', category: 'Fruit' },
  { value: 'Melon', color: 'green', category: 'Fruit' },
  { value: 'Olive', color: 'green', category: 'Fruit' },
  { value: 'Pea', color: 'green', category: 'Vegetable' },
  { value: 'Pumpkin', color: 'orange', category: 'Vegetable' },
  { value: 'Squash', color: 'yellow', category: 'Vegetable' },
  { value: 'Tomato', color: 'red', category: 'Fruit' },
  { value: 'Zucchini', color: 'green', category: 'Vegetable' },
  { value: 'Apple', color: 'red', category: 'Fruit' },
  { value: 'Banana', color: 'yellow', category: 'Fruit' },
  { value: 'Blueberry', color: 'blue', category: 'Fruit' },
  { value: 'Broccoli', color: 'green', category: 'Vegetable' },
  { value: 'Cabbage', color: 'green', category: 'Vegetable' },
  { value: 'Carrot', color: 'orange', category: 'Vegetable' },
];
const COUNTRIES = [
  'Afghanistan',
  'Albania',
  'Algeria',
  'Andorra',
  'Angola',
  'Anguilla',
  'Antigua et Barbuda',
  'Argentina',
  'Armenia',
  'Aruba',
  'Australia',
  'Austria',
  'Azerbaijan',
  'Bahamas',
  'Bahrain',
  'Bangladesh',
  'Barbados',
  'Belarus',
  'Belgium',
  'Belize',
  'Benin',
  'Bermuda',
  'Bhutan',
  'Bolivia',
  'Bosnia et Herzegovina',
  'Botswana',
  'Brazil',
  'British Virgin Islands',
  'Brunei',
  'Bulgaria',
  'Burkina Faso',
  'Burundi',
  'Cambodia',
  'Cameroon',
  'Canada',
  'Cape Verde',
  'Cayman Islands',
  'Central Arfrican Republic',
  'Chad',
  'Chile',
  'China',
  'Colombia',
  'Congo',
  'Cook Islands',
  'Costa Rica',
  'Cote D Ivoire',
  'Croatia',
  'Cuba',
  'Curacao',
  'Cyprus',
  'Czech Republic',
  'Denmark',
  'Djibouti',
  'Dominica',
  'Dominican Republic',
  'Ecuador',
  'Egypt',
  'El Salvador',
  'Equatorial Guinea',
  'Eritrea',
  'Estonia',
  'Ethiopia',
  'Falkland Islands',
  'Faroe Islands',
  'Fiji',
  'Finland',
  'France',
  'French Polynesia',
  'French West Indies',
  'Gabon',
  'Gambia',
  'Georgia',
  'Germany',
  'Ghana',
  'Gibraltar',
  'Greece',
  'Greenland',
  'Grenada',
  'Guam',
  'Guatemala',
  'Guernsey',
  'Guinea',
  'Guinea Bissau',
  'Guyana',
  'Haiti',
  'Honduras',
  'Hong Kong',
  'Hungary',
  'Iceland',
  'India',
  'Indonesia',
  'Iran',
  'Iraq',
  'Ireland',
  'Isle of Man',
  'Israel',
  'Italy',
  'Jamaica',
  'Japan',
  'Jersey',
  'Jordan',
  'Kazakhstan',
  'Kenya',
  'Kiribati',
  'Kosovo',
  'Kuwait',
  'Kyrgyzstan',
  'Laos',
  'Latvia',
  'Lebanon',
  'Lesotho',
  'Liberia',
  'Libya',
  'Liechtenstein',
  'Lithuania',
  'Luxembourg',
  'Macau',
  'Macedonia',
  'Madagascar',
  'Malawi',
  'Malaysia',
  'Maldives',
  'Mali',
  'Malta',
  'Marshall Islands',
  'Mauritania',
  'Mauritius',
  'Mexico',
  'Micronesia',
  'Moldova',
  'Monaco',
  'Mongolia',
  'Montenegro',
  'Montserrat',
  'Morocco',
  'Mozambique',
  'Myanmar',
  'Namibia',
  'Nauro',
  'Nepal',
  'Netherlands',
  'Netherlands Antilles',
  'New Caledonia',
  'New Zealand',
  'Nicaragua',
  'Niger',
  'Nigeria',
  'North Korea',
  'Norway',
  'Oman',
  'Pakistan',
  'Palau',
  'Palestine',
  'Panama',
  'Papua New Guinea',
  'Paraguay',
  'Peru',
  'Philippines',
  'Poland',
  'Portugal',
  'Puerto Rico',
  'Qatar',
  'Reunion',
  'Romania',
  'Russia',
  'Rwanda',
  'Saint Pierre et Miquelon',
  'Samoa',
  'San Marino',
  'Sao Tome and Principe',
  'Saudi Arabia',
  'Senegal',
  'Serbia',
  'Seychelles',
  'Sierra Leone',
  'Singapore',
  'Slovakia',
  'Slovenia',
  'Solomon Islands',
  'Somalia',
  'South Africa',
  'South Korea',
  'South Sudan',
  'Spain',
  'Sri Lanka',
  'St Kitts et Nevis',
  'St Lucia',
  'St Vincent',
  'Sudan',
  'Suriname',
  'Swaziland',
  'Sweden',
  'Switzerland',
  'Syria',
  'Taiwan',
  'Tajikistan',
  'Tanzania',
  'Thailand',
  "Timor L'Este",
  'Togo',
  'Tonga',
  'Trinidad et Tobago',
  'Tunisia',
  'Turkey',
  'Turkmenistan',
  'Turks et Caicos',
  'Tuvalu',
  'Uganda',
  'Ukraine',
  'United Arab Emirates',
  'United Kingdom',
  'United States of America',
  'Uruguay',
  'Uzbekistan',
  'Vanuatu',
  'Vatican City',
  'Venezuela',
  'Vietnam',
  'Virgin Islands (US)',
  'Yemen',
  'Zambia',
  'Zimbabwe',
];
