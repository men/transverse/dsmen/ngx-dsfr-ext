import { Meta, moduleMetadata, StoryObj } from '@storybook/angular';
import { controlDisabled } from '../../../../../../.storybook/storybook-utils';
import { DemoAutocompleteComponent } from './demo-autocomplete.component';

const meta: Meta<DemoAutocompleteComponent> = {
  title: 'Components/Autocomplete',
  component: DemoAutocompleteComponent,
  decorators: [moduleMetadata({ imports: [] })],
  parameters: { actions: false },
};
export default meta;
type Story = StoryObj<DemoAutocompleteComponent>;

export const ReactiveForm: Story = {
  args: {
    minLength: 1,
    appendTo: undefined,
    displayAccordion: false,
    async: false,
  },
  argTypes: {
    appendTo: controlDisabled,
    displayAccordion: controlDisabled,
  },
};

export const Asynchronous: Story = {
  args: {
    minLength: 1,
    appendTo: 'body',
    displayAccordion: false,
    async: true,
  },
  argTypes: {
    displayAccordion: controlDisabled,
    async: controlDisabled,
  },
};

export const InsideContainer: Story = {
  args: {
    minLength: 1,
    appendTo: 'body',
    displayAccordion: true,
  },
  argTypes: {
    appendTo: { control: { type: 'inline-radio' }, options: [undefined, 'body'] },
    displayAccordion: controlDisabled,
  },
};
