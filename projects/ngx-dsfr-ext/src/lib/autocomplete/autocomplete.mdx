import { Canvas, Meta, Controls, Story } from '@storybook/blocks';
import * as DemoAutocompleteStories from './~demo/demo-autocomplete.stories';
import * as Stories from './autocomplete.stories';

<Meta of={Stories} title="Autocomplete" />

<h1> Autocomplete </h1>

Le composant `dsfr-ext-autocomplete` est un champ de saisie qui fournit des suggestions en temps réel lors de la saisie
(_saisie semi-automatique_).

- _Module_ : standalone
- _Composant_ : `DsfrAutocompleteComponent`
- _Tags_ : `dsfrx-autocomplete, dsfr-ext-autocomplete`

<h2 id="fonctionnement"> Fonctionnement </h2>

Le composant est constitué d'un input `<dsfr-form-input>` et d'une liste de suggestions.

- Pour afficher l'input, il bénéficie des entrées et des possiiblité de style du composant `<dsfr-form-input>` (messages d'information, icône, bouton combo etc.)
- La liste des valeurs possibles est donnée par la propriété `suggestions`.
- Il utilise `ngModel` pour la liaison bidirectionnelle à travers l'interface `ControlValueAccessor`. Sa valeur est de type `string` si elle correspond au texte entré par l'utilisateur, ou du type des suggestions en cas de sélection de celles-ci.

<h3 id="filter"> Filtre des données </h3>

- L'événement `filterChange` est émis à chaque modification de la saisie.
- Il faut mettre à jour la propriété `suggestions` en réponse à cet événement.
- Le paramètre `event` de l'événement permet d'obtenir la saisie de l'utilisateur : `event.query`.

```html
<dsfrx-autocomplete
  [(ngModel)]="selectedItem"
  [suggestions]="suggestions"
  (filterChange)="onSearch($event)"></dsfrx-autocomplete>
```

Le minimum de caractères nécessaires au déclenchement des suggestions est indiqué par la propriété `minLength` (1 par défaut).
Si `minLength` est à 0, la liste s'ouvrira au focus sur le champ.

```html
<dsfrx-autocomplete
  minLength="2"
  [(ngModel)]="selectedItem"
  [suggestions]="suggestions"
  (filterChange)="onSearch($event)"></dsfrx-autocomplete>
```

<h3 id="auto-filter-doc"> Utilisation avec objets </h3>

La liste de suggestions peut être soit une liste de `string` soit une liste d'objets.
Dans ce deuxième cas, il faut alors indiquer sur quelle propriété sera bindée la valeur de l'autocomplete en utilisant `suggestionValueKey`.
Ce sera cette valeur que l'autocomplete renvoie à la sélection d'une suggestion et sur laquelle il se base en mode `autofilter`.

`suggestionValueKey` doit correspondre à des valeurs de type `string`.

La valeur de l'autocomplete correspondra alors un objet dans le cas de la sélection d'une suggestion.
L'évènement `suggestionSelect` permet également de récupérer l'objet suggestion qui a été sélectionné.

```html
<dsfrx-autocomplete
  suggestionValueKey="label"
  (suggestionSelect)="onSuggestionSelect($event)"
  [(ngModel)]="selectedItem"
  [suggestions]="suggestions"></dsfrx-autocomplete>
```

```typescript
  suggestions = [
    { value: '36000', label: '36000 - Châteauroux'}
    { value: '37000', label: '37000 - Tours'},
    { value: '38000', label: '38000 - Grenoble'},
  ]
```

<h3 id="auto-filter-doc"> Sélection requise </h3>

Si on ne veut autoriser que les valeurs présentes dans la liste des suggestions (notamment si on utilise des objets), on peut utiliser `requireSelection = true`.

Le champ de saisie sera alors vidé à la fermeture de la liste si la valeur entrée ne correspond a aucune suggestion.
La comparaison avec les suggestions est faite selon ce qui est passé en paramètre: la valeur `string` simple, `suggestionValueKey` ou le résultat de `formatSelection`.

🔥 👓 Le comportement de l'autocomplete se rapproche alors de celui d'un `select`. Il est préférable pour l'accessibilité de ne pas détourner l'autocomplete pour effectuer une sélection mais d'utiliser les composants dédiés à la sélection (`dsfr-form-select` ou `dsfrx-multiselect` en mode `single`).

<h3 id="auto-filter-doc"> Filtre automatique </h3>

Si la liste de suggestions est une liste de `string` ou une liste d'objets dont la valeur spécifiée dans `suggestionValueKey` est une `string`, il est possible de laisser le composant gérer seul le filtre des
données en positionnant `autoFilter=true`.

```html
<dsfrx-autocomplete
  minLength="2"
  [(ngModel)]="selectedItem"
  [suggestions]="suggestions"
  [autoFilter]="true"></dsfrx-autocomplete>
```

Dans ce cas :

- L'événement `filterChange` n'est pas émis,
- Le filtre se fait sur le contenu du texte.

<h3 id="accessibility"> Accessibilité </h3>

👓 L'autocomplete respecte le pattern défini par le WAI sur les éléments de type combobox éditable : https://www.w3.org/WAI/ARIA/apg/patterns/combobox/

** Navigation au clavier **
Le focus reste positionné sur l'input, tandis que les flêches haut/bas permettent de naviguer dans la liste des suggestions.
La touche `entrée` valide la suggestion courante (définie en aria-activedescendant), et `échap` ferme la liste sans valider.

<h2 id="preview"> Aperçu </h2>

Par défaut, le composant ne filtre pas la liste de suggestions.

<Canvas of={Stories.Default} />

<h2 id="api"> API </h2>

<Controls />

<h2> Exemples </h2>

<h3 id="auto-filter"> Filtre automatique </h3>

<Canvas of={Stories.AutoFilter} />

<h3 id="reactive-form"> Reactive Form </h3>

Il peut être utilisé avec des reactives forms. Dans ce cas, la propriété `formControlName` est utilisée pour
lier le composant à un contrôle de formulaire.

```html
<form [formGroup]="formGroup">
  <dsfrx-autocomplete
    label="Sélectionnez un fruit"
    formControlName="selectedCountry"
    suggestionValueKey="value"
    [suggestions]="suggestions"
    [message]="formControl.dirty && formControl.errors?.['required'] ? 'Champ obligatoire.' : ''"
    [messageSeverity]="formControl.errors ? 'error' : 'info'"
    (filterChange)="onSearch($event)"></dsfrx-autocomplete>
</form>
```

<Story of={DemoAutocompleteStories.ReactiveForm} />

<h3 id="virtualScroll"> Virtual scrolling </h3>

Pour améliorer les performances en cas de nombre très important de suggestions, on peut utiliser l'option `virtualScroll`.
Cette option permet d'ajouter seulement au fur et à mesure dans le viewport les éléments rendus visibles par le scroll,
en utilisant la librairie `@angular/cdk` pour le scrolling.
La hauteur des options n'est pas recalculée dynamiquement avec l'utilisation du virtual scroll, toutes les options doivent donc faire la même hauteur.
Une hauteur par défaut est définie (24px) et peut être surchargée avec l'option `virtualScrollItemHeight`.

<Canvas of={Stories.VirtualScroll} />

<h3 id="slot"> Avec slot </h3>

Il est possible de personnaliser l'affichage des suggestions avec le slot `suggestionTemplate`. Celui-ci permet d'avoir de récupérer une référence de l'objet `suggestion`. Au niveau du DOM, le slot sera passé à l'intérieur de chaque `<li>`.

```html
<dsfr-ext-autocomplete [label]="label" [(ngModel)]="value" [suggestions]="suggestions" suggestionValue="value">
  <ng-template #suggestionTemplate let-suggestion> <b>{{suggestion.category}} </b>: {{suggestion.value}} </ng-template>
</dsfr-ext-autocomplete>
```

<Canvas of={Stories.Slot} />

<h3 id="format-select"> Formater la sélection </h3>

```typescript

 public formatSelectionFn(o: any): string {
  return o.category + ' - ' + o.value;
};

```

<Canvas of={Stories.FormatSelection} />

<h3 id="asynchronous"> Asynchrone </h3>

Les résultats de suggestions peuvent être chargées de manière asynchrone, typiquement suite à une requête coté serveur.
Dans ce cas, on peut afficher un spinner de chargement le temps de récupération des données avec la propriété `loading`.
Il est alors conseillé d'utiliser `delay` qui permet d'avoir un temps d'attente entre les entrées successives (debounce), évitant ainsi l'emission trop fréquente d'évènements.

```html
<dsfrx-autocomplete
  label="Sélectionnez un pays"
  [loading]="loading"
  [delay]="500"
  [suggestions]="(suggestions | async) ?? []"
  [(ngModel)]="myModel"
  (filterChange)="searchAsync($event)"></dsfrx-autocomplete>
```

```typescript
  public myModel: string = '';
  public suggestions: Observable<string[]> = of([]);
  public loading: boolean;

  // simulation requête asynchrone
  public searchAsync(event: DsfrCompleteEvent) {
    const value = event?.query?.toLowerCase();
    this.loading = true;
    this.suggestions = of(COUNTRIES.filter((country) => country.toLowerCase().startsWith(value))).pipe(
      delay(1000),
      finalize(() => (this.loading = false)),
    );
  }
```

<Canvas of={DemoAutocompleteStories.Asynchronous} />

<h3 id="inside-container"> Attacher à un élément </h3>

Si l'on souhaite utiliser l'élément à l'interieur d'un conteneur qui ne peut contenir toute la liste déroulante (ex. un élément défini en `overflow:hidden`), on peut utiliser l'attribut `appendTo`
La liste déroulante sera alors directement attachée au body, ce qui permettra de la visualiser entièrement.

Exemple à l'intérieur d'un accordéon :

<Canvas of={DemoAutocompleteStories.InsideContainer} />
