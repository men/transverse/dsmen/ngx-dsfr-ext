// Cf. PrimeNG::AutoCompleteCompleteEvent
export interface DsfrCompleteEvent {
  /** Browser event. */
  originalEvent: Event;

  /** Selected option value. */
  query: string;
}

export type DsfrSuggestions = any[];
