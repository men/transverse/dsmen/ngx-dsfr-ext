import { Component, ViewChild } from '@angular/core';
import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { FormsModule } from '@angular/forms';
import { DsfrAutocompleteComponent } from './autocomplete.component';

@Component({
  selector: `edu-host-component`,
  template: `<dsfr-ext-autocomplete
    [suggestions]="suggestions"
    [minLength]="1"
    [autoFilter]="false"></dsfr-ext-autocomplete>`,
})
class TestHostComponent {
  @ViewChild(DsfrAutocompleteComponent)
  public autocompleteComponent: DsfrAutocompleteComponent;
  suggestions = [
    'Apple',
    'Apricot',
    'Bell pepper',
    'Carolina reaper',
    'Chili pepper',
    'Corn kernel',
    'Cucumber',
    'Eggplant',
    'Grapefruit',
    'Jalapeño',
    'Mango',
  ];
}

describe('DsfrAutocompleteComponent', () => {
  let testHostComponent: TestHostComponent;
  let fixture: ComponentFixture<TestHostComponent>;

  function mockInit() {
    testHostComponent.autocompleteComponent.ngOnInit();
    testHostComponent.autocompleteComponent.ngAfterViewInit();
    fixture.detectChanges();
  }

  function mockInputChange(value: string) {
    window.HTMLElement.prototype.scrollIntoView = jest.fn(); // mock scroll into view function
    testHostComponent.autocompleteComponent.valueInput = value;
    const inputContainer = fixture.nativeElement.querySelector('dsfr-form-input');
    inputContainer.dispatchEvent(new Event('input'));
    tick();
    fixture.detectChanges();
  }

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [FormsModule, DsfrAutocompleteComponent],
      declarations: [TestHostComponent],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TestHostComponent);
    testHostComponent = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should put id test ', () => {
    testHostComponent.autocompleteComponent.inputId = 'test';
    mockInit();

    const inputEl = fixture.nativeElement.querySelector('.fr-input');
    expect(inputEl.getAttribute('id')).toEqual('test');
  });

  it('should display suggestions list ', fakeAsync(() => {
    testHostComponent.autocompleteComponent.inputId = 'test';
    mockInit();
    const input = fixture.nativeElement.querySelector('.fr-input');
    expect(input.getAttribute('aria-expanded')).toBe('false');

    mockInputChange('o');

    // Expect the aria-expanded attribute to be true
    expect(input.getAttribute('aria-expanded')).toBe('true');
    expect(input.getAttribute('role')).toEqual('combobox');
    expect(input.getAttribute('minLength')).toEqual('1');
    expect(input.getAttribute('aria-controls')).toEqual('listbox-test');

    // Check if the suggestions list is displayed
    const list = fixture.nativeElement.querySelector('ul');
    expect(list.children).toHaveLength(11);
  }));

  it('should not display suggestions list if input is empty', fakeAsync(() => {
    mockInit();
    const input = fixture.nativeElement.querySelector('.fr-input');
    expect(input.getAttribute('aria-expanded')).toBe('false');
    mockInputChange('');

    // Expect the aria-expanded attribute to stay false
    expect(input.getAttribute('aria-expanded')).toBe('false');

    // Check if the suggestions list is displayed
    const list = fixture.nativeElement.querySelector('ul');
    expect(list).toBeFalsy();
  }));

  it('should filter suggestions with autoFilter', fakeAsync(() => {
    testHostComponent.autocompleteComponent.autoFilter = true;
    mockInit();
    mockInputChange('pepper');

    const list = fixture.nativeElement.querySelector('ul');
    expect(list.children).toHaveLength(2);
  }));

  it('should filter with suggestions as objects', fakeAsync(() => {
    testHostComponent.autocompleteComponent.suggestions = [
      { name: 'Apple', type: 'Fruit' },
      { name: 'Apricot', type: 'Fruit' },
      { name: 'Bell pepper', type: 'Fruit' },
    ];
    testHostComponent.autocompleteComponent.autoFilter = true;
    testHostComponent.autocompleteComponent.suggestionValueKey = 'name';

    mockInit();
    mockInputChange('pepper');

    const list = fixture.nativeElement.querySelector('ul');
    expect(list.children).toHaveLength(1);
    expect(list.children[0].textContent).toContain('Bell pepper');
  }));

  it('should set correct value with suggestions as objects', fakeAsync(() => {
    // GIVEN
    testHostComponent.autocompleteComponent.suggestions = [
      { name: 'Apple', type: 'Fruit' },
      { name: 'Apricot', type: 'Fruit' },
      { name: 'Bell pepper', type: 'Fruit' },
    ];
    testHostComponent.autocompleteComponent.autoFilter = true;
    testHostComponent.autocompleteComponent.suggestionValueKey = 'name';

    //WHEN
    mockInit();
    mockInputChange('p');
    const list = fixture.nativeElement.querySelector('ul');
    list.children[1].click();
    tick();
    fixture.detectChanges();

    // THEN
    expect(testHostComponent.autocompleteComponent.valueInput).toEqual('Apricot');
    expect(testHostComponent.autocompleteComponent.value).toEqual({ name: 'Apricot', type: 'Fruit' });
  }));

  it('should display spinner only on loading', fakeAsync(() => {
    testHostComponent.autocompleteComponent.loading = true;
    mockInit();
    mockInputChange('o');

    expect(fixture.nativeElement.querySelector('dsfr-ext-spinner')).toBeTruthy();

    testHostComponent.autocompleteComponent.loading = false;
    fixture.detectChanges();
    expect(fixture.nativeElement.querySelector('dsfr-ext-spinner')).toBeFalsy();
  }));

  it('should select the second option on click', fakeAsync(() => {
    mockInit();
    mockInputChange('o');

    const inputContainer = fixture.nativeElement.querySelector('dsfr-form-input');
    const list = fixture.nativeElement.querySelector('ul');
    list.children[1].click();
    tick();
    fixture.detectChanges();

    expect(testHostComponent.autocompleteComponent.value).toEqual('Apricot');
    expect(list.children[1].getAttribute('aria-selected')).toEqual('true');
    expect(list.children[0].getAttribute('aria-selected')).toEqual('false');
    expect(fixture.nativeElement.querySelector('ul')).toBeFalsy();
    expect(inputContainer.querySelector('.fr-input').getAttribute('aria-expanded')).toBe('false');
  }));

  it('should select the second option on enter with keyboard navigation', fakeAsync(() => {
    testHostComponent.autocompleteComponent.inputId = 'test';
    mockInit();
    mockInputChange('o');
    const inputContainer = fixture.nativeElement.querySelector('dsfr-form-input');
    const input = inputContainer.querySelector('.fr-input');

    inputContainer.dispatchEvent(new KeyboardEvent('keydown', { key: 'ArrowDown' }));
    inputContainer.dispatchEvent(new KeyboardEvent('keydown', { key: 'ArrowDown' }));
    tick();
    fixture.detectChanges();

    // set active-descendant to second option (focus )
    expect(input.getAttribute('aria-activedescendant')).toEqual('test-option-1');
    inputContainer.dispatchEvent(new KeyboardEvent('keydown', { key: 'Enter' }));
    fixture.detectChanges();
    expect(testHostComponent.autocompleteComponent.value).toEqual('Apricot');
  }));

  it('should format the input when formatOnSelect is set', fakeAsync(() => {
    // GIVEN
    testHostComponent.autocompleteComponent.formatOnSelect = (o) => 'test: ' + o;
    mockInit();
    mockInputChange('oun');

    // WHEN
    const list = fixture.nativeElement.querySelector('ul');
    list.children[1].click();
    tick();
    fixture.detectChanges();

    // THEN
    expect(testHostComponent.autocompleteComponent.valueInput).toEqual('test: Apricot');
    expect(testHostComponent.autocompleteComponent.value).toEqual('Apricot');
  }));

  it('should clear the input when requireSelection is true and no matching suggestion exists', fakeAsync(() => {
    // GIVEN
    testHostComponent.autocompleteComponent.requireSelection = true;
    mockInit();
    mockInputChange('oun');

    // WHEN
    const inputContainer = fixture.nativeElement.querySelector('dsfr-form-input');
    inputContainer.dispatchEvent(new KeyboardEvent('keydown', { key: 'Escape' }));
    tick();
    fixture.detectChanges();

    // THEN
    expect(inputContainer.querySelector('.fr-input').getAttribute('aria-expanded')).toBe('false');
    expect(testHostComponent.autocompleteComponent.valueInput).toEqual('');
    expect(testHostComponent.autocompleteComponent.value).toEqual(undefined);
  }));

  it('should keep input value when requireSelection is true and a matching suggestion exists', fakeAsync(() => {
    // GIVEN
    testHostComponent.autocompleteComponent.requireSelection = true;
    mockInit();
    mockInputChange('Bell pepper');

    // WHEN
    const inputContainer = fixture.nativeElement.querySelector('dsfr-form-input');
    inputContainer.dispatchEvent(new KeyboardEvent('keydown', { key: 'Escape' }));
    tick();
    fixture.detectChanges();

    // THEN
    expect(inputContainer.querySelector('.fr-input').getAttribute('aria-expanded')).toBe('false');
    expect(testHostComponent.autocompleteComponent.valueInput).toEqual('Bell pepper');
    expect(testHostComponent.autocompleteComponent.value).toEqual('Bell pepper');
  }));
});
