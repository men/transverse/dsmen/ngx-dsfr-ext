import test, { Locator, expect } from '@playwright/test';
import { BasePageObjectModel, testTabNavigation } from 'projects/ngx-dsfr-ext/shared/e2e.utils';

test.describe('Date picker', () => {
  let datePicker: DatePickerPom;

  test.beforeEach(({ page }) => {
    datePicker = new DatePickerPom(page, 'date-picker');
  });

  test.describe('properly displays the initial date', () => {
    test('when using template-driven form', async () => {
      await datePicker.goToPage('initial-value&args=value:2023-02-02');

      await expect(datePicker.nativeInput).toHaveValue('02/02/2023');
    });

    test('when using reactive form', async () => {
      await datePicker.goToPage('reactive-form&args=date:2023-02-02');

      const nativeInput = datePicker.datePickerInput.first().locator('.fr-input');

      await expect(nativeInput).toHaveValue('02/02/2023');
    });
  });

  test.describe('focuses', () => {
    test.beforeEach(async () => {
      await datePicker.goToPage();
    });

    test('the current date in grid when opened', async () => {
      await datePicker.openDatePicker();

      await expect(datePicker.dialog).toBeVisible();
      await expect(datePicker.currentDayInGrid).toBeFocused();

      const dateNumberSpan = datePicker.currentDayInGrid.locator('span:not(.fr-sr-only)');
      const currentDate = new Date().getDate().toString();
      await expect(dateNumberSpan).toHaveText(currentDate);
    });

    test('the provided date when opened', async () => {
      await datePicker.nativeInput.focus();
      await datePicker.openDatePickerToDate('17/07/2024');

      await expect(datePicker.dialog).toBeVisible();
      await expect(datePicker.currentDayInGrid).toBeFocused();
      await expect(datePicker.currentDayInGrid).toHaveText('Mercredi 17');
    });
  });

  test.describe('handles navigation', () => {
    test.beforeEach(async () => {
      await datePicker.goToPage();
      await datePicker.openDatePickerToDate('17/07/2024');
    });

    test('with arrow keys in date grid', async () => {
      const gridNavigationSequence = [
        {
          key: 'ArrowUp',
          expectedText: 'Mercredi 10',
        },
        {
          key: 'ArrowLeft',
          expectedText: 'Mardi 9',
        },
        {
          key: 'ArrowDown',
          expectedText: 'Mardi 16',
        },
        {
          key: 'ArrowRight',
          expectedText: 'Mercredi 17',
        },
      ];

      for (let navigationStep of gridNavigationSequence) {
        await datePicker.currentDayInGrid.press(navigationStep.key);
        await expect(datePicker.currentDayInGrid).toHaveText(navigationStep.expectedText);
      }
    });

    test('with tab through the dialog', async () => {
      const standardSequence: Locator[] = [
        datePicker.cancelBtn,
        datePicker.prevMonthBtn,
        datePicker.monthSelectNativeLocator,
        datePicker.yearSelectNativeLocator,
        datePicker.nextMonthBtn,
        datePicker.currentDayInGrid,
      ];

      await testTabNavigation(standardSequence, datePicker.dialog);
    });

    test('with shift-tab through the dialog', async () => {
      const reverseSequence: Locator[] = [
        datePicker.nextMonthBtn,
        datePicker.yearSelectNativeLocator,
        datePicker.monthSelectNativeLocator,
        datePicker.prevMonthBtn,
        datePicker.cancelBtn,
        datePicker.currentDayInGrid,
      ];

      await testTabNavigation(reverseSequence, datePicker.dialog, true);
    });
  });

  test('let the user chose a date with its grid', async () => {
    await datePicker.goToPage();

    await datePicker.openDatePickerToDate('17/07/2024');
    await datePicker.dialog.press('ArrowUp');
    await datePicker.dialog.press('Enter');

    await expect(datePicker.nativeInput).toHaveValue('10/07/2024');
    await expect(datePicker.dialog).toBeHidden();
  });

  test.describe('handles minimum dates', () => {
    test.beforeEach(async () => {
      await datePicker.goToPage('min');
    });

    test('by reducing the options available in the year select', async () => {
      await datePicker.openDatePicker();

      const currentYear = new Date().getFullYear().toString();
      const options = await datePicker.yearSelectNativeLocator.getByRole('option').all();
      await expect(options[0]).toHaveText(currentYear);
    });

    test('by focusing on the first possible date when the min date is posterior to a given date', async () => {
      await datePicker.sbMinIput.fill('"2024-10-02"');
      await datePicker.openDatePickerToDate('17/07/2024');

      const sequenceWithoutGrid: Locator[] = [
        datePicker.cancelBtn,
        datePicker.prevMonthBtn,
        datePicker.monthSelectNativeLocator,
        datePicker.yearSelectNativeLocator,
        datePicker.nextMonthBtn,
        datePicker.currentDayInGrid,
        datePicker.cancelBtn,
        datePicker.prevMonthBtn,
      ];

      await testTabNavigation(sequenceWithoutGrid, datePicker.dialog);
    });
  });

  test.describe('handles maximum dates', () => {
    test.beforeEach(async () => {
      await datePicker.goToPage('max');
    });

    test('by reducing the options available in the year select', async () => {
      const currentYear = new Date().getFullYear().toString();

      await datePicker.openDatePicker();

      const options = await datePicker.yearSelectNativeLocator.getByRole('option').all();
      await expect(options[options.length - 1]).toHaveText(currentYear);
    });

    test('by focusing on the last possible date when the max date is anterior to a given date', async () => {
      await datePicker.sbMaxInput.fill('"2024-02-02"');
      await datePicker.openDatePickerToDate('17/07/2024');

      const sequenceWithoutGrid: Locator[] = [
        datePicker.cancelBtn,
        datePicker.prevMonthBtn,
        datePicker.monthSelectNativeLocator,
        datePicker.yearSelectNativeLocator,
        datePicker.nextMonthBtn,
        datePicker.currentDayInGrid,
        datePicker.cancelBtn,
        datePicker.prevMonthBtn,
      ];

      await testTabNavigation(sequenceWithoutGrid, datePicker.dialog);
    });
  });

  test.describe('handles both minimum and maximum dates', () => {
    test.beforeEach(async () => {
      await datePicker.goToPage('min');
    });

    test('by hiding the year select when both min and max year are the same', async () => {
      await datePicker.sbMinIput.fill('"2024-05-02"');
      await datePicker.sbMaxInput.fill('"2024-07-25"');
      await datePicker.openDatePickerToDate('17/07/2024');

      const sequenceWithoutYearSelect: Locator[] = [
        datePicker.cancelBtn,
        datePicker.prevMonthBtn,
        datePicker.monthSelectNativeLocator,
        datePicker.nextMonthBtn,
        datePicker.currentDayInGrid,
      ];

      await testTabNavigation(sequenceWithoutYearSelect, datePicker.dialog);
    });

    test('by hiding both month and year selects when both min and max dates are the same month and year', async () => {
      await datePicker.sbMinIput.fill('"2024-07-02"');
      await datePicker.sbMaxInput.fill('"2024-07-25"');
      await datePicker.openDatePickerToDate('17/07/2024');

      const sequenceWithoutMonthAndYearSelects: Locator[] = [
        datePicker.cancelBtn,
        datePicker.prevMonthBtn,
        datePicker.nextMonthBtn,
        datePicker.currentDayInGrid,
      ];

      await testTabNavigation(sequenceWithoutMonthAndYearSelects, datePicker.dialog);
    });
  });
});

class DatePickerPom extends BasePageObjectModel {
  /****************************************
   * LOCATORS
   ****************************************/
  get sbMaxInput(): Locator {
    return this.page.locator('[name="max"]');
  }

  get sbMinIput(): Locator {
    return this.page.locator('[name="min"]');
  }

  get datePickerInput(): Locator {
    return this.frame.locator('dsfr-form-input');
  }

  get nativeInput(): Locator {
    const datePickerInput = this.datePickerInput;
    return datePickerInput.locator('.fr-input');
  }

  get dialogButton(): Locator {
    const input = this.datePickerInput;
    return input.getByRole('button');
  }

  get dialog(): Locator {
    return this.frame.getByRole('dialog');
  }

  get currentDayInGrid(): Locator {
    return this.dialog.locator('td[tabindex="0"]');
  }

  get prevMonthBtn(): Locator {
    return this.dialog.getByTestId('datepicker-prev-month');
  }

  get nextMonthBtn(): Locator {
    return this.dialog.getByTestId('datepicker-next-month');
  }

  get monthSelectNativeLocator(): Locator {
    return this.dialog.getByTestId('datepicker-month-select').locator('select');
  }

  get yearSelectNativeLocator(): Locator {
    return this.dialog.getByTestId('datepicker-year-select').locator('select');
  }

  get cancelBtn(): Locator {
    return this.dialog.getByTestId('datepicker-cancel-btn');
  }

  /****************************************
   * ACTIONS
   ****************************************/
  async openDatePicker(): Promise<void> {
    return this.dialogButton.click();
  }

  async openDatePickerToDate(date: string): Promise<void> {
    await this.nativeInput.fill(date);
    return this.openDatePicker();
  }
}
