import { Directive, HostListener, Input } from '@angular/core';

/**
 * L'objective de cette directive est d'implémenter un masque de saisie en fonction de plusieurs entrées :
 * - On peut considérer le `placeholder`, le `pattern` de l'input ou tout simplement un attribut `mask`.
 * - Dans cette première version, on gère tout simplement une date
 */
@Directive({
  selector: '[dsfrDateFormat]',
  standalone: true,
})
/**
 * L'objectif de cette directive est de proposer, en l'absence de masque de saisie un formatage par défaut
 * @author pfontanet
 * @since 0.10
 */
export class DateFormatDirective {
  @Input() dsfrDateFormat: string;

  constructor() {}

  @HostListener('input', ['$event'])
  onInput(event: Event) {
    const doFormat = !!this.dsfrDateFormat;
    if (doFormat) {
      const inputElt = event.target as HTMLInputElement;
      inputElt.value = completeDateValue(inputElt.value);
    }
  }
}

/* -- Functions ----------------------------------------------------------------------------------------------------- */

/**
 * Compléte la valeur d'une date
 */
export function completeDateValue(val: string): string {
  // On supprime tout ce n'est ni chiffre ni séparateur
  let value = filtreDigitSep(val);

  const regexp = /^(?<g1>[\d]{0,2})?(?<s1>\/)?(?<g2>[\d]{0,2})?(?<s2>\/)?(?<year>[\d]{0,4})?/;
  const reArr = regexp.exec(value);
  const groups = reArr?.groups;
  value = '';
  if (groups) {
    const g1 = groups['g1'] || '';
    let s1 = groups['s1'] || '';
    s1 = !s1 && g1.length === 2 ? '/' : s1;
    const g2 = groups['g2'] || '';
    let s2 = groups['s2'] || '';
    s2 = !s2 && g2.length === 2 ? '/' : s2;
    const year = groups['year'] || '';
    value = g1 + s1 + g2 + s2 + year;
  }

  return value;
}

/**
 * Compléte la valeur d'une date en langue 'fr'
 * (export pour les tests)
 */
export function completeDateValueFr(val: string): string {
  // On supprime tout ce n'est ni chiffre ni séparateur
  let value = filtreDigitSep(val);

  // const regexpFr = /^(?<day>3[01]|0?[1-9]|[12][0-9])?(?<s1>\/)?(?<month>1[0-2]|0?[1-9])?(?<s2>\/)?(?<year>[\d]{0,4})?/;
  const regexp = /^(?<day>[\d]{0,2})?(?<s1>\/)?(?<month>[\d]{0,2})?(?<s2>\/)?(?<year>[\d]{0,4})?/;
  const reArr = regexp.exec(value);
  const groups = reArr?.groups;
  value = '';
  if (groups) {
    const day = groups['day'] || '';
    let s1 = groups['s1'] || '';
    s1 = !s1 && day.length === 2 ? '/' : s1;
    const month = groups['month'] || '';
    let s2 = groups['s2'] || '';
    s2 = !s2 && month.length === 2 ? '/' : s2;
    const year = groups['year'] || '';
    value = day + s1 + month + s2 + year;
  }

  return value;
}

/**
 * Ne conserve que les chiffres et les séparateurs
 */
function filtreDigitSep(value: String): string {
  return value.replace(/[^\d^\/]/g, '');
}
