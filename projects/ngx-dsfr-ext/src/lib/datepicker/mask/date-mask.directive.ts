import { AfterViewInit, Directive, ElementRef, Input, OnDestroy, Renderer2 } from '@angular/core';
import { DsfrDatePickerComponent } from '../datepicker.component';

const MASK_DATE = '__/__/____';

@Directive({
  selector: '[dsfrDateMask]',
  standalone: true,
})
/**
 * L'objectif de cette directive est de proposer un masque de saisie sous la forme dd/dd/ddddd.
 * @author pfontanet
 * @since 0.10
 */
export class DateMaskDirective implements AfterViewInit, OnDestroy {
  private _dsfrDateMask: string;

  private listenKeyup: any;
  private listenKeydown: any;

  constructor(
    private hostComponent: DsfrDatePickerComponent,
    private elementRef: ElementRef,
    private renderer: Renderer2,
  ) {}

  @Input() set dsfrDateMask(value: string) {
    this._dsfrDateMask = value;
    this.doHideShowMask();
  }

  /**
   * NOTE : Pourquoi ngAfterViewInit plutôt que ngOnInit ?
   * Et bien, parce que dans ngOnInit, le DOM n'est pas encore construit et que l'ajout de listeners en fonction
   * de la valeur du masque ne marche pas car, this.elementRef.nativeElement.querySelector('.fr-input') === null
   * et du coup rien ne marche !!!
   */
  ngAfterViewInit() {
    this.doHideShowMask();
  }

  ngOnDestroy() {
    this.removeListeners();
  }

  /**
   * Cache / affiche le masque en fonction de la propriété `mask`.
   * @private
   */
  private doHideShowMask() {
    const showMask = this._dsfrDateMask !== '';
    if (showMask) {
      this.addListeners();
    } else {
      this.removeListeners();
    }
  }

  private addListeners(): void {
    this.hostComponent.value = formatDateMaskValue(this.hostComponent.value);
    // NOTE : Pourquoi ne pas utiliser renderer.selectRootElement() ?
    // Parce que le setter de dsfrDateMask est invoqué lors de l'étape d'init, le DOM n'est pas construit
    // et que selectRootElement affiche une exception dans la console, contrairement à querySelector
    // A l'init inputElt === null
    const inputElt = this.elementRef.nativeElement.querySelector('.fr-input');
    if (inputElt) {
      this.listenKeydown = this.renderer.listen(inputElt, 'keydown', onInputKeyDown);
      this.listenKeyup = this.renderer.listen(inputElt, 'keyup', onInputKeyUp);
    }
  }

  private removeListeners(): void {
    if (this.listenKeydown) {
      this.listenKeydown();
    }
    if (this.listenKeyup) {
      this.listenKeyup();
    }
  }
}

/* -- Traitement du masque ------------------------------------------------------------------------------------------ */

/**
 * Traite les événements du clavier dans le champ date.
 * - Permet la saisie des chiffres de 0 à 9
 * - Permet la tabulation avant / arrière dans les parties jour / mois / année
 * @param event
 */
function onInputKeyDown(event: KeyboardEvent): void {
  // Si ce n'est pas un nombre, on ne fait rien
  const inputElt = <HTMLInputElement>event.currentTarget;
  const isDigit = /\d/.test(event.key);
  if (isDigit) {
    doDigitKeyDown(inputElt);
  } else {
    switch (event.key) {
      case 'Tab':
        const eventProcessed = event.shiftKey ? doShiftTabKey(inputElt) : doTabKey(inputElt);
        if (eventProcessed) {
          event.preventDefault();
        }
        break;
      default: {
        const isLetter = /[a-zA-Z]/.test(event.key);
        if (isLetter) {
          event.stopPropagation();
        }
      }
    }
  }
}

/**
 * Finalise le traitement du clavier après insertion d'un digit.
 */
function onInputKeyUp(event: KeyboardEvent): void {
  // Si ce n'est pas un nombre, on ne fait rien
  const inputElt = <HTMLInputElement>event.currentTarget;
  const isDigit = /\d/.test(event.key);
  if (isDigit) {
    doDigitKeyUp(inputElt);
  }
}

/**
 * Traitement AVANT insertion d'un digit dans le champ date
 * - Suppression de '_' qui sera remplacé par le digit (le champ ne dépasse pas 10 caractères)
 * - On replace le curseur où il était
 */
function doDigitKeyDown(inputElt: HTMLInputElement) {
  // Au cas où l'utilisateur a cliqué devant un séparateur, on le saute
  // Et ben NON, car si on a 1/1^/2024 (le chapeau représentant le point d'insertion), on laisse faire
  const value = inputElt.value || '';
  let selStart = inputElt.selectionStart || 0;

  // On supprime le '_' afin de pouvoir insérer le digit à la place
  if (value[selStart] == '_') {
    inputElt.value = value.slice(0, selStart) + value.slice(selStart + 1);
    inputElt.selectionStart = selStart;
    inputElt.selectionEnd = selStart;
  }
}

/**
 * Traitement APRES insertion d'un caractère dans le champ date
 * - Saut d'un séparateur s'il y a lieu
 * - Reformatage du champ si l'utilisateur a supprimé des éléments du masque
 */
function doDigitKeyUp(inputElt: HTMLInputElement): void {
  // Au cas où l'utilisateur se retrouve devant un séparateur, on le saute
  const value = inputElt.value || '';
  let selStart = inputElt.selectionStart || 0;
  if (value[selStart] === '/' && (selStart === 2 || selStart == 5)) {
    inputElt.selectionStart = ++selStart;
  }

  // On reformate si nécessaire
  if (value.length < 8) {
    inputElt.value = formatDateMaskValue(inputElt.value);
  }
}

/**
 * Tab AVANT dans le champ date
 * - Tabulation en avant de part en part (jour, mois, année), puis tabulation sur champ suivant
 */
function doTabKey(inputElt: HTMLInputElement): boolean {
  const value = inputElt.value || '';
  const selEnd = inputElt.selectionEnd || 0;
  const pos = value.indexOf('/', selEnd);
  if (pos > -1) {
    inputElt.selectionStart = pos + 1;
    inputElt.selectionEnd = inputElt.selectionStart;
  }
  return pos > -1;
}

/**
 * Tab ARRIERE dans le champ date
 * - Tabulation en arrière de part en part (année, mois, jour), puis tabulation sur champ précédent
 */
function doShiftTabKey(inputElt: HTMLInputElement): boolean {
  const value = inputElt.value || '';
  const selStart = inputElt.selectionStart || 0;
  const pos = value.substring(0, selStart).lastIndexOf('/');
  if (pos > -1) {
    inputElt.selectionStart = pos;
    inputElt.selectionEnd = inputElt.selectionStart;
  }
  return pos > -1;
}

/**
 * Reformatage de la valeur, quelle que soit la saisie
 * @param input
 */
export function formatDateMaskValue(input: string | undefined): string {
  const regexp = /^(?<day>[\d_]{0,2})(?<s1>\/?)(?<month>[\d_]{0,2})(?<s2>\/?)(?<year>[\d_]{0,4})$/;
  const reArr = regexp.exec(input || '');
  const groups = reArr?.groups;
  let value = MASK_DATE;
  if (groups) {
    let day = groups['day'] || '__';
    day = groups['s1'] ? day : day.padEnd(2, '_');
    let month = groups['month'] || '__';
    month = groups['s2'] ? month : month.padEnd(2, '_');
    let year = groups['year'] || '____';
    year = year.padEnd(4, '_');
    value = day + '/' + month + '/' + year;
  }

  return value;
}
