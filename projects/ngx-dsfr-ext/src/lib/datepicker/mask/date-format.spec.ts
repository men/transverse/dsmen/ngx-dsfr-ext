import { Component, ViewChild } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { DsfrDatePickerComponent } from '../datepicker.component';
import { completeDateValue } from './date-format.directive';

@Component({
  selector: `edu-host-component`,
  template: ` <dsfr-ext-datepicker mask="fr" format="fr"></dsfr-ext-datepicker>`,
})
class TestHostComponent {
  @ViewChild(DsfrDatePickerComponent)
  public component: DsfrDatePickerComponent;
}

describe('DateFormatDirectiveTest', () => {
  let testHostComponent: TestHostComponent;
  let fixture: ComponentFixture<TestHostComponent>;
  let datePickerComponent: DsfrDatePickerComponent;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [DsfrDatePickerComponent],
      declarations: [TestHostComponent],
    }).compileComponents();
    fixture = TestBed.createComponent(TestHostComponent);
    testHostComponent = fixture.componentInstance;
    fixture.detectChanges();
    datePickerComponent = testHostComponent.component;
  });

  it('should be defined', () => {
    expect(datePickerComponent).toBeDefined();
  });

  it('completeDateValue', () => {
    expect(completeDateValue('')).toEqual('');
    expect(completeDateValue('abcdef')).toEqual('');
    expect(completeDateValue('3')).toEqual('3');
    expect(completeDateValue('3aa')).toEqual('3');
    expect(completeDateValue('10')).toEqual('10/');
    expect(completeDateValue('31')).toEqual('31/');
    expect(completeDateValue('31/')).toEqual('31/');
    expect(completeDateValue('31/1')).toEqual('31/1');
    expect(completeDateValue('31/1/')).toEqual('31/1/');
    expect(completeDateValue('31/01')).toEqual('31/01/');
    expect(completeDateValue('31/01/2')).toEqual('31/01/2');
    expect(completeDateValue('31/01/2024')).toEqual('31/01/2024');
    expect(completeDateValue('31/01/2024-2025')).toEqual('31/01/2024');
  });
});
