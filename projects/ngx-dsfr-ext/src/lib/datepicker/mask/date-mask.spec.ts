import { Component, ViewChild } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { DsfrDatePickerComponent } from '../datepicker.component';
import { formatDateMaskValue } from './date-mask.directive';

@Component({
  selector: `edu-host-component`,
  template: ` <dsfr-ext-datepicker mask="fr"></dsfr-ext-datepicker>`,
})
class TestHostComponent {
  @ViewChild(DsfrDatePickerComponent)
  public component: DsfrDatePickerComponent;
}

describe('DateMaskDirectiveTest', () => {
  let testHostComponent: TestHostComponent;
  let fixture: ComponentFixture<TestHostComponent>;
  let datePickerComponent: DsfrDatePickerComponent;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [DsfrDatePickerComponent],
      declarations: [TestHostComponent],
    }).compileComponents();
    fixture = TestBed.createComponent(TestHostComponent);
    testHostComponent = fixture.componentInstance;
    fixture.detectChanges();
    datePickerComponent = testHostComponent.component;
  });

  it('should be defined', () => {
    expect(datePickerComponent).toBeDefined();
  });

  it('should be well formatted', () => {
    expect(formatDateMaskValue('')).toEqual('__/__/____');
    expect(formatDateMaskValue('abc')).toEqual('__/__/____');
    expect(formatDateMaskValue('1')).toEqual('1_/__/____');
    expect(formatDateMaskValue('1/')).toEqual('1/__/____');
    expect(formatDateMaskValue('1/__/____')).toEqual('1/__/____');
    expect(formatDateMaskValue('1_/__/____')).toEqual('1_/__/____');
    expect(formatDateMaskValue('1/1')).toEqual('1/1_/____');
    expect(formatDateMaskValue('1/1/')).toEqual('1/1/____');
    expect(formatDateMaskValue('1_/1_/')).toEqual('1_/1_/____');
    expect(formatDateMaskValue('_1/_1/')).toEqual('_1/_1/____');
    expect(formatDateMaskValue('1/1/2')).toEqual('1/1/2___');
  });
});
