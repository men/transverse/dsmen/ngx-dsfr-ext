import { Component, ViewChild } from '@angular/core';
import { ComponentFixture, fakeAsync, TestBed, tick } from '@angular/core/testing';
import { DsfrDatePickerComponent } from './datepicker.component';

@Component({
  selector: `edu-host-component`,
  template: `<dsfr-ext-datepicker mask="fr"></dsfr-ext-datepicker>`,
})
class TestHostComponent {
  @ViewChild(DsfrDatePickerComponent)
  public component: DsfrDatePickerComponent;
}

describe('DsfrDatePickerComponentTest', () => {
  let testHostComponent: TestHostComponent;
  let fixture: ComponentFixture<TestHostComponent>;
  let datePickerComponent: DsfrDatePickerComponent;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [DsfrDatePickerComponent],
      declarations: [TestHostComponent],
    }).compileComponents();

    fixture = TestBed.createComponent(TestHostComponent);
    testHostComponent = fixture.componentInstance;
    fixture.detectChanges();
    datePickerComponent = testHostComponent.component;
  });

  describe('should be initialized with the right state when its dialog is opened and', () => {
    it('it is given no value', () => {
      jest.useFakeTimers();
      jest.setSystemTime(new Date('2024-01-09'));

      datePickerComponent.ngOnInit();
      datePickerComponent.ngAfterViewInit();

      datePickerComponent['open']();

      fixture.detectChanges();

      const heading: HTMLHeadingElement = fixture.nativeElement.getElementsByTagName('h2')[0];
      expect(heading.getAttribute('aria-label')).toBe('Widget de choix de date : Janvier 2024');

      jest.useRealTimers();
    });

    it('it is given a value with its main input prior', fakeAsync(() => {
      datePickerComponent.ngOnInit();
      datePickerComponent.ngAfterViewInit();

      datePickerComponent.value = '28/07/2023';
      datePickerComponent['open']();

      tick(100);
      fixture.detectChanges();

      const heading: HTMLHeadingElement = fixture.nativeElement.getElementsByTagName('h2')[0];
      expect(heading.getAttribute('aria-label')).toBe('Widget de choix de date : Juillet 2023');
    }));
  });
});
