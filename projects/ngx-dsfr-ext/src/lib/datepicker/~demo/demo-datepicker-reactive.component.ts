import { Component, Input, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, ReactiveFormsModule } from '@angular/forms';
import { DemoFormActionsComponent, DemoToolbarComponent } from '../../shared/demo';
import { DsfrDateEvent } from '../../shared/models/date-event';
import { DsfrDatePickerComponent } from '../datepicker.component';

@Component({
  selector: 'demo-datepicker-reactive',
  templateUrl: './demo-datepicker-reactive.component.html',
  standalone: true,
  imports: [DsfrDatePickerComponent, ReactiveFormsModule, DemoFormActionsComponent, DemoToolbarComponent],
})
export class DemoDatepickerReactiveComponent implements OnInit {
  @Input() date: string;
  @Input() required: boolean;
  @Input() min: string;
  @Input() max: string;
  @Input() error: string;

  /** @internal */
  protected formGroup: FormGroup;
  protected autreDate: string;

  constructor(private fb: FormBuilder) {
    this.formGroup = this.fb.group({
      dateField: [undefined],
    });
  }

  ngOnInit() {
    this.formGroup.controls['dateField'].setValue(this.date);
  }

  /** @internal */
  onDateChange(event: DsfrDateEvent) {
    this.formGroup.controls['dateField'].setValue(event.value);
  }

  /** @internal */
  onReset() {
    this.formGroup.controls['dateField'].setValue('');
  }

  /** @internal */
  onSubmit() {}
}
