import { Meta, moduleMetadata, StoryObj } from '@storybook/angular';
import { DemoDatepickerReactiveComponent } from './demo-datepicker-reactive.component';
import { dsfrDecorators } from '.storybook/storybook-utils';

const meta: Meta<DemoDatepickerReactiveComponent> = {
  title: 'Components/Date Picker',
  component: DemoDatepickerReactiveComponent,
  decorators: [moduleMetadata({ imports: [] })],
};
export default meta;
type Story = StoryObj<DemoDatepickerReactiveComponent>;

export const ReactiveForm: Story = {
  decorators: dsfrDecorators('Dans un Reactive Form'),
  args: {
    date: '2025-08-31',
    required: false,
    min: '',
    max: '',
    error: '',
  },
};
