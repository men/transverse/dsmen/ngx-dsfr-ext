// Ordre alphabétique requis
export const MESSAGES_FR = {
  datepicker: {
    button: {
      choose: { label: 'Choisir une date' },
      change: { label: 'Changer la date' },
    },
    placeholder: 'jj/mm/aaaa',
    validationMin: 'La date doit être postérieure ou égale à la date minimum.',
    validationMax: 'La date doit être antérieure ou égale à la date maximum.',
  },
};
