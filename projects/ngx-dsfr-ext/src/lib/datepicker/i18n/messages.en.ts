// Ordre alphabétique requis
export const MESSAGES_EN = {
  datepicker: {
    button: {
      choose: { label: 'Choose Date' },
      change: { label: 'Change Date' },
    },
    placeholder: 'mm/dd/yyyy',
    validationMin: 'The date must be later than or equal to the minimum date.',
    validationMax: 'The date must be earlier than or equal to the maximum date.',
  },
};
