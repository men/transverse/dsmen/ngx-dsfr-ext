import { controlEmitter, dsfrDecorators, titleDecorator } from '.storybook/storybook-utils';
import { FormsModule } from '@angular/forms';
import { DsfrAccordionModule, DsfrSeverityConst } from '@edugouvfr/ngx-dsfr';
import { Meta, moduleMetadata, StoryObj } from '@storybook/angular';
import { DsfrDatePickerComponent } from './datepicker.component';

const meta: Meta<DsfrDatePickerComponent> = {
  title: 'Components/Date Picker',
  component: DsfrDatePickerComponent,
  decorators: [moduleMetadata({ imports: [] })],
  argTypes: {
    dateChange: controlEmitter,
  },
};
export default meta;
type Story = StoryObj<DsfrDatePickerComponent>;

export const Default: Story = {
  decorators: dsfrDecorators('Défaut'),
  args: {
    /* DefaultValueAccessorComponent */
    value: '',
    disabled: false,
    /* DefaultControlComponent */
    // inputId: '',
    label: 'Saisir une date',
    hint: '',
    // name: '',
    /* Input */
    // placeholder
    required: false,
    // role: '',
    message: '',
    severity: undefined,
    min: '',
    max: '',
    showMask: false,
  },
};

export const InitialValue: Story = {
  decorators: dsfrDecorators('Date avec valeur initiale'),
  args: {
    ...Default.args,
    value: '10/07/2021',
  },
};

export const Disabled: Story = {
  decorators: dsfrDecorators('Date désactivée'),
  args: {
    ...Default.args,
    disabled: true,
  },
};

export const Error: Story = {
  decorators: dsfrDecorators("Date en erreur avec texte d'erreur"),
  args: {
    ...Default.args,
    message: 'Texte d’erreur obligatoire',
    severity: DsfrSeverityConst.ERROR,
  },
};

export const Valid: Story = {
  decorators: dsfrDecorators('Date valide avec texte de succès'),
  args: {
    ...Default.args,
    message: 'Texte de validation',
    severity: DsfrSeverityConst.SUCCESS,
  },
};

export const Min: Story = {
  decorators: dsfrDecorators('Date avec valeur minimum'),
  args: {
    ...Default.args,
    min: new Date().toISOString().substring(0, 10),
  },
};

export const Max: Story = {
  decorators: dsfrDecorators('Date avec valeur maximum'),
  args: {
    ...Default.args,
    max: new Date().toISOString().substring(0, 10),
  },
};

export const ShowMask: Story = {
  decorators: dsfrDecorators('Affichage du masque de saisie'),
  args: {
    ...Default.args,
    showMask: true,
  },
};

export const Container: Story = {
  name: 'In a Container',
  decorators: [
    moduleMetadata({ imports: [DsfrAccordionModule, FormsModule] }),
    titleDecorator('Calendrier dans un container'),
  ],
  args: {
    ...Default.args,
  },
  render: (args) => ({
    props: args,
    template: `<dsfr-accordion heading="Container" [expanded]="true">
  <dsfr-ext-datepicker [(ngModel)]="value" [disabled]="disabled" [label]="label" [hint]="hint" [message]="message" 
                     [severity]="severity" [min]="min" [max]="max"
  ></dsfr-ext-datepicker>
</dsfr-accordion>`,
  }),
};
