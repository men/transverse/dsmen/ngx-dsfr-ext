import { A11yModule } from '@angular/cdk/a11y';

import {
  AfterViewInit,
  Component,
  ElementRef,
  EventEmitter,
  forwardRef,
  Input,
  OnChanges,
  OnDestroy,
  OnInit,
  Output,
  SimpleChanges,
  ViewChild,
  ViewEncapsulation,
} from '@angular/core';
import {
  AbstractControl,
  FormsModule,
  NG_VALIDATORS,
  NG_VALUE_ACCESSOR,
  ValidationErrors,
  Validator,
} from '@angular/forms';
import {
  DefaultControlComponent,
  DsfrFormInputModule,
  DsfrFormSelectModule,
  DsfrI18nPipe,
  DsfrI18nService,
  DsfrSeverity,
  DsfrSeverityConst,
  LangService,
  Language,
  LoggerService,
  newUniqueId,
} from '@edugouvfr/ngx-dsfr';
import { DsfrCalendarComponent } from '../calendar/calendar.component';
import { MESSAGES_EN as CALENDAR_MESSAGES_EN } from '../calendar/i18n/messages.en';
import { MESSAGES_FR as CALENDAR_MESSAGES_FR } from '../calendar/i18n/messages.fr';
import { EduClickOutsideDirective } from '../shared';
import { DsfrDateEvent } from '../shared/models/date-event';
import { DateUtils } from '../shared/utils/date/date-utils';
import { MESSAGES_EN } from './i18n/messages.en';
import { MESSAGES_FR } from './i18n/messages.fr';
import { DateFormatDirective } from './mask/date-format.directive';
import { DateMaskDirective } from './mask/date-mask.directive';

/**
 * Implémente la saisie d'une date via un input ou via un calendrier affichant les jours et les mois.
 * Validator est implémenté uniquement par rapport à min et max
 * @since 0.10
 * @author pfontanet
 */
@Component({
  selector: 'dsfr-ext-datepicker, dsfrx-datepicker',
  encapsulation: ViewEncapsulation.None,
  templateUrl: './datepicker.component.html',
  styleUrls: ['./datepicker.component.scss'],
  standalone: true,
  imports: [
    DsfrFormInputModule,
    FormsModule,
    EduClickOutsideDirective,
    DateMaskDirective,
    DateFormatDirective,
    DsfrFormSelectModule,
    A11yModule,
    DsfrCalendarComponent,
    DsfrI18nPipe,
  ],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => DsfrDatePickerComponent),
      multi: true,
    },
    {
      provide: NG_VALIDATORS,
      useExisting: forwardRef(() => DsfrDatePickerComponent),
      multi: true,
    },
  ],
})
export class DsfrDatePickerComponent
  extends DefaultControlComponent<string>
  implements AfterViewInit, Validator, OnDestroy, OnChanges
{
  /** Message d'information lié au composant */
  @Input() message: string | undefined = undefined;

  /** Représente la sévérité du message. */
  @Input() severity: DsfrSeverity;

  /**
   * Indique si la date est obligatoire.
   */
  @Input() required = false;

  /**
   * Indique si le masque de saisie doit être affiché ou non.
   */
  @Input() showMask = false;

  /** Emission de la date.*/
  @Output() dateChange = new EventEmitter<DsfrDateEvent>();

  @ViewChild('modal') protected modal: ElementRef<HTMLDivElement>;
  @ViewChild('cancelBtn') protected cancelBtn: ElementRef<HTMLButtonElement>;

  protected currentMonthIndex: number = new Date().getMonth();
  protected currentYear: number = new Date().getFullYear();
  protected translatedMonthAbbreviations: string[] = [];
  protected translatedMonths: string[] = [];
  protected dialogTitle = '';
  protected modalLabel = '';

  /**
   * Placeholder par défaut de l'input. Dépend de la langue 'jj/mm/aaaa' ou 'mm/dd/yyyy'
   */
  protected placeholder: string;

  /**
   * Id aléatoire, unique, de la modale (le calendrier)
   */
  protected modalId = newUniqueId();

  protected isOpen = false;
  protected buttonLabelChoose: string;

  private prevCodeLang: string;

  /* Messages */
  private buttonLabelChange: string;
  private dayLabels: string[];
  private monthLabels: string[];

  /* Node */
  private dialogTriggerBtnNode: HTMLElement;

  private _min: string | undefined = undefined;
  private _max: string | undefined = undefined;

  /** @internal */
  constructor(
    private elementRef: ElementRef,
    private i18n: DsfrI18nService,
    private langService: LangService,
    private logger: LoggerService,
  ) {
    super();
    this.i18n.extendsLabelsBundle(Language.EN, CALENDAR_MESSAGES_EN);
    this.i18n.extendsLabelsBundle(Language.FR, CALENDAR_MESSAGES_FR);
    this.i18n.extendsLabelsBundle(Language.EN, MESSAGES_EN);
    this.i18n.extendsLabelsBundle(Language.FR, MESSAGES_FR);
    this.prevCodeLang = langService.lang;
    this.i18n.completeLangChange$.subscribe((code) => this.onLangChange(code));
  }

  /**
   * Compatibilité ascendante avec l'ancien `datepicker`.
   * @deprecated since 0.10 Utiliser `message` et `severity` à la place.
   */
  get error(): string | undefined {
    return (this.severity = DsfrSeverityConst.ERROR) ? this.message : undefined;
  }

  /**
   * Compatibilité ascendante avec l'ancien `datepicker`.
   * @deprecated since 0.10 Utiliser `message` et `severity` à la place.
   */
  get valid(): string | undefined {
    return (this.severity = DsfrSeverityConst.SUCCESS) ? this.message : undefined;
  }

  /**
   * @return Le code de la langue en cours
   */
  get codeLang() {
    return this.langService.lang;
  }

  /**
   * @internal
   */
  get min(): string | undefined {
    return this._min;
  }

  /**
   * @internal
   */
  get max(): string | undefined {
    return this._max;
  }

  protected get isEnglishLocale(): boolean {
    return this.langService.lang === 'en';
  }

  /**
   * Compatibilité ascendante avec l'ancien `datepicker`.
   * @deprecated since 0.10 Utiliser `message` et `severity` à la place.
   */
  @Input() set error(message: string | undefined) {
    this.message = message;
    this.severity = DsfrSeverityConst.ERROR;
  }

  /**
   * Compatibilité ascendante avec l'ancien `datepicker`.
   * @deprecated since 0.10 Utiliser `message` et `severity` à la place.
   */
  @Input() set valid(message: string | undefined) {
    this.message = message;
    this.severity = DsfrSeverityConst.SUCCESS;
  }

  /**
   * Date minimale inclue. Doit être au format IS0-8601 : AAAA-MM-JJ.
   * Ce paramètre peut être utilisé seul ou avec la propriété max.
   */
  @Input()
  set min(value: string | undefined) {
    if (value && !this.isValidIso(value)) {
      this.logger.warn(
        `dsfr-date-picker: La propriété min a un format incorrecte. Attendu: 'YYYY-MM-DD'. Reçu: ${value}`,
      );

      this._min = undefined;
    } else {
      this._min = value;
    }
  }

  /**
   * Date maximale inclue. Doit être au format IS0-8601 : AAAA-MM-JJ.
   * Ce paramètre peut être utilisé seul ou avec la propriété min.
   */
  @Input()
  set max(value: string | undefined) {
    if (value && !this.isValidIso(value)) {
      this.logger.warn(
        `dsfr-date-picker: La propriété max a un format incorrecte. Attendu: 'YYYY-MM-DD'. Reçu: ${value}`,
      );

      this._max = undefined;
    } else {
      this._max = value;
    }
  }

  ngOnChanges(simpleChanges: SimpleChanges) {
    if (simpleChanges['value'].previousValue !== simpleChanges['value'].currentValue) {
      this.reformatValue(this.value);
    }
  }

  /** @internal */
  ngOnDestroy() {
    this.dialogTriggerBtnNode.removeEventListener('keydown', this.handleTriggerButtonKeydown);
  }

  /** @internal */
  ngAfterViewInit() {
    this.dialogTriggerBtnNode = this.elementRef.nativeElement.querySelector('dsfr-form-input .fr-btn');

    // Listener ne pouvant pas être remplacé, car on ne reçoit pas cet événement depuis form-input
    this.dialogTriggerBtnNode.addEventListener('keydown', this.handleTriggerButtonKeydown.bind(this));
    this.setDateForTriggerButtonLabel();

    this.close(false);
  }

  /**
   * @internal
   * Interface Validator
   */
  validate(control: AbstractControl): ValidationErrors | null {
    const errors = [];
    const date = DateUtils.parse(this.codeLang, control.value);
    const minDate = this.min ? DateUtils.parseDateIso(this.min) : undefined;
    const maxDate = this.max ? DateUtils.parseDateIso(this.max) : undefined;

    if (minDate && date && date < minDate) {
      errors.push({ err_min_date: this.i18n.t('datepicker.validationMin') });
    } else if (maxDate && date && maxDate < date) {
      errors.push({ err_max_date: this.i18n.t('datepicker.validationMax') });
    }

    return errors.length === 0 ? null : errors;
  }

  override writeValue(value: string): void {
    this.reformatValue(value);

    super.writeValue(this.value);
  }

  /**
   * Gère le clic en dehors de la modale
   */
  protected onCalendarOutside() {
    if (this.isOpen) {
      this.close(false);
    }
  }

  /**
   * Gère le changement de valeur de l'input
   */
  protected onInputChange() {
    this.handleInternalValidation();
    this.setNewValue(this.value);

    this.setDateForTriggerButtonLabel();
  }

  /**********************
   * Event Handlers
   **********************/
  protected handleTriggerButtonClick(event: Event): void {
    if (this.isOpen) {
      this.close();
    } else {
      this.open();
    }

    event.stopPropagation();
    event.preventDefault();
  }

  protected handleTriggerButtonKeydown(event: KeyboardEvent): void {
    if (event.key === 'Enter' || event.key === ' ') {
      this.handleTriggerButtonClick(event);
    }
  }

  protected handleCancelButton(event: Event): void {
    let flagToPreventDefault = false;

    switch (event.type) {
      case 'keydown':
        const keyEvent = <KeyboardEvent>event;
        switch (keyEvent.key) {
          case 'Esc':
          case 'Escape':
            this.close();
            flagToPreventDefault = true;
            break;

          default:
            break;
        }
        break;

      case 'click':
        this.close();
        flagToPreventDefault = true;
        break;

      default:
        break;
    }

    if (flagToPreventDefault) {
      event.stopPropagation();
      event.preventDefault();
    }
  }

  protected handleDataChange(dateEvent: DsfrDateEvent): void {
    this.setNewValue(dateEvent.value);
    this.handleInternalValidation();

    this.close(true);
  }

  /**
   * Gère la validation interne de la date pour les critères de date min et date max.
   * Retourne `true` en cas d'erreur.
   */
  private handleInternalValidation(): boolean {
    const date = DateUtils.parse(this.codeLang, this.value);
    const minDate = this.min ? DateUtils.parseDateIso(this.min) : undefined;
    const maxDate = this.max ? DateUtils.parseDateIso(this.max) : undefined;

    if (minDate && date && date < minDate) {
      this.message = this.i18n.t('datepicker.validationMin');
      this.severity = DsfrSeverityConst.ERROR;
      return true;
    } else if (maxDate && date && maxDate < date) {
      this.message = this.i18n.t('datepicker.validationMax');
      this.severity = DsfrSeverityConst.ERROR;
      return true;
    } else {
      this.message = '';
      return false;
    }
  }

  /**
   * Changement de la valeur de l'input et formatage dans la langue courante
   * @param value Valeur au format ISO ou dans la langue courante
   */
  private reformatValue(value: string | undefined): void {
    if (value === undefined) {
      return;
    }

    const parsedIsoDate = DateUtils.parseDateIso(value);

    if (parsedIsoDate === undefined) {
      this.value = value;
      return;
    }

    this.value = DateUtils.format(this.codeLang, parsedIsoDate);
  }

  /**
   * Gestion du changement de langue dans le composant.
   *
   * @param code Code de la locale à prendre en compte
   */
  private onLangChange(code: string): void {
    this.placeholder = this.i18n.t('datepicker.placeholder');

    this.buttonLabelChoose = this.i18n.t('datepicker.button.choose.label');
    this.buttonLabelChange = this.i18n.t('datepicker.button.change.label');
    this.dayLabels = DateUtils.arr(this.i18n, this.logger, 'calendar.days');
    this.monthLabels = DateUtils.arr(this.i18n, this.logger, 'calendar.months');
    this.modalLabel = this.i18n.t('calendar.modal.ariaLabel');

    this.updateMonthTranslations();
    this.updateDialogTitle();

    // Reformatage de l'input
    const date = DateUtils.parse(this.prevCodeLang, this.value);
    const newValue = DateUtils.format(code, date);
    this.setNewValue(newValue);
    this.prevCodeLang = code;
  }

  /**
   * Met à jour le titre de la dialog (visuellement caché) pour les besoins d'accessibilité.
   */
  private updateDialogTitle(): void {
    this.updateMonthIndexAndYear();
    this.dialogTitle = `${this.i18n.t('calendar.modal.title')}${this.translatedMonths[this.currentMonthIndex]} ${this.currentYear}`;
  }

  /**
   * Ouvre la modale.
   */
  private open(): void {
    this.updateDialogTitle();
    this.isOpen = true;
  }

  /**
   * Ferme la modale.
   *
   * Positionne le focus sur le bouton d'ouverture de la modale si besoin.
   *
   * @param focusTriggerBtn Permet de savoir si le focus doit être donné au bouton d'ouverture de la modale
   */
  private close(focusTriggerBtn = true): void {
    if (!this.modal) {
      return;
    }

    this.isOpen = false;

    if (focusTriggerBtn) {
      this.dialogTriggerBtnNode.focus();
    }
  }

  // Textbox methods

  /**
   * Mets à jour la valeur du composant (donc normalement textboxNode.value) et émets un événement
   * @param value
   * @private
   */
  private setNewValue(value: string | undefined): void {
    this.value = value;
    const date = DateUtils.parse(this.codeLang, this.value);

    if (value && DateUtils.isValidDefaultString(value)) {
      this.dateChange.emit({ value: this.value, date: date });
      this.updateDialogTitle();
    }
  }

  /**
   * Méthode invoquée à la sortie de l'input text. Formate l'input et affecte la propriété 'aria-label' du bouton de
   * sélection du calendrier. A noter que, si la date est exacte, 'aria-label' contient la date, ex :
   * "Changer la date, Lundi Mai 27, 2024" ou ="Change Date, Monday July 1, 2024"
   * - i18n : le code de parsing est remplacé par un appel à DateUtils
   */
  private setDateForTriggerButtonLabel(): void {
    const day = DateUtils.parse(this.codeLang, this.value);
    if (day) {
      let label = this.buttonLabelChange;
      label += ', ' + this.dayLabels[day.getDay()];
      label += ' ' + this.monthLabels[day.getMonth()];
      label += ' ' + day.getDate();
      label += ', ' + day.getFullYear();
      this.dialogTriggerBtnNode.setAttribute('aria-label', label);
    } else {
      // If not a valid date, initialize with "Choose Date"
      this.dialogTriggerBtnNode.setAttribute('aria-label', this.buttonLabelChoose);
    }
  }

  private updateMonthTranslations(): void {
    this.translatedMonths = DateUtils.arr(this.i18n, this.logger, 'calendar.months');
    this.translatedMonthAbbreviations = DateUtils.arr(this.i18n, this.logger, 'calendar.monthsAbbr');
  }

  private isValidIso(boundaryString: string): boolean {
    const dateRegex = /^\d{4}-\d{2}-\d{2}$/;

    return dateRegex.test(boundaryString);
  }

  /**
   * Mise à jour du mois et de l'année pour le titre de la modale.
   */
  private updateMonthIndexAndYear(): void {
    const date = DateUtils.parse(this.codeLang, this.value);
    this.currentMonthIndex = date ? new Date(date).getMonth() : new Date().getMonth();
    this.currentYear = date ? new Date(date).getFullYear() : new Date().getFullYear();
  }
}
