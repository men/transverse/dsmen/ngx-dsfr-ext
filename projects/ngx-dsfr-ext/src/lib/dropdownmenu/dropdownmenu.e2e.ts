import { expect, FrameLocator, test } from '@playwright/test';
import { getSbFrame, goToComponentPage } from 'projects/ngx-dsfr-ext/shared/e2e.utils';

test.describe('Dropdown menu', () => {
  test.describe('focuses', () => {
    test.beforeEach(async ({ page }) => {
      return goToComponentPage('dropdown-menu', page, 'default');
    });

    test('correct focus navigation pattern with dropdown menu', async ({ page }) => {
      const frame = getSbFrame(page);
      await openSelectDropdown(frame);

      const listContainer = frame.getByRole('menu');
      const listOptions = listContainer.locator('li');

      // Expect focus on first option when menu is open
      const firstMenuItem = listOptions.first().getByRole('menuitem');
      await expect(firstMenuItem).toBeFocused();

      // Expect the focus to move to second option
      await firstMenuItem.press('ArrowDown');
      await expect(listOptions.nth(1).getByRole('menuitem')).toBeFocused();
    });
  });
});

async function openSelectDropdown(frame: FrameLocator): Promise<void> {
  const menu = frame.locator('.dsfrx-menu-dropdown__btn');
  return menu.click();
}
