import { ChangeDetectionStrategy, ChangeDetectorRef, Component, ViewChild } from '@angular/core';
import { ComponentFixture, fakeAsync, TestBed, tick } from '@angular/core/testing';
import { FormsModule } from '@angular/forms';
import { DsfrButtonModule, DsfrLinkModule } from '@edugouvfr/ngx-dsfr';
import { DsfrDropdownMenuGroupComponent } from './component/dropdownmenu-group.component';
import { DsfrDropdownMenuItemComponent } from './component/dropdownmenu-item.component';
import { DsfrDropdownMenuComponent } from './dropdownmenu.component';

@Component({
  selector: `edu-host-component`,
  template: `<dsfrx-dropdownmenu [menuId]="menuId" [labelMenu]="'Save as... '">
    <ng-template #contentTemplate>
      <dsfrx-dropdownmenu-item>
        <dsfr-button label="Bouton"></dsfr-button>
      </dsfrx-dropdownmenu-item>
      <dsfrx-dropdownmenu-item>
        <dsfr-button label="Bouton"></dsfr-button>
      </dsfrx-dropdownmenu-item>
      <dsfrx-dropdownmenu-item>
        <dsfr-link label="Lien" link="."></dsfr-link>
      </dsfrx-dropdownmenu-item>
    </ng-template>
  </dsfrx-dropdownmenu>`,
  changeDetection: ChangeDetectionStrategy.Default,
})
class TestHostComponent {
  @ViewChild(DsfrDropdownMenuComponent)
  public menuComponent: DsfrDropdownMenuComponent;
  public menuId: string;

  constructor(public cdRef: ChangeDetectorRef) {}
}

describe('DsfrDropdownMenuComponent', () => {
  let testHostComponent: TestHostComponent;
  let fixture: ComponentFixture<TestHostComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [
        FormsModule,
        DsfrDropdownMenuComponent,
        DsfrDropdownMenuItemComponent,
        DsfrDropdownMenuGroupComponent,
        DsfrButtonModule,
        DsfrLinkModule,
      ],
      declarations: [TestHostComponent],
    }).compileComponents();

    fixture = TestBed.createComponent(TestHostComponent);
    testHostComponent = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should put id test on button element and aria-controls ', fakeAsync(() => {
    testHostComponent.menuId = 'test';
    fixture.detectChanges();

    testHostComponent.menuComponent.ngOnInit();

    const btnMenu = fixture.nativeElement.querySelector('button');
    expect(btnMenu.getAttribute('id')).toEqual('test');
    expect(btnMenu.getAttribute('aria-controls')).toEqual('test-menu');

    btnMenu.click();
    tick();
    fixture.detectChanges();
    const menu = fixture.nativeElement.querySelector('ul[role="menu"');
    expect(menu.getAttribute('aria-labelledby')).toEqual('test');
    expect(menu.getAttribute('tabindex')).toEqual('-1');
  }));

  it('should open menu and add correct a11y attributes', fakeAsync(() => {
    testHostComponent.menuId = 'test';
    testHostComponent.menuComponent.ngOnInit();
    const btnMenu = fixture.nativeElement.querySelector('button');

    btnMenu.click();
    tick();
    fixture.detectChanges();

    const menu = fixture.nativeElement.querySelector('ul[role="menu"');
    const menuList = menu.querySelectorAll('li');

    // Expect correct menu ul element
    expect(menu.getAttribute('aria-labelledby')).toEqual('test');
    expect(menu.getAttribute('tabindex')).toEqual('-1');
    expect(menuList.length).toEqual(3);

    // Expect correct button item
    expect(menuList[0].getAttribute('role')).toEqual('presentation');
    expect(menuList[0].querySelector('button').getAttribute('role')).toEqual('menuitem');
    expect(menuList[0].querySelector('button').getAttribute('tabindex')).toEqual('0');

    // Expect correct link item
    expect(menuList[2].getAttribute('role')).toEqual('presentation');
    expect(menuList[2].querySelector('a').getAttribute('role')).toEqual('menuitem');
    expect(menuList[2].querySelector('a').getAttribute('tabindex')).toEqual('-1');
  }));
});
