import { CommonModule } from '@angular/common';
import {
  ChangeDetectionStrategy,
  Component,
  ContentChild,
  contentChildren,
  effect,
  ElementRef,
  EventEmitter,
  Input,
  OnInit,
  Output,
  signal,
  TemplateRef,
  ViewChild,
  viewChildren,
  ViewEncapsulation,
} from '@angular/core';
import { DsfrButton, DsfrButtonModule, DsfrLink, DsfrLinkModule, DsfrSize, newUniqueId } from '@edugouvfr/ngx-dsfr';
import { EduClickOutsideDirective } from '../shared';
import { DsfrDropdownMenuGroupComponent } from './component/dropdownmenu-group.component';
import { DsfrDropdownMenuItemComponent } from './component/dropdownmenu-item.component';

export interface DsfrMenuButton
  extends Omit<DsfrButton, 'size' | 'variant' | 'iconPosition' | 'loader' | 'invertedOutlineContrast'> {
  /** Action appelée lors du 'click' sur le bouton. */
  callback?: () => void;
}

/** Définition des interfaces internes pour créer le view model de DsfrDropdownMenuItem */
interface MenuButtonViewModel extends DsfrMenuButton {
  menuType: 'button' | 'link' | 'group';
}
interface MenuLinkViewModel extends DsfrMenuButton {
  menuType: 'button' | 'link' | 'group';
}

interface MenuGroupViewModel {
  menuType: 'button' | 'link' | 'group';
  items: MenuButtonViewModel[] | MenuLinkViewModel[];
}

/** Alias pour le type d'entrée de menu */
export type DsfrDropdownMenuItem = (DsfrMenuButton | DsfrLink | (DsfrMenuButton | DsfrLink)[])[];

@Component({
  selector: 'dsfr-ext-dropdownmenu, dsfrx-dropdownmenu',
  standalone: true,
  imports: [
    CommonModule,
    DsfrButtonModule,
    DsfrLinkModule,
    EduClickOutsideDirective,
    DsfrDropdownMenuItemComponent,
    DsfrDropdownMenuGroupComponent,
  ],
  templateUrl: './dropdownmenu.component.html',
  styleUrls: ['./dropdownmenu.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  encapsulation: ViewEncapsulation.None,
})
export class DsfrDropdownMenuComponent implements OnInit {
  /** Libellé du bouton du menu déroulant */
  @Input() labelMenu: string;

  /** Attribut aria-label du bouton du menu déroulant */
  @Input() ariaLabelMenu: string | undefined;

  /** Identifiant du bouton du menu déroulant (généré par défaut) */
  @Input() menuId: string;

  /** Icône optionnelle du menu déroulant  */
  @Input() iconMenu: string;

  /** Taille du menu déroulant (SM, MD ou LG). MD par défaut. */
  @Input() size: DsfrSize = 'MD';

  /** Désactiver le menu */
  @Input() disabled: boolean;

  /** Fermer le menu après un clic sur bouton ou lien.  */
  @Input() closeOnAction: boolean = false;

  /** Evènement sélection d'un bouton */
  @Output() buttonSelect: EventEmitter<DsfrButton> = new EventEmitter();

  /** Evènement sélection d'un lien */
  @Output() linkSelect: EventEmitter<DsfrLink> = new EventEmitter();

  /** Cacher les séparateurs de groupe (dans le cas d'utilisation de menus) */
  @Input() hideGroupSeparator: boolean = false;

  /** Contenu personnalisé du menu */
  @ContentChild('contentTemplate', { static: true }) contentTemplate: TemplateRef<any>;

  /**@internal */
  @ViewChild('btnMenu') protected btnMenu: ElementRef<HTMLButtonElement>;

  protected menusView: (MenuButtonViewModel | MenuLinkViewModel | MenuGroupViewModel)[] = [];
  protected isOpen = signal(false);

  protected itemsC = contentChildren(DsfrDropdownMenuItemComponent, { descendants: true }); // slot
  protected itemsV = viewChildren(DsfrDropdownMenuItemComponent); // interne

  private focusedElement: any; // élément de la liste possédant le focus
  private indexFocusedElement: number = -1;

  constructor() {
    effect(() => {
      if (this.isOpen() && this.indexFocusedElement === -1) {
        // Initialisation du focus sur le 1er élément de la liste à chaque ouverture de liste ou update des menuitems
        this.focusedElement =
          this.itemsV().length > 0 ? this.itemsV()[0]._anchorElement : this.itemsC()[0]?._anchorElement;

        if (this.focusedElement) {
          this.focusedElement.setAttribute('tabIndex', '0');
          this.focusedElement.focus();
          this.indexFocusedElement = 0;
        }
      }
    });
  }

  /** Items de menu, liens, boutons ou groupe de liens ou boutons */
  @Input()
  set menus(value: DsfrDropdownMenuItem) {
    this.menusView = [];
    if (value && value.length) {
      this.menusView = value.map((m) => {
        if (Array.isArray(m)) {
          // Type groupe
          return {
            menuType: 'group',
            items: m.map((item) => {
              if ((item as DsfrLink).link || (item as DsfrLink).route || (item as DsfrLink).routerLink) {
                return { menuType: 'link', ...item } as MenuLinkViewModel; // Type lien
              } else {
                return { menuType: 'button', ...item } as MenuButtonViewModel; // Type bouton
              }
            }),
          } as MenuGroupViewModel;
        } else if ((m as DsfrLink).link || (m as DsfrLink).route || (m as DsfrLink).routerLink) {
          return { menuType: 'link', ...m } as MenuLinkViewModel; // Type lien
        } else {
          return { menuType: 'button', ...m } as MenuButtonViewModel; // Type bouton
        }
      });
    }
  }

  public ngOnInit(): void {
    this.menuId ??= newUniqueId();
  }

  protected toggleMenu() {
    this.isOpen.set(!this.isOpen());
    if (this.isOpen()) {
      this.indexFocusedElement = -1;
    }
  }

  protected onButtonClick(button: DsfrMenuButton): void {
    if (button.callback) {
      button.callback();
    }

    if (this.closeOnAction) {
      this.isOpen.set(false);
    }

    this.buttonSelect.emit(button);
  }

  protected onClickOutside() {
    this.isOpen.set(false);
  }

  protected onLinkSelect(link: DsfrLink): void {
    if (link.route) {
      this.linkSelect.emit(link);
    }
    if (this.closeOnAction) {
      this.isOpen.set(false);
    }
  }

  /**
   * Gestion des interactions clavier sur la liste
   * - positionnement du focus sur les éléments précédents/suivants à la navigation flèches
   * - fermeture de la liste sur les touches définies, et repositonnement du focus sur le bouton d'ouverture
   * @param event keyboard event
   */
  protected doKeyEvent(event: KeyboardEvent) {
    switch (event.key) {
      case 'ArrowDown':
        this.focusToElement(1);
        event.preventDefault();
        break;

      case 'ArrowUp':
        this.focusToElement(-1);
        event.preventDefault();
        break;

      case ' ':
        this.emitKeyEventElement(event, true);
        break;

      case 'Enter':
        this.emitKeyEventElement(event, false);
        if (this.closeOnAction) {
          this.isOpen.set(false);
        }
        break;
      case 'Tab':
        this.isOpen.set(false);

        break;

      case 'Escape':
        this.isOpen.set(false);
        this.btnMenu?.nativeElement.focus();
        event.preventDefault();
        break;

      default:
    }
  }

  /**
   * Si entrée est pressé / ou espace et que l'élément de liste est de type 'checkbox' ou 'radio'
   * Envoi de l'evenement selectItem
   */
  private emitKeyEventElement(event: Event, onlySelection: boolean) {
    if (!onlySelection || (this.focusedElement && this.focusedElement.getAttribute('aria-checked'))) {
      const items = [...this.itemsC(), ...this.itemsV()];
      items[this.indexFocusedElement].selectItem?.emit(event);
    }
  }

  private focusToElement(direction: number) {
    const items = [...this.itemsC(), ...this.itemsV()];
    const totalItems = items.length;
    if (items.length === 0) return;

    let nextIndex = this.indexFocusedElement + direction;

    if (nextIndex >= totalItems) {
      nextIndex = 0;
    } else if (nextIndex < 0) {
      nextIndex = totalItems - 1;
    }

    this.indexFocusedElement = nextIndex;
    this.setFocusElement(items[nextIndex]._anchorElement);
  }

  /** Gestion des tabindex pour le focus sur le nouvel élément de la liste */
  private setFocusElement(newElement: HTMLElement) {
    this.focusedElement.setAttribute('tabIndex', '-1');
    this.focusedElement = newElement;
    this.focusedElement.setAttribute('tabIndex', '0');
    this.focusedElement.focus();
  }
}
