import { CommonModule } from '@angular/common';
import {
  AfterContentInit,
  AfterViewInit,
  ChangeDetectionStrategy,
  Component,
  ContentChild,
  ElementRef,
  EventEmitter,
  inject,
  Input,
  OnDestroy,
  Output,
  Renderer2,
  ViewEncapsulation,
} from '@angular/core';
import { DsfrButtonComponent, DsfrLinkComponent } from '@edugouvfr/ngx-dsfr';

/**
 * Composant affichage d'un menuitem ou d'un menuitemcheckbox du composant dropdown-menu
 */
@Component({
  selector: 'dsfrx-dropdownmenu-item, dsfr-ext-dropdownmenu-item',
  standalone: true,
  imports: [CommonModule],
  template: `<ng-content></ng-content>`,
  changeDetection: ChangeDetectionStrategy.OnPush,
  encapsulation: ViewEncapsulation.None,
})
export class DsfrDropdownMenuItemComponent implements AfterViewInit, AfterContentInit, OnDestroy {
  @Input() type: undefined | 'checkbox' | 'radio';
  @Output() selectItem = new EventEmitter<Event>(); // Événement pour l'utilisateur

  @ContentChild(DsfrButtonComponent, { static: false }) button: DsfrButtonComponent | undefined;
  @ContentChild(DsfrLinkComponent, { static: false }) link: DsfrLinkComponent | undefined;

  _anchorElement: HTMLElement; // élément focusable (role = menuitem / menuitemcheckbox)
  _checked: boolean = false; // si type = checkbox

  private readonly elementRef: ElementRef = inject(ElementRef);
  private readonly renderer: Renderer2 = inject(Renderer2);
  private _unlistenFn: Function;

  get checked() {
    return this._checked;
  }

  @Input()
  set checked(value: boolean) {
    this._checked = value;
    if (this._anchorElement && this.type === 'checkbox') {
      this._anchorElement.setAttribute('aria-checked', '' + this._checked);
    }
  }

  /**
   * Positionner les classes CSS si dsfr-button ou dsfr-link
   */
  public ngAfterContentInit(): void {
    if (this.button) {
      this.button.customClass = this.button.customClass
        ? this.button.customClass + ' dsfrx-menu-item'
        : 'dsfrx-menu-item';
      this.button.variant = 'tertiary';
    }

    if (this.link) {
      this.link.customClass = this.link.customClass
        ? this.link.customClass + ' fr-sidemenu__link'
        : 'fr-sidemenu__link';
    }
  }

  /**
   * Positionnement des éléments dans le DOM pour l'accessibilité
   * - ajout d'un élément wrapper li
   * - récupération de l'élément _anchorElement comme élément focusable (dsfr-link, dsfr-button ou li)
   * - ajout de l'attribut [role] menuitem sur l'élément focusable si type est undefined
   * - ajout de [aria-checked] et [role] menuitemcheckbox sur sur l'élément focusable si le type est checkbox
   */
  public ngAfterViewInit(): void {
    // surround element with li
    const elem = this.elementRef.nativeElement;
    const li = document.createElement('li');
    li.setAttribute('role', 'presentation');
    elem.replaceWith(li); // Ne remplace pas l'élément en lui même mais modifie la Node parent de l'élément
    li.appendChild(elem);

    this._unlistenFn = this.renderer.listen(li, 'click', (event) => {
      this.selectItem.emit(event);
    });

    // get anchor element
    if (this.link) {
      this._anchorElement = this.elementRef.nativeElement.querySelector('a');
    } else if (this.button) {
      this._anchorElement = this.elementRef.nativeElement.querySelector('button');
    } else {
      li.className = 'interactive';
      this._anchorElement = li;
    }

    // add role
    if (this.type) {
      this._anchorElement.setAttribute('role', this.type === 'checkbox' ? 'menuitemcheckbox' : 'menuitemradio');
      this._anchorElement.setAttribute('aria-checked', '' + this._checked);
    } else {
      this._anchorElement.setAttribute('role', 'menuitem');
    }

    this._anchorElement.setAttribute('tabindex', '-1');
  }

  ngOnDestroy(): void {
    if (this._unlistenFn) this._unlistenFn();
  }
}
