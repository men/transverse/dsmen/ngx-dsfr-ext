import {
  AfterViewInit,
  ChangeDetectionStrategy,
  Component,
  ElementRef,
  inject,
  Input,
  ViewEncapsulation,
} from '@angular/core';

/**
 * Composant affichage d'un groupe de menu dropdown
 */
@Component({
  selector: 'dsfrx-dropdownmenu-group, dsfr-ext-dropdownmenu-group',
  template: `
    <ul role="group">
      <ng-content></ng-content>
    </ul>
  `,
  standalone: true,
  changeDetection: ChangeDetectionStrategy.OnPush,
  encapsulation: ViewEncapsulation.None,
})
export class DsfrDropdownMenuGroupComponent implements AfterViewInit {
  @Input() showSeparator: boolean;

  protected element = inject(ElementRef);

  /**
   * Ajout d'un wrapper <li> avec role presentation
   * si showSeparator, ajout d'un separateur apres ce li
   */
  public ngAfterViewInit(): void {
    const elem = this.element.nativeElement;
    const li = document.createElement('li');
    li.setAttribute('role', 'presentation');
    elem.replaceWith(li); // Ne remplace pas l'élément en lui même mais modifie la Node parent de l'élément
    li.appendChild(elem);

    if (this.showSeparator) {
      li.insertAdjacentHTML('afterend', '<li role="separator"></li>');
    }
  }
}
