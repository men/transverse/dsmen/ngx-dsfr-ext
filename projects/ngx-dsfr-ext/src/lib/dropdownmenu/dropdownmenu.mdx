import * as Stories from './dropdownmenu.stories';
import { Canvas, Controls, Meta } from '@storybook/blocks';

<Meta of={Stories} title="Dropdownmenu" />

# Menu déroulant (dropdown menu)

Le _menu déroulant_  présente une liste d'actions ou de choix parmi lesquels un utilisateur peut sélectionner une ou plusieurs options. 
Cet élément n'est pas destiné à la navigation mais représente plutôt un menu contextuel utilisé pour les interactions au sein d'une application

👓 Il implémente le pattern défini ici [Menu Button Pattern W3C](https://www.w3.org/WAI/ARIA/apg/patterns/menu-button/)

Il reprend le visuel défini par le DSFR (version bêta) [Menu Déroulant DSFR](https://www.systeme-de-design.gouv.fr/composants-et-modeles/composants-beta/menu-deroulant)

- _Composant_ : `DsfrDropdownMenuComponent`,
- _Tags_ : `dsfr-ext-dropdownmenu, dsfrx-dropdownmenu`
- Sous-composants:  `DsfrDropdownMenuItemComponent`, `DsfrDropdownMenuGroupComponent`

<h2 id="fonctionnement"> Fonctionnement </h2>

Le menu déroulant permet de :

- Naviguer vers d’autres page avec des liens 
- Réaliser des actions liées à un contenu avec des boutons d’actions
- Sélectionner des options

Il a trois variantes de tailles disponibles : `SM`, `MD`, `LG`.

<h3 id="mode-prog"> Mode programmatique </h3>

Utilisation avec des entrées de menu programmatiques, à l'aide de `[menus] `de type `DsfrDropdownMenuItem[]`. Il attend un ensemble de `DsfrMenuButton` ou `DsfrLink`. Il permet également la création de groupes.

```typescript
type DsfrMenuInput = (DsfrMenuButton | DsfrLink | (DsfrMenuButton | DsfrLink)[])[];
```

```html
    <dsfrx-dropdownmenu-item 
      [menus]="{ label: 'Lien', routerLink: 'path' }, { label: 'Bouton', callback: '', disabled: true }" 
      labelMenu="Mon menu..">
    </dsfrx-dropdownmenu-item>
```
<h3 id="mode-temp"> Template </h3>

Utilisation avec un slot `contentTemplate`. 
Dans ce cas la définition du composant se fera à l'aide des sous-composants `DsfrDropdownMenuItemComponent` et `DsfrDropdownMenuGroupComponent` :

```html
<dsfrx-dropdownmenu labelMenu="Mon menu..">
  <ng-template #contentTemplate>
    <dsfrx-dropdownmenu-item>
        <dsfrx-link label="Lien 1" routePath="/path1"></dsfr-link>
    </dsfrx-dropdownmenu-item>
        <dsfrx-dropdownmenu-item>
        <dsfrx-link label="Lien 2" routePath="/path2"></dsfr-link>
    </dsfrx-dropdownmenu-item>
  </ng-template>
</dsfrx-dropdownmenu>
```

<h2 id="preview"> Aperçu </h2>

<Canvas of={Stories.Default} />

<h2 id="api"> API </h2>

<Controls />

<h2 id="fonctionnalites"> Fonctionnalités </h2>

<h3 id="f-button"> Boutons d'actions </h3>

Les boutons attendus dans `[menus]` sont de type `DsfrMenuButton[]`. Un évènement `buttonSelect()` est également émis après chaque clic d'un bouton.


```typescript
export interface DsfrMenuButton
  extends Omit<DsfrButton, 'size' | 'variant' | 'iconPosition' | 'loader' | 'invertedOutlineContrast'> {
  /** Action appelée lors du 'click' sur le bouton. */
  callback?: () => void;
}
```

👆  Les `<dsfr-button>` seront forcément en variant `tertiary` et en cas d'ajout d'icône celle-ci sera positionnée à gauche.

<Canvas of={Stories.Default} />

<h3 id="f-link"> Liens </h3>

Les liens attendus dans `[menus]` sont de type `DsfrLink[]`
Un évènement `linkSelect()` est émis après chaque sélection d'un lien.

<Canvas of={Stories.WithLinks} />

<h3 id="f-group"> Groupes </h3>

L'input `[menus]` permet également de définir des groupes, sous forme de tableaux d'entrées de type `DsfrMenuButton` ou `DsfrMenuLink`.
Par défaut un séparateur sera affiché entre chaque groupe, il peut être caché avec l'input `hideGroupSeparator`.

```typescript
menus = [
      [
        { label: 'Groupe 1 - bouton 1', callback: '' },
        { label: 'Groupe 1 - bouton 2', callback: '' },
      ],
      [
        { label: 'Groupe 2 - lien 1', routerLink: 'path' },
        { label: 'Groupe 2 - lien 2', routerLink: 'path' },
      ],
```

<Canvas of={Stories.WithGroups} />


<h3 id="template"> Utilisation avec template </h3>

<h4 id="temp-button"> Boutons et liens </h4>

<Canvas of={Stories.WithTemplate} />

<h4 id="temp-group">  Groupes </h4>

Il est possible de grouper les éléments avec le slot, en utilisant le composant `<DsfrDropdownMenuGroupComponent>`, `<dsfrx-dropdownmenu-group>`.
L'input `showSeparator` sur le composant `<dsfrx-dropdownmenu-group>` permet d'afficher un séparateur.

```html
<dsfrx-dropdownmenu-group showSeparator="true"> ... </dsfrx-dropdownmenu-group >
```

<Canvas of={Stories.WithGroups} />

<h4 id="temp-selection"> Sélection d'options </h4>

Le composant permet selon la définition du [W3C](https://www.w3.org/WAI/ARIA/apg/patterns/menu-button/) de représenter des choix d'options.

👓  Dans ce cas, le composant utilise le rôle aria `menuitemcheckbox` ou `menuitemradio`

Pour refléter une sélection d'options multiples, il est nécessaire :
- de passer par le slot 
- d'activer l'option `type=checkbox` (multiple) ou `type="radio"` (single)
- de passer l'attribut checked représentant l'état de l'option. C'est à vous de mettre à jour cet état.

L'évènement `selectItem()` est émis au clic et sur les évènement clavier `entrée` et `espace`. Il permet de mettre à jour l'état `checked`.

```html
    <dsfrx-dropdownmenu-item  (selectItem)="checkedButton1 = !checkedButton1" type="checkbox" [checked]="checkedButton1">
      <div label="Lien 1">Bouton check 1</div>
    </dsfrx-dropdownmenu-item>
```
👓  Dans ces cas d'utilisation, le rôle `menuitemcheckbox` ou `menuitemradio` est ajouté directement sur les balises `li`, qui prendront alors le focus et renverront un évènement au clic.

🔥 Pour respecter l'accessibilité, seuls des composants <b>non interactifs</b> doivent être présents à l'intérieur de `dsfrx-dropdownmenu-item`. 
Il ne peux pas y avoir d'éléments sémantiques à l'interieur d'un container `menuitemcheckbox` ou `menuitemradio`, tous les éléments auront un rôle présentation.


<Canvas of={Stories.Selection} />

<h4 id="temp-custom">  Eléments personnalisés </h4>

Le composant accepte d'autres types d'éléments que `<dsfr-button>` ou `<dsfr-link>`, même en type par défaut.
Ce sera alors à vous de gérer le style applicable sur ces éléments.
L'évènement `selectItem()` est émis au clic sur l'élément.

👓  Dans ces cas d'utilisation, le rôle `menuitem` est ajouté directement sur les balises `li`, qui prendront alors le focus et renverront un évènement au clic.

🔥 Pour respecter l'accessibilité, seuls des composants <b>non interactifs</b> doivent être présents à l'intérieur de `dsfrx-dropdownmenu-item`

<Canvas of={Stories.CustomElement} />

<h3 id="close-action"> Close on action </h3>

Fermer automatiquement le menu lors du clic sur un bouton d'action (ou appui sur la touche `entrée`)

<Canvas of={Stories.CloseOnAction} />