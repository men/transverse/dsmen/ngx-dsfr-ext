import { CommonModule } from '@angular/common';
import { Component, Input } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { DsfrButton, DsfrButtonModule, DsfrLink, DsfrLinkModule } from '@edugouvfr/ngx-dsfr';
import { DsfrDropdownMenuGroupComponent } from '../component/dropdownmenu-group.component';
import { DsfrDropdownMenuItemComponent } from '../component/dropdownmenu-item.component';
import { DsfrDropdownMenuComponent } from '../dropdownmenu.component';

@Component({
  selector: `demo-menu`,
  templateUrl: './demo-dropdownmenu.component.html',
  standalone: true,
  imports: [
    CommonModule,
    FormsModule,
    DsfrDropdownMenuComponent,
    DsfrLinkModule,
    DsfrButtonModule,
    DsfrDropdownMenuItemComponent,
    DsfrDropdownMenuGroupComponent,
  ],
})
export class DemoDropdownMenuComponent {
  /**@internal */
  @Input() minLength: number;
  /**@internal */
  @Input() appendTo: 'body' | HTMLElement | string | undefined;
  /**@internal */
  checkedButton1: boolean = true;
  /**@internal */
  checkedButton2: boolean = false;
  /**@internal */
  links: DsfrLink[] = [
    { label: 'Lien 1', link: '.' },
    { label: 'Lien 2', link: '.' },
    { label: 'Lien 3', link: '.' },
  ];
  /**@internal */
  buttons: DsfrButton[] = [{ label: 'Button action 1' }];

  selectItem(i: number): void {
    console.log('select item');
    if (i === 1) {
      this.checkedButton1 = true;
      this.checkedButton2 = false;
    } else {
      this.checkedButton1 = false;
      this.checkedButton2 = true;
    }
  }
}
