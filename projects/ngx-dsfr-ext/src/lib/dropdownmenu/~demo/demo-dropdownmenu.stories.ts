import { RouterTestingModule } from '@angular/router/testing';
import { Meta, moduleMetadata, StoryObj } from '@storybook/angular';
import { DsfrDropdownMenuComponent } from '../dropdownmenu.component';
import { DemoDropdownMenuComponent } from './demo-dropdownmenu.component';

const meta: Meta<DemoDropdownMenuComponent> = {
  title: 'Components/Dropdown Menu',
  component: DemoDropdownMenuComponent,
  decorators: [moduleMetadata({ imports: [RouterTestingModule, DsfrDropdownMenuComponent] })],
  parameters: {
    docs: { toc: { disabled: true } },
  },
};
export default meta;
type Story = StoryObj<DemoDropdownMenuComponent>;

/*export const WithSlot: Story = {
  args: {},
};*/
