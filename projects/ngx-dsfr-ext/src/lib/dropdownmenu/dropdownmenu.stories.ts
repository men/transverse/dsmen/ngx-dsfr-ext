import { controlEmitter } from '.storybook/storybook-utils';
import { RouterTestingModule } from '@angular/router/testing';
import { DsfrButtonModule, DsfrLinkModule } from '@edugouvfr/ngx-dsfr';
import { Meta, moduleMetadata, StoryObj } from '@storybook/angular';
import { DsfrDropdownMenuGroupComponent } from './component/dropdownmenu-group.component';
import { DsfrDropdownMenuItemComponent } from './component/dropdownmenu-item.component';
import { DsfrDropdownMenuComponent } from './dropdownmenu.component';

const meta: Meta<DsfrDropdownMenuComponent> = {
  title: 'Components/Dropdown Menu',
  component: DsfrDropdownMenuComponent,
  decorators: [
    moduleMetadata({
      imports: [
        RouterTestingModule,
        DsfrButtonModule,
        DsfrLinkModule,
        DsfrDropdownMenuItemComponent,
        DsfrDropdownMenuGroupComponent,
      ],
    }),
  ],
  argTypes: {
    buttonSelect: controlEmitter,
    linkSelect: controlEmitter,
    size: { control: { type: 'inline-radio' }, options: ['SM', 'MD', 'LG'] },
  },
};
export default meta;
type Story = StoryObj<DsfrDropdownMenuComponent>;

export const Default: Story = {
  args: {
    labelMenu: 'Actions',
    menuId: 'monmenu',
    iconMenu: 'fr-icon-file-line',
    menus: [
      { label: 'Télécharger', icon: 'fr-icon-file-line' },
      { label: 'Marquer comme brouillon', icon: 'fr-icon-file-line' },
      { label: 'Créer une copie', icon: 'fr-icon-file-line' },
      { label: 'Supprimer', icon: 'fr-icon-file-line' },
    ],
  },
};

export const Small: Story = {
  args: {
    labelMenu: 'Menu',
    size: 'SM',
    menus: [
      { label: 'Télécharger', icon: 'fr-icon-file-line' },
      { label: 'Marquer comme brouillon', icon: 'fr-icon-file-line' },
      { label: 'Créer une copie', icon: 'fr-icon-file-line' },
      { label: 'Supprimer', icon: 'fr-icon-file-line' },
    ],
  },
};

export const Large: Story = {
  args: {
    labelMenu: 'Menu',
    size: 'LG',
    menus: [
      { label: 'Télécharger', icon: 'fr-icon-file-line' },
      { label: 'Marquer comme brouillon', icon: 'fr-icon-file-line' },
      { label: 'Créer une copie', icon: 'fr-icon-file-line' },
      { label: 'Supprimer', icon: 'fr-icon-file-line' },
    ],
  },
};

export const WithLinks: Story = {
  args: {
    labelMenu: 'Menu',
    menus: [
      { label: 'Profil', routerLink: 'test' },
      { label: 'Mon compte', routerLink: 'test2' },
      { label: 'Déconnexion', routerLink: 'test2' },
    ],
  },
};

export const WithButtonsAndLinks: Story = {
  args: {
    labelMenu: 'Menu',
    menus: [
      { label: 'Lien 1', routerLink: 'test' },
      { label: 'Lien 2', routerLink: 'test2' },
      { label: 'Button 1', icon: 'fr-icon-file-line' },
      { label: 'Button 2', icon: 'fr-icon-file-line' },
      { label: 'Lien 4', disabled: true, routerLink: '/' },
    ],
  },
};

export const WithGroups: Story = {
  args: {
    labelMenu: 'Menu',
    menus: [
      [
        { label: 'Groupe 1 - lien 1', routerLink: 'test2' },
        { label: 'Groupe 1 - lien 2', routerLink: 'test2' },
      ],
      [
        { label: 'Groupe 2 - lien 1', routerLink: 'test2' },
        { label: 'Groupe 2 - lien 2', routerLink: 'test2' },
      ],
    ],
  },
};

export const CloseOnAction: Story = {
  args: {
    labelMenu: 'Menu',
    menus: [{ label: 'Bouton 1' }, { label: 'Bouton 2', disabled: true }, { label: 'Bouton 3' }],
    closeOnAction: true,
  },
};

const templateSlot = `
<dsfrx-dropdownmenu [labelMenu]="labelMenu">
  <ng-template #contentTemplate>
    <dsfrx-dropdownmenu-item>
      <dsfr-button label="Bouton"></dsfr-button>
    </dsfrx-dropdownmenu-item>
    <dsfrx-dropdownmenu-item>
      <dsfr-button label="Bouton"></dsfr-button>
    </dsfrx-dropdownmenu-item>
    <dsfrx-dropdownmenu-item>
      <dsfr-link label="Lien" link="."></dsfr-link>
    </dsfrx-dropdownmenu-item>
  </ng-template>
</dsfrx-dropdownmenu> `;

export const WithTemplate: Story = {
  args: {
    //...Default.args,
    labelMenu: 'Save as...',
  },
  render: (args) => ({
    props: { ...args },
    template: templateSlot,
  }),
};

const templatewithGroups = `
<dsfrx-dropdownmenu [labelMenu]="labelMenu">
  <ng-template #contentTemplate>
    <dsfrx-dropdownmenu-group showSeparator="true">
      <dsfrx-dropdownmenu-item>
        <dsfr-button label="Bouton groupe 1"></dsfr-button>
      </dsfrx-dropdownmenu-item>
      <dsfrx-dropdownmenu-item>
        <dsfr-button label="Bouton groupe 1"></dsfr-button>
      </dsfrx-dropdownmenu-item>
    </dsfrx-dropdownmenu-group>
    <dsfrx-dropdownmenu-group>
      <dsfrx-dropdownmenu-item>
        <dsfr-button label="Bouton groupe 2"></dsfr-button>
      </dsfrx-dropdownmenu-item>
      <dsfrx-dropdownmenu-item>
        <dsfr-button label="Bouton groupe 2"></dsfr-button>
      </dsfrx-dropdownmenu-item>
    </dsfrx-dropdownmenu-group>
  </ng-template>
</dsfrx-dropdownmenu> `;

export const WithTemplateGroups: Story = {
  args: {
    //...Default.args,
    labelMenu: 'Formater...',
  },
  render: (args) => ({
    props: { ...args },
    template: templatewithGroups,
  }),
};

const templateSelection = `
<dsfrx-dropdownmenu [labelMenu]="'Save as... '">
  <ng-template #contentTemplate>
    <dsfrx-dropdownmenu-item type="checkbox" [checked]="checkedButton1" (selectItem)="checkedButton1 = !checkedButton1">
      <div label="Lien 1">Mon option 1</div>
    </dsfrx-dropdownmenu-item>
    <dsfrx-dropdownmenu-item type="checkbox" (selectItem)="checkedButton2 = !checkedButton2" [checked]="checkedButton2">
      <div>Mon option 2</div>
    </dsfrx-dropdownmenu-item>
  </ng-template>
</dsfrx-dropdownmenu>`;

export const Selection: Story = {
  args: {
    //...Default.args,
    labelMenu: 'Afficher...',
  },
  render: (args) => ({
    props: { ...args },
    template: templateSelection,
  }),
};

function onSelect(value: number) {
  alert('click on element : ' + value);
}

const templateCustom = `
<dsfrx-dropdownmenu [labelMenu]="'Save as... '">
  <ng-template #contentTemplate>
    <dsfrx-dropdownmenu-item (selectItem)="onSelect(1)">
      <span>Elément custom 
      <i class="fr-icon-accessibility-line"></i></span>
    </dsfrx-dropdownmenu-item>
    <dsfrx-dropdownmenu-item (selectItem)="onSelect(2)">
      <div><b>Elément</b></div>
    </dsfrx-dropdownmenu-item>
  </ng-template>
</dsfrx-dropdownmenu>`;

export const CustomElement: Story = {
  args: {
    //...Default.args,
    labelMenu: 'Save as...',
  },
  render: (args) => ({
    props: { ...args, onSelect },
    template: templateCustom,
  }),
};
