import { Component, Input, ViewEncapsulation } from '@angular/core';
import {
  DsfrBadge,
  DsfrButtonModule,
  DsfrButtonsGroupModule,
  DsfrCardModule,
  DsfrHeadingLevel,
  DsfrModalAction,
} from '@edugouvfr/ngx-dsfr';

//TODO: dans ngx-dsfr, sortir DsfrModalAction pour le partager entre la modal et la timeline (shared_bak + public-api).
export interface DsfrTimelineAction extends DsfrModalAction {
  disabled?: boolean | undefined;
}

export interface DsfrTimelineEvent {
  heading: string;
  headingLevel?: DsfrHeadingLevel;
  description?: string;
  badges?: DsfrBadge[];
  actions?: DsfrTimelineAction[];
}

@Component({
  selector: 'dsfr-ext-timeline, dsfrx-timeline',
  templateUrl: './timeline.component.html',
  styleUrls: ['./timeline.scss'],
  encapsulation: ViewEncapsulation.None,
  standalone: true,
  imports: [DsfrCardModule, DsfrButtonModule, DsfrButtonsGroupModule],
})
export class DsfrTimelineComponent {
  /**
   * Liste d'évènements.
   */
  @Input() events: DsfrTimelineEvent[] = [];

  /** @internal */
  performAction(action: DsfrTimelineAction): void {
    if (action.callback) {
      action.callback();
    }
  }
}
