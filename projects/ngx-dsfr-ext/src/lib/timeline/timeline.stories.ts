import { DsfrButtonModule, DsfrButtonsGroupModule, DsfrCardModule } from '@edugouvfr/ngx-dsfr';
import { Meta, StoryFn, moduleMetadata } from '@storybook/angular';
import { DsfrTimelineComponent } from './timeline.component';

const meta: Meta = {
  title: 'Components/Timeline',
  component: DsfrTimelineComponent,
  decorators: [moduleMetadata({ imports: [DsfrButtonModule, DsfrCardModule, DsfrButtonsGroupModule] })],
  parameters: {
    docs: {
      toc: {
        disable: true, // Disables the table of contents
      },
    },
  },
};
export default meta;

const Template: StoryFn<DsfrTimelineComponent> = (args) => ({
  props: args,
});

export const Default = {
  render: Template,

  args: {
    events: [
      {
        heading: 'Entre janvier et avril',
        description: 'Vous exprimez vos intentions vers une voie générale ou professionnelle.',
        actions: [
          {
            label: 'Saisir les intentions',
            callback: () => alert('Saisir les intentions'),
          },
        ],
      },
      {
        heading: 'En mai',
        description:
          'Vous indiquez par ordre de préférence vos choix pour formations proposées par les établissements.',
        badges: [
          {
            label: 'Prochainement',
          },
        ],
        actions: [
          {
            label: 'Saisir les choix',
            callback: () => alert('Saisir les choix'),
          },
        ],
      },
      {
        heading: 'En juin',
        description: 'Vous consultez les réponses du conseil de classe et répondez aux propositions.',
        badges: [
          {
            label: 'Prochainement',
          },
        ],
        actions: [
          {
            label: 'Répondre',
            icon: 'fr-icon-alarm-warning-line',
            callback: () => alert('Répondre'),
          },
        ],
      },
      {
        heading: 'En juin',
        description: 'Vous consultez les réponses du conseil de classe et répondez aux propositions.',
        badges: [
          {
            label: 'Prochainement',
          },
        ],
        actions: [
          {
            label: 'Répondre',
            callback: () => alert('Répondre'),
            disabled: true,
          },
          {
            label: 'Voir',
            callback: () => alert('Voir'),
          },
        ],
      },
    ],
  },
};
