import { CdkVirtualScrollViewport, ScrollingModule } from '@angular/cdk/scrolling';
import { CommonModule } from '@angular/common';
import {
  AfterViewInit,
  ChangeDetectionStrategy,
  Component,
  ElementRef,
  Input,
  OnInit,
  ViewChild,
  ViewEncapsulation,
  forwardRef,
} from '@angular/core';
import { FormsModule, NG_VALUE_ACCESSOR, ReactiveFormsModule } from '@angular/forms';
import { DsfrButtonModule, DsfrI18nPipe, DsfrTagModule } from '@edugouvfr/ngx-dsfr';
import { DsfrNode } from '../../public-api';
import { AbstractSelectComponent, DropdownContainerComponent, EduCheckboxComponent } from '../shared/components';
import { DsfrSelectionMode, DsfrSelectionModeConst } from '../shared/models/selection-mode.type';
import { TreeFocusManager, TreeFocusable } from '../shared/utils/tree/tree-focus-manager';
import { TreeSelectManager, TreeSelectable } from '../shared/utils/tree/tree-select-manager';
import { DsfrViewNode } from '../shared/utils/tree/tree.model';

@Component({
  selector: 'dsfr-ext-treeselect, dsfrx-treeselect',
  styleUrls: ['./treeselect.component.scss'],
  templateUrl: './treeselect.component.html',
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush,
  standalone: true,
  imports: [
    CommonModule,
    FormsModule,
    ScrollingModule,
    ReactiveFormsModule,
    DropdownContainerComponent,
    DsfrTagModule,
    DsfrButtonModule,
    EduCheckboxComponent,
    DsfrI18nPipe,
  ],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => DsfrTreeselectComponent),
      multi: true,
    },
  ],
})
export class DsfrTreeselectComponent
  extends AbstractSelectComponent<DsfrViewNode>
  implements OnInit, AfterViewInit, TreeFocusable, TreeSelectable
{
  /** @internal */
  @ViewChild(CdkVirtualScrollViewport) viewport: CdkVirtualScrollViewport;

  /** @internal*/
  @ViewChild('listbox') listbox: ElementRef;

  /** @internal*/
  isAllSelected: boolean = false;

  protected visibleNodes: DsfrViewNode[] = []; // clone de liste utilisé en cas de virtualScroll
  protected countFilteredOptions = -1;

  private treeFocusManager = new TreeFocusManager(this);
  private treeSelectManager = new TreeSelectManager(this);

  // Element HTML correspondant à l'ensemble du composant qui possède un attribut tabindex. */
  private firstNodeElt: HTMLElement;

  /** @internal Equivalent a options */
  get nodes(): DsfrNode[] {
    return this.optionsFilter as DsfrNode[];
  }

  /** @internal Synonyme de value, mais plus explicite. */
  override get options() {
    return this._options;
  }

  /** @internal */
  override get selectionMode() {
    return this._selectionMode;
  }

  /** Personnalisation de la fonction de recherche pour filtrer les options. Fonction à 2 arguments : node (DsfrNode) et texte cherché (string) */
  @Input()
  set searchFn(fn: (n: DsfrNode, text: string) => boolean) {
    if (typeof fn !== 'function') {
      throw Error('`searchFn` must be a function.');
    }
    this._searchFn = fn;

    // reset filter
    this.searchText = '';
    this.optionsFilter = this.optionsFilter ?? [];
  }

  /** Liste des options (DsfrNode[]) disponibles. */
  @Input()
  override set options(val: any[] | undefined) {
    this._options = val;
    this.optionsFilter = this.options ? [...this.options] : [];

    if (!this.treeSelectManager.selectionMode) this.treeSelectManager.selectionMode = this.selectionMode;
    if (this.value !== undefined) {
      this.writeValue(this.value);
    } else {
      this.optionsFilter = this.treeSelectManager.init(this.optionsFilter);
      if (this.virtualScroll) this.visibleNodes = this.optionsFilter;
    }
  }

  /** Mode de sélection. Checkbox par défaut. */
  @Input()
  override set selectionMode(mode: DsfrSelectionMode) {
    super.setSelectionMode(mode);
    this.treeSelectManager.selectionMode = this.selectionMode;
  }

  public override ngOnInit(): void {
    super.ngOnInit();

    if (!this._compareWith.name && this.options && this.options.length > 0) {
      this.treeSelectManager.noGenericId = true;
    }
  }

  /** Positionne un tabindex de 0 sur le premier élément de la liste si aucun n'est sélectionné */
  public ngAfterViewInit(): void {
    const firstElement: HTMLElement = this.listbox.nativeElement?.children[0];

    if (this.treeSelectManager.selectedNodes.length === 0 && firstElement) {
      firstElement.setAttribute('tabindex', '0');
    }
  }

  /**
   * Mise a jour de la valeur de selectedOptions et value programmaticalement (ex. à l'init reactive form)
   * @param value
   * @internal
   */
  override writeValue(value: any) {
    this.treeSelectManager.selectedNodes = [];
    if (value?.length) {
      if (!(value instanceof Array)) {
        throw Error('`value` must be an array.');
      }
      // Sélection dans l'arbre des valeurs sélectionnées programmaticalement
      this.setSelectedNodes(this.nodes, value);
    } else {
      //si des options etaient sélectionnées, on vide la selection de l'arbre
      if (this.selectedOptions.length > 0) this.treeSelectManager.selectAllNodes(this.optionsFilter, false);
      this.indexFirstSelected = null;
    }

    this.optionsFilter = this.treeSelectManager.init(this.optionsFilter);
    if (this.virtualScroll) this.visibleNodes = this.optionsFilter;
    this.selectedOptions = [...this.treeSelectManager.selectedNodes];
    this.searchText = '';

    super.writeValue(value);
    this.cd.markForCheck();
  }

  /** @internal */
  public notifySelectEvent(node: DsfrNode): void {
    // nothing
  }

  /** Mise a jour de l'état des checkbox selon leur id, nécessaire avec le tri en virtualScroll (perte du binding)
   * @internal */
  public updateIndeterminate(node: DsfrNode): void {
    if (this.virtualScroll) {
      const checkboxId = `cb-${node.id}`;
      const inputElt = <HTMLInputElement>this.listbox?.nativeElement?.querySelector('#' + checkboxId);
      if (inputElt) {
        if (node.selected !== undefined) inputElt.checked = node.selected;
        inputElt.indeterminate = node.selected === undefined;
      }
    }
  }

  /**
   * Selection/Deselection du noeud
   * Mise a jour de la liste des noeuds sélectionnés
   * @internal */
  public doNodeDefaultAction(node: DsfrNode, event: Event): void {
    const nodeElt = <HTMLElement>event.currentTarget;
    this.treeSelectManager.doSelectNode(node, nodeElt);

    // Mise a jour de la value
    this.selectedOptions = this.treeSelectManager.updateSelectedNodes();
    this.value = this.selectedOptions;
    this.selectionChange.emit(this.value);
  }

  /** @internal */
  public findFocusElement(): HTMLElement {
    const nativeElt = this.listbox.nativeElement;
    return nativeElt.querySelector('.tree-node-content:focus');
  }

  /** @internal */
  public findFirstSelectedNode(): HTMLElement {
    const nodes = this.listbox?.nativeElement?.querySelectorAll('.tree-node-content .selected');
    if (nodes[0]) {
      return nodes[0];
    }

    return this.findVisibleNodesElt()[0];
  }

  /**
   * Clic sur une option en mode tag supprimable.
   * Déselection option et mise a jour des parents.
   * @internal */
  public clearOption(event: Event, node: DsfrNode) {
    event.preventDefault();
    this.onOptionClick(event, node);
    this.updateValueSelected();
  }

  /**
   * Trouver tous les noeuds <li> visibles
   *  @internal */
  public findVisibleNodesElt(): HTMLElement[] {
    const nativeElt = this.listbox.nativeElement;
    const nodeList = nativeElt.querySelectorAll('.tree-node-content');
    const nodesArr: HTMLElement[] = [];

    nodeList.forEach((n: HTMLElement) => nodesArr.push(n));

    return nodesArr;
  }

  /** @internal */
  public selectAll(): void {
    this.isAllSelected = !this.isAllSelected;
    this.treeSelectManager.selectAllNodes(this.optionsFilter, this.isAllSelected);
    this.updateValueSelected();

    this.selectionChange.emit(this.value);
  }

  /** @internal */
  public clearAllOptions(): void {
    this.isAllSelected = false;
    this.treeSelectManager.selectAllNodes(this.optionsFilter, this.isAllSelected);
    this.updateValueSelected();

    this.selectionChange.emit(this.value);
  }

  /** @internal */
  public expandAll(): void {
    this.treeFocusManager.expandAll(this.optionsFilter);
  }

  /** @internal */
  public collapseAll(): void {
    this.treeFocusManager.collapseAll(this.optionsFilter);
  }

  /** @internal */
  public handleListFocus(): void {
    this.firstNodeElt = this.listbox.nativeElement.querySelector('.tree-node-content');
    this.firstNodeElt.focus();
  }

  /**
   * Au clic sur le label ou la checkbox
   * Sélection ou déselection des noeuds
   * @internal */
  public override onOptionClick(event: Event, node: DsfrNode): void {
    if (node.disabled) return;

    const nodeElt = <HTMLElement>event.currentTarget;
    this.treeSelectManager.doSelectNode(node, nodeElt);

    if (this.selectionMode === DsfrSelectionModeConst.SINGLE && !node.children) {
      // Cacher la liste a la selection si selection simple
      this.hide();
    }

    this.selectedOptions = this.treeSelectManager.updateSelectedNodes();
    this.value = this.selectedOptions;

    this.selectionChange.emit(this.value);
  }

  /**
   * Filtrer les options selon la fonction de recherche
   * @internal */
  public onSearch(): void {
    this.countFilteredOptions = 0;

    if (!this.virtualScroll) {
      // appliquer l'état hidden pour filtrer
      this.optionsFilter.forEach((node) => this.updateNodeVisibility(node));
    } else {
      // Dans le cas du virtual scroll on est obligé de filtrer physiquement les noeuds
      this.visibleNodes = this.optionsFilter
        .map((node) => this.filterNodes(node))
        .filter((node) => node !== null) as DsfrNode[];
    }
  }

  /**
   * Au clic sur les boutons flèches plier/déplier les noeuds
   *  @internal */
  public onNodeButton(node: DsfrNode): void {
    this.treeFocusManager.doToggleExpand(node);
  }

  /**
   * Interaction clavier avec un des noeuds
   * @internal */
  public onNodeKeydown(node: DsfrNode, event: KeyboardEvent): void {
    //retour tab
    if (event.shiftKey && event.key === 'Tab') {
      if (this.showSearch && this.appendTo) {
        // manually place focus back to search container if appendTo is active
        this.searchBox?.nativeElement?.focus();
      }
      return;
    }

    if (event.key === 'Escape' || event.key === 'Tab') {
      this.hide();
      return;
    }
    if (event.key === 'Enter') {
      this.treeFocusManager.doKeyEvent(node, event, !this.isSingleMode());
      this.hide();
      return;
    }

    this.treeFocusManager.doKeyEvent(node, event, !this.isSingleMode());

    if (!this.virtualScroll) {
      this.treeFocusManager.currentElt?.scrollIntoView({ behavior: 'smooth', block: 'center' });
    } else {
      this.treeFocusManager.scrollForVirtualScroll(this.viewport);
    }
  }

  /** @internal */
  trackById(index: number, item: DsfrNode): string | undefined {
    return item?.id;
  }

  /** @internal */
  public isCheckboxMode(): boolean {
    return this.treeSelectManager.isCheckboxMode();
  }

  /** @internal */
  public isSelectable(): boolean {
    return this.treeSelectManager.isSelectable();
  }

  /** @internal */
  public isSelected(n: DsfrNode) {
    if (this.isSingleMode()) {
      return this._compareWith(this.value, n);
    }
    if (this.value && !(this.value instanceof Array)) throw Error('`value` must be an array.');
    return !!this.value?.find((o: any) => this._compareWith(o, n));
  }

  /** Surcharge de compareWith par défaut pour utiliser les id */
  protected override _compareWith: (o1: any, o2: any) => boolean = (o1, o2) => o1?.id === o2?.id;

  /** Mise a jour selectedOptions puis value */
  private updateValueSelected() {
    this.selectedOptions = [...this.treeSelectManager.selectedNodes];
    this.value = this.selectedOptions;

    this.cd.markForCheck();
  }

  /** Fonction de recherche par défaut */
  private _searchFn: (n: DsfrNode, searchText: string) => boolean = function (o, searchText) {
    return o.label?.toLowerCase().includes(searchText.toLowerCase());
  };

  /**
   * Filtre visuellement les nœuds en appliquant `hidden` en fonction du résultat de la recherche.
   * - nœud parent visible : correspond à la recherche ou au moins un de ses enfants est visible.
   * - nœud enfant visible : correspond à la recherche ou un de ses ancêtres correspond à la recherche.
   * @param node Le nœud à filtrer de manière récursive.
   * @returns `true` si le nœud est visible, sinon `false`.
   */
  private updateNodeVisibility(node: DsfrNode, isAncestorVisible?: boolean): boolean {
    const isMatch = this._searchFn(node, this.searchText);
    // si un des ancètres est visible on conserve la visibilité
    const isNodeVisible = isAncestorVisible || isMatch;

    if (node.children) {
      // Appliquer hidden récursivement aux enfants
      const anyChildVisible = node.children.reduce((acc, child) => {
        return this.updateNodeVisibility(child, isNodeVisible) || acc;
      }, false);

      node.hidden = !(isMatch || anyChildVisible);
      if (!node.hidden && this.searchText.length > 1) node.expanded = true;
    } else {
      node.hidden = !isNodeVisible;
    }

    if (!node.hidden) {
      this.countFilteredOptions++;
    }

    return !node.hidden;
  }

  /**
   * Filtrer physiquement les noeuds dans le cadre du virtualScroll
   * /!\ Le virtualScroll ne permet pas d'utiliser la propriété hidden
   * Identique à updateNodeVisibility mais supprime au lieu d'utiliser la propriété hidden
   * @param node Le nœud à filtrer de manière récursive.
   * @returns le noeud si match, null sinon
   */
  private filterNodes(node: DsfrNode): DsfrNode | null {
    const isMatch = this._searchFn(node, this.searchText);

    if (isMatch) {
      this.countFilteredOptions++;
    }

    if (node.children) {
      // Filtrer récursivement les enfants
      const filteredChildren = node.children
        .map((child) => this.filterNodes(child))
        .filter((child) => child !== null) as DsfrNode[];

      // match ou un des enfants match
      if (isMatch || filteredChildren.length > 0) {
        return { ...node, children: isMatch ? node.children : filteredChildren };
      }
      return null; // suppression du noeud
    }

    return isMatch ? node : null;
  }

  /**
   * Parcours d'arbre pour mettre a jour la valeur selected des nodes à l'init,
   * selon les valeurs sélectionnées programmatiquement
   * @param nodes arbre complet
   * @param value liste des noeuds sélectionnés
   */
  private setSelectedNodes(nodes: DsfrNode[], value: DsfrNode[]) {
    nodes.forEach((n) => {
      if (!n.children) {
        // recherche du noeud correspondant dans la liste sélectionnée
        if (this.isSingleMode()) {
          n.selected = this._compareWith(n, value);
        } else {
          n.selected = value.find((o: DsfrNode) => this._compareWith(n, o)) ? true : false;
        }
      }

      // Passage aux enfants
      if (n.children) this.setSelectedNodes(n.children, value);
    });
  }
}
