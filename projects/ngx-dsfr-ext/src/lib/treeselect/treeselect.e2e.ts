import test, { expect, Locator } from '@playwright/test';
import { BasePageObjectModel } from 'projects/ngx-dsfr-ext/shared/e2e.utils';

test.describe('TreeSelect', () => {
  let treeSelect: TreeselectPom;

  test.beforeEach(async ({ page }) => {
    treeSelect = new TreeselectPom(page, 'treeselect');
  });

  test('opens on dropdown click', async () => {
    await treeSelect.goToPage();

    await expect(treeSelect.tree).not.toBeVisible();

    await treeSelect.openDropDown();

    await expect(treeSelect.tree).toBeVisible();
  });

  test('properly displays selected item onto its dropdown', async () => {
    await treeSelect.goToPage();
    await treeSelect.openDropDown();
    await treeSelect.clickOnTreeItem('Genres');

    await expect(treeSelect.dropdown).toContainText(
      'The Godfather, Scarface, Pulp Fiction, Forrest Gump, The Shawshank Redemption',
    );

    await treeSelect.clickOnTreeItem('Directors');

    await expect(treeSelect.dropdown).toContainText('9 sélectionnés');

    await treeSelect.clickOnTreeItem('Genres');

    await expect(treeSelect.dropdown).toContainText('Goodfellas, Taxi Driver, The Godfather, Apocalypse Now');
  });

  test.describe('with search field', () => {
    test.beforeEach(async () => {
      await treeSelect.goToPage('with-search-field');
    });

    test('filters tree nodes by labels', async () => {
      await treeSelect.openDropDown();
      await treeSelect.typeInSearchInput('co');

      await expect(treeSelect.findTreeItemLabel('Martin Scorsese')).toBeVisible();
      await expect(treeSelect.findTreeItemLabel('Francis Ford Coppola')).toBeVisible();
      await expect(treeSelect.findTreeItemLabel('Genres')).not.toBeVisible();
    });

    test('does not select filtered nodes', async () => {
      await treeSelect.openDropDown();
      await treeSelect.typeInSearchInput('Actors');
      await treeSelect.clickToutSelectionner();

      await expect(treeSelect.dropdown).toContainText('Scarface, Heat, Goodfellas, Raging Bull');
    });
  });

  test.describe('with clearable options', () => {
    test.beforeEach(async () => {
      await treeSelect.goToPage('with-clearable-options');
    });

    test('display selected options on selection', async () => {
      await treeSelect.openDropDown();
      await treeSelect.clickOnTreeItem('Genres');

      expect((await treeSelect.clearableOptions.all()).length).toBe(5);
      await expect(treeSelect.dropdown).toContainText(
        'The Godfather Scarface Pulp Fiction Forrest Gump The Shawshank Redemption',
      );
    });

    test('unselect the option items when clicked', async () => {
      await treeSelect.openDropDown();
      await treeSelect.clickOnTreeItem('Genres');
      await treeSelect.clickClearableOption('Forrest Gump');
      await treeSelect.expandNode('Genres');
      await treeSelect.expandNode('Drama');

      await expect(treeSelect.findTreeItemCheckBox('Forrest Gump')).not.toBeChecked();
      await expect(treeSelect.findTreeItemCheckBox('The Shawshank Redemption')).toBeChecked();
    });
  });
});

class TreeselectPom extends BasePageObjectModel {
  /**
   * LOCATORS
   */
  get dropdown(): Locator {
    return this.frame.getByLabel('Label Description').getByRole('status');
  }

  get tree(): Locator {
    return this.frame.getByRole('tree');
  }

  get toutSelectionnerBtn(): Locator {
    return this.frame.getByRole('button', { name: 'Tout sélectionner' });
  }

  get clearableOptions(): Locator {
    return this.dropdown.locator('button');
  }

  /**
   * ACTIONS
   */
  findTreeItem(itemLabel: string): Locator {
    return this.tree.getByRole('treeitem', { name: itemLabel });
  }

  findTreeItemLabel(itemLabel: string): Locator {
    return this.tree.locator('label').filter({ hasText: itemLabel });
  }

  findTreeItemCheckBox(itemLabel: string): Locator {
    return this.findTreeItem(itemLabel).locator('input').first();
  }

  async openDropDown(): Promise<void> {
    return this.dropdown.click();
  }

  async clickOnTreeItem(itemLabel: string): Promise<void> {
    const item = this.findTreeItemLabel(itemLabel);

    return item.click();
  }

  async typeInSearchInput(text: string): Promise<void> {
    const searchInput = this.frame.getByPlaceholder('Rechercher');

    return searchInput.fill(text);
  }

  async clickToutSelectionner(): Promise<void> {
    return this.toutSelectionnerBtn.click();
  }

  async clickClearableOption(text: string): Promise<void> {
    return this.clearableOptions.filter({ hasText: text }).click();
  }

  async expandNode(itemLabel: string): Promise<void> {
    return this.tree.getByRole('treeitem', { name: itemLabel }).locator('span').first().click();
  }
}
