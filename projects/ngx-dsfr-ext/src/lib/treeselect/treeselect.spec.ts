import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';

import { Component, ViewChild } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { DsfrNode } from '../../public-api';
import { DsfrTreeselectComponent } from './treeselect.component';

@Component({
  selector: `edu-host-component`,
  template: `<dsfr-ext-treeselect [options]="files"></dsfr-ext-treeselect>`,
})
class TestHostComponent {
  @ViewChild(DsfrTreeselectComponent)
  public treeselectComponent: DsfrTreeselectComponent;

  files: DsfrNode[] = [
    {
      label: 'Documents',
      children: [
        {
          label: 'Work',
          children: [
            {
              label: 'Expenses.doc',
              children: [{ label: 'Insurance.doc' }, { label: 'Debt.doc' }],
            },
            { label: 'Resume.doc' },
          ],
        },
        { label: 'Home', children: [{ label: 'Invoices.txt' }] },
      ],
    },
    {
      label: 'Events',
      expanded: true,
      children: [{ label: 'Meeting' }, { label: 'Product Launch' }, { label: 'Report Review' }],
    },
  ];
}

describe('DsfrTreeselectComponent', () => {
  let testHostComponent: TestHostComponent;
  let fixture: ComponentFixture<TestHostComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [FormsModule, DsfrTreeselectComponent],
      declarations: [TestHostComponent],
    }).compileComponents();

    fixture = TestBed.createComponent(TestHostComponent);
    testHostComponent = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should set selectId and validate attributes', () => {
    const treeselectComponent = testHostComponent.treeselectComponent;
    treeselectComponent.selectId = 'test';
    treeselectComponent.ngOnInit();
    fixture.detectChanges();
    const inputEl = fixture.nativeElement.querySelector('.fr-select');
    expect(inputEl.getAttribute('role')).toBe('combobox');
    expect(inputEl.getAttribute('id')).toBe('test');
  });

  describe('DsfrTreeSelectComponent dropdown manipulation', () => {
    function openDropdown(): HTMLElement {
      testHostComponent.treeselectComponent.ngOnInit();
      fixture.detectChanges();

      const inputEl: HTMLElement = fixture.nativeElement.querySelector('.fr-select');
      inputEl.click();
      tick(100);
      fixture.detectChanges();

      return inputEl;
    }

    it('should display correct number of nodes ', fakeAsync(() => {
      const inputEl = openDropdown();
      const listEl = fixture.nativeElement.querySelector('.dsfrx-multiselect__list');

      expect(testHostComponent.treeselectComponent.displayList).toBeTruthy();
      // 2 parents nodes
      expect(listEl.children).toHaveLength(2);
      // parent node 1 not expanded
      expect(listEl.children[0].querySelector('.tree-node')).toBeNull();
      // parent node 2 expanded
      expect(listEl.children[1].querySelector('.tree-node').children).toHaveLength(3);
      expect(listEl.getAttribute('role')).toEqual('tree');
      expect(listEl.getAttribute('aria-multiselectable')).toEqual('true');
      expect(inputEl.textContent).toContain('Sélectionner une option');
    }));

    it('should expand children of first node on arrow click ', fakeAsync(() => {
      const inputEl = openDropdown();
      const listEl = fixture.nativeElement.querySelector('.dsfrx-multiselect__list');
      const firstNode = listEl.children[0];

      // parent node 1 not expanded
      expect(firstNode.querySelector('.tree-node')).toBeFalsy();
      expect(firstNode.getAttribute('aria-expanded')).toEqual('false');
      expect(firstNode.getAttribute('role')).toEqual('treeitem');

      firstNode.querySelector('.tree-button').click();
      tick();
      fixture.detectChanges();

      // parent node 2 expanded
      expect(firstNode.querySelector('.tree-node').children).toHaveLength(2);
      expect(firstNode.getAttribute('aria-expanded')).toEqual('true');
    }));

    it('should select parent node and all child nodes on click', fakeAsync(() => {
      const inputEl = openDropdown();
      const listEl = fixture.nativeElement.querySelector('.dsfrx-multiselect__list');
      const parentNode = listEl.children[1];
      const childNodes = parentNode.querySelectorAll('li');

      expect(listEl.children[0].getAttribute('aria-selected')).toBe('false');

      parentNode.querySelector('.fr-label').click(); // click on parent node
      tick();
      fixture.detectChanges();

      // all childs nodes selected
      expect(inputEl.children[0].children).toHaveLength(3); // input set to 3 elements
      expect(parentNode.getAttribute('aria-selected')).toEqual('true'); // parent node selected
      expect(parentNode.querySelector('li').getAttribute('aria-selected')).toEqual('true'); // child node selected
      expect([...childNodes].every((node: any) => node.getAttribute('aria-selected') === 'true')).toBe(true); // all child nodes selected

      expect(listEl.children[0].getAttribute('aria-selected')).toBe('false'); // other parent node not selected
    }));

    it('should set parent node to indeterminate when unselecting one child node ', fakeAsync(() => {
      const inputEl = openDropdown();
      const listEl = fixture.nativeElement.querySelector('.dsfrx-multiselect__list');
      const parentNode = listEl.children[1];
      parentNode.querySelector('.fr-label').click(); // click on parent node
      tick();
      fixture.detectChanges();
      expect(parentNode.querySelector('input').indeterminate).toBe(false);

      const childNode = parentNode.querySelector('li');
      childNode.querySelector('.fr-label').click(); // unselect child
      tick(100);
      fixture.detectChanges();

      // all childs nodes selected
      expect(inputEl.children[0].children).toHaveLength(2); // input set to 2 elements
      expect(parentNode.getAttribute('aria-selected')).toEqual('false'); // parent node unselected
      expect(childNode.getAttribute('aria-selected')).toEqual('false'); // child node unselected
      expect(parentNode.querySelector('input').indeterminate).toBe(true);
    }));

    it('should set parent node to unselected when unselecting all child nodes ', fakeAsync(() => {
      const inputEl = openDropdown();
      const listEl = fixture.nativeElement.querySelector('.dsfrx-multiselect__list');
      const parentNode = listEl.children[1];
      parentNode.querySelector('.fr-label').click(); // click on parent node
      tick();
      fixture.detectChanges();
      expect(parentNode.querySelector('input').indeterminate).toBe(false);

      const childNodes = parentNode.querySelectorAll('li');
      childNodes.forEach((childNode: any) => {
        childNode.querySelector('.fr-label').click(); // unselect child
      });
      tick(100);
      fixture.detectChanges();

      expect(inputEl.textContent).toContain('Sélectionner une option'); // no elements selected
      expect(parentNode.getAttribute('aria-selected')).toEqual('false'); // parent node unselected
      expect([...childNodes].every((node: any) => node.getAttribute('aria-selected') === 'false')).toBe(true); // all child nodes unselected
      expect(parentNode.querySelector('input').indeterminate).toBe(false);
    }));
  });
});
