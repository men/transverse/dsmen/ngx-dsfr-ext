import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { DsfrButtonModule, DsfrFormInputModule } from '@edugouvfr/ngx-dsfr';
import { Meta, moduleMetadata, StoryObj } from '@storybook/angular';
import { DsfrNode } from '../../shared/utils/tree/tree.model';
import { DemoTreeselectComponent } from './demo-treeselect.component';

const meta: Meta<DemoTreeselectComponent> = {
  title: 'Components/Treeselect',
  component: DemoTreeselectComponent,
  decorators: [moduleMetadata({ imports: [FormsModule, ReactiveFormsModule, DsfrFormInputModule, DsfrButtonModule] })],
  parameters: { actions: false },
};
export default meta;
type Story = StoryObj<DemoTreeselectComponent>;

const options: DsfrNode[] = [
  {
    id: 'myid',
    label: 'Test',
    children: [
      {
        id: 'myid',
        label: 'Test child 1',
      },
      {
        id: 'myid',
        label: 'Test child 2',
      },
    ],
  },
];

const instrumentsData: DsfrNode[] = [
  {
    label: 'Cordes',
    children: [
      { label: 'Guitare' },
      {
        label: 'Violons',
        children: [{ label: 'Violon électrique' }, { label: 'Violon acoustique' }],
      },
      { label: 'Violoncelle' },
    ],
  },
  {
    label: 'Vent',
    children: [{ label: 'Flûte' }, { label: 'Saxophone', id: 'id-saxo', data: 'data' }],
  },
  {
    label: 'Percussions',
    children: [
      {
        label: 'Batterie',
        children: [{ label: 'Caisse claire' }, { label: 'Tambour' }, { label: 'Cymbales' }],
      },
      { label: 'Tambour' },
      { label: 'Xylophone' },
    ],
  },
  {
    label: 'Electroniques',
    children: [{ label: 'Synthétiseur' }, { label: 'Boîte à rythmes' }],
  },
];

/** Default */
export const ReactiveForm: Story = {
  args: {
    options: instrumentsData,
    maxDisplayOptions: 2,
    appendToBody: true,
    showSearch: true,
  },
};
