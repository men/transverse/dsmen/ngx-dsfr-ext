import { CommonModule } from '@angular/common';
import { Component, Input, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, FormsModule, ReactiveFormsModule, Validators } from '@angular/forms';
import {
  DsfrAccordionModule,
  DsfrButtonModule,
  DsfrFormInputModule,
  DsfrFormSelectModule,
  DsfrOption,
} from '@edugouvfr/ngx-dsfr';
import { delay, finalize, Observable, of } from 'rxjs';
import { DemoToolbarComponent } from '../../shared/demo/demo-toolbar.component';
import { DsfrNode } from '../../shared/utils/tree/tree.model';
import { DsfrTreeselectComponent } from '../treeselect.component';

@Component({
  selector: `demo-treeselect`,
  templateUrl: './demo-treeselect.component.html',
  styleUrls: ['./demo-treeselect.component.scss'],
  standalone: true,
  imports: [
    CommonModule,
    FormsModule,
    DsfrTreeselectComponent,
    DsfrFormInputModule,
    DsfrButtonModule,
    ReactiveFormsModule,
    DsfrAccordionModule,
    DsfrFormSelectModule,
    DemoToolbarComponent,
  ],
})
export class DemoTreeselectComponent implements OnInit {
  /** @internal */ @Input() options: DsfrNode[];
  /** @internal */ @Input() appendToBody: boolean;
  /** @internal */ @Input() maxDisplayOptions: number;
  /** @internal */ @Input() showSearch: boolean;

  /** @internal */ @Input() myOptionsAsync: Observable<DsfrNode[]> = of([]);

  protected value = null;
  protected myOptions: DsfrNode[] = [
    {
      label: 'Cordes',
      children: [
        { label: 'Guitare' },
        {
          label: 'Violons',
          children: [
            { label: 'Violon électrique', id: 'id-violon-elec' },
            { label: 'Violon acoustique', id: 'id-violon' },
          ],
        },
        { label: 'Violoncelle' },
      ],
    },
    {
      label: 'Vent',
      children: [
        { label: 'Flûte', id: 'id-flute' },
        { label: 'Saxophone', id: 'id-saxo', data: 'data' },
      ],
    },
    {
      label: 'Percussions',
      children: [
        {
          label: 'Batterie',
          id: 'batterie',
          children: [{ label: 'Caisse claire' }, { label: 'Tambour' }, { label: 'Cymbales' }],
        },
        { label: 'Tambour', id: 'id-tamb' },
        { label: 'Xylophone', id: 'id-xyl' },
      ],
    },
    {
      label: 'Electroniques',
      children: [
        { label: 'Synthétiseur', id: 'id-synth' },
        { label: 'Boîte à rythmes', id: 'id-rythm' },
      ],
    },
  ];
  /** @internal */
  protected form: FormGroup;
  /**@internal */
  protected searchFn: (item: DsfrNode, text: string) => boolean;
  /**@internal */
  protected optionSelect: DsfrOption[] = [
    { label: 'contient', value: 1 },
    { label: 'commence par', value: 2 },
  ];
  protected loading: boolean;

  protected hideBtbLabel: string;

  private labelHideNode = 'Masquer <Batterie>';

  private labelShowNode = 'Afficher <Batterie>';

  constructor(private fb: FormBuilder) {
    this.hideBtbLabel = this.labelHideNode;
    this.form = this.fb.group({
      test: [[{ id: '3', libelle: 'Libellé 4' }], Validators.required],
    });
  }

  /** @internal */
  get formControl(): FormControl {
    return this.form.get('formControl') as FormControl;
  }

  /**@internal */
  ngOnInit(): void {
    this.form.addControl('formControl', new FormControl([], Validators.required));
    this.form.addControl('formControl2', new FormControl([], Validators.required));
    this.formControl.setValue([
      this.myOptions[1].children![1],
      this.myOptions[1].children![0],
      this.myOptions[0].children![1].children![0],
    ]);

    this.searchFn = this.searchIncludes;

    this.myOptionsAsync = of(this.myOptions).pipe(
      delay(200),
      finalize(() => (this.loading = false)),
    );
  }

  /**@internal */
  updateSelect(value: any) {
    if (value === 1) {
      this.searchFn = this.searchIncludes;
    } else {
      this.searchFn = this.searchBeginsWith;
    }
  }

  /**@internal */
  compareFn(item: any, selected: any) {
    return item.id === selected.id;
  }

  /**@internal */
  searchIncludes(item: DsfrNode, text: string) {
    return item.label.toLowerCase().includes(text.toLowerCase());
  }

  /**@internal */
  searchBeginsWith(item: DsfrNode, text: string) {
    return item.label.toLowerCase().startsWith(text.toLowerCase());
  }

  /**@internal */
  updateSearchFunction() {
    this.searchFn = this.searchBeginsWith;
  }

  /**@internal */
  reset() {
    //@ts-ignore
    this.formControl.setValue([
      this.myOptions[1].children![1],
      this.myOptions[0].children![2],
      this.myOptions[1].children![0],
    ]);
  }

  /**@internal */
  noSelect() {
    this.formControl.reset();
  }

  /**@internal */
  switchSelect() {
    //@ts-ignore
    this.formControl.setValue([this.myOptions[2].children[0], this.myOptions[2].children[1]]);
  }

  protected hideNode() {
    const nodes = this.myOptions[2].children;
    if (nodes) {
      const nodeToHide = nodes[0];
      nodeToHide.hidden = !!!nodeToHide.hidden;
      this.hideBtbLabel = nodeToHide.hidden ? this.labelShowNode : this.labelHideNode;
      this.myOptions = [...this.myOptions];
    }
  }
}
