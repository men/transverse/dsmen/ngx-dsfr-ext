import { controlEmitter } from '.storybook/storybook-utils';
import { FormsModule } from '@angular/forms';
import { DsfrAccordionModule, DsfrButtonModule, DsfrFormInputModule, DsfrSeverityConst } from '@edugouvfr/ngx-dsfr';
import { Meta, StoryObj, moduleMetadata } from '@storybook/angular';
import { DropdownContainerComponent } from '../shared/components';
import { DsfrSelectionModeConst } from '../shared/models/selection-mode.type';
import { DsfrNode } from '../shared/utils/tree/tree.model';
import { DsfrTreeselectComponent } from './treeselect.component';

const meta: Meta<DsfrTreeselectComponent> = {
  title: 'Components/Treeselect',
  component: DsfrTreeselectComponent,
  decorators: [
    moduleMetadata({
      imports: [FormsModule, DsfrFormInputModule, DsfrButtonModule, DsfrAccordionModule, DropdownContainerComponent],
    }),
  ],
  parameters: {
    actions: false,
    docs: {
      toc: {
        disable: true, // Disables the table of contents
      },
    },
  },
  argTypes: {
    selectionChange: { control: controlEmitter },
    maxSelectedOptions: { control: { type: 'number' } },
    compareWith: { control: { type: 'object' } },
    selectionMode: { control: { type: 'inline-radio' }, options: ['single', 'multiple', 'checkbox'] },
    messageSeverity: { control: 'inline-radio', options: Object.values(DsfrSeverityConst) },
  },
};
export default meta;

type Story = StoryObj<DsfrTreeselectComponent>;

const library: DsfrNode[] = [
  {
    label: 'Bibliothèque',
    icon: 'fr-icon-book-2-fill',
    children: [
      {
        label: 'Littérature Classique',
        icon: 'fr-icon-book-2-line',
        children: [
          {
            label: 'Romans Français',
            icon: 'fr-icon-file-fill',
            children: [
              { label: 'Les Misérables', icon: 'fr-icon-file-line' },
              { label: 'Le Comte de Monte-Cristo', icon: 'fr-icon-file-line' },
            ],
          },
          {
            label: 'Romans Étrangers',
            icon: 'fr-icon-file-fill',
            children: [
              { label: 'Orgueil et Préjugés', icon: 'fr-icon-file-line' },
              { label: 'Guerre et Paix', icon: 'fr-icon-file-line' },
            ],
          },
        ],
      },
      {
        label: 'Contes et Fables',
        icon: 'fr-icon-star-line',
        children: [
          {
            label: 'Contes de Perrault',
            icon: 'fr-icon-star-line',
            children: [
              { label: 'Le Petit Chaperon Rouge', icon: 'fr-icon-film-line' },
              { label: 'La Belle au Bois Dormant', icon: 'fr-icon-film-line' },
            ],
          },
          {
            label: 'Fables de La Fontaine',
            icon: 'fr-icon-star-line',
            children: [
              { label: 'Le Corbeau et le Renard', icon: 'fr-icon-film-line' },
              { label: 'La Cigale et la Fourmi', icon: 'fr-icon-film-line' },
            ],
          },
        ],
      },
    ],
  },
  {
    label: 'Sélection littéraire',
    icon: 'fr-icon-star-line',
    children: [
      {
        label: 'Poésie',
        icon: 'fr-icon-star-line',
        children: [
          { label: 'Les Fleurs du Mal', icon: 'fr-icon-film-line' },
          { label: 'Les Contemplations', icon: 'fr-icon-film-line' },
        ],
      },
      {
        label: 'Théâtre',
        icon: 'fr-icon-star-line',
        children: [
          { label: 'Hamlet', icon: 'fr-icon-film-line' },
          { label: 'Le Malade Imaginaire', icon: 'fr-icon-film-line' },
        ],
      },
    ],
  },
];

const librarySingle: DsfrNode[] = [
  {
    label: 'Littérature Classique',
    icon: 'fr-icon-book-2-line',
    children: [
      {
        label: 'Romans Français',
        icon: 'fr-icon-file-fill',
        children: [
          { label: 'Les Misérables', icon: 'fr-icon-file-line' },
          { label: 'Le Comte de Monte-Cristo', icon: 'fr-icon-file-line' },
        ],
      },
      {
        label: 'Romans Étrangers',
        icon: 'fr-icon-file-fill',
        children: [
          { label: 'Orgueil et Préjugés', icon: 'fr-icon-file-line' },
          { label: 'Guerre et Paix', icon: 'fr-icon-file-line' },
        ],
      },
    ],
  },
  {
    label: 'Contes et Fables',
    icon: 'fr-icon-star-line',
    children: [
      {
        label: 'Contes de Perrault',
        icon: 'fr-icon-star-line',
        children: [
          { label: 'Le Petit Chaperon Rouge', icon: 'fr-icon-film-line' },
          { label: 'La Belle au Bois Dormant', icon: 'fr-icon-film-line' },
        ],
      },
      {
        label: 'Fables de La Fontaine',
        icon: 'fr-icon-star-line',
        children: [
          { label: 'Le Corbeau et le Renard', icon: 'fr-icon-film-line' },
          { label: 'La Cigale et la Fourmi', icon: 'fr-icon-film-line' },
        ],
      },
    ],
  },
];

const files: DsfrNode[] = [
  {
    label: 'Genres',
    children: [
      {
        label: 'Crime',
        children: [
          {
            label: 'Classics',
            children: [{ label: 'The Godfather' }, { label: 'Scarface' }],
          },
          {
            label: 'Neo-Noir',
            children: [{ label: 'Pulp Fiction' }],
          },
        ],
      },
      {
        label: 'Drama',
        children: [{ label: 'Forrest Gump' }, { label: 'The Shawshank Redemption' }],
      },
    ],
  },
  {
    label: 'Directors',
    expanded: true,
    children: [
      {
        label: 'Martin Scorsese',
        children: [{ label: 'Goodfellas' }, { label: 'Taxi Driver' }],
      },
      {
        label: 'Francis Ford Coppola',
        children: [{ label: 'The Godfather' }, { label: 'Apocalypse Now' }],
      },
    ],
  },
  {
    label: 'Actors',
    children: [
      {
        label: 'Al Pacino',
        children: [{ label: 'Scarface' }, { label: 'Heat' }],
      },
      {
        label: 'Robert De Niro',
        children: [{ label: 'Goodfellas' }, { label: 'Raging Bull' }],
      },
    ],
  },
];

function generateFiles(): DsfrNode[] {
  const files: DsfrNode[] = [];
  for (let i = 0; i < 1500; i++) {
    files.push({
      label: 'Movies ' + i,
      children: [
        {
          label: 'Al Pacino',
          children: [{ label: 'Scarface' }, { label: 'Heat' }],
        },
        {
          label: 'Robert De Niro',
          children: [{ label: 'Goodfellas' }, { label: 'Raging Bull' }],
        },
      ],
    });
  }
  return files;
}

const filesLarge: DsfrNode[] = generateFiles();

export const Default: Story = {
  args: {
    label: 'Label',
    hint: 'Description additionnelle',
    selectId: 'my-select',
    scrollHeight: '224px',
    value: [],
    options: files,
    disabled: false,
    selectionMode: DsfrSelectionModeConst.CHECKBOX,
    maxDisplayOptions: 5,
    showSelectAll: true,
    showSearch: false,
    noResultsMessage: 'Aucun résultat',
    clearSearchOnHide: false,
    virtualScroll: false,
  },
};

export const WithIcons: Story = {
  args: {
    ...Default.args,
    value: [],
    options: library,
  },
};

export const WithErrorMessage: Story = {
  args: {
    ...Default.args,
    value: [],
    options: library,
    error: "Message d'erreur",
  },
};

export const WithSearchField: Story = {
  args: {
    ...Default.args,
    showSearch: true,
    options: [...files],
  },
};

export const VirtualScroll: Story = {
  args: {
    ...Default.args,
    showSearch: true,
    options: filesLarge,
    virtualScroll: true,
  },
};

export const Single: Story = {
  args: {
    ...Default.args,
    showSearch: true,
    selectionMode: 'single',
    options: [...librarySingle],
  },
};

export const Disabled: Story = {
  args: {
    ...Default.args,
    showSearch: true,
    selectionMode: DsfrSelectionModeConst.CHECKBOX,
    disabled: true,
    options: [...files],
  },
};

const filesWithDisabled: DsfrNode[] = [
  {
    label: 'Littérature Classique',
    icon: 'fr-icon-book-2-line',
    children: [
      {
        label: 'Romans Français',
        icon: 'fr-icon-file-fill',
        children: [
          { label: 'Les Misérables', icon: 'fr-icon-file-line' },
          { label: 'Le Comte de Monte-Cristo', icon: 'fr-icon-file-line' },
        ],
      },
      {
        label: 'Romans Étrangers',
        icon: 'fr-icon-file-fill',
        children: [
          { label: 'Orgueil et Préjugés', icon: 'fr-icon-file-line', disabled: true },
          { label: 'Guerre et Paix', icon: 'fr-icon-file-line' },
        ],
      },
    ],
  },
  {
    label: 'Contes et Fables',
    icon: 'fr-icon-star-line',
    children: [
      {
        label: 'Contes de Perrault',
        icon: 'fr-icon-star-line',
        disabled: true,
        children: [
          { label: 'Le Petit Chaperon Rouge', disabled: true, icon: 'fr-icon-film-line', selected: true },
          { label: 'La Belle au Bois Dormant', disabled: false, icon: 'fr-icon-film-line' },
        ],
      },
      {
        label: 'Fables de La Fontaine',
        icon: 'fr-icon-star-line',
        children: [
          { label: 'Le Corbeau et le Renard', icon: 'fr-icon-film-line' },
          { label: 'La Cigale et la Fourmi', icon: 'fr-icon-film-line' },
        ],
      },
    ],
  },
];
export const WithDisabledNodes: Story = {
  args: {
    label: 'Label',
    hint: 'Description additionnelle',
    value: [],
    options: filesWithDisabled,
  },
};

export const WithClearableOptions: Story = {
  args: {
    label: 'Label',
    hint: 'Description additionnelle',
    value: [],
    options: [...files],
    clearableOptions: true,
  },
};

const templateAccordion = `<dsfr-accordion heading="Test">
<ng-container content>
  <dsfr-ext-treeselect
  [label]="label"
  [options]="options"
  [appendTo]="appendTo"
  [zIndex]="zIndex"
  [showSearch]="showSearch"
  [hint]="hint"
  [(ngModel)]="value"></dsfr-ext-treeselect>
</ng-container>
</dsfr-accordion>`;

/** Inside container */
export const InsideContainer: Story = {
  args: {
    ...Default.args,
    label: 'Treeselect avec appendTo',
    hint: "appendTo = 'body' permet d'avoir la liste par-dessus le container (overlay) en l'ajoutant directement au body",
    options: [...files],
    showSearch: true,
    zIndex: 9000,
    appendTo: 'body',
    value: [],
  },
  render: (args) => ({
    props: args,
    template: templateAccordion,
  }),
};
