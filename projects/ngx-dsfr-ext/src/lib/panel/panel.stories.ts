import { controlEmitter } from '.storybook/storybook-utils';
import { DsfrButtonModule, DsfrFormSelectModule, DsfrModalModule } from '@edugouvfr/ngx-dsfr';
import { Meta, StoryFn, StoryObj, moduleMetadata } from '@storybook/angular';
import { DsfrPanelComponent } from './panel.component';

const meta: Meta = {
  title: 'Components/Panel',
  component: DsfrPanelComponent,
  decorators: [moduleMetadata({ imports: [DsfrButtonModule, DsfrFormSelectModule, DsfrModalModule] })],
  parameters: {
    docs: {
      toc: {
        disable: true, //  Disables the table of contents
      },
    },
  },
  argTypes: {
    expandedChange: { control: controlEmitter },
    headingLevel: { control: 'inline-radio', options: ['H2', 'H3', 'H4', 'H5', 'H6'] },
  },
};
export default meta;

const Template: StoryFn<DsfrPanelComponent> = (args) => ({
  props: args,
  template: `<dsfr-ext-panel [heading]="heading" [expanded]="expanded">${content}<p footer>${footer}</p></dsfr-ext-panel>`,
});

const actions = `<div actions><dsfr-button ariaLabel="Ajouter" [variant]="'tertiary'" [size]="'SM'" [icon]="'fr-icon-survey-fill'"></dsfr-button></div>`;

const content = `
    <p>Lorem ipsum dolor sit amet, consectetur adipiscing, <a href="https://www.systeme-de-design.gouv.fr/" target="_blank">link test</a> incididunt, ut labore et dolore magna aliqua. Vitae sapien pellentesque habitant morbi tristique senectus et. Diam maecenas sed enim ut. Accumsan lacus vel facilisis volutpat est. Ut aliquam purus sit amet luctus. Lorem ipsum dolor sit amet consectetur adipiscing elit ut.</p>
    <p>Lorem ipsum dolor sit amet, consectetur adipiscing, <a href="https://www.systeme-de-design.gouv.fr/" target="_blank">link test</a> incididunt, ut labore et dolore magna aliqua. Vitae sapien pellentesque habitant morbi tristique senectus et. Diam maecenas sed enim ut. Accumsan lacus vel facilisis volutpat est. Ut aliquam purus sit amet luctus. Lorem ipsum dolor sit amet consectetur adipiscing elit ut.</p>
    <ul>
        <li>list item</li>
        <li>list item<ul>
                <li>list item niveau 2</li>
                <li>list item niveau 2</li>
                <li>list item niveau 2</li>
            </ul>
        </li>
        <li>list item</li>
    </ul>
`;

const footer = `ceci est un footer`;

export const Default = {
  render: Template,

  args: {
    heading: 'Ceci est un titre',
  },

  name: 'Default',
};

export const WithAction: StoryObj = {
  render: (args) => ({
    props: { ...args },
    template: `<dsfr-ext-panel [heading]="heading">${actions} ${content}<p footer>${footer}</p></dsfr-ext-panel>`,
  }),

  args: {
    heading: 'Ceci est un titre',
  },

  name: 'Action',
};

export const WithoutCollapse = {
  render: Template,

  args: {
    heading: 'Ceci est un titre',
    expanded: null,
  },

  name: 'Without Collapse',
};

export const WithModal: StoryObj = {
  tags: ['!dev'], // story utilisée seulement pour les tests e2e
  args: {},
  render: (args) => ({
    props: { ...args },
    template: `
      <dsfr-button label="Ouvrir la modale qui contient un panel" ariaControls="my-modal" data-fr-opened="false" data-testid="open-modal-btn"></dsfr-button>
      <dsfr-modal dialogId="my-modal" titleModal="Modale contenant un panel">
        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et 
        dolore magna aliqua.</p>
        <dsfrx-panel class="fr-mb-2w" [expanded]="null">
          <dsfr-form-select [label]="'Label pour liste déroulante'"
                            [options]="[
                              { label: 'Option 1', value: 1 },
                              { label: 'Option 2', value: 2 },
                              { label: 'Option 3', value: 3 },
                              { label: 'Option 4', value: 4 }
                            ]"
                            data-testid="my-select"></dsfr-form-select>
        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et 
        dolore magna aliqua.</p>
       </dsfrx-panel>
      </dsfr-modal>
    `,
  }),
};
