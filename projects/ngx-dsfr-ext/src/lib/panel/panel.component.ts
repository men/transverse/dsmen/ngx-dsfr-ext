import { Component, EventEmitter, Input, OnInit, Output, ViewEncapsulation, inject } from '@angular/core';

import { CommonModule } from '@angular/common';
import { DsfrButtonModule, DsfrI18nPipe, DsfrI18nService, Language, newUniqueId } from '@edugouvfr/ngx-dsfr';
import { DsfrHeadingLevel } from '../shared/models/heading-level';
import { MESSAGES_EN } from './i18n/messages.en';
import { MESSAGES_FR } from './i18n/messages.fr';

@Component({
  selector: 'dsfr-ext-panel, dsfrx-panel',
  templateUrl: './panel.component.html',
  styleUrls: ['./panel.component.scss'],
  standalone: true,
  imports: [CommonModule, DsfrButtonModule, DsfrI18nPipe],
  encapsulation: ViewEncapsulation.None,
})
export class DsfrPanelComponent implements OnInit {
  /**
   * Titre du panel.
   */
  @Input() heading: string;

  /** Le niveau de titre devant être utilisé. */
  @Input() headingLevel: DsfrHeadingLevel;

  /**
   *  Identifiant du panel sur la page. Identifiant unique généré par défaut si non renseigné.
   */
  @Input() panelId: string;

  /**
   * Indique si le panel doit être fermé initialement.
   * Si null le panel ne sera pas refermable.
   */
  @Input() expanded: boolean | null = true;

  /**
   * Événement présentant le nouvel état du panel.
   */
  @Output() expandedChange = new EventEmitter<boolean>();

  protected messageExpandButton: string;

  private i18n = inject(DsfrI18nService);

  constructor() {
    this.i18n.extendsLabelsBundle(Language.EN, MESSAGES_EN);
    this.i18n.extendsLabelsBundle(Language.FR, MESSAGES_FR);
  }

  ngOnInit(): void {
    this.panelId ??= newUniqueId();

    if (this.expanded === undefined) {
      this.expanded = true;
    }
    this.messageExpandButton = this.expanded ? 'panel.collapse' : 'panel.expand';
  }

  /** @Internal */
  toggleExpand(e: Event) {
    e.preventDefault();
    e.stopPropagation();
    this.expanded = !this.expanded;
    this.expandedChange.emit(this.expanded);
    this.messageExpandButton = this.expanded ? 'panel.collapse' : 'panel.expand';
  }
}
