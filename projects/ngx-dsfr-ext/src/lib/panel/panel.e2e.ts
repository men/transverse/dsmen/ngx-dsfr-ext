import test, { expect, Locator } from '@playwright/test';
import { getSbFrame, goToComponentPage } from 'projects/ngx-dsfr-ext/shared/e2e.utils';

test.describe('Panel', () => {
  test('should collapse', async ({ page }) => {
    await goToComponentPage('panel', page);
    const frame = getSbFrame(page);
    const panelContent: Locator = frame.locator('.dsfrx-panel__collapse.dsfrx-panel__collapse--expanded');
    const expandBtn: Locator = frame.locator('.dsfrx-panel__btn');
    // perform test
    await expect(panelContent).toBeVisible();
    await expect(expandBtn).toBeVisible();
    await expandBtn.click();
    await expect(panelContent).not.toBeVisible();
  });
  test('should not have select component visible after closing the modal', async ({ page }) => {
    await goToComponentPage('panel', page, 'with-modal');
    const frame = getSbFrame(page);
    const openModalBtn: Locator = frame.getByTestId('open-modal-btn').locator('button');
    const modalCloseBtn = frame.locator('.fr-modal__header > button.fr-btn.fr-btn--close');
    const dialog: Locator = frame.locator('dialog#my-modal');
    const select: Locator = dialog.getByTestId('my-select');
    // perform test
    await expect(openModalBtn).toBeVisible();
    await openModalBtn.click();
    await expect(dialog).toBeVisible();
    await expect(select).toBeVisible();
    await modalCloseBtn.click();
    await expect(dialog).not.toBeVisible();
    await expect(select).not.toBeVisible();
  });
});
