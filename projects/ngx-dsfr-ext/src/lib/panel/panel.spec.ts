import { Component, ViewChild } from '@angular/core';
import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { FormsModule } from '@angular/forms';
import { DsfrPanelComponent } from './panel.component';

@Component({
  selector: `edu-host-component`,
  template: `<dsfr-ext-panel [heading]="'Panel header'" [expanded]="false"></dsfr-ext-panel>`,
})
class TestHostComponent {
  @ViewChild(DsfrPanelComponent)
  public panelComponent: DsfrPanelComponent;
}

describe('DsfrPanelComponent', () => {
  let testHostComponent: TestHostComponent;
  let fixture: ComponentFixture<TestHostComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [FormsModule, DsfrPanelComponent],
      declarations: [TestHostComponent],
    }).compileComponents();

    fixture = TestBed.createComponent(TestHostComponent);
    testHostComponent = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should put id test on elements ', () => {
    testHostComponent.panelComponent.panelId = 'test';
    testHostComponent.panelComponent.ngOnInit();
    fixture.detectChanges();

    const element = fixture.nativeElement.querySelector('.dsfrx-panel');
    const button = element.querySelector('.dsfrx-panel__btn');
    expect(element.children[1].getAttribute('id')).toEqual('test');
    expect(button.getAttribute('aria-controls')).toEqual('test');
  });

  it('should expand panel on click on control button ', fakeAsync(() => {
    testHostComponent.panelComponent.ngOnInit();
    fixture.detectChanges();

    const element = fixture.nativeElement.querySelector('.dsfrx-panel');
    const button = element.querySelector('.dsfrx-panel__btn');
    const messageExpandButton = button.querySelector('.fr-sr-only');
    expect(button.getAttribute('aria-expanded')).toEqual('false');
    expect(button.getAttribute('aria-label')).toEqual('Étendre');
    expect(messageExpandButton.innerHTML).toEqual('Étendre');

    button.click();
    tick();
    fixture.detectChanges();

    const panelContent = element.querySelector('.dsfrx-panel__collapse--expanded');
    expect(panelContent).toBeTruthy();
    expect(button.getAttribute('aria-expanded')).toEqual('true');
    expect(button.getAttribute('aria-label')).toEqual('Réduire');
    expect(messageExpandButton.innerHTML).toEqual('Réduire');
  }));

  it('should reduce panel on click on control button ', fakeAsync(() => {
    testHostComponent.panelComponent.expanded = true;
    testHostComponent.panelComponent.ngOnInit();
    fixture.detectChanges();

    const element = fixture.nativeElement.querySelector('.dsfrx-panel');
    const button = element.querySelector('.dsfrx-panel__btn');
    expect(button.getAttribute('aria-expanded')).toEqual('true');
    expect(button.getAttribute('aria-label')).toEqual('Réduire');

    button.click();
    tick();
    fixture.detectChanges();

    const panelContent = element.querySelector('.dsfrx-panel__collapse--expanded');
    expect(panelContent).toBeFalsy();
    expect(button.getAttribute('aria-expanded')).toEqual('false');
    expect(button.getAttribute('aria-label')).toEqual('Étendre');
  }));
});
