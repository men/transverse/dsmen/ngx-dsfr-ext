import { DsfrLink } from '@edugouvfr/ngx-dsfr';

export interface DsfrSlide {
  /** Url de l'image de l'entête. */
  imagePath?: string;

  /** Texte alternatif de l'image, obligatoire si image renseigné. */
  alt?: string;

  /** Titre associé à l'image, optionnel */
  caption?: string;

  /** Description associée à l'image, optionnel */
  content?: string;

  /** Lien associé, optionnel */
  link?: DsfrLink;

  /** Données supplémentaires associées, optionnel */
  data?: DsfrLink;
}
