import { controlEmitter } from '.storybook/storybook-utils';
import { DsfrButtonModule, DsfrCardModule } from '@edugouvfr/ngx-dsfr';
import { Meta, StoryObj, moduleMetadata } from '@storybook/angular';
import { DEFAULT_INTERVAL, DsfrCarouselComponent } from './carousel.component';
import { DsfrSlide } from './carousel.model';

const meta: Meta<DsfrCarouselComponent> = {
  title: 'Components/Carousel',
  component: DsfrCarouselComponent,
  decorators: [moduleMetadata({ imports: [DsfrCardModule, DsfrButtonModule] })],
  argTypes: {
    carouselChange: controlEmitter,
  },
};
export default meta;
type Story = StoryObj<DsfrCarouselComponent>;

const items: DsfrSlide[] = [
  { imagePath: 'carousel/mountain-2.jpg', caption: 'Slide 1', content: 'Description de mon premier slide' },
  { imagePath: 'carousel/mountain-8.jpg', caption: 'Slide 2' },
  { imagePath: 'carousel/mountain-3.jpg', caption: 'Slide 3', content: 'Description de mon troisième slide' },
  { imagePath: 'carousel/mountain-4.jpg', caption: 'Slide 4' },
  { imagePath: 'carousel/mountain-5.jpg', caption: 'Slide 5' },
];

const items2: DsfrSlide[] = [
  {
    imagePath: 'carousel/upload_accordion.png',
    alt: "Illustration de l'accordéon du DSFR",
    caption: 'Accordéon',
    content: 'Description du composant accordéon du DSFR',
  },
  { imagePath: 'carousel/upload_alerte.png', caption: 'Alerte', content: 'Description du composant alerte du DSFR' },
  { imagePath: 'carousel/upload_bandeau.png', caption: 'Bandeau', content: 'Description du composant Bandeau du DSFR' },
  { imagePath: 'carousel/upload_boutons.png', caption: 'Boutons', content: 'Description du composant boutons du DSFR' },
  { imagePath: 'carousel/upload_card.png', caption: 'Card', content: 'Description du composant card du DSFR' },
];

export const Default: Story = {
  args: {
    autoplay: false,
    circular: true,
    interval: DEFAULT_INTERVAL,
    withCaptions: false,
    maxWidth: '700px',
    withControls: true,
    withControlsInside: false,
    withIndicators: true,
    keyboard: true,
    slides: items,
  },
};

export const autoplay: Story = {
  args: {
    ...Default.args,
    slides: items,
    carouselId: 'carousel2',
    autoplay: true,
  },
};

export const notCircular: Story = {
  args: {
    ...Default.args,
    slides: items,
    carouselId: 'carousel2',
    autoplay: true,
    circular: false,
  },
};

export const disableTransition: Story = {
  args: {
    ...Default.args,
    slides: items,
    carouselId: 'carousel2',
    autoplay: true,
    disableTransition: true,
  },
};

export const InsideControls: Story = {
  args: {
    ...Default.args,
    slides: items2,
    carouselId: 'carousel3',
    autoplay: true,
    withControls: true,
    withIndicators: true,
    reverseColorsIndicators: false,
    maxWidth: '900px',
    withControlsInside: true,
  },
};

export const InsideControlsReverseColors: Story = {
  args: {
    ...Default.args,
    slides: items,
    carouselId: 'carousel3',
    autoplay: true,
    withControls: true,
    withIndicators: true,
    reverseColorsIndicators: true,
    withControlsInside: true,
  },
};

const templateCarouselSlot = `
<dsfr-ext-carousel [maxWidth]="maxWidth" [ariaLabel]="carrousel" [slides]="slides">
  <ng-template let-slide>
    <dsfr-card [imageAlt]="slide.content" [heading]="slide.caption" [horizontal]="true" [imageFit]="contain" [description]="slide.content" [hasFooter]="true" [imagePath]="slide.imagePath">
      <div footer>
      </div>
  </dsfr-card>
  </ng-template>
</dsfr-ext-carousel>`;

export const Templating: Story = {
  args: {
    carouselId: 'carousel4',
    slides: items2,
  },
  render: (args) => ({
    props: args,
    template: templateCarouselSlot,
  }),
};

export const Captions: Story = {
  args: {
    ...Default.args,
    slides: items,
    carouselId: 'carousel4',
    autoplay: true,
    withIndicators: true,
    withCaptions: true,
  },
};
