// Ordre alphabétique requis
export const MESSAGES_EN = {
  carousel: {
    pause: 'Stop automatic slide show',
    play: 'Start automatic slide show',
    next: 'Next',
    nextSlide: 'Next slide',
    previous: 'Previous',
    previousSlide: 'Previous slide',
  },
};
