// Ordre alphabétique requis
export const MESSAGES_FR = {
  carousel: {
    next: 'Suivant',
    nextSlide: 'Slide suivante',
    pause: 'Stopper le défilement automatique du carousel',
    play: 'Démarrer le défilement automatique du carousel',
    previous: 'Précédent',
    previousSlide: 'Slide précédente',
  },
};
