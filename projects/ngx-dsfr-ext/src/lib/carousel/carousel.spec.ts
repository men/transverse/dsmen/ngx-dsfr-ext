import { Component, ViewChild } from '@angular/core';
import { ComponentFixture, TestBed, discardPeriodicTasks, fakeAsync, tick } from '@angular/core/testing';
import { FormsModule } from '@angular/forms';
import { DsfrCarouselComponent } from './carousel.component';
import { DsfrSlide } from './carousel.model';

@Component({
  selector: `edu-host-component`,
  template: `<dsfr-ext-carousel [slides]="items"></dsfr-ext-carousel>`,
})
class TestHostComponent {
  @ViewChild(DsfrCarouselComponent)
  public carouselComponent: DsfrCarouselComponent;
  items: DsfrSlide[] = [
    { imagePath: 'carousel/mountain-2.jpg', caption: 'Slide 1', content: 'Description de mon premier slide' },
    { imagePath: 'carousel/mountain-8.jpg', caption: 'Slide 2' },
    { imagePath: 'carousel/mountain-8.jpg', caption: 'Slide 3' },
  ];
}

describe('DsfrCarouselComponent', () => {
  let testHostComponent: TestHostComponent;
  let fixture: ComponentFixture<TestHostComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [FormsModule, DsfrCarouselComponent],
      declarations: [TestHostComponent],
    }).compileComponents();

    fixture = TestBed.createComponent(TestHostComponent);
    testHostComponent = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should put id test on elements ', () => {
    testHostComponent.carouselComponent.carouselId = 'test';
    testHostComponent.carouselComponent.ngOnInit();
    testHostComponent.carouselComponent.ngAfterViewInit();
    fixture.detectChanges();
    const element = fixture.nativeElement.querySelector('.dsfrx-carousel');

    expect(element.getAttribute('id')).toEqual('test');
    expect(element.getAttribute('aria-roledescription')).toEqual('carousel');
    expect(element.querySelector('.fr-pagination__link--next').getAttribute('aria-controls')).toEqual(
      'carousel-inner-test',
    );
    expect(testHostComponent.carouselComponent.autoplay).toBeFalsy();
    expect(testHostComponent.carouselComponent.withControlsInside).toBeFalsy();
  });

  it('should display controls on slides ', () => {
    testHostComponent.carouselComponent.withControlsInside = true;
    testHostComponent.carouselComponent.ngOnInit();
    testHostComponent.carouselComponent.ngAfterViewInit();
    fixture.detectChanges();

    const container = fixture.nativeElement.querySelector('.inside-controls');
    expect(container.querySelector('.control--prev')).toBeTruthy();
    expect(container.querySelector('.control--next')).toBeTruthy();
    expect(fixture.nativeElement.querySelector('.controls')).toBeFalsy();
  });

  it('should start carousel on autoplay ', fakeAsync(() => {
    testHostComponent.carouselComponent.autoplay = true;
    const startCarousel = jest.spyOn(testHostComponent.carouselComponent, 'startSlideshow');
    testHostComponent.carouselComponent.ngOnInit();
    testHostComponent.carouselComponent.ngAfterViewInit();
    tick();
    fixture.detectChanges();

    const container = fixture.nativeElement.querySelector('.carousel-inner');
    expect(container.getAttribute('aria-live')).toEqual('off');
    expect(startCarousel).toHaveBeenCalled();
    discardPeriodicTasks();
  }));

  it('should display second slide on next click ', fakeAsync(() => {
    testHostComponent.carouselComponent.ngOnInit();
    testHostComponent.carouselComponent.ngAfterViewInit();
    fixture.detectChanges();

    const controlsContainer = fixture.nativeElement.querySelector('.controls');
    const slidesContainer = fixture.nativeElement.querySelector('.carousel-inner');
    expect(slidesContainer.getAttribute('aria-live')).toEqual('polite');

    controlsContainer.querySelector('.fr-pagination__link--next').click();
    tick();
    fixture.detectChanges();

    const activeSlide = slidesContainer.querySelector('.active');
    expect(activeSlide.getAttribute('aria-label')).toEqual('2 of 3');
  }));

  it('should display second slide on prev click without transition', fakeAsync(() => {
    testHostComponent.carouselComponent.disableTransition = true;
    testHostComponent.carouselComponent.ngOnInit();
    testHostComponent.carouselComponent.ngAfterViewInit();
    fixture.detectChanges();

    const controlsContainer = fixture.nativeElement.querySelector('.controls');
    const slidesContainer = fixture.nativeElement.querySelector('.carousel-inner');
    expect(slidesContainer.getAttribute('aria-live')).toEqual('polite');

    controlsContainer.querySelector('.fr-pagination__link--prev').click();
    tick();
    fixture.detectChanges();

    const activeSlide = slidesContainer.querySelector('.active');
    expect(activeSlide.getAttribute('aria-label')).toEqual('3 of 3');
  }));
});
