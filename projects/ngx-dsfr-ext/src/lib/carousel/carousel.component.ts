import { CommonModule } from '@angular/common';
import {
  AfterViewInit,
  Component,
  ContentChild,
  ElementRef,
  EventEmitter,
  Input,
  OnDestroy,
  OnInit,
  Output,
  Renderer2,
  TemplateRef,
  ViewChild,
  ViewEncapsulation,
  inject,
} from '@angular/core';
import { DsfrI18nPipe, DsfrI18nService, Language, newUniqueId } from '@edugouvfr/ngx-dsfr';
import { Point } from '../shared/models/point';
import { DsfrSlide } from './carousel.model';
import { MESSAGES_EN } from './i18n/messages.en';
import { MESSAGES_FR } from './i18n/messages.fr';

export const DEFAULT_INTERVAL: number = 5000;

// https://getbootstrap.com/docs/5.3/components/carousel/
@Component({
  selector: 'dsfrx-carousel, dsfr-ext-carousel',
  standalone: true,
  imports: [CommonModule, DsfrI18nPipe],
  templateUrl: './carousel.component.html',
  styleUrls: ['./carousel.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class DsfrCarouselComponent implements OnInit, OnDestroy, AfterViewInit {
  /** @internal */
  @ViewChild('slidesContainer', { static: true }) slidesContainer: ElementRef;

  /** @internal */
  @ContentChild(TemplateRef) templateSlide: TemplateRef<any>;

  /** Identifiant du champ, généré automatiquement par défaut. */
  @Input() carouselId: string | undefined;

  /** Attribut aria-label du composant */
  @Input() ariaLabel: string | undefined;

  /** Défilement automatique, faux par défaut */
  @Input() autoplay: boolean = false;

  /** Indique si le carrousel boucle ou non, vrai par défaut. */
  @Input() circular = true;

  /* Le délai entre chaque slide. */
  @Input() interval: number = DEFAULT_INTERVAL;

  /** Indique si le carrousel doit réagir aux événements du clavier, vrai par défaut. */
  @Input() keyboard = true;

  /** Tableau de slides. */
  @Input() slides: DsfrSlide[] = [];

  /** Taille maximale du container de slides (optionnel) */
  @Input() maxWidth: string;

  /** Indique si le carrousel doit prendre en charge les interactions par balayage gauche/droite sur les appareils à écran tactile, vrai par défaut. */
  @Input() touch = true;

  /** Avec les contrôles de chaque côté des slides. */
  @Input() withControls: boolean = true;

  /** Avec des indicateurs d'avancement. */
  @Input() withIndicators: boolean = true;

  /** Avec des indicateurs d'avancement. */
  @Input() inverse: boolean = true;

  /** Afficher les indicateur et contrôles à l'intérieur de l'image */
  @Input() withControlsInside: boolean = false;

  /** Dans le cas de controlsInside à l'interieur, place les indicateur sur un fond noir en couleurs inversées */
  @Input() reverseColorsIndicators: boolean = false;

  /** Avec des labels. */
  @Input() withCaptions: boolean = false;

  /** Désactiver l'animation de transition */
  @Input() disableTransition: boolean;

  /** Au changement de slide, emission de l'index du slide actif */
  @Output() carouselChange = new EventEmitter<number>();

  /** @internal Le carousel est en pause et ne restart que si l'utilisateur clique explicitement sur play */
  public onPause: boolean = false;

  /** @internal */
  transition = true;

  private i18n = inject(DsfrI18nService);

  /** Slide courant. */
  private _current = 0;

  /** Id du timer */
  private timeoutId: any;

  /** Evénements touch */
  private startPoint: Point;

  private renderer = inject(Renderer2);
  private transitionListener: () => void;

  constructor() {
    this.i18n.extendsLabelsBundle(Language.EN, MESSAGES_EN);
    this.i18n.extendsLabelsBundle(Language.FR, MESSAGES_FR);
  }

  /**
   * @internal
   * Duplique le premier et dernier slide pour l'effet de transition circulaire
   */
  get visibleSlides() {
    return [
      this.slides[this.slides.length - 1], // Ajoute le dernier slide au début
      ...this.slides,
      this.slides[0], // Ajoute le premier slide à la fin
    ];
  }

  get current(): number {
    return this._current;
  }

  private set current(value: number) {
    if (value !== this._current) {
      this._current = value;
      this.carouselChange.emit(value);
    }

    if (this.disableTransition && this.circular) {
      this.handleFirstLastTransition();
    }
  }

  public ngOnInit(): void {
    this.carouselId ??= newUniqueId();
    if (this.circular) {
      // Gestion de l'effet de transition pour le premier et le dernier slide en version circulaire
      this.transitionListener = this.renderer.listen(this.slidesContainer.nativeElement, 'transitionend', () => {
        this.handleFirstLastTransition();
      });
    }

    // Afficher le premier slide au chargement
    this.renderer.setStyle(this.slidesContainer.nativeElement, 'transform', this.getTransform());
  }

  public ngOnDestroy(): void {
    // Nettoyer le listener à la destruction du composant
    if (this.transitionListener) {
      this.transitionListener();
    }
  }

  public ngAfterViewInit(): void {
    if (this.autoplay) this.startSlideshow();
  }

  /**
   * Spécifique mobile
   * @internal */
  public onTouchStart(event: TouchEvent): void {
    this.startPoint = Point.fromEvent(event);
  }

  /**
   * Spécifique mobile
   * @internal */
  public onTouchEnd(event: TouchEvent): void {
    if (this.touch) {
      const endPoint = Point.fromEvent(event);
      if (endPoint.x < this.startPoint.x - 32) this.next();
      else if (endPoint.x > this.startPoint.x + 32) this.previous();
    }
  }

  /** @internal */
  public onKeydown(event: KeyboardEvent): void {
    // stop du défilement automatique pendant l'interaction utilisateur
    if (this.autoplay) this.stopSlideshow();
    this.onPause = true;

    if (!this.keyboard) return;
    event.stopPropagation();

    switch (event.code) {
      case 'ArrowLeft':
        this.previous();
        break;

      case 'ArrowRight':
        this.next();
        break;

      case 'Space':
        this.toggleSlideshow();
        break;

      default:
    }
  }

  /**
   * Transition vers le slide précédent
   *  @internal */
  public previous(origin: 'user' | 'timer' = 'user'): void {
    if (this.current > 0 || this.circular) {
      this.current--;
      this.transition = true;
      this.renderer.setStyle(this.slidesContainer.nativeElement, 'transform', this.getTransform());
    }

    if (origin === 'user' && this.autoplay) this.startSlideshow();
  }

  /**
   * Transition vers le slide suivant
   *  @internal */
  public next(origin: 'user' | 'timer' = 'user'): void {
    if (this.current < this.slides.length || this.circular) {
      this.transition = true;
      this.current++;
      this.renderer.setStyle(this.slidesContainer.nativeElement, 'transform', this.getTransform());
    }

    if (origin === 'user' && this.autoplay) this.startSlideshow();
    if (!this.circular && (this.current === 0 || this.current === this.slides?.length - 1)) this.stopSlideshow();
  }

  /** @internal */
  public togglePlayPause() {
    this.onPause = !this.onPause;
    if (!this.onPause) {
      this.startSlideshow();
    } else {
      this.stopSlideshow();
    }
  }

  /**
   * @internal
   * Lance le carousel si en autoplay et non explicitement sur pause
   */
  public startSlideshow(): void {
    if (this.autoplay && !this.onPause) {
      this.stopSlideshow();
      this.timeoutId = setInterval(() => this.next('timer'), this.interval);
    }
  }

  /**
   * @internal
   * Stoppe le carousel
   */
  public stopSlideshow(): void {
    if (this.timeoutId && this.autoplay) {
      clearTimeout(this.timeoutId);
      this.timeoutId = undefined;
    }
  }

  /** Effet de transition des slides */
  private getTransform(): string {
    return `translate3d(${-((this.current + 1) * 100)}%, 0, 0)`;
  }

  private toggleSlideshow(): void {
    if (this.timeoutId) this.stopSlideshow();
    else this.startSlideshow();
  }

  /**
   * Gestion de l'affichage avant le premier et apres le dernier slide en mode circulaire
   */
  private handleFirstLastTransition() {
    if (this.current >= this.slides.length) {
      this.current = 0;
      this.transition = false;
      this.renderer.setStyle(this.slidesContainer.nativeElement, 'transform', this.getTransform());
    } else if (this.current < 0) {
      this.current = this.slides.length - 1;
      this.transition = false;
      this.renderer.setStyle(this.slidesContainer.nativeElement, 'transform', this.getTransform());
    }
  }
}
