import {
  ChangeDetectorRef,
  Component,
  ElementRef,
  EventEmitter,
  Input,
  OnInit,
  Output,
  ViewChild,
  forwardRef,
  inject,
} from '@angular/core';
import { NG_VALUE_ACCESSOR } from '@angular/forms';
import {
  DefaultValueAccessorComponent,
  DsfrI18nService,
  DsfrOption,
  DsfrSeverity,
  DsfrSeverityConst,
  Language,
  newUniqueId,
} from '@edugouvfr/ngx-dsfr';
import { DsfrSelectionMode, DsfrSelectionModeConst } from '../../models/selection-mode.type';
import { MESSAGES_EN } from './i18n/messages.en';
import { MESSAGES_FR } from './i18n/messages.fr';

@Component({
  template: '',
  styleUrls: ['./abstract-select.component.scss'],
  standalone: true,
  imports: [],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => AbstractSelectComponent),
      multi: true,
    },
  ],
})
export abstract class AbstractSelectComponent<T>
  extends DefaultValueAccessorComponent<any[] | any | undefined>
  implements OnInit
{
  /** @internal **/
  @ViewChild('searchBox') searchBox: ElementRef;

  /** Les options sélectionnées sont affichées sous forme de tags supprimables */
  @Input() clearableOptions: boolean;

  /** Id du composant, généré par défaut */
  @Input() selectId: string;

  /** Hauteur maximale avant le scroll sur la liste déroulante. */
  @Input() scrollHeight: string = '224px';

  /** Label du champ de sélection */
  @Input() label: string;

  /** Texte de description additionnel */
  @Input() hint: string;

  /** Nombre max. d'options sélectionnées affichées */
  @Input() maxDisplayOptions = 5;

  /** Nombre max. d'options sélectionnables */
  @Input() maxSelectedOptions: number | undefined;

  /** Afficher la case à cocher tout sélectionner */
  @Input() showSelectAll = false;

  /** Attacher l'élément de liste déroulante au 'body' ou a un container (réf. vers un élément #div ou nom de la classe du parent) */
  @Input() appendTo: 'body' | HTMLElement | string | undefined;

  /** Afficher la barre de recherche (filtre sur les options) */
  @Input() showSearch = false;

  /** Afficher un bouton pour vider le champ de la barre de recherche */
  @Input() showClearSearch = false;

  /** Placeholder pour le champ de sélection */
  @Input() placeHolder: string | undefined;

  /** Placeholder pour le champ de recherche */
  @Input() searchMessage: string | undefined;

  /** Placeholder pour le champ de recherche */
  @Input() noResultsMessage: string | undefined;

  /**
   * Message d'information lié au composant
   */
  @Input() message: string;

  /**
   * Représente la sévérité du message
   */
  @Input() messageSeverity: DsfrSeverity;

  /** Activer le virtual scroll  */
  @Input() virtualScroll: boolean = false;

  /** Surcharger la hauteur d'un item dans le cas du virtual scroll. Par défaut 32px. */
  @Input() virtualScrollItemHeight: number = 32;

  /** Propriété CSS z-index de la liste, par défaut supérieur aux composants 'lifted' DSFR */
  @Input() zIndex: number;

  /** Vider le champ de recherche lorsque la liste est cachée */
  @Input() clearSearchOnHide: boolean;

  /** Liste d'options sélectionnée modifiée */
  @Output() selectionChange = new EventEmitter<any[] | undefined>();

  /** @internal texte pour filtrer */
  searchText = '';

  /** @internal liste des options sélectionnées */
  selectedOptions: T[] = [];

  /** @internal toutes les options visibles sélectionnées */
  isCheckedSelectAll = false;

  /** @internal afficher la liste déroulante */
  displayList = false;

  /** @internal liste d'options filtrées */
  optionsFilter: T[] = [];

  /** @internal Sauvegarder l'index de la premiere option sélectionnée pour la gestion du focus */
  indexFirstSelected: number | null = null;

  /* @internal */
  protected cd = inject(ChangeDetectorRef);

  /** @internal   Afin de pouvoir utiliser le type dans l'HTML*/
  protected readonly severityConst = DsfrSeverityConst;

  /** @internal */
  protected _options: T[] | undefined;
  /** @internal */
  protected _selectionMode: DsfrSelectionMode = DsfrSelectionModeConst.CHECKBOX;

  /** @internal compareWith reprise de l'implémentation angular SelectControlValueAccessor */
  protected _compareWith: (o1: any, o2: any) => boolean = Object.is;

  private i18n = inject(DsfrI18nService);

  /** @internal */
  constructor() {
    super();

    this.i18n.extendsLabelsBundle(Language.EN, MESSAGES_EN);
    this.i18n.extendsLabelsBundle(Language.FR, MESSAGES_FR);
  }

  /** @internal */
  get options() {
    return this._options;
  }

  /** @internal */
  get showCheckboxes() {
    return this.selectionMode === DsfrSelectionModeConst.CHECKBOX;
  }

  /** @internal */
  get selectionMode() {
    return this._selectionMode;
  }

  /**
   * Positionne un message d'erreur
   * @deprecated utiliser message et messageSeverity
   */
  @Input() set error(value: string) {
    this.message = value;
    this.messageSeverity = DsfrSeverityConst.ERROR;
  }

  /**
   * Positionne un message de validation
   * @deprecated utiliser message et messageSeverity
   */
  @Input() set valid(value: string) {
    this.message = value;
    this.messageSeverity = DsfrSeverityConst.SUCCESS;
  }

  /** Liste des options disponibles. */
  @Input()
  set options(val: any[] | undefined) {
    this._options = val;
    this.optionsFilter = this.options ?? [];
    if (this.value && this.value.length > 0) {
      this.writeValue(this.value);
    }
  }

  /** Mode de sélection. Checkbox, multiple ou single */
  @Input()
  set selectionMode(mode: DsfrSelectionMode) {
    this.setSelectionMode(mode);
  }

  /** Personnalisation de la comparaison. Fonction à 2 arguments: val1 et val2 (les values des deux options).
  Si compareWith est fourni, sélection de l'option selon le retour de la fonction.*/
  @Input()
  set compareWith(fn: (v1: any, v2: any) => boolean) {
    if (typeof fn !== 'function') {
      throw Error('`compareWith` must be a function.');
    }
    this._compareWith = fn;
  }

  public ngOnInit(): void {
    this.selectId ??= newUniqueId();
  }

  /**  @internal */
  public getLabelKeyOptionsSelected(): string {
    return this.selectedOptions.length === 1 ? 'multiselect.selected' : 'multiselect.selectedPlural';
  }

  /** @internal */
  public isSingleMode() {
    return this.selectionMode === DsfrSelectionModeConst.SINGLE;
  }

  /** @internal */
  public override setDisabledState(isDisabled: boolean) {
    this.disabled = isDisabled;
    this.cd.markForCheck();
  }

  /** 
   * Réinitialiser le champ de recherche
   @internal
  */
  public resetSearchInput(): void {
    if (this.showSearch) {
      this.searchText = '';
      this.onSearch();
    } else {
      this.value = this.isSingleMode() ? undefined : [];
      this.selectedOptions = [];
    }
  }

  /**
   * Ouverture du multiselect sur arrow down
   *  @internal */
  public onButtonKeyUp(e: KeyboardEvent): void {
    if ((e.key === 'ArrowDown' || e.key === 'Enter') && !this.displayList) {
      this.toggleList();
      return;
    }
    if ((e.key === 'ArrowUp' || e.key === 'Enter' || e.key === 'Escape') && this.displayList) {
      this.toggleList();
      return;
    }
  }

  /** @internal */
  public toggleList(e?: any): void {
    if (e && e.target && (e.target as HTMLElement).className.includes('fr-tag ')) {
      return;
    }
    this.displayList = !this.displayList;
  }

  /** @internal */
  public hide(): void {
    this.displayList = false;
    if (this.clearSearchOnHide) {
      this.searchText = '';
      this.optionsFilter = this.options && this.options.length ? this.options : [];
    }
  }

  /** @internal **/
  protected setSelectionMode(mode: DsfrSelectionMode) {
    this._selectionMode = mode ? mode : DsfrSelectionModeConst.CHECKBOX;
    if (this.isSingleMode()) {
      this.showSelectAll = false;
    }
  }

  /** 
   * Sélectionner ou déselectionner toutes les options
   * Si maxSelectedOptions est défini, sélection uniquement des (max) premières options
   @internal
  */
  public abstract selectAll(): void;

  /**
   * Sélection ou désélection d'une option
   * @internal */
  public abstract onOptionClick(e: any, option: DsfrOption): void;

  /**
   * Filtrer les options selon la fonction de recherche
   * @internal */
  public abstract onSearch(): void;
}
