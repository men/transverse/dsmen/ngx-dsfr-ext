// Ordre alphabétique requis
export const MESSAGES_FR = {
  multiselect: {
    unselectAll: 'Tout désélectionner',
    search: 'Rechercher',
    selectAll: 'Tout sélectionner',
    selected: 'sélectionné',
    selectedPlural: 'sélectionnés',
    resetSearch: 'Réinitialiser le champ de recherche',
    selectOption: 'Sélectionner une option',
    noResult: 'Aucun résultat',
    availableResults: 'résultats',
    searchList: 'Rechercher dans la liste',
  },
};
