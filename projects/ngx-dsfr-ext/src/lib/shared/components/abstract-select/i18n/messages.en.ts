// Ordre alphabétique requis
export const MESSAGES_EN = {
  multiselect: {
    unselectAll: 'Unselect all',
    search: 'Search',
    selectAll: 'Select all',
    selected: 'selected',
    selectedPlural: 'selected',
    resetSearch: 'Reset search field',
    selectOption: 'Select an option',
    noResult: 'No result',
    availableResults: 'results',
    searchList: 'Search on results list',
  },
};
