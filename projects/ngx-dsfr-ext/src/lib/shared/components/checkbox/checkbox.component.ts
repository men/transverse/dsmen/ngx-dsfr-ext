import { CommonModule } from '@angular/common';
import { Component, EventEmitter, Input, Output, ViewEncapsulation } from '@angular/core';

@Component({
  selector: 'dsfr-ext-checkbox, dsfrx-checkbox',
  styleUrls: ['./checkbox.component.scss'],
  templateUrl: './checkbox.component.html',
  encapsulation: ViewEncapsulation.None,
  standalone: true,
  imports: [CommonModule],
})
export class EduCheckboxComponent {
  /* Identifiant de la checkbox (obligatoire) */
  @Input() id: string;

  /* Label associé à la checkbox */
  @Input() label: string;

  /* Etat de la checkbox */
  @Input() selected = false;

  /* Surcharge de la propriété tabIndex  (optionnel) */
  @Input() tabIndex: number;

  /* Checkbox désactivée, faux par défaut */
  @Input() disabled = false;

  /* Icone affichée à gauche du label (optionnel) */
  @Input() icon: string;

  /* Evenement émis lors du clic sur la checkbox */
  @Output() checkboxClick: EventEmitter<Event> = new EventEmitter();

  /**@internal */
  public onClick(event: Event): void {
    this.checkboxClick.emit(event);
  }

  /**@internal */
  public isIndeterminate(): boolean {
    return this.selected === undefined;
  }
}
