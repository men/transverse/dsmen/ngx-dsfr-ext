import { ScrollingModule } from '@angular/cdk/scrolling';
import { CommonModule } from '@angular/common';
import {
  ChangeDetectorRef,
  Component,
  ContentChild,
  ElementRef,
  EventEmitter,
  Input,
  OnDestroy,
  Output,
  Renderer2,
  ViewChild,
  ViewEncapsulation,
} from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { EduClickOutsideDirective } from '../../directives';

@Component({
  selector: 'edu-dropdown-container',
  templateUrl: './dropdown-container.component.html',
  encapsulation: ViewEncapsulation.None,
  standalone: true,
  imports: [CommonModule, FormsModule, ScrollingModule, ReactiveFormsModule, ScrollingModule, EduClickOutsideDirective],
})
export class DropdownContainerComponent implements OnDestroy {
  @ViewChild('combobox') comboBox: ElementRef;
  @ContentChild('listbox') listbox: ElementRef;

  @Input() showSearch: boolean = false;

  /** Activer le virtual scroll  */
  @Input() virtualScroll: boolean = false;

  /** Surcharger la hauteur d'un item dans le cas du virtual scroll. Par défaut 24px. */
  @Input() virtualScrollItemHeight: number = 24;

  /** Attacher l'élément de liste déroulante au 'body' ou a un container (réf. vers un élément #div ou nom de la classe du parent) */
  @Input() appendTo: 'body' | HTMLElement | string | undefined;

  @Input() hasOptions: boolean = false;

  /** Propriété CSS z-index de la liste, par défaut supérieur aux composants 'lifted' DSFR */
  @Input() zIndex: number | undefined;

  /** Hauteur maximale avant le scroll sur la liste déroulante. */
  @Input() scrollHeight: string = '224px';

  @Input() id: string = '';

  @Input() class: string = 'dsfrx-multiselect';

  @Input() hasToolBar: boolean = false;

  @Input() indexFirstSelected: number | null;

  /** Set focus on list on opening */
  @Input() autofocus: boolean = false;

  @Output() hideList: EventEmitter<void> = new EventEmitter();
  @Output() focusOnList: EventEmitter<HTMLElement> = new EventEmitter();

  _displayList: boolean = false;

  /** @internal */
  _parentContainer: HTMLElement;
  /** @internal */
  hasNoFocus: boolean = true;

  private _unlistenFn: Function;
  private _searchBox: HTMLElement;

  /** @internal */
  constructor(
    private _renderer: Renderer2,
    private _cd: ChangeDetectorRef,
  ) {}

  get displayList() {
    return this._displayList;
  }

  @Input()
  set displayList(value: boolean) {
    this._displayList = value;
    if (!this.displayList && this.comboBox) {
      if (this.appendTo && this.listbox) {
        this.destroyList();
      }
      this.comboBox.nativeElement?.querySelector('button')?.focus();
      this._cd.markForCheck();
    }
    if (this.displayList) {
      this._cd.detectChanges(); // append listbox
      this.appendListBox();
      if (this.autofocus) this.setFocus();
    }
  }

  /**
   * Fermer la liste déroulante en cas de clic en dehors du composant et detruire le composant si il a été ajouté en appendTo
   *  @internal */
  public onClickOutside(): void {
    this.hideList.emit();
  }

  /**
   * Positionne le focus sur la serch box ou un élément de la liste à l'ouverture de la dropdown
   * @internal */
  public setFocus() {
    if (this.showSearch && !this._searchBox) {
      this._searchBox = this.comboBox.nativeElement.querySelector('.fr-input');
    }

    if (this.listbox && !this._searchBox) {
      this.focusOnListElement();
    } else if (this._searchBox) {
      this.focusOnSearchBox();
    }
  }

  /**
   * Fix focus on tab key
   * @param e Event
   */
  public onTabKey(e: Event): void {
    const firstElement: HTMLElement = this.listbox.nativeElement?.children[0];

    if ((e.target as HTMLElement)?.classList.contains('fr-icon-close-line') && firstElement) {
      firstElement.focus();
      e.preventDefault();

      this.focusOnList.emit(firstElement);
    }
  }

  /** A la destruction du composant, supprimer l'élément listbox si il a été ajouté au parent ou body (appendTo) */
  public ngOnDestroy(): void {
    if (this.appendTo && this.listbox && this.displayList) {
      this.destroyList();
    }
  }

  /**
   * Positionne le focus sur un élément de la liste lors de l'ouverture ou après "flèche bas" depuis le champ de recherche.
   * - Si un élément est déjà sélectionné, le focus est positionné sur celui-ci.
   * - Sinon, le focus est placé sur le premier élément de la liste.
   */
  private focusOnListElement() {
    // en virtualScroll le premier container est cdk-virtualscroll
    const listboxNode = this.virtualScroll
      ? this.listbox.nativeElement?.querySelector('ul')
      : this.listbox.nativeElement;

    let elementToFocus: HTMLElement = listboxNode?.firstElementChild;

    if (this.indexFirstSelected && this.indexFirstSelected !== -1) {
      elementToFocus = listboxNode?.children[this.indexFirstSelected] ?? listboxNode?.firstElementChild;
    }

    if (elementToFocus) {
      this.setAriaActiveDescendant(listboxNode, elementToFocus.getAttribute('id'));
      elementToFocus.focus();
      this.focusOnList.emit(elementToFocus);
    }
  }

  /**
   * Positionne le focus sur le champ de recherche à l'ouverture.
   * Si l'utilisateur appuie sur "flèche bas", le focus est alors repositionné manuellement sur la liste.
   */
  private focusOnSearchBox() {
    this._searchBox.focus();

    this._unlistenFn = this._renderer?.listen(this._searchBox, 'keyup', (event: KeyboardEvent) => {
      if (event.key === 'ArrowDown' && this.listbox) {
        this.focusOnListElement();
      }
    });
  }

  /**
   * Attribut aria-activedescendant sur l'input égal à l'option qui a le focus
   * @internal */
  private setAriaActiveDescendant(listboxNode: HTMLElement, optionId: string | null) {
    if (optionId) {
      this._renderer.setAttribute(listboxNode, 'aria-activedescendant', optionId);
    }
  }

  /**
   * Add listbox element
   *  @internal */
  private appendListBox() {
    // attacher la liste en overlay si appendTo est spécifié
    if (this.listbox && this.appendTo) {
      this.appendElement(this.listbox.nativeElement);
    }
  }

  /**
   * Ajouter l'element listbox directement au body ou container spécifié dans le cas de appendTo en calculant la position
   * Permet de passer la liste en overlay si le multiselect est utilisé dans un container
   * @param element listbox element natif
   */
  private appendElement(element: HTMLElement): void {
    const box = this.comboBox.nativeElement as HTMLElement;
    let top = 0;

    // Calculate left and top position absolute or relative
    if (this.appendTo === 'body') {
      this._renderer.setStyle(element, 'left', `${box.getBoundingClientRect().left}px`);
      top = box.getBoundingClientRect().top + box.offsetHeight + window.scrollY;
    } else {
      if (!this._parentContainer) {
        this.setParentContainer();
      }
      if (box.offsetLeft > 0) {
        this._renderer.setStyle(element, 'left', `${box.offsetLeft}px`);
      }
      top = this.computeTopPositionRelative(box, this._parentContainer as HTMLElement);
    }

    // Adjustment if tool bar is shown
    if (this.hasToolBar) {
      const insideBox = box.getElementsByClassName('dsfrx-multiselect__wrapper')[0] as HTMLElement;
      if (insideBox) {
        this._renderer.setStyle(insideBox, 'width', `${box.offsetWidth}px`);
        top += insideBox?.offsetHeight;
      }
    }

    this._renderer.setStyle(element, 'width', `${box.offsetWidth}px`);
    this._renderer.setStyle(element, 'top', `${top}px`);

    // Attach element to new parent
    this._renderer.appendChild(this.appendTo === 'body' ? document.body : this._parentContainer, element);
  }

  /**
   * Compute top position relative to parent element (top position, margin/padding, scroll inside parent)
   * @param element element to move relatively
   * @param parentElement parent container
   * @returns new top position
   */
  private computeTopPositionRelative(element: HTMLElement, parentElement: HTMLElement): number {
    return (
      element.getBoundingClientRect().top +
      element.offsetHeight +
      parentElement.scrollTop -
      parentElement.getBoundingClientRect().top
    );
  }

  /** If appendTo is set, find parent container (element or class ancestor)  */
  private setParentContainer() {
    if (this.appendTo && this.appendTo !== 'body') {
      if (typeof this.appendTo === 'string') {
        this._parentContainer = this.comboBox.nativeElement.closest(`.${this.appendTo}`);
      } else {
        this._parentContainer = this.appendTo;
      }
      if (!this._parentContainer) {
        throw Error('`appendTo` must be equal to `body`, an element ref or a class of an ancestor container');
      }
      // surcharge du style de l'élément parent avec position relative si appendTo défini
      this._parentContainer.style.position = 'relative';
    }
  }

  /** Suppression de la liste rattachée au container appendTo */
  private destroyList(): void {
    this._renderer.removeChild(
      this.appendTo === 'body' ? document.body : this._parentContainer,
      this.listbox.nativeElement,
    );
    if (this._unlistenFn) this._unlistenFn();
  }
}
