/**
 * @author pfontanet
 * @since 0.9
 */

import { DsfrI18nService, jsonPath2Value, LoggerService } from '@edugouvfr/ngx-dsfr';

//TODO: à déplacer dans la lib ngx-dsfr
export class DateUtils {
  private constructor() {}

  /**
   * Parse une chaine selon le format 'fr' si codeLang = 'fr', 'en'
   * @returns Date ou undefined
   */
  static parse(codeLang: string, dateStr: string | undefined): Date | undefined {
    if (dateStr === undefined) return undefined;
    return codeLang === 'fr' ? DateUtils.parseDateFr(dateStr) : DateUtils.parseDateEn(dateStr);
  }

  /**
   * Parse une chaîne au format `'mm/dd/yyyy'` (1 digit pour mois et jour accepté)
   * @returns Date ou undefined
   */
  static parseDateFr(dateStr: string): Date | undefined {
    const regexp = /^(?<day>3[01]|0?[1-9]|[12][0-9])\/(?<month>1[0-2]|0?[1-9])\/(?<year>[0-9]{4})$/;
    return DateUtils.parseDate(dateStr, regexp);
  }

  /**
   * Parse une chaîne au format `'dd/mm/yyyy'` (1 digit pour mois et jour accepté)
   * @returns Date ou undefined
   */
  static parseDateEn(dateStr: string): Date | undefined {
    const regexp = /^(?<month>1[0-2]|0?[1-9])\/(?<day>3[01]|0?[1-9]|[12][0-9])\/(?<year>[0-9]{4})$/;
    return DateUtils.parseDate(dateStr, regexp);
  }

  /**
   * Parse une chaîne au format ISO 8601
   * @returns Date ou undefined
   */
  static parseDateIso(dateStr: string): Date | undefined {
    // https://www.oreilly.com/library/view/regular-expressions-cookbook/9781449327453/ch04s07.html
    const regexp =
      /^(?<year>-?(?:[1-9][0-9]*)?[0-9]{4})-(?<month>1[0-2]|0[1-9])-(?<day>3[01]|0[1-9]|[12][0-9])(T(?<hour>2[0-4]|[01][0-9]):(?<minute>[0-5][0-9]):(?<second>[0-5][0-9])(?<ms>\.[0-9]+)?(?<timezone>Z|[+-](?:2[0-3]|[01][0-9]):[0-5][0-9])?)?$/;

    return DateUtils.parseDate(dateStr, regexp);
  }

  /**
   * Retourne une Date UTC, sans heure, minute seconde, à partir d'une date
   * @param date
   */
  static date2Utc(date: Date): Date {
    return new Date(Date.UTC(date.getFullYear(), date.getMonth(), date.getDate()));
  }

  /**
   * Retourne une Date UTC, sans heure, minute seconde, selon plusieurs formats en entrée
   * @param value
   */
  static dateUtcOf(value: Date | string | number | undefined | null): Date | undefined {
    let date: Date | undefined = undefined;

    if (typeof value === 'string') date = DateUtils.parseDateIso(value);
    else if (typeof value === 'number') date = DateUtils.numberToDateUtc(value);
    else if (value) date = DateUtils.date2Utc(value);

    return date;
  }

  static format(codeLang: string, date: Date | undefined): string {
    if (date === undefined) return '';
    const code = codeLang === 'fr' ? codeLang : 'en';
    return date.toLocaleDateString(code);
  }

  //FIXME: à relocaliser dans ngx-dsfr#I18nService
  static arr(i18n: DsfrI18nService, logger: LoggerService, jsonPath: string): any[] {
    let arr = <any[]>jsonPath2Value((<any>i18n).currentBundle, jsonPath);
    if (!arr) {
      logger.warn(`Aucune traduction pour '${jsonPath}'`);
      arr = [];
    }

    return arr;
  }

  /** Date valide au format dd/mm/yyyy ou mm/dd/yyyy */
  static isValidDefaultString(boundaryString: string): boolean {
    const dateRegex = /^\d{2}\/\d{2}\/\d{4}$/;

    return dateRegex.test(boundaryString);
  }

  /**
   * Parse la date en fonction d'une expression régulière
   * @param dateStr String représentant la date
   * @param regexp
   * @return Date ou undefined si dateStr ne respecte pas la regexp
   */
  private static parseDate(dateStr: string, regexp: RegExp): Date | undefined {
    if (!dateStr) return undefined;

    const execArr = regexp.exec(dateStr);
    const groups = execArr?.groups;
    if (groups === undefined) return undefined;

    let year = Number(groups['year']);
    const month = Number(groups['month']);
    const day = Number(groups['day']);
    const valid = Number.isInteger(year) && Number.isInteger(month) && Number.isInteger(day);

    return !valid ? undefined : new Date(Date.UTC(year, month - 1, day));
  }

  private static numberToDateUtc(n: number): Date {
    const d = new Date(n);
    return new Date(Date.UTC(d.getUTCFullYear(), d.getUTCMonth(), d.getUTCDate()));
  }
}
