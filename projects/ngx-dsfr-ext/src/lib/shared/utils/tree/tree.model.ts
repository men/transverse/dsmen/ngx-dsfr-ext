/**
 * Description d'un item, nœud, d'un arbre.
 */
export interface DsfrNode {
  /**
   * Identifiant unique de l'élément. Obligatoire pour la mise à jour du contenu de l'arbre
   */
  id?: string;

  /** Ouvert ou non à l'initialisation, `false` par défaut  */
  expanded?: boolean;

  /** Icône du nœud de l'arbre, préfix `fr-icon-` */
  icon?: string;

  /** Libellé du nœud de l'arbre, obligatoire. */
  label: string;

  /** Sélection du nœud désactivée */
  disabled?: boolean;

  /** Nœud sélectionné à l'initialisation, `false` par défaut. */
  selected?: boolean;

  /** Indique si le noeud doit être masqué, utilisé pour filtrer le contenu de l'arbre. */
  hidden?: boolean;

  /** Données associées, à disposition de l'utilisateur. */
  data?: any;

  /** Arborescences imbriquées, `undefined` par défaut. */
  children?: DsfrNode[];
}

/**
 * Interface d'un noeud interne, utilisé pour la visualisation.
 */
export interface DsfrViewNode extends DsfrNode {
  /** Attribut a11y  */
  ariaLevel?: number;

  /** Attribut a11y */
  ariaSize?: number;
}

/**
 * Actions d'une arborescence
 */
export interface DsfrTree {
  /** Mise a jour du contenu de l'arbre. Conserve l'état (nœuds étendus et sélectionnés) si l'id des nœuds est renseigné. */
  updateContent(): void;

  /**
   * Ouvre tous les nœuds de l'arborescence.
   */
  expandAll(): void;

  /**
   * Ferme tous les nœuds de l'arborescence.
   */
  collapseAll(): void;

  /**
   * Sélectionne / désélectionne tous les nœuds en mode multiple ou checkbox
   */
  selectAll(selected: boolean): void;

  /**
   * Réinitialise l'état de l'arbre (nœuds étendus et sélectionnés)
   */
  reinitializeState(): void;
}
