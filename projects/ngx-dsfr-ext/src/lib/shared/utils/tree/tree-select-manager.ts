import { newUniqueId } from '@edugouvfr/ngx-dsfr';
import { DsfrSelectionMode, DsfrSelectionModeConst } from '../../models/selection-mode.type';
import { DsfrNode, DsfrViewNode } from './tree.model';

/**
 * Interface utilisée par TreeSelectManager
 */
// eslint-disable-next-line @typescript-eslint/naming-convention
export interface TreeSelectable {
  /** Indique si toutes les options sont sélectionnées ou non. */
  isAllSelected: boolean;

  /** Retourne la structure arborescente du composant.  */
  get nodes(): DsfrNode[];

  /** Sélectionne / désélectionne tous les nœuds en mode multiple ou checkbox. */
  selectAll(select: boolean): void;

  /** Notifie le composant que node a changé d'état. */
  notifySelectEvent(node: DsfrNode): void;

  /** Mise à jour, graphique, du statut indeterminate. */
  updateIndeterminate(node: DsfrNode): void;
}

/**

 * TreeSelectManager implémente les fonctionnalités de sélection des nœuds d'une arborescence.
 * - Gère le mode de sélection
 * - Gère les checkbox
 * - Ne gère pas l'apparence des libellés sélectionnés
 */
export class TreeSelectManager {
  /**
   * Mode de sélection géré par le manager. Accessible par les composants utilisant ce manager.
   */
  selectionMode: DsfrSelectionMode = undefined;

  /** Liste des nœuds sélectionnés. Si le mode de sélection est 'checkbox', seul les noeuds feuilles sont sélectionnables. */
  selectedNodes: DsfrNode[] = [];

  /** Les noeuds feuilles doivent posséder un id défini à l'initialisation si compareWith n'est pas renseigné */
  noGenericId: boolean = false;

  /** Composant géré par le manager. */
  private treeSelectable: TreeSelectable;

  /**
   * Attribut permettant de notifier qu'un noeud sélectionné à déjà été rencontré en mode single, permettant d'ignorer
   * l'état sélectionné de tous les noeuds subséquents.
   */
  private singleModeSelectedSkip = false;

  constructor(treeSelectable: TreeSelectable) {
    this.treeSelectable = treeSelectable;
  }

  /**
   * À appeler à chaque modification de l'arborescence.
   *
   * @param nodes Nœuds à initialiser
   */
  init(nodes: DsfrNode[]): DsfrViewNode[] {
    // Initialisation de selected et checkboxId
    const nodesClone = this.initNodes(nodes, 1);

    // Dans le cas d'utilisation des checkboxes, on met à jour la valeur de `selected` en fonction des enfants.
    if (this.isCheckboxMode()) this.updateBranches(nodesClone);

    return nodesClone;
  }

  /**
   * Sélection / désélection du nœud suite à un clic.
   * - Le nœud devient le nœud courant en mettant tabIndex=0
   * - Gestion des différents modes de sélection
   * - Envoie d'une notification sur le changement d'état de node.selected
   * Dans le cas du mode checkbox, il n'y a pas de modification suite aux changements d'états des enfants
   */
  public doSelectNode(node: DsfrNode, nodeElt: HTMLElement): void {
    // Le nœud qui reçoit l'événement devient focusable
    nodeElt.tabIndex = 0;

    if (!this.isSelectable()) return;

    if (this.isSingleMode()) this.handleSingleModeSelection(node);
    else if (this.isMultipleMode()) this.handleMultipleModeSelection(node);
    else if (this.isCheckboxMode()) this.handleCheckboxModeSelection(node);
  }

  public isSelectable(): boolean {
    return this.selectionMode !== undefined;
  }

  public isCheckboxMode(): boolean {
    return this.selectionMode === DsfrSelectionModeConst.CHECKBOX;
  }

  public isSingleMode(): boolean {
    return this.selectionMode === DsfrSelectionModeConst.SINGLE;
  }

  public isMultipleMode(): boolean {
    return this.selectionMode === DsfrSelectionModeConst.MULTIPLE;
  }

  /**
   * Initialisation par défaut des propriétés d'un nœud : 'selected', 'id'.
   *
   * @param nodes Les noeuds à initialiser
   * @param level Le niveau du noeud dans la hiérarchie
   */
  public initNodes(nodes: DsfrNode[], level: number): DsfrViewNode[] {
    const size = nodes.length;

    return nodes.map((n) => {
      const nodeView: DsfrViewNode = {
        ...n,
        id: n.id || newUniqueId(),
        ariaLevel: level,
        ariaSize: size,
        children: n.children ? this.initNodes(n.children, level + 1) : undefined,
      };

      if (!n.children && !n.id && this.noGenericId) {
        console.warn(`La fonction compareWith doit être définie: "${n.label}" n'a pas d'attribut 'id'.`);
      }

      if (this.isSelectable()) {
        this.handleNodeSelectedInitialization(n, nodeView);
      }

      return nodeView;
    });
  }

  /**
   * Mise à jour des propriétés des nœuds (label, data, id) sans modifier l'état (select, expand) et ajouts éventuels.
   * @param nodes Nœuds d'entrée à mettre à jour.
   * @param viewNodes Nœuds clonés représentant l'état actuel de l'arbre.
   */
  public updateNodesContent(nodes: DsfrNode[], viewNodes: DsfrViewNode[], level: number): DsfrNode[] {
    const size = nodes.length;

    return nodes.map((node) => {
      // noeud correspondant dans l'arbre cloné
      const matchingNodeView = viewNodes.find((n) => n.id === node.id);

      if (matchingNodeView) {
        return {
          ...node, // on récupère les nouvelles valeurs transmises par l'utilisateur du composant
          id: node.id || matchingNodeView.id, // si l'id n'a pas été positionné, on récupère celui qui a été généré
          selected: matchingNodeView.selected, // on restaure l'état de sélection courant
          expanded: matchingNodeView.expanded, // on restaure l'état d'expansion courant
          ariaLevel: matchingNodeView.ariaLevel, // on restaure le niveau de hiérarchie
          ariaSize: size, // on restaure la taille, car elle a pu changer
          children: node.children
            ? this.updateNodesContent(node.children, matchingNodeView.children || [], level + 1)
            : matchingNodeView.children,
        };
      } else {
        //  aucun noeud correspondant, ajouter la nouvelle structure
        const nodeView: DsfrViewNode = {
          ...node,
          id: node.id || newUniqueId(),
          ariaLevel: level,
          ariaSize: size,
          children: node.children ? this.initNodes(node.children, level + 1) : undefined,
        };

        return nodeView;
      }
    });
  }

  public selectAllNodes(nodes: DsfrNode[], select = true) {
    const isMultiple =
      this.selectionMode === DsfrSelectionModeConst.MULTIPLE || this.selectionMode === DsfrSelectionModeConst.CHECKBOX;
    if (isMultiple || (this.isSingleMode() && !select)) this.toggleNodesState(nodes, select);
  }

  /**
   * Mise à jour liste des feuilles sélectionnées
   * (suppression des nœuds non sélectionnés)
   */
  public updateSelectedNodes(): DsfrNode[] {
    this.selectedNodes = this.selectedNodes.filter((n) => n.selected);
    return this.selectedNodes;
  }

  /**
   * Détermine l'état de l'attribut selected d'un noeud en fonction du mode courant et du model fournit.
   *
   * @param node Nœud du model fournit.
   * @param viewNode Nœud cloné représentant l'état actuel de l'arbre.
   */
  private handleNodeSelectedInitialization(node: DsfrNode, viewNode: DsfrViewNode) {
    if (this.singleModeSelectedSkip) {
      viewNode.selected = false;
    }

    if ((!this.isCheckboxMode() || !node.children) && !this.singleModeSelectedSkip) {
      viewNode.selected = node.selected || false;
    }

    if (!viewNode.children && viewNode.selected && !this.singleModeSelectedSkip) {
      // update list of selected nodes
      this.selectedNodes.push(viewNode);
    }

    if (node.selected && this.isSingleMode()) {
      this.singleModeSelectedSkip = true;
    }
  }

  private toggleNodeSelectedState(node: DsfrNode) {
    node.selected = !node.selected;

    if (!node.selected) {
      this.treeSelectable.isAllSelected = false;
    }
  }

  private handleSingleModeSelection(node: DsfrNode) {
    const prevSingleNode = this.findFirstNodeSelected(this.treeSelectable.nodes);

    if (prevSingleNode && prevSingleNode !== node) {
      prevSingleNode.selected = false;
      // envoi de l'évènement notifiant la déselection du noeud précédant
      this.treeSelectable.notifySelectEvent(prevSingleNode);
    }

    // ajout de l'unique nœud sélectionnable
    if (!node.children) this.selectedNodes = [node];

    this.toggleNodeSelectedState(node);
    this.treeSelectable.notifySelectEvent(node);
  }

  private handleMultipleModeSelection(node: DsfrNode) {
    this.selectedNodes.push(node);
    this.updateSelectedNodes();
    this.toggleNodeSelectedState(node);
    this.treeSelectable.notifySelectEvent(node);
  }

  private handleCheckboxModeSelection(node: DsfrNode) {
    this.toggleNodeSelectedState(node);

    // Propagation de la sélection
    this.updateChildren(node, <boolean>node.selected);
    // Mise à jour des parents pour tout le model
    this.updateBranches(this.treeSelectable.nodes);

    this.updateSelectedNodes();
    this.treeSelectable.notifySelectEvent(node);
  }

  /**
   * Met à jour, récursivement, l'état de chaque nœud en parcourant l'arborescence de haut en bas.
   */
  private updateChildren(node: DsfrNode, selected: boolean): void {
    if (node.disabled || node.hidden) return;

    node.selected = selected;

    // Dans le cas d'utilisation des checkboxes, on propage l'état aux enfants
    if (this.isCheckboxMode()) {
      node.children?.forEach((n) => this.updateChildren(n, selected));

      if (!node.children && node.selected && !this.selectedNodes.find((n) => n.id === node.id)) {
        // Ajout du nœud (feuille) sélectionné dans la liste
        this.selectedNodes.push(node);
      }
    }
  }

  /**
   * Met à jour l'état `selected` d'une branche en fonction de ses nœuds enfants ('true', 'false' ou 'undefined').
   * Émet un événement si l'état du nœud change.
   * Met à jour la propriété indeterminate de la checkbox
   * @param node Nœud de l'arbre
   */
  private updateBranchState(node: DsfrNode): void {
    if (!node.children) return;

    /**
     * Partie recursive de l'algorythme. La suite de cette méthode n'est prise en compte qu'après la mise à jour
     * de tous les descendants de la branche en cours.
     */
    node.children.forEach((n) => this.updateBranchState(n));

    const allSelected = !node.children.some((n) => !n.selected);
    const noneSelected = !allSelected && !node.children.some((n) => n.selected || n.selected === undefined);
    const newState = allSelected ? true : noneSelected ? false : undefined;

    if (node.selected !== newState) {
      node.selected = newState;
      this.treeSelectable.updateIndeterminate(node);
    }
  }

  /**
   * Lance la mise à jour de l'état de sélection de chaque banche en parcourant l'arborescence de bas en haut de manière recursive.
   *
   * ex: Pour un model tel que le suivant:
   *    B1
   *   / \
   *  B2  B3
   *  |   |
   *  F1  F2
   *
   * La méthode updateBranches(B1) tentera de mettre à jour le noeud B2 qui recursivement mettra à jour la
   * feuille F1 avant de se mettre elle même (B2) à jour. Ensuite, la méthode mettra à jour les noeuds suivants de
   * la même façon, d'abord F2, puis B3. Enfin, une fois tous ses descendants à jour, B1 se mettra lui même à jour.
   */
  private updateBranches(nodes: DsfrNode[]) {
    nodes?.forEach((n) => this.updateBranchState(n));
  }

  /**
   * Retourne le 1er nœud sélectionné dans l'arborescence.
   * @param nodes arborescence
   * @return 1er nœud sélectionné ou undefined
   */
  private findFirstNodeSelected(nodes: DsfrNode[]): DsfrNode | undefined {
    let node: DsfrNode | undefined = undefined;
    nodes.find((n) => {
      if (node) return;
      node = n.selected ? n : !n.children ? undefined : this.findFirstNodeSelected(n.children);
    });
    return node;
  }

  /**
   * Sélectionne / déselectionne récursivement tous les nœuds nodes et leurs descendants
   * @param nodes Liste de nœuds à sélectionner
   * @param select true : selection des noeuds, false : fermeture
   */
  private toggleNodesState(nodes: DsfrNode[], select: boolean) {
    nodes.forEach((node) => {
      if (node.hidden) return;
      node.selected = select;
      if (node.children) {
        this.toggleNodesState(node.children, select);
      } else {
        // Mise a jour liste des noeuds sélectionnés
        if (select && !this.selectedNodes.find((n) => n === node) && !node.disabled && !node.hidden) {
          this.selectedNodes.push(node);
        } else if (!select && !node.disabled && !node.hidden) {
          // suppression des noeuds déselectionnés (on ne supprime pas toute la liste au cas ou un filtre est actif)
          const i: number = this.selectedNodes.findIndex((n) => n === node);
          if (i !== -1) this.selectedNodes.splice(i, 1);
        }
      }
    });
  }
}
