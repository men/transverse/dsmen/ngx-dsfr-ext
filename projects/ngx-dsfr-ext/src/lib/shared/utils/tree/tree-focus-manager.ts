import { CdkVirtualScrollViewport } from '@angular/cdk/scrolling';
import { DsfrNode } from './tree.model';

/**
 * Interface utilisée par TreeFocusManager
 */
// eslint-disable-next-line @typescript-eslint/naming-convention
export interface TreeFocusable {
  /** Retourne la structure arborescente du composant. */
  get nodes(): DsfrNode[];

  /** Ouvre tous les nœuds de l'arborescence. */
  expandAll(): void;

  /** Ferme tous les nœuds de l'arborescence. */
  collapseAll(): void;

  /** Effectue l'action par défaut (sélection / déselection) sur le nœud courant. */
  doNodeDefaultAction(node: DsfrNode, event: Event): void;

  /** Recherche la liste des nœuds visibles, ce sont les éléments qui posséderont le tabindex */
  findVisibleNodesElt(): HTMLElement[];

  /** Retourne l'élément avec focus ou undefined. */
  findFocusElement(): HTMLElement | undefined;
}

/**
 * TreeFocusManager a pour objectif de :
 * - Gérer le focus dans un arbre lié aux événements clavier de navigation,
 * - Gérer l'ouverture la fermeture des nœuds
 */
export class TreeFocusManager {
  currentElt: HTMLElement | undefined;

  /** Composant géré par le manager. */
  private treeFocusable: TreeFocusable;

  private firstNodeElt: HTMLElement;

  constructor(treeFocusable: TreeFocusable) {
    this.treeFocusable = treeFocusable;
  }

  /**
   * Initialise le tabindex du 1er élément sur lequel placer un focus
   */
  public initTabindex(): void {
    const elements = this.treeFocusable.findVisibleNodesElt();
    if (elements && elements.length > 0) {
      elements[0].tabIndex = 0;
      this.firstNodeElt = elements[0];
      // Notre composant prend le focus. Est-ce ce que l'on veut par défaut ?
      this.firstNodeElt.focus({ preventScroll: true });
      this.setAriaActiveDescendant(this.firstNodeElt);
    }
  }

  /** Ouvre tous les nœuds de l'arborescence. */
  public expandAll(nodes: DsfrNode[]): void {
    this.setNodesExpanded(nodes, true);
  }

  /** Ferme tous les nœuds de l'arborescence. */
  public collapseAll(nodes: DsfrNode[]): void {
    this.setNodesExpanded(nodes, false);
  }

  /**
   * Répond à un événement clavier.
   * @param currentNode nœud courant (qui possède le focus)
   * @event event événement clavier
   */
  public doKeyEvent(currentNode: DsfrNode, event: KeyboardEvent, isMultipleMode: boolean): void {
    switch (event.code) {
      case 'Space':
      case 'Enter':
        if (!currentNode.disabled && (!currentNode.children || isMultipleMode))
          this.treeFocusable.doNodeDefaultAction(currentNode, event);
        break;

      case 'ArrowLeft':
        this.doCollapseNode(currentNode);
        break;

      case 'ArrowRight':
        this.doExpandNode(currentNode);
        break;

      case 'ArrowDown':
        this.doFocusNext();
        break;

      case 'ArrowUp':
        this.doFocusPrevious();
        break;

      case 'Home':
        this.doFocusFirst();
        break;

      case 'End':
        this.doFocusLast();
        break;

      default:
        return; // no prevent Default on others cases
    }
    event.preventDefault();
    event.stopPropagation();
  }

  /** Bascule l'état ouvert / fermé du nœud. */
  doToggleExpand(node: DsfrNode) {
    node.expanded = !node.expanded;
  }

  /**
   * Gestion du scroll dans la liste lors de la navigation clavier avec virtualScroll activé
   * @param viewport composant de virtualScroll CDK
   */
  public scrollForVirtualScroll(viewport: CdkVirtualScrollViewport): void {
    const prevOffset = this.currentElt?.offsetTop ?? 0;

    if (!this.currentElt?.offsetTop || prevOffset > this.currentElt?.offsetTop) {
      viewport.scrollToOffset(prevOffset);
    } else {
      this.currentElt?.scrollIntoView({ behavior: 'smooth', block: 'center' });
    }

    this.currentElt = this.treeFocusable.findFocusElement();
    this.currentElt?.focus();
  }

  /** Place le focus sur le 1er élément visible. */
  private doFocusFirst(): void {
    const elements = this.treeFocusable.findVisibleNodesElt();
    const count = elements.length;
    if (count > 0) {
      elements[0].focus();
      this.setAriaActiveDescendant(elements[0]);
    }
  }

  /** Place le focus sur le dernier élément visible. */
  private doFocusLast(): void {
    const elements = this.treeFocusable.findVisibleNodesElt();
    const count = elements.length;
    if (count > 0) {
      elements[count - 1].focus();
      this.setAriaActiveDescendant(elements[count - 1]);
    }
  }

  /** Place le focus sur l'élément suivant l'élément courant. */
  private doFocusNext(): void {
    const elements = this.treeFocusable.findVisibleNodesElt();
    this.currentElt = this.treeFocusable.findFocusElement();
    const index = elements.findIndex((elt: HTMLElement) => elt === this.currentElt);

    // Si on est déjà sur le dernier élément, on y reste
    const nextIndex = index === elements.length - 1 ? index : index + 1;
    const nextElt = elements[nextIndex];
    if (this.currentElt) this.currentElt.tabIndex = -1;

    if (nextElt && nextElt !== this.currentElt) {
      nextElt.tabIndex = 0;
      nextElt.focus({ preventScroll: true });
      this.setAriaActiveDescendant(nextElt);
    }
  }

  /** Place le focus sur l'élément précédant le nœud courant. */
  private doFocusPrevious(): void {
    const elements = this.treeFocusable.findVisibleNodesElt();
    if (!elements) return;
    this.currentElt = this.treeFocusable.findFocusElement();
    const index = elements.findIndex((elt: HTMLElement) => elt === this.currentElt);
    // Si on est déjà sur le premier élément, on y reste
    const prevIndex = index === 0 ? index : index - 1;
    const prevElt = elements[prevIndex];

    if (this.currentElt) this.currentElt.tabIndex = -1;

    if (prevElt && prevElt !== this.currentElt) {
      prevElt.tabIndex = 0;
      prevElt?.focus({ preventScroll: true });
      this.setAriaActiveDescendant(prevElt);
    }
  }

  /** Ferme le nœud. */
  private doCollapseNode(node: DsfrNode): void {
    node.expanded = false;
  }

  /** Ouvre le nœud. */
  private doExpandNode(node: DsfrNode): void {
    node.expanded = true;
  }

  /**
   * Ouvre / ferme récursivement tous les nœuds nodes et leurs descendants
   * @param nodes Liste de nœuds à ouvrir
   * @param expand true : ouverture des nœuds, false : fermeture
   */
  private setNodesExpanded(nodes: DsfrNode[], expand: boolean): void {
    nodes.forEach((node) => {
      if (node.children) {
        node.expanded = expand;
        this.setNodesExpanded(node.children, expand);
      }
    });
  }

  /** Set focus on element and add aria-activedescendant attribute to parent */
  private setAriaActiveDescendant(optionEl: HTMLElement): void {
    const optionId: string | undefined | null = optionEl.getAttribute('id');
    if (optionId) optionEl.closest('ul')?.setAttribute('aria-activedescendant', optionId);
  }
}
