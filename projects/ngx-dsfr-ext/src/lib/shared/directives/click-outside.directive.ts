import { Directive, ElementRef, EventEmitter, NgZone, OnDestroy, OnInit, Output, Renderer2 } from '@angular/core';

/**
 * Directive pour détecter un click en dehors du composant (ex. pour fermer une liste déroulante)
 * Nécessite l'ajout d'un attribut data-expanded sur le composant possedant clickOutside
 */
@Directive({
  selector: '[clickOutside]',
  standalone: true,
})
export class EduClickOutsideDirective implements OnInit, OnDestroy {
  @Output() clickOutsideEvent = new EventEmitter<void>();

  private listener: Function;

  constructor(
    private _elementRef: ElementRef,
    private ngZone: NgZone,
    private renderer2: Renderer2,
  ) {}

  ngOnInit() {
    this.ngZone.runOutsideAngular(() => {
      this.listener = this.renderer2.listen('window', 'mouseup', (e) => {
        this.onDocumentClick(e);
      });
      this.listener = this.renderer2.listen('window', 'resize', (e) => {
        this.onDocumentResize(e);
      });
    });
  }

  ngOnDestroy() {
    if (this.listener) this.listener();
  }

  /**
   * En cas de redimensionnement ou de zoom de la fenetre
   * Si on est en mode appendTo, on force la fermeture de la dropdown pour eviter son mauvais positionnement
   *  @internal */
  private onDocumentResize(e: Event): void {
    if (
      this._elementRef.nativeElement.hasAttribute('data-expanded') &&
      !this._elementRef.nativeElement.querySelector('ul.dsfrx-multiselect__list')
    ) {
      this.onNeedToCloseDropdown(e);
    }
  }

  /**
   * Emet l'evenement click outside en cas de clic en dehors du composant
   *  @internal */
  private onDocumentClick(e: MouseEvent): void {
    // positionnement d'un attribut data-expanded qui indique l'affiche de la liste sur le composant
    if (
      this._elementRef.nativeElement.hasAttribute('data-expanded') &&
      !this._elementRef.nativeElement.contains(e?.target) &&
      !(
        (e?.target as HTMLElement).closest('[data-dsfrx-option]') ||
        (e?.target as HTMLElement).closest('.tree-node-container')
      )
    ) {
      this.onNeedToCloseDropdown(e);
    }
  }

  /** Emet l'evenement clickOutsideEvent pour forcer la fermeture de la dropdown */
  private onNeedToCloseDropdown(e: Event) {
    this.ngZone.run(() => {
      e.preventDefault();
      this.clickOutsideEvent.emit();
    });
  }
}
