import { Component, EventEmitter, Output } from '@angular/core';
import { DsfrTranslateModule } from '@edugouvfr/ngx-dsfr';

@Component({
  selector: 'demo-toolbar',
  templateUrl: './demo-toolbar.component.html',
  standalone: true,
  imports: [DsfrTranslateModule],
})
export class DemoToolbarComponent {
  @Output() langChange = new EventEmitter<string>();

  protected readonly languages = [
    { value: 'fr', label: 'FR - Français' },
    { value: 'en', label: 'EN - English' },
    { value: 'it', label: 'IT - Not available' },
  ];

  /** @Internal */
  onLangChange(codeLang: string) {
    this.langChange.emit(codeLang);
  }
}
