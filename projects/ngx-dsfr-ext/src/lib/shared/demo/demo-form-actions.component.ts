import { Component, Input } from '@angular/core';
import { DsfrButtonModule, DsfrButtonsGroupModule } from '@edugouvfr/ngx-dsfr';

@Component({
  selector: 'demo-form-actions',
  templateUrl: './demo-form-actions.component.html',
  standalone: true,
  imports: [DsfrButtonsGroupModule, DsfrButtonModule],
})
export class DemoFormActionsComponent {
  @Input() valid: boolean;
}
