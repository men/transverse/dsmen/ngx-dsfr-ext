export class Point {
  constructor(
    public x = 0,
    public y = 0,
  ) {}

  static fromEvent(event: TouchEvent): Point {
    return new Point(event.changedTouches[0].clientX, event.changedTouches[0].clientY);
  }
}
