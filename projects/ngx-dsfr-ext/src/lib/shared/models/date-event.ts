export interface DsfrDateEvent {
  value: string | undefined;
  date: Date | undefined;
}
