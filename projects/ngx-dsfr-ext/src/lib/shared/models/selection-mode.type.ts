export namespace DsfrSelectionModeConst {
  export const SINGLE = 'single';
  export const MULTIPLE = 'multiple';
  export const CHECKBOX = 'checkbox';
}
type SelectionModeType = typeof DsfrSelectionModeConst;
export type DsfrSelectionMode = SelectionModeType[keyof SelectionModeType] | undefined;
