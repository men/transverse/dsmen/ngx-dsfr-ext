import { CommonModule } from '@angular/common';
import { AfterViewInit, Component, EventEmitter, Input, OnDestroy, Output, ViewEncapsulation } from '@angular/core';
import { Subscription } from 'rxjs';
import { DEFAULT_DURATION, ToastItemComponent } from './item/toast-item.component';
import { DsfrToast, DsfrToastPosition, DsfrToastPositionConst } from './toast.model';
import { DsfrToastService } from './toast.service';

export const DEFAULT_POSITION = DsfrToastPositionConst.BOTTOM_CENTER;

@Component({
  selector: 'dsfr-ext-toaster, dsfrx-toaster',
  templateUrl: './toast.component.html',
  styleUrls: ['./toast.component.scss'],
  encapsulation: ViewEncapsulation.None,
  standalone: true,
  imports: [CommonModule, ToastItemComponent],
})
export class DsfrToastComponent implements AfterViewInit, OnDestroy {
  /**
   * La durée d'affichage en ms des notifications avant fermeture.
   *
   * Chaque notification peut recevoir elle même une durée d'affichage spécifique
   * à l'appel de la méthode show du ToastService.
   *
   * Valeur par défaut: 5000 ms.
   */
  @Input() duration: number = DEFAULT_DURATION;

  /**
   * Position par défaut des notifications.
   *
   * Chaque notification peut recevoir elle même une position spécifique
   * à l'appel de la méthode show du ToastService.
   *
   * Valeur par défaut: 'bottom-center'.
   */
  @Input() position: DsfrToastPosition = DEFAULT_POSITION;

  /**
   * Propriété z-index du toaster.
   *
   * Le toaster crée un nouveau contexte d'empilement qui peut nécessiter des
   * ajustement en fonction des autres composants du contexte cible.
   *
   * Valeur par défaut: 2500
   */
  @Input() zIndex: number = 2500;

  /**
   * Notifie la fermeture d'une notification.
   */
  @Output() itemClose = new EventEmitter<DsfrToast>();

  /**
   * Liste des toasts.
   */
  protected toasts = new Array<DsfrToast>();

  private subscription = new Subscription();

  /** @internal */
  constructor(private toastService: DsfrToastService) {}

  /** @internal */
  ngAfterViewInit(): void {
    const toast$ = this.toastService.toasts().subscribe((toast) => {
      this.add(toast);
    });

    this.subscription.add(toast$);
  }

  /** @internal */
  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }

  protected add(toast: DsfrToast): void {
    this.toasts.unshift(toast);
  }

  protected onToastClose(toast: DsfrToast) {
    const index = this.toasts.indexOf(toast);
    this.toasts.splice(index, 1);
    this.itemClose.emit(toast);
  }
}
