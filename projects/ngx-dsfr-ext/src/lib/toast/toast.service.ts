import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs';
import { DsfrToast } from './toast.model';

@Injectable({
  providedIn: 'root',
})
export class DsfrToastService {
  private _toast: Subject<DsfrToast> = new Subject();

  constructor() {}

  /** Affiche la nouvelle notification. */
  show(toast: DsfrToast): void {
    this._toast.next(toast);
  }

  toasts(): Observable<DsfrToast> {
    return this._toast;
  }
}
