import { DsfrSeverity } from '@edugouvfr/ngx-dsfr';

/**
 * Décrit une notification.
 */
export interface DsfrToast {
  /**
   * Identifiant unique de la notification (sera généré automatiquement si non valorisé).
   */
  toastId?: string;
  /**
   *  Titre de la notification dans une balise `div` de style `toast--header`, accepte le format HTML.
   */
  heading?: string;

  /**
   * Contenu de la notification dans une balise `div` de style `toast--content`, accepte le format HTML.
   */
  content?: string;

  /**
   *  Sévérité du toast ('info' par défaut).
   */
  severity?: DsfrSeverity;

  /**
   * Permet d'individualiser la durée d'affichage de cette notification (5000ms par défaut).
   */
  duration?: number;
}

export namespace DsfrToastPositionConst {
  export const CENTER = 'center';
  export const TOP_LEFT = 'top-left';
  export const TOP_CENTER = 'top-center';
  export const TOP_RIGHT = 'top-right';
  export const BOTTOM_LEFT = 'bottom-left';
  export const BOTTOM_CENTER = 'bottom-center';
  export const BOTTOM_RIGHT = 'bottom-right';
}

/**
 * Le type d'un bouton au sens HTML.
 * @see https://developer.mozilla.org/en-US/docs/Web/HTML/Element/button
 */
type Type = typeof DsfrToastPositionConst;
export type DsfrToastPosition = Type[keyof Type];
