import { Component, Input, ViewEncapsulation } from '@angular/core';
import { DsfrButtonModule, DsfrButtonsGroupModule, DsfrSeverity, DsfrSeverityConst } from '@edugouvfr/ngx-dsfr';
import { DsfrToastComponent } from '../toast.component';
import { DsfrToastPosition } from '../toast.model';
import { DsfrToastService } from '../toast.service';

@Component({
  selector: `demo-toast`, // Non utilisé, car l'affichage des toasts est géré via un service
  templateUrl: './demo-toast-service.component.html',
  styles: ['dsfr-buttons-group > ul { margin-top: 0.5rem}'],
  encapsulation: ViewEncapsulation.None,
  standalone: true,
  imports: [DsfrButtonModule, DsfrButtonsGroupModule, DsfrToastComponent],
})
export class DemoToastServiceComponent {
  /**
   * La durée d'affichage en ms des notifications avant fermeture.
   *
   * Chaque notification peut recevoir elle même une durée d'affichage spécifique
   * à l'appel de la méthode show du ToastService.
   *
   * Valeur par défaut: 5000 ms.
   */
  @Input() duration: number = 5000;

  /**
   * Position par défaut des notifications.
   *
   * Chaque notification peut recevoir elle même une position spécifique
   * à l'appel de la méthode show du ToastService.
   *
   * Valeur par défaut: 'bottom-center'.
   */
  @Input() position: DsfrToastPosition = 'bottom-center';

  /**
   * Propriété z-index du toaster.
   *
   * Le toaster crée un nouveau contexte d'empilement qui peut nécessiter des
   * ajustements en fonction des autres composants du contexte cible.
   *
   * Valeur par défaut: 2500
   */
  @Input() zIndex: number = 2500;

  private heading = 'Titre du message';
  private content = 'Contenu du message un peu plus long pour le test';

  constructor(private toastService: DsfrToastService) {}

  protected addToast(severity: DsfrSeverity = 'info') {
    const durations = {
      [DsfrSeverityConst.ERROR]: 5000,
      [DsfrSeverityConst.INFO]: 4000,
      [DsfrSeverityConst.WARNING]: 3000,
      [DsfrSeverityConst.SUCCESS]: 2000,
      [DsfrSeverityConst.VALID]: 1000,
    };

    const prefix = {
      [DsfrSeverityConst.ERROR]: 'Erreur :',
      [DsfrSeverityConst.INFO]: 'Info :',
      [DsfrSeverityConst.WARNING]: 'Attention :',
      [DsfrSeverityConst.SUCCESS]: 'Opération réussie :',
      [DsfrSeverityConst.VALID]: 'Opération validée :',
    };

    this.toastService.show({
      heading: `${prefix[severity as keyof typeof prefix]} ${this.heading}`,
      content: this.content,
      severity: severity,
      duration: durations[severity as keyof typeof durations],
    });
  }
}
