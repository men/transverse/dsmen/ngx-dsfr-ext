import { animate, style, transition, trigger } from '@angular/animations';
import { CommonModule } from '@angular/common';
import {
  AfterViewInit,
  Component,
  EventEmitter,
  inject,
  Input,
  OnDestroy,
  OnInit,
  Output,
  ViewEncapsulation,
} from '@angular/core';
import {
  DsfrButtonModule,
  DsfrI18nPipe,
  DsfrI18nService,
  DsfrSeverity,
  DsfrSeverityConst,
  Language,
  newUniqueId,
} from '@edugouvfr/ngx-dsfr';
import { MESSAGES_EN } from '../i18n/messages.en';
import { MESSAGES_FR } from '../i18n/messages.fr';
import { DsfrToast } from '../toast.model';

const shrinkInTransition = transition('void => *', [
  style({ 'height': 0, 'opacity': 0, 'margin-top': 0 }),
  animate(200, style({ 'height': '*', 'opacity': 1, 'margin-top': '1rem' })),
]);

const shrinkOutTransition = transition('* => void', [
  style({ 'height': '!', 'opacity': 1, 'margin-top': '1rem' }),
  animate(150, style({ 'height': 0, 'opacity': 0, 'margin-top': 0 })),
]);

export const DEFAULT_DURATION: number = 5000;

@Component({
  selector: 'edu-toast-item',
  templateUrl: './toast-item.component.html',
  styleUrls: ['./toast-item.component.scss'],
  encapsulation: ViewEncapsulation.None,
  standalone: true,
  imports: [CommonModule, DsfrButtonModule, DsfrI18nPipe],
  animations: [trigger('shrink', [shrinkInTransition, shrinkOutTransition])],
})
export class ToastItemComponent implements DsfrToast, OnInit, AfterViewInit, OnDestroy {
  /** Identifiant de la notification. */
  @Input() toastId: string | undefined;

  /** Titre de la notification. */
  @Input() heading: string | undefined;

  /** Contenu de la notification. */
  @Input() content: string | undefined;

  /** Sévérité de la notification (info par défaut). */
  @Input() severity: DsfrSeverity;

  /** La durée en ms avant qu'une notification soit fermée. */
  @Input() duration: number;

  /**
   * Notifie la fermeture d'une notification (que la fermeture soit automatique ou manuelle).
   */
  @Output() itemClose = new EventEmitter<string>();

  protected headingTemplate: string;
  protected contentTemplate: string;
  protected readonly severityConst = DsfrSeverityConst;

  private i18n = inject(DsfrI18nService);

  /** Id du timer */
  private timeoutId: any;

  constructor() {
    this.i18n.extendsLabelsBundle(Language.EN, MESSAGES_EN);
    this.i18n.extendsLabelsBundle(Language.FR, MESSAGES_FR);
  }

  ngOnInit(): void {
    this.toastId ??= newUniqueId();
    this.duration ??= DEFAULT_DURATION;
    this.severity ??= DsfrSeverityConst.INFO;
  }

  ngAfterViewInit() {
    this.startTimeout();
  }

  ngOnDestroy() {
    this.stopTimeout();
  }

  /**
   * Fermeture du toast (suppression)
   * Informe les observateurs de la fermeture du toast.
   */
  protected onClose(): void {
    this.itemClose.emit(this.toastId);
  }

  protected onMouseOut(): void {
    this.startTimeout();
  }

  protected onMouseOver(): void {
    this.stopTimeout();
  }

  protected onTouchEnd(): void {
    this.startTimeout();
  }

  protected onTouchStart(): void {
    this.stopTimeout();
  }

  protected isError(): boolean {
    return this.severity === 'error';
  }

  private startTimeout(): void {
    this.stopTimeout();
    this.timeoutId = setTimeout(() => this.onClose(), this.duration);
  }

  private stopTimeout(): void {
    if (this.timeoutId) {
      clearTimeout(this.timeoutId);
      this.timeoutId = undefined;
    }
  }
}
