import { controlEmitter, dsfrDecorators } from '.storybook/storybook-utils';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { DsfrButtonModule, DsfrSeverityConst } from '@edugouvfr/ngx-dsfr';
import { Meta, StoryObj, moduleMetadata } from '@storybook/angular';
import { ToastItemComponent } from './toast-item.component';

const meta: Meta<ToastItemComponent> = {
  title: 'Components/Toast/Item',
  component: ToastItemComponent,
  decorators: [moduleMetadata({ imports: [DsfrButtonModule, BrowserAnimationsModule] })],
  argTypes: {
    itemClose: { control: controlEmitter },
    severity: { control: 'inline-radio', options: Object.values(DsfrSeverityConst) },
  },
};
export default meta;
type Story = StoryObj<ToastItemComponent>;

export const Default: Story = {
  decorators: dsfrDecorators('Toast Info'),
  args: {
    heading: 'Titre du message',
    content: 'Contenu du message',
  },
};

export const ToastSuccess: Story = {
  decorators: dsfrDecorators('Toast Success'),
  args: {
    ...Default.args,
    severity: 'success',
  },
};

export const ToastWarning: Story = {
  decorators: dsfrDecorators('Toast Warning'),
  args: {
    ...Default.args,
    severity: 'warning',
  },
};

export const ToastError: Story = {
  decorators: dsfrDecorators('Toast Error'),
  args: {
    ...Default.args,
    severity: 'error',
  },
};
