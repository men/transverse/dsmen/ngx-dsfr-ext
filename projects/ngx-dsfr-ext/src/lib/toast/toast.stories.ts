import { dsfrDecorators } from '.storybook/storybook-utils';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { DsfrButtonModule, DsfrButtonsGroupModule } from '@edugouvfr/ngx-dsfr';
import { Meta, StoryObj, moduleMetadata } from '@storybook/angular';
import { DsfrToastComponent } from './toast.component';
import { DsfrToastPositionConst } from './toast.model';
import { DemoToastServiceComponent } from './~demo/demo-toast-service.component';

const meta: Meta<DemoToastServiceComponent> = {
  title: 'Components/Toast',
  component: DemoToastServiceComponent,
  decorators: [
    moduleMetadata({
      imports: [DsfrButtonModule, DsfrButtonsGroupModule, BrowserAnimationsModule, DsfrToastComponent],
    }),
  ],
  argTypes: {
    position: { control: 'select', options: Object.values(DsfrToastPositionConst) },
  },
  parameters: {
    docs: {
      toc: {
        disable: true, // Disables the table of contents
      },
    },
  },
};
export default meta;
type Story = StoryObj<DemoToastServiceComponent>;

export const Service: Story = {
  decorators: dsfrDecorators('Affichage via un service', "L'affichage d'un message est obtenu via un service."),
  args: {
    duration: 5000,
    position: 'bottom-center',
    zIndex: 2500,
  },
};
