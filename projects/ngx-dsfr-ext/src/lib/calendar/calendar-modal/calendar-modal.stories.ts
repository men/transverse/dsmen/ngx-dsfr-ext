import { controlEmitter, dsfrDecorators } from '.storybook/storybook-utils';
import { Meta, moduleMetadata, StoryObj } from '@storybook/angular';
import { DsfrCalendarModalComponent } from './calendar-modal.component';

const meta: Meta<DsfrCalendarModalComponent> = {
  title: 'Components/Calendar/Modal',
  component: DsfrCalendarModalComponent,
  decorators: [moduleMetadata({ imports: [] })],
  parameters: {
    docs: { toc: { disabled: true } },
    argTypes: {
      dateChange: controlEmitter,
    },
  },
  args: {
    value: '',
    min: '',
    max: '',
    disabled: false,
    ariaLabel: '',
    tooltipMessage: '',
  },
};
export default meta;
type Story = StoryObj<DsfrCalendarModalComponent>;

export const Default: Story = {
  decorators: dsfrDecorators('Défaut'),
  args: {
    ariaLabel: 'Arialabel du bouton',
    tooltipMessage: 'Title du bouton',
  },
};

export const Disabled: Story = {
  decorators: dsfrDecorators('Disabled'),
  args: {
    disabled: true,
    ariaLabel: 'Arialabel du bouton',
    tooltipMessage: 'Title du bouton',
  },
};
