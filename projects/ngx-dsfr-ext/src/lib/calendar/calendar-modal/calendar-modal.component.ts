import { A11yModule } from '@angular/cdk/a11y';

import {
  AfterViewInit,
  Component,
  ElementRef,
  EventEmitter,
  Input,
  OnDestroy,
  Output,
  ViewChild,
  ViewEncapsulation,
} from '@angular/core';
import {
  DsfrButtonModule,
  DsfrI18nPipe,
  DsfrI18nService,
  Language,
  LoggerService,
  newUniqueId,
} from '@edugouvfr/ngx-dsfr';
import { Subscription } from 'rxjs';
import { EduClickOutsideDirective } from '../../shared';
import { DsfrDateEvent } from '../../shared/models/date-event';
import { DateUtils } from '../../shared/utils/date/date-utils';
import { DsfrCalendarComponent } from '../calendar.component';
import { MESSAGES_EN } from '../i18n/messages.en';
import { MESSAGES_FR } from '../i18n/messages.fr';

@Component({
  selector: 'dsfr-ext-calendar-modal, dsfrx-calendar-modal',
  encapsulation: ViewEncapsulation.None,
  templateUrl: './calendar-modal.component.html',
  styleUrls: ['./calendar-modal.component.scss'],
  standalone: true,
  imports: [DsfrCalendarComponent, EduClickOutsideDirective, A11yModule, DsfrButtonModule, DsfrI18nPipe],
})
export class DsfrCalendarModalComponent implements AfterViewInit, OnDestroy {
  /**
   * Date minimale inclue. Doit être au format IS0-8601 : AAAA-MM-JJ.
   * Ce paramètre peut être utilisé seul ou avec la propriété max.
   */
  @Input() min = '';

  /**
   * Date maximale inclue. Doit être au format IS0-8601 : AAAA-MM-JJ.
   * Ce paramètre peut être utilisé seul ou avec la propriété min.
   */
  @Input() max = '';

  /**
   * Valeur de l'attribut aria-label du bouton d'ouverture de la modale.
   */
  @Input({ required: true }) ariaLabel = '';

  /**
   * Message du `tooltip` (attribut `title` du bouton) ou ajout d'un `span` en `sr-only` si pas de label.
   */
  @Input({ required: true }) tooltipMessage = '';

  /**
   * Valeur de l'attribut disabled du bouton d'ouverture de la modale.
   */
  @Input() disabled = false;

  /**
   * Emission de la date au format DsfrDateEvent
   */
  @Output() dateChange = new EventEmitter<DsfrDateEvent>();

  @ViewChild('modal') protected modal: ElementRef<HTMLDivElement>;
  @ViewChild('calendar') protected calendar: DsfrCalendarComponent;

  /**
   * Id aléatoire, unique, de la modale (le calendrier)
   */
  protected modalId = newUniqueId();
  protected isOpen = false;
  protected dialogTitle = '';
  protected translatedMonths: string[] = [];
  protected dialogTriggerBtnNode: HTMLButtonElement;

  private _value: string | undefined = '';
  private currentMonthIndex = 0;
  private currentYear = 0;

  private subscription = new Subscription();

  constructor(
    private elementRef: ElementRef,
    private i18n: DsfrI18nService,
    private logger: LoggerService,
  ) {
    this.i18n.extendsLabelsBundle(Language.EN, MESSAGES_EN);
    this.i18n.extendsLabelsBundle(Language.FR, MESSAGES_FR);
    const langChange$ = this.i18n.completeLangChange$.subscribe(() => this.onLangChange());
    this.updateMonthIndexAndYear();
    this.updateDialogTitle();

    this.subscription.add(langChange$);
  }

  get value(): string | undefined {
    return this._value;
  }

  /**
   * Valeur initial de la date.
   * La valeur doit prendre la forme dd/mm/yyyy pour la locale fr et mm/dd/yyyy pour la locale en.
   */
  @Input() set value(value: string | undefined) {
    if (value && DateUtils.isValidDefaultString(value)) {
      this._value = value;
      this.updateMonthIndexAndYear(value);
    }
  }

  ngAfterViewInit(): void {
    const nativeElt = this.elementRef.nativeElement;
    this.dialogTriggerBtnNode = nativeElt.querySelector('.fr-btn') as HTMLButtonElement;
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }

  /**
   * Gère le clic en dehors de la modale
   */
  protected onCalendarOutside(): void {
    if (this.isOpen) {
      this.close(false);
    }
  }

  protected handleTriggerBtnClick(): void {
    this.isOpen = true;
  }

  protected handleDataChange(dateEvent: DsfrDateEvent): void {
    if (!dateEvent.date || !dateEvent.value) {
      return;
    }

    this.value = dateEvent.value;
    this.dateChange.emit(dateEvent);

    this.close();
  }

  protected handleModalKeydown(keyEvent: KeyboardEvent): void {
    if (keyEvent.key === 'Escape') {
      keyEvent.stopPropagation();
      keyEvent.preventDefault();
      this.close();
    }
  }

  protected handleCancelButton(): void {
    this.close();
  }

  /**
   * Met à jour le titre de la dialog (visuellement caché) pour les besoins d'accessibilité.
   */
  private updateDialogTitle(): void {
    this.dialogTitle = `${this.i18n.t('calendar.modal.title')}${this.translatedMonths[this.currentMonthIndex]} ${this.currentYear}`;
  }

  /**
   * Ferme la modale.
   *
   * Positionne le focus sur le bouton d'ouverture de la modale si besoin.
   *
   * @param focusTriggerBtn Permet de savoir si le focus doit être donné au bouton d'ouverture de la modale
   */
  private close(focusTriggerBtn = true): void {
    if (!this.modal) {
      return;
    }

    // Pour être raccord avec la directive click-outside : on enlève 'data-expanded'
    this.modal.nativeElement.removeAttribute('data-expanded');

    this.isOpen = false;

    if (focusTriggerBtn) {
      this.dialogTriggerBtnNode.focus();
    }
  }

  private updateMonthTranslations(): void {
    this.translatedMonths = DateUtils.arr(this.i18n, this.logger, 'calendar.months');
  }

  /**
   * Gestion du changement de langue dans le composant.
   *
   * @param code Code de la locale à prendre en compte
   */
  private onLangChange(): void {
    this.updateMonthTranslations();
    this.updateDialogTitle();
  }

  /**
   * Mise à jour des valeurs des deux listes déroulantes, mois et année.
   */
  private updateMonthIndexAndYear(date?: string): void {
    this.currentMonthIndex = date ? new Date(date).getMonth() : new Date().getMonth();
    this.currentYear = date ? new Date(date).getFullYear() : new Date().getFullYear();
  }
}
