import { controlEmitter, dsfrDecorators } from '.storybook/storybook-utils';
import { Meta, moduleMetadata, StoryObj } from '@storybook/angular';
import { DemoToolbarComponent } from '../shared/demo';
import { DsfrCalendarComponent } from './calendar.component';

const meta: Meta<DsfrCalendarComponent> = {
  title: 'Components/Calendar',
  component: DsfrCalendarComponent,
  decorators: [moduleMetadata({ imports: [DemoToolbarComponent] })],
  parameters: {
    docs: { toc: { disabled: true } },
  },
  argTypes: {
    dateChange: controlEmitter,
  },
};
export default meta;
type Story = StoryObj<DsfrCalendarComponent>;

const template = `
  <demo-toolbar></demo-toolbar>
  <p id="id-grid-label">Calendrier de sélection de date</p>
  <dsfr-ext-calendar
    [value]="value"
    [max]="max"
    [min]="min"
    [autofocus]="true"
    [ariaLabelledBy]="'id-grid-label'"></dsfr-ext-calendar>`;

export const Default: Story = {
  decorators: dsfrDecorators('Défaut'),
  render: (args) => ({
    props: {
      ...args,
    },
    template,
  }),
  args: {
    value: '',
    min: '',
    max: '',
  },
};

export const WithInitialValue: Story = {
  decorators: dsfrDecorators('Calendrier avec valeur initiale'),
  render: (args) => ({
    props: {
      ...args,
    },
    template,
  }),
  args: {
    value: '2021-02-14',
    min: '',
    max: '',
  },
};

export const WithMin: Story = {
  decorators: dsfrDecorators('Calendrier avec date minimum'),
  render: (args) => ({
    props: {
      ...args,
    },
    template,
  }),
  args: {
    value: '',
    min: new Date().toISOString().substring(0, 10),
    max: '',
  },
};

export const WithMax: Story = {
  decorators: dsfrDecorators('Calendrier avec date maximum'),
  render: (args) => ({
    props: {
      ...args,
    },
    template,
  }),
  args: {
    value: '',
    min: '',
    max: new Date().toISOString().substring(0, 10),
  },
};
