// Ordre alphabétique requis
export const MESSAGES_FR = {
  calendar: {
    cancel: 'Annuler',
    days: ['Dimanche', 'Lundi', 'Mardi', 'Mercredi', 'Jeudi', 'Vendredi', 'Samedi'],
    daysAbbr: ['Di', 'Lu', 'Ma', 'Me', 'Je', 'Ve', 'Sa'],
    months: [
      'Janvier',
      'Février',
      'Mars',
      'Avril',
      'Mai',
      'Juin',
      'Juillet',
      'Août',
      'Septembre',
      'Octobre',
      'Novembre',
      'Décembre',
    ],
    monthsAbbr: ['Janv.', 'Févr.', 'Mars', 'Avril', 'Mai', 'Juin', 'Juil.', 'Août', 'Sept.', 'Oct.', 'Nov.', 'Déc.'],
    prevMonth: 'Mois précédent',
    nextMonth: 'Mois suivant',
    monthSelect: 'Sélectionner le mois',
    yearSelect: `Sélectionner l'année`,
    modal: {
      ariaLabel: 'Choisir une date',
      title: 'Widget de choix de date : ',
    },
  },
};
