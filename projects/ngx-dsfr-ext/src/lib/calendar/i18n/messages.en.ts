// Ordre alphabétique requis
export const MESSAGES_EN = {
  calendar: {
    cancel: 'Cancel',
    days: ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'],
    daysAbbr: ['Su', 'Mo', 'Tu', 'We', 'Th', 'Fr', 'Sa'],
    months: [
      'January',
      'February',
      'March',
      'April',
      'May',
      'June',
      'July',
      'August',
      'September',
      'October',
      'November',
      'December',
    ],
    monthsAbbr: ['Jan.', 'Feb.', 'Mar.', 'Apr.', 'May', 'June', 'July', 'Aug.', 'Sept.', 'Oct.', 'Nov.', 'Dec.'],
    prevMonth: 'Previous month',
    nextMonth: 'Next month',
    monthSelect: 'Select a month',
    yearSelect: 'Select a year',
    modal: {
      ariaLabel: 'Choose Date',
      title: 'Date picker widget: ',
    },
  },
};
