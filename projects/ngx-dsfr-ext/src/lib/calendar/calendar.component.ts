import {
  AfterViewInit,
  Component,
  ElementRef,
  EventEmitter,
  Input,
  OnChanges,
  OnDestroy,
  OnInit,
  Output,
  ViewChild,
  ViewEncapsulation,
} from '@angular/core';
import {
  DsfrFormSelectModule,
  DsfrI18nPipe,
  DsfrI18nService,
  DsfrSelectOption,
  LangService,
  Language,
  LoggerService,
} from '@edugouvfr/ngx-dsfr';
import { Subscription } from 'rxjs';
import { DsfrDateEvent } from '../shared/models/date-event';
import { DateUtils } from '../shared/utils/date/date-utils';
import { MESSAGES_EN } from './i18n/messages.en';
import { MESSAGES_FR } from './i18n/messages.fr';

/**
 * Implémente la saisie d'une date via un input ou via un calendrier affichant les jours et les mois.
 * Validator est implémenté uniquement par rapport à min et max
 * @since 0.10
 * @author pfontanet
 */
@Component({
  selector: 'dsfr-ext-calendar, dsfrx-calendar',
  encapsulation: ViewEncapsulation.None,
  templateUrl: './calendar.component.html',
  styleUrls: ['./calendar.component.scss'],
  standalone: true,
  imports: [DsfrFormSelectModule, DsfrI18nPipe],
})
export class DsfrCalendarComponent implements AfterViewInit, OnDestroy, OnInit, OnChanges {
  /**
   * Valeur initial de la date.
   * La valeur doit prendre la forme dd/mm/yyyy pour la locale fr et mm/dd/yyyy pour la locale en.
   */
  @Input() value: string | undefined = '';

  /** Force le focus sur la date courante à l'initialisation du composant */
  @Input() autofocus: boolean = false;

  /** Id de l'élément correspondant au label de la grille de dates */
  @Input({ required: true }) ariaLabelledBy: string = '';

  /** Emission de la date au format DsfrDateEvent */
  @Output() dateChange = new EventEmitter<DsfrDateEvent>();

  @ViewChild('previousMonth') protected previousMonth: ElementRef<HTMLButtonElement>;
  @ViewChild('nextMonth') protected nextMonth: ElementRef<HTMLButtonElement>;
  @ViewChild('monthSelect', { read: ElementRef }) protected monthSelect: ElementRef<HTMLElement>;
  @ViewChild('yearSelect', { read: ElementRef }) protected yearSelect: ElementRef<HTMLElement>;
  @ViewChild('tbody') protected tbody: ElementRef<HTMLTableElement>;

  protected monthSelectOptions: DsfrSelectOption[] = [];
  protected yearSelectOptions: DsfrSelectOption[] = [];
  protected currentMonthIndex: number = new Date().getMonth();
  protected currentYear: number = new Date().getFullYear();
  protected translatedMonthAbbreviations: string[] = [];
  protected translatedMonths: string[] = [];
  protected prevMonthLabel = '';
  protected nextMonthLabel = '';
  protected monthSelectLabel = '';
  protected yearSelectLabel = '';
  protected isMonoYear = false;
  protected isMonoMonth = false;
  protected readonly rows = new Array(6);
  protected readonly columns = new Array(7);

  /* Node */
  private dayCells: HTMLTableCellElement[] = [];

  private prevCodeLang: string;
  private focusDay = new Date();
  private selectedDay: Date | undefined = new Date(0, 0, 1);
  private lastDate = -1;
  private _min: string | undefined = '';
  private _max: string | undefined = '';

  private subscription = new Subscription();

  /** @internal */
  constructor(
    private i18n: DsfrI18nService,
    private langService: LangService,
    private loggerService: LoggerService,
  ) {
    this.i18n.extendsLabelsBundle(Language.EN, MESSAGES_EN);
    this.i18n.extendsLabelsBundle(Language.FR, MESSAGES_FR);
    this.prevCodeLang = langService.lang;
    const langChange$ = i18n.completeLangChange$.subscribe((code) => this.onLangChange(code));
    this.subscription.add(langChange$);
  }

  /**
   * @return Le code de la langue en cours
   */
  get codeLang() {
    return this.langService.lang;
  }

  get min(): string | undefined {
    return this._min;
  }

  get max(): string | undefined {
    return this._max;
  }

  protected get isEnglishLocale(): boolean {
    return this.langService.lang === 'en';
  }

  /**
   * Date minimale inclue. Doit être au format IS0-8601 : AAAA-MM-JJ.
   * Ce paramètre peut être utilisé seul ou avec la propriété max.
   */
  @Input()
  set min(value: string | undefined) {
    if (value && !this.isValidIso(value)) {
      this.loggerService.warn(
        `dsfr-date-picker: La propriété min a un format incorrecte. Attendu: 'YYYY-MM-DD'. Reçu: ${value}`,
      );
    } else if (value && this.isValidIso(value)) {
      this._min = value;
      this.computeYearSelectOptions();
    }
  }

  /**
   * Date maximale inclue. Doit être au format IS0-8601 : AAAA-MM-JJ.
   * Ce paramètre peut être utilisé seul ou avec la propriété min.
   */
  @Input()
  set max(value: string | undefined) {
    if (value && !this.isValidIso(value)) {
      this.loggerService.warn(
        `dsfr-date-picker: La propriété max a un format incorrecte. Attendu: 'YYYY-MM-DD'. Reçu: ${value}`,
      );
    } else if (value && this.isValidIso(value)) {
      this._max = value;
      this.computeYearSelectOptions();
    }
  }

  /** @internal */
  ngOnInit(): void {
    this.updateMonthAndYearSelects();
    this.computeMonoYearAndMonoMonth();
  }

  /** @internal */
  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

  /** @internal */
  ngAfterViewInit() {
    const tableCells = this.tbody.nativeElement.getElementsByTagName('td');

    this.dayCells = Array.from(tableCells);

    this.computeMonthSelectOptions();
    this.computeYearSelectOptions();

    this.initializeCalendar();
  }

  /** @internal */
  ngOnChanges() {
    const validDate = this.reformatValue(this.value);

    if (validDate) {
      this.setNewValue(validDate, true);

      this.computeMonoYearAndMonoMonth();
      this.computeMonthSelectOptions();
      this.computeYearSelectOptions();

      this.initializeCalendar();
    }
  }

  /**
   * Permet de forcer le focus sur la date active, la dernière date focusée ou la date du jour par défaut.
   *
   * Cette méthode est notamment utilisée dans la gestion du calendrier embarqué dans une modale.
   */
  public focusDate(): void {
    for (let i = 0; i < this.dayCells.length; i++) {
      const dayNode = this.dayCells[i];
      const day = this.getDayFromDataDateAttribute(dayNode);

      if (this.isSameDay(day, this.focusDay) && this.inMinMaxRange(day)) {
        dayNode.tabIndex = 0;
        dayNode.focus();
      }
    }
  }

  /**********************
   * Event Handlers
   **********************/
  protected handleCancelButton(event: Event): void {
    let flagToPreventDefault = false;

    switch (event.type) {
      case 'keydown':
        const keyEvent = <KeyboardEvent>event;
        switch (keyEvent.key) {
          case 'Tab':
            const reachedBoundary = this.getReachedBoundary();

            if (reachedBoundary !== undefined) {
              this.autoFocusBoundaryDate(reachedBoundary);
              flagToPreventDefault = true;
              break;
            }

            if (!this.inMinMaxRange(this.focusDay)) {
              this.nextMonth.nativeElement.focus();
              flagToPreventDefault = true;
              break;
            }
            break;

          default:
            break;
        }
        break;

      case 'click':
        flagToPreventDefault = true;
        break;

      default:
        break;
    }

    if (flagToPreventDefault) {
      event.stopPropagation();
      event.preventDefault();
    }
  }

  protected handleNextMonthButton(event: Event): void {
    let flagToPreventDefault = false;

    switch (event.type) {
      case 'keydown':
        const keyEvent = <KeyboardEvent>event;
        switch (keyEvent.key) {
          case 'Enter':
            this.moveToNextMonth();
            this.updateTableCellsTabIndex(false);
            flagToPreventDefault = true;
            break;

          case 'Tab':
            const reachedBoundary = this.getReachedBoundary();

            if (reachedBoundary !== undefined) {
              this.autoFocusBoundaryDate(reachedBoundary);
              flagToPreventDefault = true;
            }

            break;
        }

        break;

      case 'click':
        this.moveToNextMonth();
        this.updateTableCellsTabIndex(false);
        break;

      default:
        break;
    }

    if (flagToPreventDefault) {
      event.stopPropagation();
      event.preventDefault();
    }
  }

  protected handlePreviousMonthButton(event: Event): void {
    let flagToPreventDefault = false;

    switch (event.type) {
      case 'keydown':
        const keyEvent = <KeyboardEvent>event;
        switch (keyEvent.key) {
          case 'Enter':
            this.moveToPreviousMonth();
            this.updateTableCellsTabIndex(false);
            flagToPreventDefault = true;
            break;
        }

        break;

      case 'click':
        this.moveToPreviousMonth();
        this.updateTableCellsTabIndex(false);
        flagToPreventDefault = true;
        break;

      default:
        break;
    }

    if (flagToPreventDefault) {
      event.stopPropagation();
      event.preventDefault();
    }
  }

  protected handleMonthSelectKeydown(event: KeyboardEvent): void {
    let flagToPreventDefault = false;

    switch (event.key) {
      case 'Up':
      case 'ArrowUp':
        this.moveToPreviousMonth();
        this.updateTableCellsTabIndex(false);
        flagToPreventDefault = true;
        break;

      case 'Down':
      case 'ArrowDown':
        this.moveToNextMonth();
        this.updateTableCellsTabIndex(false);
        flagToPreventDefault = true;
        break;

      case 'Left':
      case 'ArrowLeft':
      case 'Right':
      case 'ArrowRight':
        flagToPreventDefault = true;
    }

    if (flagToPreventDefault) {
      event.stopPropagation();
      event.preventDefault();
    }
  }

  protected handleYearSelectKeydown(event: KeyboardEvent): void {
    let flagToPreventDefault = false;

    switch (event.key) {
      case 'Up':
      case 'ArrowUp':
        this.moveToPreviousYear();
        this.updateTableCellsTabIndex(false);
        flagToPreventDefault = true;
        break;

      case 'Down':
      case 'ArrowDown':
        this.moveToNextYear();
        this.updateTableCellsTabIndex(false);
        flagToPreventDefault = true;
        break;

      case 'Left':
      case 'ArrowLeft':
      case 'Right':
      case 'ArrowRight':
        flagToPreventDefault = true;
    }

    if (flagToPreventDefault) {
      event.stopPropagation();
      event.preventDefault();
    }
  }

  protected handleDayClick(event: MouseEvent): void {
    if (!this.isDayDisabled(<HTMLElement>event.currentTarget)) {
      this.updateCalendarValue(<HTMLTableCellElement>event.currentTarget);
    }

    event.stopPropagation();
    event.preventDefault();
  }

  protected handleDayKeyDown(event: KeyboardEvent): void {
    let flagToPreventDefault = false;

    switch (event.key) {
      case ' ':
      case 'Enter':
        if (this.inMinMaxRange(this.focusDay)) {
          this.updateCalendarValue(<HTMLTableCellElement>event.currentTarget);
          flagToPreventDefault = true;
        }
        break;

      case 'Right':
      case 'ArrowRight':
        this.moveFocusToNextDay();
        flagToPreventDefault = true;
        break;

      case 'Left':
      case 'ArrowLeft':
        this.moveFocusToPreviousDay();
        flagToPreventDefault = true;
        break;

      case 'Down':
      case 'ArrowDown':
        this.moveFocusToNextWeek();
        flagToPreventDefault = true;
        break;

      case 'Up':
      case 'ArrowUp':
        this.moveFocusToPreviousWeek();
        flagToPreventDefault = true;
        break;

      case 'PageUp':
        if (event.shiftKey) {
          this.moveToPreviousYear();
        } else {
          this.moveToPreviousMonth();
        }
        this.updateTableCellsTabIndex();
        flagToPreventDefault = true;
        break;

      case 'PageDown':
        if (event.shiftKey) {
          this.moveToNextYear();
        } else {
          this.moveToNextMonth();
        }
        this.updateTableCellsTabIndex();
        flagToPreventDefault = true;
        break;

      case 'Home':
        this.moveFocusToFirstDayOfWeek();
        flagToPreventDefault = true;
        break;

      case 'End':
        this.moveFocusToLastDayOfWeek();
        flagToPreventDefault = true;
        break;
    }

    if (flagToPreventDefault) {
      event.stopPropagation();
      event.preventDefault();
    }
  }

  /**
   * Gestion de la sélection dans la liste déroulante des mois
   *
   * @param selectedMonthIndex Index du mois sélectionné (indexage commençant à 1)
   */
  protected handleMonthSelect(selectedMonthIndex: number): void {
    const currentYear = this.focusDay.getFullYear();
    const currentDay = this.focusDay.getDate();

    const dayToFocus = this.computeDayToFocus(currentYear, selectedMonthIndex, currentDay);

    const targetDate = new Date(currentYear, selectedMonthIndex, dayToFocus);

    this.lastDate = targetDate.getDate();
    this.focusDay = targetDate;

    this.updateCalendar();
    this.moveFocusToDay(targetDate);
  }

  /**
   * Gestion de la sélection dans la liste déroulante des années
   *
   * @param selectedYear Année sélectionnée
   */
  protected handleYearSelect(selectedYear: number): void {
    const currentMonth = this.focusDay.getMonth();
    const currentDay = this.focusDay.getDate();

    const dayToFocus = this.computeDayToFocus(selectedYear, currentMonth, currentDay);
    const dateToFocus = new Date(selectedYear, currentMonth, dayToFocus);

    this.lastDate = dateToFocus.getDate();
    this.focusDay = dateToFocus;

    this.updateCalendar();
    this.moveFocusToDay(dateToFocus);
  }

  /**
   * Permet de calculer si le jour a focus est bien disponible dans le mois et l'année de destination après sélection
   * dans la liste déroulante.
   *
   * Dans le cas où l'on se trouve le 29 Février 2024 et où l'on change d'année grâce à la liste déroulante à 2023,
   * le jour a focus devrait être le 28 Février 2023.
   *
   * @param currentYear Année cible
   * @param selectedMonthIndex Mois cible
   * @param currentDay Jour courant dans le calendrier
   * @returns Le numéro du jour à focus
   */
  private computeDayToFocus(currentYear: number, selectedMonthIndex: number, currentDay: number): number {
    const numberOfDaysInMonth = this.getNumberOfDaysInMonth(currentYear, selectedMonthIndex);
    const dayToFocus = numberOfDaysInMonth < currentDay ? numberOfDaysInMonth : currentDay;
    return dayToFocus;
  }

  /**
   * Formatage de la valeur dans la langue courante
   * @param value Valeur au format ISO ou dans la langue courante
   */
  private reformatValue(value: string | undefined): string | undefined {
    if (value === undefined || value === null) {
      return undefined;
    }

    const parsedDate = DateUtils.parse(this.codeLang, value);

    const parsedIsoDate = DateUtils.parseDateIso(value);

    if (parsedIsoDate === undefined && parsedDate === undefined) {
      return undefined;
    }

    return DateUtils.format(this.codeLang, parsedIsoDate ?? parsedDate);
  }

  /**
   * Gestion du changement de langue dans le composant.
   *
   * @param code Code de la locale à prendre en compte
   */
  private onLangChange(code: string): void {
    this.prevMonthLabel = this.i18n.t('calendar.prevMonth');
    this.nextMonthLabel = this.i18n.t('calendar.nextMonth');
    this.monthSelectLabel = this.i18n.t('calendar.monthSelect');
    this.yearSelectLabel = this.i18n.t('calendar.yearSelect');

    this.computeMonthSelectOptions();
    this.computeYearSelectOptions();
    this.updateMonthTranslations();

    // Reformatage de l'input
    const date = DateUtils.parse(this.prevCodeLang, this.value);
    const newValue = DateUtils.format(code, date);
    this.setNewValue(newValue);
    this.prevCodeLang = code;
    this.initializeCalendar();
  }

  /**
   * Compare deux dates et retourne true si ces dernières sont les même.
   *
   * @param day1 Date 1 à comparer
   * @param day2 Date 2 à comparer
   * @returns true si les deux dates sont les même
   */
  private isSameDay(day1: Date | undefined, day2: Date | undefined): boolean {
    if (day1 === undefined || day2 === undefined) {
      return false;
    } else {
      return (
        day1.getFullYear() === day2.getFullYear() &&
        day1.getMonth() === day2.getMonth() &&
        day1.getDate() === day2.getDate()
      );
    }
  }

  /**
   * Mets à jour l'ensemble du Date Picker
   */
  private updateCalendar(): void {
    this.updateMonthAndYearSelects();

    const firstDayOfMonth = new Date(this.focusDay.getFullYear(), this.focusDay.getMonth(), 1);
    const currentMonthStartingColumn = this.getCurrentMonthStartingColumn(firstDayOfMonth);
    const firstDisplayedDayOnCalendar = firstDayOfMonth.setDate(firstDayOfMonth.getDate() - currentMonthStartingColumn);

    let dateOfCellForGridLoop = new Date(firstDisplayedDayOnCalendar);

    for (let i = 0; i < this.dayCells.length; i++) {
      const isDayFromCurrentMonth = dateOfCellForGridLoop.getMonth() !== this.focusDay.getMonth();

      this.updateCell(this.dayCells[i], isDayFromCurrentMonth, dateOfCellForGridLoop);

      dateOfCellForGridLoop.setDate(dateOfCellForGridLoop.getDate() + 1);
    }
  }

  /**
   * Détermine la colonne à partir de laquelle le mois commence dans le calendrier.
   *
   * Cette colonne est dépendante du jour de la semaine mais également de la locale : le calendrier anglais a comme
   * première colonne le dimanche, contrairement au calendrier français pour lequel la première colonne est le lundi.
   *
   * @param firstDayOfMonth Index du premier jour de la semaine
   * @returns Index de la colonne où commence le mois
   */
  private getCurrentMonthStartingColumn(firstDayOfMonth: Date): number {
    if (this.isEnglishLocale) {
      return firstDayOfMonth.getDay();
    } else if (firstDayOfMonth.getDay() - 1 === -1) {
      return 6;
    } else {
      return firstDayOfMonth.getDay() - 1;
    }
  }

  /**
   * Mise à jour des valeurs des deux listes déroulantes, mois et année.
   */
  private updateMonthAndYearSelects(): void {
    this.currentMonthIndex = this.focusDay.getMonth();
    this.currentYear = this.focusDay.getFullYear();
  }

  /**
   * Mise à jour de l'état d'une cellule de la table en fonction de :
   * - si elle se trouve dans le mois,
   * - si elle se trouve en dehors des dates limites,
   * - si elle est sélectionnée.
   *
   * @param tdElement Une cellule td du tableau
   * @param outsideOfCurrentMonth boolean true si le jour fait partie du mois ou non
   * @param cellDate jour correspondant à la cellule
   */
  private updateCell(tdElement: HTMLTableCellElement, outsideOfCurrentMonth: boolean, cellDate: Date): void {
    const numericDayOfTheMonth = cellDate.getDate();
    const monthCalendarIndex = cellDate.getMonth() + 1;

    const yyyy = cellDate.getFullYear().toString();
    const mm = monthCalendarIndex <= 9 ? `0${monthCalendarIndex}` : monthCalendarIndex.toString();
    const dd = numericDayOfTheMonth <= 9 ? `0${numericDayOfTheMonth}` : numericDayOfTheMonth.toString();

    tdElement.tabIndex = -1;
    tdElement.removeAttribute('aria-selected');
    tdElement.classList.remove('disabled', 'out-of-month');
    tdElement.setAttribute('data-date', `${yyyy}-${mm}-${dd}`);

    if (!this.inMinMaxRange(cellDate)) {
      tdElement.classList.add('disabled');
    } else if (outsideOfCurrentMonth) {
      tdElement.classList.add('out-of-month');
    }

    // daySpan est un span caché ('.fr-sr-only') permettant la vocalisation du jour de la semaine au focus d'une date.
    // numberSpan est la partie visible dans la grille représentant le nombre du jour.
    const [daySpan, numberSpan] = [
      tdElement.getElementsByTagName('span')[0],
      tdElement.getElementsByTagName('span')[1],
    ];

    daySpan.textContent = `${this.i18n.t('calendar.days')[cellDate.getDay()]} `;
    numberSpan.textContent = cellDate.getDate().toString();

    if (this.isSameDay(cellDate, this.selectedDay)) {
      tdElement.setAttribute('aria-selected', 'true');
    }

    if (this.isSameDay(cellDate, this.focusDay)) {
      tdElement.tabIndex = 0;
    }
  }

  /**
   * Initie le changement de focus de la date.
   *
   * @param targetDate Date cible attendue
   */
  private moveFocusToDay(targetDate: Date): void {
    const focusDayMemory = this.focusDay;

    this.focusDay = targetDate;

    if (
      focusDayMemory.getMonth() !== this.focusDay.getMonth() ||
      focusDayMemory.getFullYear() !== this.focusDay.getFullYear()
    ) {
      this.updateCalendar();
    }

    this.updateTableCellsTabIndex();
  }

  /**
   * Met à jour le tabindex de toutes les cellules de la grille.
   *
   * Nécessaire pour rendre un élement de la table focusable.
   *
   * @param autofocus Permet de forcer le focus sur l'élément défini comme étant à tabindex 0. Valeur par défaut à true.
   */
  private updateTableCellsTabIndex(autofocus = true): void {
    for (let i = 0; i < this.dayCells.length; i++) {
      const dayNode = this.dayCells[i];
      const day = this.getDayFromDataDateAttribute(dayNode);

      dayNode.tabIndex = -1;

      if (this.isSameDay(day, this.focusDay) && this.inMinMaxRange(day)) {
        dayNode.tabIndex = 0;

        if (autofocus) {
          dayNode.focus();
        }
      }
    }
  }

  /**
   * Force le focus sur la limite min ou max d'un mois pour éviter que le focus soit indisponible.
   *
   * Exemple: L'utilisateur sélectionne le 17 Juillet 2024. Il décide ensuite de changer de mois via la liste
   * déroulante et cible Octobre. Cependant, la date max donnée au date picker est le 10 Octobre 2024. Etant incapable
   * de focus cette date au retour du focus dans la grille, la nouvelle date de focus bascule alors à la date max déterminée,
   * cad. le 10 Octobre 2024.
   *
   * @param boundary type du boundary à prendre en compte, entre le min et le max
   */
  private autoFocusBoundaryDate(boundary: 'min' | 'max'): void {
    const boundaryDate = boundary === 'min' ? DateUtils.parseDateIso(this.min!) : DateUtils.parseDateIso(this.max!);

    for (let i = 0; i < this.dayCells.length; i++) {
      const dayNode = this.dayCells[i];
      const day = this.getDayFromDataDateAttribute(dayNode);

      if (boundaryDate && this.isSameDay(day, boundaryDate)) {
        dayNode.tabIndex = 0;

        dayNode.focus();
      }
    }
  }

  private initializeCalendar(): void {
    this.computeInitialDate();
    this.lastDate = this.focusDay.getDate();
    this.updateCalendar();

    if (this.autofocus) {
      this.focusDate();
    }
  }

  private getNumberOfDaysInMonth(year: number, monthIndex: number): number {
    return new Date(year, monthIndex + 1, 0).getDate();
  }

  /**
   * Calcul la date cible en fonction de la date actuelle et du nombre de mois à ajouter ou enlever
   * à cette dernière.
   *
   * @param currentDate Date actuelle
   * @param monthsOffset Nombre de mois à ajouter ou enlever à la date actuelle
   * @returns La date cible
   */
  private computeTargetDateWithMonthOffset(currentDate: Date, monthsOffset: number): Date {
    const getDays = (year: number, month: number) => new Date(year, month, 0).getDate();

    const isPrev = monthsOffset < 0;
    const numYears = Math.trunc(Math.abs(monthsOffset) / 12);
    monthsOffset = Math.abs(monthsOffset) % 12;

    const newYear = isPrev ? currentDate.getFullYear() - numYears : currentDate.getFullYear() + numYears;

    const newMonth = isPrev ? currentDate.getMonth() - monthsOffset : currentDate.getMonth() + monthsOffset;

    const newDate = new Date(newYear, newMonth, 1);

    const daysInMonth = getDays(newDate.getFullYear(), newDate.getMonth() + 1);

    this.lastDate = this.lastDate ? this.lastDate : currentDate.getDate();

    if (this.lastDate > daysInMonth) {
      newDate.setDate(daysInMonth);
    } else {
      newDate.setDate(this.lastDate);
    }

    return newDate;
  }

  private moveToNextYear(): void {
    this.focusDay = this.computeTargetDateWithMonthOffset(this.focusDay, 12);
    this.updateCalendar();
  }

  private moveToPreviousYear(): void {
    this.focusDay = this.computeTargetDateWithMonthOffset(this.focusDay, -12);
    this.updateCalendar();
  }

  private moveToNextMonth(): void {
    this.focusDay = this.computeTargetDateWithMonthOffset(this.focusDay, 1);
    this.updateCalendar();
  }

  private moveToPreviousMonth(): void {
    this.focusDay = this.computeTargetDateWithMonthOffset(this.focusDay, -1);
    this.updateCalendar();
  }

  private moveFocusToNextDay(): void {
    const d = new Date(this.focusDay);
    d.setDate(d.getDate() + 1);

    if (this.inMinMaxRange(d)) {
      this.lastDate = d.getDate();
      this.moveFocusToDay(d);
    }
  }

  private moveFocusToNextWeek(): void {
    const d = new Date(this.focusDay);
    d.setDate(d.getDate() + 7);

    if (this.inMinMaxRange(d)) {
      this.lastDate = d.getDate();
      this.moveFocusToDay(d);
    }
  }

  private moveFocusToPreviousDay(): void {
    const d = new Date(this.focusDay);
    d.setDate(d.getDate() - 1);

    if (this.inMinMaxRange(d)) {
      this.lastDate = d.getDate();
      this.moveFocusToDay(d);
    }
  }

  private moveFocusToPreviousWeek(): void {
    const d = new Date(this.focusDay);
    d.setDate(d.getDate() - 7);

    if (this.inMinMaxRange(d)) {
      this.lastDate = d.getDate();
      this.moveFocusToDay(d);
    }
  }

  private moveFocusToFirstDayOfWeek(): void {
    const d = new Date(this.focusDay);
    d.setDate(d.getDate() - d.getDay());
    this.lastDate = d.getDate();
    this.moveFocusToDay(d);
  }

  private moveFocusToLastDayOfWeek(): void {
    const d = new Date(this.focusDay);
    d.setDate(d.getDate() + (6 - d.getDay()));
    this.lastDate = d.getDate();
    this.moveFocusToDay(d);
  }

  // Day methods

  private isDayDisabled(domNode: HTMLElement): boolean {
    return domNode.classList.contains('disabled');
  }

  private getDayFromDataDateAttribute(dayNode: HTMLElement): Date {
    const dataDate = dayNode.getAttribute('data-date');
    const parts = dataDate ? dataDate.split('-') : [];
    return parts.length >= 3 ? new Date(parseInt(parts[0]), parseInt(parts[1]) - 1, parseInt(parts[2])) : new Date();
  }

  // Textbox methods

  /**
   * Mets à jour la valeur du composant et émets un événement de type DsfrDateEvent.
   * Cette emission peut être omise via le paramètre booléen skipEmission, notamment à l'initialisation du composant.
   * @param value
   * @private
   */
  private setNewValue(value: string | undefined, skipEmission = false): void {
    this.value = value;
    const date = DateUtils.parse(this.codeLang, this.value);

    if (!skipEmission) {
      this.dateChange.emit({ value: this.value, date: date });
    }
  }

  /**
   * Valorise la date de l'input suite à la sélection d'une date dans le calendrier
   * @param domNode cellule td du calendrier telle que 'data-date' contienne la date ISO, ex : data-date="2024-05-28".
   * - i18n : le code de formatage fait appel à DateUtils
   */
  private updateCalendarValue(domNode: HTMLTableCellElement): void {
    this.selectedDay = this.getDayFromDataDateAttribute(domNode);
    this.focusDay = this.getDayFromDataDateAttribute(domNode);
    this.updateCalendar();

    const newValue = DateUtils.format(this.codeLang, this.selectedDay);
    this.setNewValue(newValue);
  }

  /**
   * Positionne la date du calendrier à partir de
   * - i18n : la date est obtenue à partir de DateUtils
   */
  private computeInitialDate(): void {
    const day = DateUtils.parse(this.codeLang, this.value);
    const isOnBoundary = day ? this.isInBounds(day) : false;

    if (day && isOnBoundary) {
      this.focusDay = day;
      this.selectedDay = new Date(this.focusDay);
    } else if (day && !isOnBoundary) {
      this.focusDay = new Date();
      this.selectedDay = new Date(day);
    } else {
      // If not a valid date (MM/DD/YY) initialize with todays date
      this.focusDay = new Date();
      this.selectedDay = new Date(0, 0, 1);
    }
  }

  /**
   * @param date date
   * @returns true si la data 'd' est dans l'intervalle [min, max] du cou
   */
  private inMinMaxRange(date: Date): boolean {
    const minDate = this.min ? DateUtils.parseDateIso(this.min) : undefined;
    const maxDate = this.max ? DateUtils.parseDateIso(this.max) : undefined;

    return (
      (minDate === undefined || minDate < date || this.isSameDay(date, minDate)) &&
      (maxDate === undefined || date < maxDate || this.isSameDay(date, maxDate))
    );
  }

  private computeMonthSelectOptions(): void {
    const translatedMonths = DateUtils.arr(this.i18n, this.loggerService, 'calendar.months');

    this.monthSelectOptions = translatedMonths.map((monthLabel, i) => ({
      label: monthLabel,
      value: i, // Pour information, en JS, les mois sont indexés à partir de 0 contrairement aux jours et années
    }));
  }

  private computeYearSelectOptions(): void {
    const currentYear = new Date().getFullYear();
    const defaultMinOffset = 100;
    const defaultMaxOffset = 100;
    const minYear = this.min ? new Date(this.min).getFullYear() : undefined;
    const maxYear = this.max ? new Date(this.max).getFullYear() : undefined;

    const lowerBoundYear = minYear ?? currentYear - defaultMinOffset;
    const higherBoundYear = maxYear ?? currentYear + defaultMaxOffset;

    const options: DsfrSelectOption[] = [];

    for (let year = lowerBoundYear; year <= higherBoundYear; year++) {
      options.push({ label: year.toString(), value: year });
    }

    this.yearSelectOptions = options;
  }

  private updateMonthTranslations(): void {
    this.translatedMonths = DateUtils.arr(this.i18n, this.loggerService, 'calendar.months');
    this.translatedMonthAbbreviations = DateUtils.arr(this.i18n, this.loggerService, 'calendar.monthsAbbr');
  }

  private isValidIso(boundaryString: string): boolean {
    const dateRegex = /^\d{4}-\d{2}-\d{2}$/;

    return dateRegex.test(boundaryString);
  }

  /**
   * Dans le cas d'un changement de mois ou d'année dans le calendrier, cette fonction détermine le type de butée atteinte
   * dans le cas ou le nouveau focusDay se trouverait en dehors des dates min ou max, uniquement dans un mois contenant lesdits min ou max.
   *
   * Exemple: Le focusDay est le 20 Juin 2024 et la date max le 15 Juillet 2024. Si l'utilisateur navigue vers le mois de Juillet via la
   * liste déroulante ou la flèche de "Mois suivant", getReachedBoundary retourne 'max', qui est le type de butée atteinte.
   *
   * @returns Retourne le type de butée atteinte (min ou max) ou undefined si aucune butée n'est atteinte ou qu'aucun focusDay n'est valide pour
   * cette date.
   */
  private getReachedBoundary(): undefined | 'min' | 'max' {
    const minDate = this.min ? DateUtils.parseDateIso(this.min) : undefined;
    const maxDate = this.max ? DateUtils.parseDateIso(this.max) : undefined;

    if (!this.min && !this.max) {
      return undefined;
    }

    if (
      minDate &&
      this.focusDay.getFullYear() === minDate.getFullYear() &&
      this.focusDay.getMonth() === minDate.getMonth() &&
      this.focusDay.getDate() < minDate.getDate()
    ) {
      return 'min';
    }

    if (
      maxDate &&
      this.focusDay.getFullYear() === maxDate.getFullYear() &&
      this.focusDay.getMonth() === maxDate.getMonth() &&
      this.focusDay.getDate() > maxDate.getDate()
    ) {
      return 'max';
    }

    return undefined;
  }

  private computeMonoYearAndMonoMonth() {
    const hasMinAndMax = !!this.min && !!this.max;

    this.isMonoYear =
      hasMinAndMax &&
      DateUtils.parseDateIso(this.min!)?.getFullYear() === DateUtils.parseDateIso(this.max!)?.getFullYear();

    this.isMonoMonth =
      hasMinAndMax &&
      this.isMonoYear &&
      DateUtils.parseDateIso(this.min!)?.getMonth() === DateUtils.parseDateIso(this.max!)?.getMonth();
  }

  /**
   * Vérifie si la date active du composant se trouve dans les limites données du composant.
   *
   * @param date
   * @returns Un booléen indiquant si la date se trouve bien entre les limites minimum et maximum données.
   */
  private isInBounds(date: Date): boolean {
    const minDate = this.min ? DateUtils.parseDateIso(this.min) : undefined;
    const maxDate = this.max ? DateUtils.parseDateIso(this.max) : undefined;

    if (!minDate && !maxDate) {
      return true;
    } else if (maxDate && !minDate) {
      return date.getTime() < maxDate.getTime();
    } else if (minDate && !maxDate) {
      return date.getTime() > minDate.getTime();
    } else {
      return date.getTime() < maxDate!.getTime() && date.getTime() > minDate!.getTime();
    }
  }
}
