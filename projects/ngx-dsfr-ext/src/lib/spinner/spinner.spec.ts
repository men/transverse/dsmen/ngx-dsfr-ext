import { Component, ViewChild } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { DsfrSpinnerComponent } from './spinner.component';

@Component({
  selector: `edu-host-component`,
  template: `<dsfr-ext-spinner typeLoader="quarter"></dsfr-ext-spinner>`,
})
class TestHostComponent {
  @ViewChild(DsfrSpinnerComponent)
  public spinnerComponent: DsfrSpinnerComponent;
}

describe('DsfrSpinnerComponent', () => {
  let testHostComponent: TestHostComponent;
  let fixture: ComponentFixture<TestHostComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [DsfrSpinnerComponent],
      declarations: [TestHostComponent],
    }).compileComponents();

    fixture = TestBed.createComponent(TestHostComponent);
    testHostComponent = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should put default message context ', () => {
    fixture.detectChanges();

    const element = fixture.nativeElement.querySelector('span.fr-sr-only');
    expect(element.innerHTML).toEqual('Chargement en cours');
  });

  it('should put user message context ', () => {
    testHostComponent.spinnerComponent.labelSrOnly = 'message test';
    fixture.detectChanges();

    const element = fixture.nativeElement.querySelector('span.fr-sr-only');
    expect(element.innerHTML).toEqual('message test');
  });
});
