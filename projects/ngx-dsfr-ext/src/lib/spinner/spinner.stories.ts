import { Meta, moduleMetadata, StoryFn } from '@storybook/angular';
import { DsfrSpinnerComponent } from './spinner.component';

const meta: Meta = {
  title: 'Components/Spinner',
  component: DsfrSpinnerComponent,
  decorators: [moduleMetadata({ imports: [] })],
  parameters: {
    docs: {
      toc: {
        disable: true, // Disables the table of contents
      },
    },
  },
  argTypes: {
    typeLoader: { control: { type: 'inline-radio' } },
  },
};
export default meta;

const Template: StoryFn<DsfrSpinnerComponent> = (args) => ({
  props: args,
});

export const Default = {
  render: Template,
};

export const Quarter = {
  render: Template,

  args: {
    typeLoader: 'quarter',
    labelSrOnly: 'test'
  },
};

export const Line = {
  render: Template,

  args: {
    typeLoader: 'line',
  },
};

export const WithOverlay = {
  render: Template,

  args: {
    typeLoader: 'line',
    overlay: true,
  },
};
