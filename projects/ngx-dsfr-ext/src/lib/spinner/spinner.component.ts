import { CommonModule } from '@angular/common';
import { Component, inject, Input, ViewEncapsulation } from '@angular/core';
import { DsfrI18nPipe, DsfrI18nService, Language } from '@edugouvfr/ngx-dsfr';
import { MESSAGES_EN } from './i18n/messages.en';
import { MESSAGES_FR } from './i18n/messages.fr';

@Component({
  selector: 'dsfr-ext-spinner',
  templateUrl: './spinner.component.html',
  styleUrls: ['./spinner.scss'],
  encapsulation: ViewEncapsulation.None,
  standalone: true,
  imports: [CommonModule, DsfrI18nPipe],
})
export class DsfrSpinnerComponent {
  /**
   * Type de loader.
   */
  @Input() typeLoader: 'sun' | 'quarter' | 'line' = 'sun';

  /**
   * Conditionne un overlay fullscreen.
   */
  @Input() overlay = false;

  /**
   *  Message permettant de donner plus de contexte aux usagers utilisant un lecteur d'écran.
   * (ex : "En attente de la validation de l'inscription.")
   */
  @Input() labelSrOnly: string | undefined;

  private i18n = inject(DsfrI18nService);

  constructor() {
    this.i18n.extendsLabelsBundle(Language.EN, MESSAGES_EN);
    this.i18n.extendsLabelsBundle(Language.FR, MESSAGES_FR);
  }

  /** @internal */
  getClasses(): {} {
    return {
      'dsfrx-spinner': true,
      'dsfrx-spinner--sun': this.typeLoader === 'sun',
      'dsfrx-spinner--quarter': this.typeLoader === 'quarter',
      'dsfrx-spinner--line': this.typeLoader === 'line',
    };
  }
}
