import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import {
  DsfrButtonModule,
  DsfrFooterComponent,
  DsfrFooterModule,
  DsfrFormSelectModule,
  DsfrPaginationModule,
  DsfrTableFooterComponent,
} from '@edugouvfr/ngx-dsfr';
import { AgGridModule } from 'ag-grid-angular';
import { DsfrButtonCellGridComponent } from './components/button-cell-grid.component';
import { EduRemoteAgGridDirective } from './directives/remote-ag-grid.directive';
import { DsfrGridComponent } from './grid.component';

@NgModule({
  declarations: [EduRemoteAgGridDirective, DsfrGridComponent],
  exports: [DsfrGridComponent],
  imports: [
    CommonModule,
    FormsModule,
    HttpClientModule,
    DsfrButtonModule,
    AgGridModule,
    DsfrButtonCellGridComponent,
    DsfrPaginationModule,
    DsfrFormSelectModule,
    DsfrTableFooterComponent,
  ],
})
export class DsfrGridModule {}
