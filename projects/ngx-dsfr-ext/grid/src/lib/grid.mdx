import { Meta, Story, Canvas, Controls, Source } from '@storybook/blocks';
import * as GridStories from './grid.stories';

# Grid

<Meta of={GridStories} title="Grid" />

- _Module_ : `DsfrGridModule`
- _Composant_ : `DsfrGridComponent`
- _Tag_ : `dsfrx-ag-grid`, `dsfr-ext-ag-grid`
- _Composants optionnels_ : `DsfrButtonCellGridComponent`, `DsfrSelectFilterComponent`

<h2 id="usage"> Usage </h2>

Le composant datagrid permet de reprendre les fonctionnalités de la librairie ag-grid tout en ayant un style proche du composant de table du DSFR.
Il nécessite l'installation des dépendances `ag-grid-angular` et `ag-grid-community` :

  ```bash
  $ npm install --save ag-grid-community
  ```

Deux fichiers de style sont à rajouter dans le build angular.json :

```json
   "styles": [
              ...
              "node_modules/ag-grid-community/styles/ag-grid.min.css",
              "node_modules/ag-grid-community/styles/ag-theme-alpine.min.css"
            ]
```

Les imports sont à effectuer depuis `@edugouvfr/ngx-dsfr-ext/grid`.

```typescript
import { DsfrGridModule } from '@edugouvfr/ngx-dsfr-ext/grid';
```

🔥 La version spécifique de ag-grid `32.2.0` introduit une régression sur le modèle de sélection. Les versions utilisées à partir de la 32.2 doivent être `32.2.1` ou supérieur et `ngx-dsfr-ext@0.11.1` ou supérieur.

<h3 id="data-client"> Données côté client </h3>

Par défaut le composant propose une implémentation avec les données côté client (`mode='client'`).
Il faut renseigner l'attribut `rowData` qui contient alors l'ensemble des données.

**_Exemple :_**

```html
<dsfr-ext-ag-grid
  caption="Titre de la grid"
  [pageSize]="10"
  [defaultColDef]="{sortable: true}"
  [columnDefs]="[{ headerName: 'Id', field: 'id'}, { headerName: 'Nom', field: 'name'}]"
  [rowData]="[{'id': 1, 'name': 'Pen'},{ 'id': 2, 'name': 'Brody'}]"
  [gridOptions]="{rowSelection: 'multiple'}"></dsfr-ext-ag-grid>
```
<h3 id="data-server"> Données côté serveur </h3>

Le composant propose une utilisation avec les données côté serveur.
Pour cela, l'implémentation doit renseigner :

- L'attribut `mode='server'`
- La fonction de récupération des données. Elle sera appelée au retour de l'évènement `dataChange` qui concerne chaque changement d'affichage, par exemple à la pagination ou l'application de filtres. Le paramètre de retour de type `DsfrGridParams` contient les filtres applicables gérés par ag-grid (pagination, tri, filtres).
- L'attribut `result` de type `DsfrRemoteGridResult` qui contient les données de résultat. La grid sera mise à jour dès la modification de cet attribut.
- `rowId` : Il est fortement conseillé de préciser le champ utilisé en tant qu'identifiant de la donnée (notamment si on définit un `initialState`)


**_Exemple :_**

```html
<dsfr-ext-ag-grid ... 
  mode="server" 
  [result]="result" 
  [rowId]="'recordId"
  (dataChange)="getDataServerSide($event)"
  [pageSize]="10"
  [defaultColDef]="{sortable: true}"
  [gridOptions]="{rowSelection: 'multiple', rowMultiSelectWithClick: true}"
  [columnDefs]="[{ headerName: 'Id', field: 'id'}, { headerName: 'Nom', field: 'name'}]"
></dsfr-ext-ag-grid>
```

```typescript
 public getDataServerSide(queryParams: DsfrGridParams): void {
    // Exemple de mapping des paramètres pour qu'ils correspondent au format attendu de l'api
    const params: any = {
      start: queryParams.offset,
      rows: queryParams.limit,
      sort: queryParams.sort[0].colId,
      filters: queryParams.filters
    };

    httpClient
      .get<any>(url, {
        params: { ...params },
      })
      .subscribe((res: any) => {
        // Exemple de mapping des données renvoyées par l'api pour qu'elles correspondent au format DsfrRemoteGridResult
        this.result = { data: res.records, totalRecords: res.totalCount };
      });
  };
```

<h3 id="accessibility"> Accessibilité </h3>

👓  La librairie ag-grid garantit un support pour l'accessibilité dans le tableau : cf. [Doc. AG-GRID - accessibility](https://www.ag-grid.com/angular-data-grid/accessibility/)
. Cependant, selon les fonctionnalités souhaitées il peut être nécessaire de surcharger l'implémentation pour offrir le niveau d'accessibilité souhaitée. 
Ag-grid permet ainsi de créer ses propres filtres ou composants de cellules sur lesquelles on aura alors totalement la main.

Du côté du composant `DsfrGridComponent`, nous nous efforçons d'avoir un niveau d'accessibilité suffisant sans avoir besoin de surcharger l'API ag-grid 
pour une utilisation "basique" du composant, ce scope couvrant les exemples proposés dans la documentation.
Côté visuel, le thème appliqué reprend le visuel du DSFR concernant les tableaux ou les composants comme les champs de saisie et boutons. 
A ce titre il couvre les recommandations concernant les couleurs, contrastes, labels etc. préconisées côté DSFR.

<h4> Attributs aria </h4>

Il est obligatoire d'avoir un titre associé au tableau en renseignant l'attribut `caption`.

Les attributs aria suivants sont ajoutés sur la balise contenant la grid (`role="treegrid"`) :
- `aria-labelledby` : lorsqu'un titre (`caption`) est renseigné
- `aria-multiselectable` : lorsque la multisélection est activée
- `aria-describedby`: lorsqu'il n'y a aucun résultat, référence l'identifiant du template de `overlayNoRowsTemplate`. Si ce template est redéfini, le nouvel identifiant doit être passé à l'attribut `idNoRowsTemplate`.
Il est possible d'en ajouter ou les surcharger avec la méthode de l'api ag-grid `setGridAriaProperty`

<h3 id="api-ag-grid"> API  de AG-Grid </h3>

L'évènement <b> `gridReady` </b> renvoie l'API d'ag-grid après initialisation de celle-ci (cf. évènement `gridReady` de ag-grid). Cela permet un accès à l'API si nécessaire pour ajouter des fonctionnalités non disponibles directement avec le composant dsfrx-ag-grid. cf.
[Doc. AG-GRID - API](https://www.ag-grid.com/angular-data-grid/grid-api/)

```typescript
<dsfr-ext-ag-grid ... (gridReady)="getGridApi($event)"></dsfr-ext-ag-grid>;

getGridApi(agGridApi: GridApi<any>): void {
  if(!this.agGridApi) this.agGridApi = agGridApi;
};

this.agGridApi.getColDefs();

```

En dehors des attributs directement exposés par le composant dsfrx-ag-grid, La documentation de ag-grid est la référence pour personnaliser les colonnes ou gérer les évènements.
Le composant agissant en tant que wrapper de ag-grid, toutes les fonctionnalités de ag-grid, version angular, sont utilisables.
Par exemple :

- si on veut définir une taille minimale ou maximale des colonnes on utilisera `minWidth` ou `maxWidth` sur la définition des colonnes [Doc. AG-GRID - column sizing](https://www.ag-grid.com/angular-data-grid/column-sizing/#column-flex)

<h2 id="fonct"> Fonctionnement  </h2>

<h3 id="pagination"> Pagination </h3>

Le composant de pagination reprend une partie du composant `ngx-dsfr-pagination `et intègre le markup et style visuel du DSFR. Il doit prochainement être intégré complètement à la librairie ngx-dsfr (v.1.12).
Nous n'utilisons pas le composant de pagination proposé par ag-grid.

<h3 id="keyboard"> Navigation au clavier </h3>

Ag-grid propose une implémentation de navigation au clavier sur la grid, cf. [Doc. AG-GRID - keyboard navigation](https://www.ag-grid.com/angular-data-grid/keyboard-navigation/).
Nous utilisons cette implémentatin en surchargeant la méthode `suppressKeyboardEvent` afin de permettre la navigation sur les éléments interactifs au sein d'une cellule avec la touche TAB (ex. liste de boutons). 

Si cette implémentation ne correspond pas au besoin, il est possible à partir de l'api d'ag-grid de redéfinir totalement la méthode `suppressKeyboardEvent` ou les autres méthodes de navigation (voir la rubrique `Custom Navigation` sur la doc. ag-grid).

<h3 id="filters"> Filtres </h3>

L'activation des filtres par défaut affiche uniquement un filtre rapide (champ de saisie ou composant personnalisé) sur la colonne.

La popup de filtres avancée est cachée par défaut dans les options de la grid. 
Sur ces filtres avancés, nous avons limité le nombre de filtrages à `maxNumConditions: 1` pour les problématiques de groupement de champs mais d'autres problématiques d'accessibilité subsistent : le manque de contexte des aria-label, les difficultés de navigation au clavier sur les listes d'option etc. 
C'est pourquoi nous on avons pris la décision de les désactiver de base sur le composant. 

Il est possible de les réactiver avec l'option `suppressHeaderFilterButton: false`, mais nous conseillons plutôt de définir des filtres customs avec la fonctionnalité d'ag-grid  [Doc. AG-GRID - filtre custom](https://www.ag-grid.com/angular-data-grid/component-filter/) afin de contrôler l'accessibilité du composant.
Les problématiques d'accessibilité sont connues d'AG-Grid et notées dans leur roadmap, nous espérons donc pouvoir les proposer par défaut dans le futur.

Pour plus d'information sur l'utilisation des filtres voir la partie [Avec filtres](/docs/components-grid--docs#with-filters) ci-dessous.


<h2 id="api"> API </h2>

<Controls />

<h2 id="exemples"> Exemples </h2>

<Canvas of={GridStories.Default} />

Définition des propriétés table/colonnes : 

```typescript
   defaultColDef: {
    sortable: true,
    floatingFilter: true,
    filter: true,
    flex: 1,
    autoHeight: true,
  },
  pageSize: 10,
  columnDefs: [
    { headerName: 'Id', field: 'id', filter: false, sort: 'asc',  suppressSizeToFit: true, minWidth: 150, maxWidth: 100},
    { headerName: 'Prénom', field: 'firstname', minWidth: 150 },
    { headerName: 'Nom', field: 'name', minWidth: 150 },
    { headerName: 'Date de naissance',  field: 'date', cellDataType: 'date', minWidth: 150 },
  ],
  rowData: clientSideData,
  gridOptions: {
    dataTypeDefinitions: {
      // surcharge de `date` pour gérer un format personnalisé `dd/mm/yyyy`
      date: {
        baseDataType: 'date',
        extendsDataType: 'date',
        valueParser: (params: any) => {
          return new Date(formatDate(params.newValue, 'dd/MM/yyyy', 'en'));
        },
        // conversion en `dd/mm/yyyy`  
        valueFormatter: (params: any) => {  return params.value == null ? '' : formatDate(params.value, 'dd/MM/yyyy', 'en'); },
      },
    },
  },
```

 <h3 id="fixed-height"> Footer </h3>

Le footer reprend directement le composant de footer implémenté côté `ngx-dsfr` sur la table.
Il comprend la pagination, le nombre de résultats et un slot optionnel (`footerActionsTemplate`) pour des boutons d'actions. 
Vous pouvez [lire ici la documentation](https://foad.phm.education.gouv.fr/edugouvfr/ngx-dsfr/1-12-1/?path=/docs/components-table--docs#table-footer)

 <h3 id="fixed-height"> Hauteur fixe </h3>

<Canvas of={GridStories.FixedHeight} />

<h3 id="style"> Style </h3>

L'input <b> `customClass` </b> permet d'appliquer des styles prédéfinis: 
- `fr-table-bordered` : afficher les bordures verticales, 
- `fr-table--striped`: afficher les zébras
- une autre classe personnalisée

Utilisation de customClass : `'fr-table-bordered'`

<Story of={GridStories.Borders} />


<h3 id="with-buttons"> Avec boutons d'actions </h3>

- Story : [Grid with buttons](/story/components-grid--with-buttons)

Utilisation du composant <b> `DsfrButtonCellGridComponent` </b> qui implémente `ICellRendererAngularComp` et utilise le composant ngx-dsfr-button.
Il est possible de créer son propre composant n'importe quel composant implémentant `ICellRendererAngularComp` et le passer dans les propriétés
de la colonne.

**_Exemple de déclaration pour ci-dessous :_**

```typescript
  columnDefs: ColDef[] = [
    { headerName: 'Id', field: 'id', filter: false, sort: 'asc', suppressSizeToFit: true, maxWidth: 100, minWidth: 100 },
    { headerName: 'Prénom', field: 'firstname', editable: true, minWidth: 250 },
    { headerName: 'Nom', field: 'name', minWidth: 250 },
    {
      headerName: 'Date de naissance',
      field: 'date',
      cellDataType: 'date',
      minWidth: 200,
    },
    {
      filter: false,
      sortable: false,
      cellRenderer: DsfrButtonCellGridComponent,
      maxWidth: 200,
      minWidth: 150
      cellRendererParams: {
        label: 'ajouter',
        icon: 'fr-icon-add-circle-line',
        size: 'SM',
        iconPosition: 'left',
        onClick: alert('click button')
      },
      headerName: 'Action',
    }];
```

<h3 id="with-filters"> Avec filtres </h3>

- Story : [Grid with filters](/story/components-grid--default)

Les filtres présentés sur les stories sont des *floating filter* activés avec les propriétés (qui peuvent être définies pour chaque colonne) :
```typescript
   floatingFilter: true,
   filter: true,
   defaultOption: `startWith` // optionnel pour remplacer la comparaison `contains` par défaut
```

Par défaut ces filtres sont de type champ de saisie (texte, nombre ou date) et font une comparaison de texte "contient". 
Cette comparaison peut être changée avec `defaultOption` ainsi que d'autres paramètres voir [Doc. AG-GRID - Filters params](https://www.ag-grid.com/angular-data-grid/filter-text/#example-text-filter).

Pour utiliser une liste de sélection de valeurs il existe le composant <b> `DsfrSelectFilterComponent` </b> qui implémente `IFloatingFilterAngularComp` et utilise le composant ngx-dsfr-select.
Pour l'activer sur une colonne :

```typescript
    {
      headerName: 'Statut',
      field: 'status',
      floatingFilterComponent: DsfrSelectFilterComponent,
      floatingFilterComponentParams: {
        label: 'Filtrer la colonne statut',
        placeholder: 'Tous',
        id: 'select-status',
        // liste d'options de type DsfrOption
        options: [
          { label: 'Tous', value: '' },
          { label: 'En attente', value: 'en attente' },
          { label: 'Inscrit', value: 'inscrit' },
          { label: 'Refusé', value: 'refusé' },
        ]
      },
    },
```

Il est possible de surcharger ce composant, ou de créer votre composant de filtre personnalisé qui implémente `IFloatingFilterAngularComp`, 
par exemple pour utiliser le multiselect. Il est recommandé de créer ses propres filtres complexes plutot que d'utiliser ceux de Ag-grid, cf. partie accessibilité.

<h3 id="with-selection"> Avec sélection </h3>

- Story : [Grid with selection](/story/components-grid--server-side-with-selection)

<h4> Activer la sélection </h4>

Pour activer la sélection, <b> `selection` </b> doit être défini avec le mode `checkboxes` à true `single` ou `multiple`. 
`rowMultiSelectWithClick` peut également être activé pour simplifier la multiselection.
(cf. [Doc AG-Grid row selection ](https://ag-grid.com/angular-data-grid/row-selection/))

Un style proche de celui du DSFR sera alors appliqué sur la première colonne contenant les cases à cocher.

```typescript
  gridOptions: {
      rowSelection: {
        checkboxes: true,
        mode: 'multiRow',
        headerCheckbox: true,
      },
  }
```

Note: si vous avez des colonnes fixées (`pinned`), pour que la colonne de case à cocher soit fixe également et que le style soit cohérent, il faut appliquer `selectionColumnDef`

```typescript
  gridOptions: {
    ...
    selectionColumnDef: { pinned: true },
  }
```

<h4> Evènement de sélection  </h4>

Deux évènements concernent la sélection :
- L'évènement <b>`selectionChange`</b> est envoyé à chaque changement du modèle de sélection. (cf. [Doc AG-GRID selectionChanged](https://www.ag-grid.com/angular-data-grid/grid-events/#reference-selection-selectionChanged))

- L'évènement `rowSelect` est émis à la sélection ET à la déselection d'une ligne. Il renvoie l'évènement concerné et la liste de tous les noeuds couramment sélectionnés.
Si on est en mode `singleRow`, il renverra donc deux évènements à la suite lors d'un changement de ligne sélectionnée : un pour la dé-sélection de la ligne précédente, puis un pour la sélection de la nouvelle ligne.

_Exemple d'utilisation_ 

```html
<dsfr-ext-ag-grid
  [defaultColDef]="defaultColDef"
  [pageSize]="pageSize"
  mode="server"
  [columnDefs]="columnDefs"
  [gridOptions]="gridOptions"
  (selectionChange)="onSelectionChange($event)"
  [rowId]="'record'"
  [result]="result"
  [autoRowsHeight]="true">
</dsfr-ext-ag-grid>

```
```typescript
  public onSelectionChange(e: DsfrGridSelectionEvent) {
    console.log('Liste des données sélectionnées :');
    console.log(e.selectedNodes.map((d: IRowNode) => d.data));
  }
```

Il est également possible de retrouver la liste des noeuds sélectionnés en utilisant l'api :

```typescript
this.agGridApi.getSelectedNodes();
this.agGridApi.getSelectedRows();
```

<h3 id="server-side"> Server Side </h3>

<Source of={GridStories.ServerSide} />

- Story : [On server side](/story/components-grid--server-side)

<h3 id="dynamic-height"> Hauteur de ligne dynamique </h3>

Par défaut la hauteur des lignes n'est pas calculée et sera donc la même pour toutes les lignes d'un tableau, avec des troncatures sur les textes.

🔥 <b> En mode `server`, l'attribut `autoHeight` de ag-grid n'est pas pris en compte </b>. Si on souhaite avoir une hauteur dynamique des lignes en fonction du contenu,
on peut utiliser l'attribut <b>`autoRowsHeight`</b> sur le composant.
Cet attribut passera les lignes en wrap (retour à la ligne) automatiquement. La hauteur des lignes dépendra donc de la hauteur de la cellule la plus grande et le tableau sera redimensionné en fonction.

Il est déconseillé d'utiliser cet attribut si on souhaite afficher un grand nombre de lignes sur une même page pour des problèmes de performance.

- Story : [Grid with dynamic row height](/story/components-grid--with-dynamic-row-height)

<h3 id="initial-state"> Etat initial </h3>

La propriété <b> `initialState`</b> permet d'exposer la propriété ag-grid représentant l'état courant [Grid state](https://www.ag-grid.com/angular-data-grid/grid-state/).

Elle peut être utilisée pour restaurer la page courante, des filtres etc. Par exemple, pour avoir une page initiale à la page 5, deux lignes pré-sélectionnées et un contexte de tri : 

   ```html
    <dsfr-ext-ag-grid 
      [rowId]="'recordId'"
      [initialState]="{ 
      pagination: { page: 5 }, 
      rowSelection: ['73', '74'], 
      sort: { sortModel: [{ colId: 'nom_etablissement', sort: 'desc' }] }}" ...> </dsfr-ext-ag-grid>
  ```
L'évènement exposé `gridPreDestroyed` permet de sauvegarder l'état de la grille avant la destruction du composant (par exemple lors d'un changement de page), 
afin de pouvoir le restaurer en le passant en paramètre lors du nouvel affichage de la grid.

🔥 <b> En mode `server`</b> il faut préciser le champ utilisé en tant qu'identifiant unique de la donnée avec <b> `rowId` </b>, ou rédéfinir la fonction `getRowId`.

🔥 <b> En mode `server` rowSelection ne s'appliquera pas </b>. Pour pré-sélectionner des lignes, utiliser par exemple l'api `setNodesSelected` apres le callback de `dataRender` :


```html
  <dsfr-ext-ag-grid  mode="server" (gridReady)="getGridApi($event)" (dataRender)="setSelectedRows()" ..></dsfr-ext-ag-grid>
```
```typescript
  public setSelectedRows(): void {
    this.agGridApi?.forEachNode((n, i) => {
      n.setSelected(true);
    });
  }
```

<Story of={GridStories.InitialState} />


