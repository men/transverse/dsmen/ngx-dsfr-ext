export const AG_GRID_LOCALE_FR = {
  // Set Filter
  selectAll: '(Tout sélectionner)',
  selectAllSearchResults: '(Sélectionner tous les résultats)',
  addCurrentSelectionToFilter: 'Ajouter la sélection courante au filtre',
  searchOoo: 'Rechercher...',
  blanks: '(Blancs)',
  noMatches: 'Aucun résultat',

  // Number Filter & Text Filter
  filterOoo: 'Filtre...',
  equals: 'Egal',
  notEqual: 'Différent de',
  blank: 'Vide',
  notBlank: 'Non vide',
  empty: 'Choisir',

  // Number Filter
  lessThan: 'Inférieur à',
  greaterThan: 'Supérieur à',
  lessThanOrEqual: 'Inférieur ou égal à',
  greaterThanOrEqual: 'Supérieur ou égal à',
  inRange: 'Fourchette',
  inRangeStart: 'entre',
  inRangeEnd: 'et',

  // Text Filter
  contains: 'Contient',
  notContains: 'Ne contient pas',
  startsWith: 'Commence par',
  endsWith: 'Finit par',

  // Date Filter
  dateFormatOoo: 'jj/mm/aaaa',
  before: 'Avant',
  after: 'Après',

  // Filter Conditions
  andCondition: 'ET',
  orCondition: 'OU',

  // Filter Buttons
  applyFilter: 'Appliquer',
  resetFilter: 'Réinitialiser',
  clearFilter: 'Vider',
  cancelFilter: 'Annuler',

  // Filter Titles
  textFilter: 'Filtre textuel',
  numberFilter: 'Filtre numérique',
  dateFilter: 'Filtre de date',
  setFilter: 'Appliquer le filtre',

  // Side Bar
  columns: 'Colonnes',
  filters: 'Filtres',

  // columns tool panel
  pivotMode: 'Mode Pivot',
  groups: 'Groupes de lignes',
  rowGroupColumnsEmptyMessage: 'Faites glisser ici pour définir les groupes de lignes',
  values: 'Valeurs',
  valueColumnsEmptyMessage: 'Faites glisser ici pour aggréger',
  pivots: 'Labels de colonnes',
  pivotColumnsEmptyMessage: 'Faites glisser ici pour définir les labels de colonnes',

  // Header of the Default Group Column
  group: 'Groupe',

  // Row Drag
  rowDragRow: 'ligne',
  rowDragRows: 'lignes',

  // Other
  loadingOoo: 'Chargement...',
  loadingError: 'ERREUR',
  noRowsToShow: 'Aucun résultat',
  enabled: 'Activé',

  // Group Column Filter
  groupFilterSelect: 'Sélectionner un champ:',

  // Menu
  pinColumn: 'Epingler la colonne',
  pinLeft: 'Epingler à gauche',
  pinRight: 'Epingler à droite',
  noPin: "Pas d'épingle",
  valueAggregation: "Valeur d'aggrégation",
  autosizeThiscolumn: 'Adapter la taille de cette colonne',
  autosizeAllColumns: 'Adapter la taille de toutes les colonnes',
  groupBy: 'Grouper par',
  ungroupBy: 'Dégrouper par',
  addToValues: 'Ajouter ${variable} aux valeurs',
  removeFromValues: 'Retirer ${variable} des valeurs',
  addToLabels: 'Ajouter ${variable} aux labels',
  removeFromLabels: 'Retirer ${variable} des labels',
  resetColumns: 'Réinitialiser les colonnes',
  expandAll: 'Développer tout',
  collapseAll: 'Fermer tout',
  copy: 'Copier',
  ctrlC: 'Ctrl + C',
  copyWithHeaders: 'Copier avec les en-têtes',
  copyWithGroupHeaders: 'Copier avec les en-têtes de groupes',
  paste: 'Coller',
  ctrlV: 'Ctrl + V',
  export: 'Exporter',
  csvExport: 'Export en CSV ',
  excelExport: 'Export au format Excel',
  noAggregation: 'Aucune',
  ungroupAll: 'Tout dégrouper',
  ctrlX: 'Ctrl+X',
  cut: 'Couper',
  columnFilter: 'Filtre de colonne',
  columnChooser: 'Choisir la colonne',
  sortAscending: 'Tri ascendant',
  sortDescending: 'Tri descendant',
  sortUnSort: 'Effacer le tri',

  // Enterprise Menu Aggregation and Status Bar
  sum: 'Somme',
  min: 'Min',
  max: 'Max',
  none: 'Aucun',
  count: 'Compte',
  avg: 'Moyenne',
  filteredRows: 'Filtré',
  selectedRows: 'Selectionné',
  totalRows: 'Nombre total de lignes',
  totalAndFilteredRows: 'Lignes',
  more: 'Plus',
  to: 'à',
  of: 'sur',
  page: 'Page',
  nextPage: 'Page suivante',
  lastPage: 'Dernière Page',
  firstPage: 'Premère Page',
  previousPage: 'Page précédente',
  pageSize: 'Nombre de résultats affichés par page',

  // Pivoting
  pivotColumnGroupTotals: 'Total',

  // ARIA
  ariaChecked: 'sélectionné',
  ariaColumn: 'colonne',
  ariaColumnGroup: 'Groupe de colonnes',
  ariaColumnFiltered: 'Colonne Filtrée',
  ariaColumnList: 'Liste de colonnes',
  ariaColumnSelectAll: 'Sélectionner toutes les colonnes',
  ariaDateFilterInput: 'Champ du filtre de date',
  ariaDefaultListName: 'Liste',
  ariaFilterColumnsInput: 'Champ du filtre des colonnes',
  ariaFilterFromValue: 'Filtrer à partir de la valeur',
  ariaFilterInput: '',
  ariaFilterList: 'Filtre de liste',
  ariaFilterToValue: 'Valeur du filtre "de"',
  ariaFilterValue: 'Valeur du filtre',
  ariaFilteringOperator: 'Opérateur de filtre',
  ariaHidden: 'masqué',
  ariaIndeterminate: 'indéterminé',
  ariaInputEditor: 'Editeur',
  ariaMenuColumn: 'Appuyez sur CTRL ENTREE pour ouvrir le menu de colonne.',
  ariaRowDeselect: 'Appuyez sur ESPACE pour déselectionner cette ligne',
  ariaRowSelectAll: 'Appuyez sur ESPACE pour activer la sélection de toutes les lignes',
  ariaRowToggleSelection: 'Appuyez sur ESPACE pour activer la sélection de ligne',
  ariaRowSelect: 'Appuyez sur ESPACE pour sélectionner cette ligne',
  ariaSearch: 'Rechercher',
  ariaSortableColumn: 'Appuyez sur ENTREE pour trier',
  ariaToggleVisibility: 'Appuyez sur ESPACE pour activer la visibilité',
  ariaUnchecked: 'désélectionné',
  ariaVisible: 'visible',
  ariaSearchFilterValues: 'Chercher les valeurs de filtres',
  ariaFilterMenuOpen: 'Ouvrir le menu de filtre',
  ariaFilterColumn: 'Appuyer sur CTRL ENTER pour ouvrir le filtre',
  ariaRowSelectionDisabled: 'La sélection est désactivée pour cette ligne',
  ariaToggleCellValue: 'Appuyez sur ESPACE pour changer la valeur de la cellule',
  ariaPageSizeSelectorLabel: 'Nombre de résultats affichés par page',
  ariaSkeletonCellLoadingFailed: 'Le chargement de la ligne a échoué',
  ariaSkeletonCellLoading: 'Chargement des données en cours',

  // ARIA Labels for Drop Zones

  ariaRowGroupDropZonePanelLabel: 'Groupes de lignes',
  ariaValuesDropZonePanelLabel: 'Valeurs',
  ariaPivotDropZonePanelLabel: 'Labels des colonnes',
  ariaDropZoneColumnComponentDescription: 'Appuyez sur SUPPR pour supprimer',
  ariaDropZoneColumnValueItemDescription: "Appuyez sur ENTREE pour changer le type d'aggregation",
  ariaDropZoneColumnGroupItemDescription: 'Appuyez sur ENTREE pour trier',
  // used for aggregate drop zone, format: {aggregation}{ariaDropZoneColumnComponentAggFuncSeperator}{column name}
  ariaDropZoneColumnComponentAggFuncSeperator: ' de ',
  ariaDropZoneColumnComponentSortAscending: 'croissant',
  ariaDropZoneColumnComponentSortDescending: 'décroissant',

  // ARIA Labels for Dialogs
  ariaLabelColumnMenu: 'Menu des colonnes',
  ariaLabelCellEditor: 'Editeur de cellule',
  ariaLabelDialog: 'Modale',
  ariaLabelSelectField: 'Sélectionner le champ',
  ariaLabelTooltip: 'Info-bulle',
  ariaLabelContextMenu: 'Menu contextuel',
  ariaLabelSubMenu: 'Sous-Menu',
  ariaLabelAggregationFunction: "Fonction d'aggrégation",
  ariaLabelColumnFilter: 'Filtre de colonne',
  // Number Format (Status Bar, Pagination Panel)
  thousandSeparator: ' ',
  decimalSeparator: ',',

  // Data types
  true: 'Vrai',
  false: 'Faux',
  invalidDate: 'Date invalide',
  invalidNumber: 'Nombre invalide',
  january: 'Janvier',
  february: 'Février',
  march: 'Mars',
  april: 'Avril',
  may: 'Mai',
  june: 'Juin',
  july: 'Juillet',
  august: 'Août',
  september: 'Septembre',
  october: 'Octobre',
  november: 'Novembre',
  december: 'Décembre',

  // Time formats
  timeFormatSlashesDDMMYYYY: 'JJ/MM/AAAA',
  timeFormatSlashesMMDDYYYY: 'MM/JJ/AAAA',
  timeFormatSlashesDDMMYY: 'JJ/MM/AA',
  timeFormatSlashesMMDDYY: 'MM/AA/AA',
  timeFormatDotsDDMYY: 'JJ.M.AA',
  timeFormatDotsMDDYY: 'M.JJ.AA',
  timeFormatDashesYYYYMMDD: 'AAAA-MM-JJ',
  timeFormatSpacesDDMMMMYYYY: 'JJ MMMM AAAA',
  timeFormatHHMMSS: 'HH:MM:SS',
  timeFormatHHMMSSAmPm: 'HH:MM:SS AM/PM',
};
