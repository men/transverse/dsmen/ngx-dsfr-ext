// Ordre alphabétique requis
export const MESSAGES_FR = {
  grid: {
    noData: 'Aucune donnée correspondante',
    loading: 'Chargement des données...',
  },
};
