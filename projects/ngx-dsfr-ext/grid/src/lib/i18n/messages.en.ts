// Ordre alphabétique requis
export const MESSAGES_EN = {
  grid: {
    noData: 'No matching data',
    loading: 'Data loading...',
  },
};
