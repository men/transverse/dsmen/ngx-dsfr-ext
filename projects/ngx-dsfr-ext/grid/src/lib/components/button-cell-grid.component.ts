import { Component, ViewEncapsulation } from '@angular/core';
import { DsfrButtonModule, DsfrPosition, DsfrSize } from '@edugouvfr/ngx-dsfr';
import { ICellRendererAngularComp } from 'ag-grid-angular';

@Component({
  selector: 'dsfr-button-cell-grid',
  styles: ['.ag-cell-value .fr-btn { margin: 0 0.25rem; vertical-align: sub }'],
  template: `<dsfr-button
    [id]="id"
    [icon]="icon"
    [label]="label"
    [iconPosition]="iconPosition"
    [ariaLabel]="ariaLabel"
    [customClass]="customClass"
    [tooltipMessage]="tooltipMessage"
    [ariaControls]="ariaControls"
    [size]="size"
    (click)="onClick($event)"
    variant="secondary"></dsfr-button>`,
  standalone: true,
  encapsulation: ViewEncapsulation.None,
  imports: [DsfrButtonModule],
})
export class DsfrButtonCellGridComponent implements ICellRendererAngularComp {
  id: string;
  params: any;
  label!: string;
  size: DsfrSize;
  icon: string;
  iconPosition: DsfrPosition;
  ariaLabel: string;
  customClass: string;
  tooltipMessage: string;
  ariaControls: string;

  agInit(params: any): void {
    this.id = params.id;
    this.params = params;
    this.label = params.label;
    this.size = params.size;
    this.icon = params.icon;
    this.tooltipMessage = params.tooltipMessage;
    this.ariaLabel = params.ariaLabel;
    this.customClass = params.customClass;
    this.iconPosition = params.iconPosition;
    this.ariaControls = params.ariaControls;
  }

  refresh(params?: any): boolean {
    return true;
  }

  /**
   * Permet d'appeler la méthode définie sur le bouton en passant les données de la ligne
   * @param $event Event lié au clic sur le bouton
   */
  onClick($event: any) {
    if (this.params.onClick instanceof Function) {
      const params = {
        event: $event,
        rowData: this.params.node.data,
      };
      this.params.onClick(params);
    }
  }
}
