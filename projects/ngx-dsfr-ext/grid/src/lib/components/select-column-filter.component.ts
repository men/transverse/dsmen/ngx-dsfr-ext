import { Component } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { DsfrFormSelectModule, DsfrOption } from '@edugouvfr/ngx-dsfr';
import { IFloatingFilterAngularComp } from 'ag-grid-angular';
import { IFloatingFilterParams, ISimpleFilter } from 'ag-grid-community';

export interface DsfrFilterSelectParams {
  label: string;
  id: string;
  options: DsfrOption[];
  compareFn?: any;
  placeholder?: string;
}

@Component({
  standalone: true,
  imports: [FormsModule, ReactiveFormsModule, DsfrFormSelectModule],
  template: `<dsfr-form-select
    [id]="params.id"
    [(ngModel)]="currentValue"
    [placeHolder]="params.placeholder ?? ''"
    [options]="options"
    (selectChange)="onSelectChanged()">
    <ng-container label
      ><span class="fr-sr-only"> {{ label }}</span>
    </ng-container>
  </dsfr-form-select>`,
})
export class DsfrSelectFilterComponent<T> implements IFloatingFilterAngularComp {
  params!: IFloatingFilterParams<ISimpleFilter> & DsfrFilterSelectParams;
  currentValue: T | undefined;
  style: any;
  options: DsfrOption[];
  label: string;
  compareFn: any;

  agInit(params: IFloatingFilterParams<ISimpleFilter> & DsfrFilterSelectParams): void {
    this.params = params;
    this.label = params.label;
    this.options = params.options;
  }

  onParentModelChanged(parentModel: any) {
    // When the filter is empty we will receive a null value here
    if (!parentModel) {
      this.currentValue = undefined;
    } else {
      this.currentValue = parentModel.filter;
    }
  }

  onSelectChanged() {
    if (!this.currentValue) {
      // Remove the filter
      this.params.parentFilterInstance((instance: any) => {
        instance.onFloatingFilterChanged(null, null);
      });
      return;
    }

    //this.currentValue = Number(this.currentValue);
    this.params.parentFilterInstance((instance: any) => {
      instance.onFloatingFilterChanged('equals', this.currentValue);
    });
  }
}
