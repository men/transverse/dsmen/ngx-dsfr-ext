import test, { expect, FrameLocator, Locator, Page, Request, Response } from '@playwright/test';
import { getSbFrame, goToComponentPage, testTabNavigation } from 'projects/ngx-dsfr-ext/shared/e2e.utils';

test.describe('Grid', () => {
  test('should handle Tab button navigation accordingly when using interactive elements', async ({ page }) => {
    const frame = getSbFrame(page);
    const ajouterBtn = frame.getByRole('row', { name: 'Pen Brody 28/10/1950 Ajouter' }).getByRole('button');
    const grid = getGrid(frame);
    const firstLineCells = [
      frame.getByRole('gridcell', { name: '1', exact: true }),
      frame.getByRole('gridcell', { name: 'Pen', exact: true }),
      frame.getByRole('gridcell', { name: 'Brody', exact: true }),
      frame.getByRole('gridcell', { name: '28/10/1950', exact: true }),
      frame.getByRole('row', { name: 'Pen Brody 28/10/1950 Ajouter' }).getByRole('gridcell').nth(4),
    ];

    await goToComponentPage('grid', page, 'buttons');
    await firstLineCells[0].click();

    const navigationSequence = [firstLineCells[1], firstLineCells[2], firstLineCells[3], firstLineCells[4], ajouterBtn];

    await testTabNavigation(navigationSequence, grid);
  });

  test.describe('in server side mode', () => {
    test.describe('should fetch data when changing page', () => {
      test('with next and previous page buttons', async ({ page }) => {
        const frame = getSbFrame(page);
        const nextPageBtn = getNextPage(frame);
        const prevPageBtn = getPreviousPage(frame);
        const footer = getFooter(frame);
        const pageOneBtn = getPageOneButton(footer);
        const pageTwoBtn = getPageTwoButton(footer);
        const dataRequestPromise = getRequest(page);

        await goToComponentPage('grid', page, 'server-side');
        await dataRequestPromise;

        const firstPageFirstRowText = await getRows(frame).then((rows) => rows[1].allInnerTexts());

        await nextPageBtn.click();

        await expect(frame.getByText('Chargement...')).toBeVisible();

        await dataRequestPromise;

        const secondPageFirstRowText = await getRows(frame).then((rows) => rows[1].allInnerTexts());

        await expect(frame.getByText('Chargement...')).toBeHidden();
        await expect(pageTwoBtn).toHaveAttribute('aria-current');

        await prevPageBtn.click();

        await expect(frame.getByText('Chargement...')).toBeVisible();

        await dataRequestPromise;

        await expect(frame.getByText('Chargement...')).toBeHidden();
        await expect(pageOneBtn).toHaveAttribute('aria-current');
        expect(firstPageFirstRowText !== secondPageFirstRowText).toBe(true);
      });

      test('with the nunber of rows selector', async ({ page }) => {
        const frame = getSbFrame(page);
        const dataRequestPromise = getRequest(page);
        const rowsNumberSelector = getRowsNumberSelector(frame);

        await goToComponentPage('grid', page, 'server-side');
        await dataRequestPromise;

        await rowsNumberSelector.click();
        await rowsNumberSelector.press('ArrowDown');
        await rowsNumberSelector.press('Enter');

        await expect(frame.getByText('Chargement...')).toBeVisible();

        await dataRequestPromise;

        await expect(frame.getByText('Chargement...')).toBeHidden();

        const rows = await getRows(frame);
        expect(rows.length).toBe(21); // 21 = 20 rows + header row
      });

      test('with the sort order buttons', async ({ page }) => {
        const frame = getSbFrame(page);
        const dataRequestPromise = getRequest(page);
        const sortHeader = getFirstColumnSort(frame);

        await goToComponentPage('grid', page, 'server-side');
        await dataRequestPromise;
        const firstPageFirstRowText = await getRows(frame).then((rows) => rows[1].allInnerTexts());

        await sortHeader.click();

        await expect(frame.getByText('Chargement...')).toBeVisible();

        await dataRequestPromise;
        const secondPageFirstRowText = await getRows(frame).then((rows) => rows[1].allInnerTexts());

        await expect(frame.getByText('Chargement...')).toBeHidden();
        expect(firstPageFirstRowText !== secondPageFirstRowText).toBe(true);
      });
    });
  });
});

/** ACTIONS */
function getRequest(page: Page): Promise<Request> {
  return page.waitForRequest(new RegExp('https://data.education.gouv.fr/*'));
}

/** LOCATORS */
function getGrid(sbFrame: FrameLocator): Locator {
  return sbFrame.locator('dsfr-ext-ag-grid');
}

function getPageTwoButton(footer: Locator) {
  return footer.getByLabel('Page 2', { exact: true });
}

function getPageOneButton(footer: Locator) {
  return footer.getByLabel('Page 1', { exact: true });
}

function getFooter(sbFrame: FrameLocator): Locator {
  return sbFrame.getByTestId('ag-grid-footer');
}

function getNextPage(sbFrame: FrameLocator): Locator {
  const footer = getFooter(sbFrame);
  return footer.locator('.fr-pagination__link--next');
}

function getRowsNumberSelector(sbFrame: FrameLocator): Locator {
  return sbFrame.getByLabel('Nombre de lignes par page');
}

function getPreviousPage(sbFrame: FrameLocator): Locator {
  const footer = getFooter(sbFrame);
  return footer.locator('.fr-pagination__link--prev');
}

function getFirstColumnSort(sbFrame: FrameLocator): Locator {
  return sbFrame.getByRole('columnheader', { name: "Nom d'établissement" });
}

async function getRows(sbFrame: FrameLocator): Promise<Locator[]> {
  return sbFrame.getByRole('row').all();
}
