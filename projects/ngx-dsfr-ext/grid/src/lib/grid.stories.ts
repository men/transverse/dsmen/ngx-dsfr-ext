import { controlEmitter, dsfrDecorators } from '.storybook/storybook-utils';
import { formatDate } from '@angular/common';
import { HttpClient, HttpClientModule, HttpXhrBackend } from '@angular/common/http';
import { FormsModule } from '@angular/forms';
import {
  DsfrButtonModule,
  DsfrButtonsGroupModule,
  DsfrFormSelectModule,
  DsfrPaginationModule,
  DsfrTableFooterComponent,
} from '@edugouvfr/ngx-dsfr';
import { Meta, StoryObj, moduleMetadata } from '@storybook/angular';
import { AgGridModule } from 'ag-grid-angular';
import clientSideData from '../docs/assets/data.json';
import { DsfrButtonCellGridComponent } from './components/button-cell-grid.component';
import { DsfrSelectFilterComponent } from './components/select-column-filter.component';
import { EduRemoteAgGridDirective } from './directives/remote-ag-grid.directive';
import { DsfrGridComponent } from './grid.component';
import { DsfrGridParams } from './interfaces/remote-ag-grid-api';

const meta: Meta = {
  title: 'Components/Grid',
  component: DsfrGridComponent,
  decorators: [
    moduleMetadata({
      declarations: [EduRemoteAgGridDirective],
      imports: [
        HttpClientModule,
        AgGridModule,
        DsfrFormSelectModule,
        DsfrPaginationModule,
        DsfrButtonsGroupModule,
        DsfrButtonModule,
        FormsModule,
        DsfrTableFooterComponent,
      ],
    }),
  ],
  argTypes: {
    ngAfterViewInit: { table: { disable: true } },
    cellEditingStopped: { control: controlEmitter },
    cellSelect: { control: controlEmitter },
    dataChange: { control: controlEmitter },
    dataFail: { control: controlEmitter },
    dataRender: { control: controlEmitter },
    filterChanged: { control: controlEmitter },
    sortChanged: { control: controlEmitter },
    rowData: { control: { type: 'object' } },
    rowSelect: { control: controlEmitter },
  },
};

export default meta;
type Story = StoryObj<DsfrGridComponent>;

const httpClient = new HttpClient(new HttpXhrBackend({ build: () => new XMLHttpRequest() }));
let url =
  'https://data.education.gouv.fr/api/records/1.0/search/?refine.libelle_region=Auvergne-Rhône-Alpes&dataset=fr-en-annuaire-education&timezone=Europe/Berlin&lang=fr';
const template = `<dsfr-ext-ag-grid 
mode="server"
[result]="result" 
[pageSize]="pageSize" 
[rowId]="rowId"
[columnDefs]="columnDefs" 
[autoRowsHeight]="autoRowsHeight"
[customClass]="customClass"
[fixedHeight]="fixedHeight"
[defaultColDef]="defaultColDef"
[initialState]="initialState"
[gridOptions]="gridOptions"
[caption]="caption"
(dataChange)="getDataRemote($event)">

</dsfr-ext-ag-grid>`;

const templateDefault = `<dsfr-ext-ag-grid 
[pageSize]="pageSize" 
[columnDefs]="columnDefs" 
[customClass]="customClass"
[fixedHeight]="fixedHeight"
[caption]="caption"
[initialState]="initialState"
[showExportCsv]="showExportCsv"
[defaultColDef]="defaultColDef"
[rowData]="rowData"

[gridOptions]="gridOptions">
<ng-template #footerActionsTemplate>
      <dsfr-buttons-group inline="always" alignment="right">
      <dsfr-button (click)="addData()" label="Une action "></dsfr-button>
      <dsfr-button variant="secondary" (click)="updateSingleData()" label="Une deuxième action"></dsfr-button>
    </dsfr-buttons-group>
 </ng-template>
</dsfr-ext-ag-grid>`;

const templateClientSide = `<dsfr-ext-ag-grid 
[pageSize]="pageSize" 
[columnDefs]="columnDefs" 
[customClass]="customClass"
[fixedHeight]="fixedHeight"
[caption]="caption"
[initialState]="initialState"
[showExportCsv]="showExportCsv"
[defaultColDef]="defaultColDef"
[rowData]="rowData"
[gridOptions]="gridOptions">
</dsfr-ext-ag-grid>`;

// Client Side ------------------------------------------------------------------------------------------------------------

const propsClientSide = {
  defaultColDef: {
    sortable: true,
    floatingFilter: true,
    filter: true,
    flex: 1,
    autoHeight: true,
  },
  columnDefs: [
    {
      headerName: 'Id',
      field: 'id',
      filter: false,
      sort: 'asc',
      suppressSizeToFit: true,
      minWidth: 150,
      maxWidth: 100,
    },
    {
      headerName: 'Prénom',
      field: 'firstname',
      minWidth: 150,
    },
    { headerName: 'Nom', field: 'name', minWidth: 150, suppressFloatingFilterButton: true },
    {
      headerName: 'Date de naissance',
      field: 'date',
      cellDataType: 'date',
      minWidth: 150,
    },
    {
      headerName: 'Statut',
      field: 'status',
      floatingFilterComponent: DsfrSelectFilterComponent,
      minWidth: 150,
      floatingFilterComponentParams: {
        label: 'Filtrer la colonne statut',
        placeholder: 'Tous',
        options: [
          { label: 'Tous', value: '' },
          { label: 'En attente', value: 'en attente' },
          { label: 'Inscrit', value: 'inscrit' },
          { label: 'Refusé', value: 'refusé' },
        ],
      },
    },
  ],
  rowData: clientSideData as any[],
  gridOptions: {
    dataTypeDefinitions: {
      // override `date` to handle custom date format `dd/mm/yyyy`
      date: {
        baseDataType: 'date',
        extendsDataType: 'date',
        valueParser: (params: any) => {
          return new Date(formatDate(params.newValue, 'dd/MM/yyyy', 'en'));
        },
        valueFormatter: (params: any) => {
          // convert to `dd/mm/yyyy`
          return params.value == null ? '' : formatDate(params.value, 'dd/MM/yyyy', 'en');
        },
      },
    },
  },
  pageSize: 10,
};

export const Default: Story = {
  decorators: dsfrDecorators('Données côté client'),
  render: (args) => ({
    props: {
      ...propsClientSide,
      ...args,
    },
    template: templateDefault,
  }),
  args: {
    showExportCsv: true,
    caption: 'Titre du tableau (accessibilité)',
  },
};

export const Striped: Story = {
  render: (args) => ({
    props: {
      ...propsClientSide,
      ...args,
    },
    template: templateClientSide,
  }),
  args: {
    customClass: 'fr-table--striped',
    caption: 'Tableau',
  },
};

export const Borders: Story = {
  render: (args) => ({
    props: {
      ...propsClientSide,
      ...args,
    },
    template: templateClientSide,
  }),
  args: {
    customClass: 'fr-table-bordered',
    caption: 'Tableau avec bordures',
  },
};

export const FixedHeight: Story = {
  render: (args) => ({
    props: {
      ...propsClientSide,
      ...args,
    },
    template: templateClientSide,
  }),
  args: {
    fixedHeight: '200px',
    caption: 'Tableau avec hauteur fixe définie (200px)',
  },
};

export const Buttons: Story = {
  render: (args) => ({
    props: {
      ...propsClientSide,
      ...args,
    },
    template: templateClientSide,
  }),
  args: {
    columnDefs: [
      {
        headerName: 'Id',
        field: 'id',
        filter: false,
        sort: 'asc',
        suppressSizeToFit: true,
        maxWidth: 100,
        minWidth: 100,
      },
      { headerName: 'Prénom', field: 'firstname', editable: true, minWidth: 300 },
      { headerName: 'Nom', field: 'name', minWidth: 300 },
      {
        headerName: 'Date de naissance',
        field: 'date',
        cellDataType: 'date',
        minWidth: 200,
      },
      {
        filter: false,
        sortable: false,
        cellRenderer: DsfrButtonCellGridComponent,
        maxWidth: 200,
        minWidth: 150,
        cellRendererParams: {
          label: 'Ajouter',
          icon: 'fr-icon-add-circle-line',
          size: 'SM',
          tooltipMessage: 'Ajouter un utilisateur',
          iconPosition: 'left',
          onClick: (params: any) => alert('click on name ' + params?.rowData?.firstname),
        },
        headerName: 'Action',
      },
    ],
    caption: 'Tableau avec composant de bouton (DsfrButtonCellGridComponent)',
  },
};

export const Selection: Story = {
  render: (args) => ({
    props: args,
    template: templateClientSide,
  }),
  args: {
    ...propsClientSide,
    caption: 'Tableau',
    gridOptions: {
      rowSelection: {
        checkboxes: true,
        mode: 'multiRow',
        enableClickSelection: true,
        headerCheckbox: true,
      },
    },
    columnDefs: [
      {
        headerName: 'Id',
        field: 'id',
        filter: false,
        sort: 'asc',
        suppressSizeToFit: true,
        maxWidth: 150,
      },
      { headerName: 'Prénom', field: 'firstname', editable: true },
      { headerName: 'Nom', field: 'name' },
    ],
  },
};

export const InitialState: Story = {
  render: (args) => ({
    props: {
      ...propsClientSide,
      ...args,
    },
    template: templateClientSide,
  }),
  args: {
    caption: 'Avec état initial (page et sélection)',
    initialState: { pagination: { page: 7 }, rowSelection: ['6', '70', '73', '74'] },
    gridOptions: {
      rowSelection: {
        checkboxes: true,
        mode: 'multiRow',
        headerCheckbox: true,
        enableClickSelection: true,
      },
    },
    columnDefs: [
      {
        headerName: 'Id',
        field: 'id',
        filter: false,
        sort: 'asc',
        suppressSizeToFit: true,
        maxWidth: 150,
      },
      { headerName: 'Prénom', field: 'firstname', editable: true },
      { headerName: 'Nom', field: 'name' },
    ],
  },
};

// Server Side ------------------------------------------------------------------------------------------------------------

const getDataRemote = function (queryParams: DsfrGridParams) {
  let params: any = { start: queryParams.offset, rows: queryParams.limit };
  if (queryParams.sort && queryParams.sort[0]) {
    params.sort = queryParams.sort[0].sort === 'asc' ? queryParams.sort[0].colId : '-' + queryParams.sort[0].colId;
  }
  httpClient
    .get<any>(url, {
      params: {
        ...params,
        //@ts-ignore
        q: queryParams?.filters?.nom_etablissement ? queryParams.filters.nom_etablissement.filter : '',
      },
    })
    .subscribe((res: any) => {
      console.log('Set results from API ---------------------------------');
      //@ts-ignore
      this['result'] = {
        data: res.records.map((r: any) => {
          return { ...r.fields, record: r.recordid };
        }),
        totalRecords: res.nhits,
      }; // Mise à jour de la propriété results
      //@ts-ignore
    });
};

const defaultArgs = {
  pageSize: 10,
  columnDefs: [
    { field: 'nom_etablissement', headerName: "Nom d'établissement" },
    { field: 'nombre_d_eleves', headerName: "Nombre d'élèves" },
    { field: 'type_etablissement', headerName: 'Type établissement' },
    { field: 'nom_commune', headerName: 'Nom commune' },
    { field: 'libelle_departement', headerName: 'Libellé département' },
  ],
};

export const ServerSide: Story = {
  render: (args) => ({
    props: {
      ...args,
      getDataRemote: getDataRemote,
    },
    rowId: 'record',
    template: template,
  }),
  args: { ...defaultArgs, caption: 'Filtre externe au tableau' },
};

export const ServerSideWithSelection: Story = {
  render: (args) => ({
    props: {
      ...args,
      getDataRemote: getDataRemote,
    },
    template: template,
  }),
  args: {
    ...defaultArgs,
    caption: 'Données côté serveur et sélection',
    initialState: {
      rowSelection: ['1', '2', '3'],
    },
    rowId: 'record',
    gridOptions: {
      rowSelection: {
        checkboxes: true,
        mode: 'multiRow',
      },
    },
    columnDefs: [
      {
        field: 'nom_etablissement',
        headerName: "Nom d'établissement",
      },
      { field: 'nombre_d_eleves', headerName: "Nombre d'élèves" },
      { field: 'type_etablissement', headerName: 'Type établissement' },
      { field: 'nom_commune', headerName: 'Nom commune' },
      { field: 'libelle_departement', headerName: 'Libellé département' },
    ],
  },
};

export const WithFiltersAndSort: Story = {
  render: (args) => ({
    props: {
      ...args,
      getDataRemote: getDataRemote,
    },
    template: template,
  }),
  args: {
    ...defaultArgs,
    caption: 'Filtres et tri côté serveur',
    rowId: 'record',
    defaultColDef: {
      sortable: true,
      floatingFilter: true,
      suppressHeaderMenuButton: true,
      filter: true,
      flex: 1,
      resizable: true,
    },
    columnDefs: [
      { field: 'nom_etablissement', headerName: "Nom d'établissement", sort: 'asc' },
      { field: 'nombre_d_eleves', headerName: "Nombre d'élèves" },
      { field: 'type_etablissement', headerName: 'Type établissement' },
      { field: 'nom_commune', headerName: 'Nom commune' },
      {
        field: 'date_ouverture',
        headerName: 'Date ouverture',
        valueFormatter: (p: any) => {
          return p.value ? formatDate(p.value, 'dd/MM/yyyy', 'en') : '';
        },
        filter: 'agDateColumnFilter',
      },
    ],
  },
};

export const WithDynamicRowHeight: Story = {
  render: (args) => ({
    props: {
      ...args,
      getDataRemote: getDataRemote,
    },
    template: template,
  }),
  args: {
    ...defaultArgs,
    caption: 'Hauteur de ligne dynamique',
    defaultColDef: {
      sortable: true,
      floatingFilter: true,
      suppressHeaderMenuButton: true,
      filter: true,
      flex: 1,
      resizable: true,
    },
    autoRowsHeight: true,
    rowId: 'record',
    columnDefs: [
      { field: 'nom_etablissement', headerName: "Nom d'établissement", sort: 'asc' },
      { field: 'nombre_d_eleves', headerName: "Nombre d'élèves" },
      { field: 'type_etablissement', headerName: 'Type établissement' },
      { field: 'nom_commune', headerName: 'Nom commune' },
      {
        field: 'date_ouverture',
        headerName: 'Date ouverture',
      },
    ],
  },
};

export const WithInitialState: Story = {
  decorators: dsfrDecorators("Etat initial (page 4 et tri par nombre d'élèves)"),
  render: (args) => ({
    props: {
      ...args,
      getDataRemote: getDataRemote,
    },
    template: template,
  }),
  args: {
    ...defaultArgs,
    gridOptions: {
      rowSelection: {
        checkboxes: true,
        mode: 'multiRow',
      },
    },
    rowId: 'record',
    columnDefs: [
      {
        field: 'nom_etablissement',
        headerName: "Nom d'établissement",
      },
      { field: 'nombre_d_eleves', headerName: "Nombre d'élèves" },
      { field: 'type_etablissement', headerName: 'Type établissement' },
      { field: 'nom_commune', headerName: 'Nom commune' },
      { field: 'libelle_departement', headerName: 'Libellé département' },
    ],
    initialState: {
      pagination: { page: 4 },
      sort: { sortModel: [{ colId: 'nombre_d_eleves', sort: 'asc' }] },
    },
  },
};

/** Filtres externes */
const urlExternalFilter =
  'https://data.education.gouv.fr/api/records/1.0/search/?dataset=fr-en-annuaire-education&timezone=Europe/Berlin&lang=fr';

export const WithExternalFilter: Story = {
  render: (args) => ({
    props: {
      ...args,
      getDataRemote: function (queryParams: DsfrGridParams) {
        const params: any = { start: queryParams.offset, rows: queryParams.limit };
        if (queryParams.sort && queryParams.sort[0]) {
          params.sort =
            queryParams.sort[0].sort === 'asc' ? queryParams.sort[0].colId : '-' + queryParams.sort[0].colId;
        }
        httpClient
          .get<any>(urlExternalFilter, {
            params: { ...params, q: 'Collège Maria Sa' },
          })
          .subscribe((res: any) => {
            this['result'] = {
              data: res.records.map((r: any) => {
                return {
                  ...r.fields,
                  record: r.recordid,
                };
              }),
              totalRecords: res.nhits,
            }; // Mise à jour de la propriété results
          });
      },
    },
    template: template,
  }),
  args: {
    ...defaultArgs,
    caption: 'Tableau avec filtres externes',
    rowId: 'record',
  },
};

export const WithNoResults: Story = {
  render: (args) => ({
    props: {
      ...args,
      getDataRemote: function (queryParams: DsfrGridParams) {
        this['result'] = { data: [], totalRecords: 0 };
      },
    },
    template: template,
  }),
  args: {
    ...defaultArgs,
    caption: 'Tableau sans résultat',
  },
};
