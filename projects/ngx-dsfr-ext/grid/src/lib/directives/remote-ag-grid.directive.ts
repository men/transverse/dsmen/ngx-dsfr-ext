import { Directive, EventEmitter, HostListener, NgZone, Output } from '@angular/core';
import { AgGridAngular } from 'ag-grid-angular';
import { IDatasource, IGetRowsParams } from 'ag-grid-community';
import { EMPTY } from 'rxjs';
import { DsfrRemoteGridResult, EduRemoteAgGridApi } from '../interfaces/remote-ag-grid-api';

/**
 * Directive pour l'implémentation de la pagination coté backend de Ag-grid community
 * cf: https://medium.com/weekly-webtips/tech-building-reusable-server-side-pagination-for-ag-grid-in-angular-c8127e2da346
 */
@Directive({
  selector: '[remoteAgGridBinding]',
})
export class EduRemoteAgGridDirective {
  @Output()
  remoteGridReady = new EventEmitter();

  // DOC: https://www.ag-grid.com/javascript-data-grid/infinite-scrolling/#datasource-interface
  dataSource: IDatasource = {
    getRows: (params: IGetRowsParams) => {
      this.zone.run(() => {
        // force event to run inside angular zone for detection (#fix 31.0.2)
        this.host.prepareUpdateData(params);
      });
      this.host.result$.subscribe({
        next: (e: DsfrRemoteGridResult) => {
          params.successCallback(e.data, e.totalRecords);
        },
        error: (error) => {
          this.handleError(error);
        },
      });
    },
  };

  constructor(
    private host: EduRemoteAgGridApi,
    private zone: NgZone,
  ) {}

  // https://www.ag-grid.com/angular-data-grid/grid-events/
  /**
   * Initialisation grid
   * @param grid
   */
  @HostListener('gridReady', ['$event'])
  gridReady(grid: AgGridAngular) {
    grid.api.setGridOption('datasource', this.dataSource);
    this.remoteGridReady.emit(grid);
  }

  handleError(err: any) {
    this.host.getDataError && this.host.getDataError(err);
    return EMPTY;
  }
}
