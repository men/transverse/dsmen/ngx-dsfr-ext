import {
  ChangeDetectionStrategy,
  Component,
  ContentChild,
  ElementRef,
  EventEmitter,
  Input,
  OnInit,
  Output,
  TemplateRef,
  ViewChild,
  ViewEncapsulation,
  forwardRef,
} from '@angular/core';
import { DsfrI18nService, DsfrOption, DsfrTableState, Language, newUniqueId } from '@edugouvfr/ngx-dsfr';
import { AgGridAngular } from 'ag-grid-angular';
import {
  CellClickedEvent,
  CellEditingStoppedEvent,
  ColDef,
  FilterChangedEvent,
  GetRowIdParams,
  GridApi,
  GridOptions,
  GridPreDestroyedEvent,
  GridReadyEvent,
  GridState,
  IGetRowsParams,
  IRowNode,
  RowSelectedEvent,
  SelectionChangedEvent,
  SortChangedEvent,
  SortModelItem,
  SuppressHeaderKeyboardEventParams,
  SuppressKeyboardEventParams,
} from 'ag-grid-community';
import { Observable, ReplaySubject, Subject } from 'rxjs';
import { AG_GRID_LOCALE_FR } from './i18n/grid.fr';
import { MESSAGES_EN } from './i18n/messages.en';
import { MESSAGES_FR } from './i18n/messages.fr';
import {
  DsfrGridMode,
  DsfrGridModeConst,
  DsfrGridParams,
  DsfrRemoteGridResult,
  EduRemoteAgGridApi,
} from './interfaces/remote-ag-grid-api';

/** Représentation d'un évènement selectionChange ou rowSelect */
export interface DsfrGridSelectionEvent {
  /** type d'évènement ag-grid */
  event: RowSelectedEvent<any> | SelectionChangedEvent<any>;
  /** liste des noeuds ag-grid IRowNode */
  selectedNodes: IRowNode<any>[];
}

@Component({
  selector: 'dsfr-ext-ag-grid, dsfrx-ag-grid',
  templateUrl: './grid.component.html',
  providers: [{ provide: EduRemoteAgGridApi, useExisting: forwardRef(() => DsfrGridComponent) }],
  styleUrls: ['./styles/grid.component.scss'],
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class DsfrGridComponent implements EduRemoteAgGridApi, OnInit {
  /** @internal **/
  @ViewChild('grid', { read: ElementRef }) grid: ElementRef;

  /** Template personnalisé pour afficher le total de lignes */
  @ContentChild('footerResultsTemplate', { static: true }) footerResultsTemplate: TemplateRef<any>;

  /** Template pour les groupes de boutons d'actions du footer */
  @ContentChild('footerActionsTemplate', { static: true }) footerActionsTemplate: TemplateRef<any>;

  /** Id du composant, généré par défaut */
  @Input() gridId: string;

  /* Liste des colonnes au format agGrid ColDef[] */
  @Input() columnDefs: ColDef[];

  /* Titre du tableau */
  @Input() caption: string;

  /** Nombre d'éléments affichés par page */
  @Input() pageSize = 20;

  /** Traductions des éléments de agGrid. Français par défaut, anglais si valeur nulle. */
  @Input() localeText: any;

  /** Mode de la grid: données côté client ou serveur (défaut client) */
  @Input() mode: DsfrGridMode = DsfrGridModeConst.CLIENT;

  /** Données locale dans le cas de clientSide */
  @Input() rowData: any[];

  /** Options des colonnes par défaut (cf. https://www.ag-grid.com/angular-data-grid/column-properties/ ) */
  @Input() defaultColDef: ColDef = {
    suppressHeaderMenuButton: true,
    sortable: true,
    filter: false,
    resizable: false,
    filterParams: {
      maxNumConditions: 1,
    },
  };

  /** Etat initial du tableau (ex. page courante, filtres..) */
  @Input() initialState: GridState;

  /** Classes personnalisées pour la grid. Prédéfinies : "fr-table-bordered", "fr-table-no-strip".  */
  @Input() customClass: string;

  /** Afficher un texte par-dessus la grid pendant le chargement */
  @Input() showLoadingOverlay: boolean = true;

  /** Redéfinir le template de chargement */
  //FIXME: nommage non consistant, à déprécier au profit de `overlayLoadingTemplate`
  @Input() loadingOverlayTemplate: string | undefined = undefined;

  /** Redéfinir le template aucun résultat. /!\ L'id de ce container doit être passé à idNoRowsTemplate */
  @Input() overlayNoRowsTemplate: string | undefined = undefined;

  /** Id du template overlayNoRowsTemplate contenant le texte utilisé pour le aria-describedby */
  @Input() idNoRowsTemplate = 'no-data-container';

  /** Hauteur fixe (scroll vertical si nécessaire) */
  @Input() fixedHeight: string;

  /** Calcul dynamique de la hauteur des lignes pour afficher le contenu des cellules sur plusieurs lignes (uniquement utilisé en serverSide).
   * Déconseillé d'utiliser si le nombre de lignes par pages est élevé, peut causer des problèmes de performance d'affichage.
   */
  @Input() autoRowsHeight: boolean = false;

  /** Surcharge d'options supplémentaires pour agGrid (optionnel) */
  @Input() gridOptions: GridOptions<any>;

  /** Afficher le bouton d'action exporter au format CSV */
  @Input() showExportCsv = false;

  /** Champ qui correspond à l'identifiant de la donnée (optionnel) */
  @Input() rowId: string;

  /** Afficher la pagination */
  @Input() showPagination: boolean = true;

  /** Afficher le total de lignes */
  @Input() showFooterResult: boolean = true;

  /** Mise à jour et application du tri des colonnes */
  @Output() sortChanged: EventEmitter<SortChangedEvent<any>> = new EventEmitter();

  /** Callback apres l'initialisation de la grid, retourne l'accès à l'API de agGrid */
  @Output() gridReady: EventEmitter<GridApi<any>> = new EventEmitter();

  /** Callback avant la destruction de la grid (destruction du composant), permet de récuperer le state */
  @Output() gridPreDestroyed: EventEmitter<GridPreDestroyedEvent<any>> = new EventEmitter();

  /** Mise à jour et application des filtres */
  @Output() filterChanged: EventEmitter<FilterChangedEvent<any>> = new EventEmitter();

  /** Sélection d'une cellule du tableau */
  @Output() cellSelect: EventEmitter<CellClickedEvent<any>> = new EventEmitter();

  /** Sélection ou Déselection d'une ligne du tableau. Emission de l'évènement (RowSelectedEvent) et des noeuds sélectionnés (IRowNode[]) */
  @Output() rowSelect: EventEmitter<DsfrGridSelectionEvent> = new EventEmitter();

  /** La sélection des lignes a changé. Emission de l'évènement (SelectionChangedEvent) et des noeuds sélectionnés (IRowNode[]) */
  @Output() selectionChange: EventEmitter<DsfrGridSelectionEvent> = new EventEmitter();

  /** Indique une mise à jour nécessaire des données suite à un changement d'affichage (pagination, tri ou filtres). Si données côté serveur. */
  @Output() dataChange: EventEmitter<DsfrGridParams> = new EventEmitter();

  /** Callback après chaque changement des lignes affichées (modelUpdated) */
  @Output() dataRender: EventEmitter<void> = new EventEmitter();

  /** Callback après appel à l'api en cas d'erreur de celle-ci */
  @Output() dataFail: EventEmitter<any> = new EventEmitter();

  /** Evenement de fin d'édition d'une cellule */
  @Output() cellEditingStopped: EventEmitter<CellEditingStoppedEvent<any>> = new EventEmitter();

  /** @internal Pour acceder a Grid API */
  @ViewChild(AgGridAngular) agGrid!: AgGridAngular;

  /** Pagination */

  /** Choix de taille de page (par défaut 10, 20, 50, 100). Pas d'affichage du select si non défini */
  @Input()
  pageSizes: DsfrOption[] = [
    {
      value: 10,
      label: '10 lignes par page',
    },
    {
      value: 20,
      label: '20 lignes par page',
    },
    {
      value: 50,
      label: '50 lignes par page',
    },
    {
      value: 100,
      label: '100 lignes par page',
    },
  ];

  /** @internal Total résultats*/
  totalElements = 0;
  /** @internal  Page courante*/
  currentPage: number | undefined = undefined;
  /** @internal Total pages */
  totalPage = 0;
  /**@internal Id généré du titre du tableau */
  captionId: string;
  /** @internal Index de la première ligne affichée */
  startRow: number = 1;
  /** @internal Index de la première ligne affichée */
  endRow: number = 1;
  /** @internal */
  isLoadingServerSide = false;
  /** @internal en cas d'initalState avec pagination en mode serveur */
  initialPagination = 0;

  protected initialStatePagination: DsfrTableState; // Pour compatibilité avec le composant footer de la lib ngx-dsfr

  protected loading = false;

  protected selectable = false;

  /** @internal */
  private resultSubject$: Subject<DsfrRemoteGridResult> = new ReplaySubject<DsfrRemoteGridResult>(1);

  constructor(private i18n: DsfrI18nService) {
    this.gridId ??= newUniqueId();
    this.i18n.extendsLabelsBundle(Language.EN, MESSAGES_EN);
    this.i18n.extendsLabelsBundle(Language.FR, MESSAGES_FR);
    this.i18n.completeLangChange$.subscribe((code: string) => this.onLangChange(code));
  }

  /** @internal */
  public get result$(): Observable<DsfrRemoteGridResult> {
    return this.resultSubject$.asObservable();
  }

  /** Données renvoyées par l'API */
  @Input() set result(value: DsfrRemoteGridResult) {
    if (value && this.agGrid?.api) {
      this.totalElements = value.totalRecords;
      this.totalPage = this.calculateTotalPage();
      this.resultSubject$.next(value);
      if (this.autoRowsHeight) {
        this.setHeightOnRows();
      }

      this.isLoadingServerSide = false;

      this.forceReloadOnNoData();
      this.loading = false;

      this.agGrid.api.sizeColumnsToFit();

      if (this.totalElements === 0) {
        this.agGrid.api.showNoRowsOverlay();
      }
    }
  }

  public ngOnInit(): void {
    this.captionId = 'caption-' + newUniqueId();
    this.defaultColDef = {
      wrapText: this.autoRowsHeight ? true : false,
      filterParams: { maxNumConditions: 1, debounceMs: 1000 },
      suppressFloatingFilterButton: true,
      suppressHeaderMenuButton: true,
      resizable: false,
      ...this.defaultColDef,
    };

    // gestion de la navigation au clavier par défaut
    if (this.defaultColDef && !this.defaultColDef?.suppressKeyboardEvent) this.suppressCellKeyboardEvent();
    if (this.defaultColDef && !this.defaultColDef?.suppressHeaderKeyboardEvent) this.suppressHeaderKeyboardEvent();
    this.gridOptions = {
      rowModelType: 'infinite',
      pagination: this.showPagination,
      suppressContextMenu: true,
      paginationPageSize: this.pageSize,
      maxBlocksInCache: 1,
      domLayout: this.fixedHeight ? 'normal' : 'autoHeight',
      enableCellTextSelection: true,
      suppressPaginationPanel: true,
      ensureDomOrder: true,
      rowBuffer: 20,
      unSortIcon: true,
      icons: {
        filter: '<i class="fr-icon-filter-line"></i>',
        sortAscending: '<i class="fr-icon-arrow-down-line"></i>',
        sortDescending: '<i class="fr-icon-arrow-up-line"></i>',
        sortUnSort: '<i class="fr-icon-arrow-up-down-line"></i> ',
      },
      onGridPreDestroyed: ($event) => this.onGridPreDestroyed($event),
      ...this.gridOptions,
      getRowId: this.rowId ? (rowParams) => this.getRowNodeId(rowParams) : undefined,
    };

    this.selectable = !!this.gridOptions.rowSelection;

    if (this.mode === DsfrGridModeConst.CLIENT) {
      this.gridOptions.rowModelType = 'clientSide';
    } else {
      this.gridOptions.cacheBlockSize = this.pageSize;
      this.gridOptions.paginationPageSize = this.pageSize;
      this.isLoadingServerSide = true;
      this.initialPagination = this.initialState?.pagination?.page ? this.initialState.pagination.page : 0;

      this.initialStatePagination = {
        rowsPerPage: this.pageSize,
        page: this.initialPagination === 0 ? 1 : this.initialPagination, // l'indexation de la pagination doit commencer à 1 ici
        sort: null,
      };
    }
  }

  /**
   * Au changement de paramètres des lignes affichées (pagination, filtre etc.), préparation des query params et envoi de l'evenement updateData
   * @param params IGetRowsParams paramètres modifiés
   * @internal
   */
  public prepareUpdateData(params: IGetRowsParams): void {
    if (this.showLoadingOverlay) this.loading = true;

    // Paramètres de la pagination
    if (!this.currentPage && this.initialState) {
      // si c'est le premier chargement du tableau et initialState renseigné, on remet les paramètres
      this.restoreInitialStateServer(params);
    }

    this.currentPage = Math.floor(params.startRow / this.pageSize) + 1;
    const httpParams: DsfrGridParams = { limit: this.pageSize, offset: params.startRow };

    // Ajout du tri
    if (params.sortModel && params.sortModel[0]) {
      this.addSort(params.sortModel, httpParams);
    }

    //Ajout des filtres
    if (params.filterModel) {
      this.addFilters(params.filterModel, httpParams);
    }

    this.dataChange.emit(httpParams);
  }

  /**
   * Erreur au retour de l'appel à l'api
   * @internal
   */
  public getDataError(err: any) {
    this.loading = false;
    this.agGrid.api.showNoRowsOverlay();
    this.dataFail.emit(err);
  }

  /**
   * Modification et application du tri
   * @internal
   */
  public onSortChange(e: any): void {
    this.sortChanged.emit(e);
  }

  /**
   * Modification et application des filtres
   * @internal
   */
  public onFilterChange(e: any): void {
    this.filterChanged.emit(e);
  }

  /**@internal */
  public onCellEditingStop(e: any) {
    this.cellEditingStopped.emit(e);
  }

  /**
   * Après mise à jour des données
   * @internal
   */
  public onModelUpdated(e: any) {
    if (this.mode === DsfrGridModeConst.CLIENT) {
      this.loading = false;
      this.updatePagination();
      if (this.totalElements === 0) {
        this.agGrid.api.showNoRowsOverlay();
      }
    }
    // this.updateFooterPagination();
    this.dataRender.emit();
  }

  /**
   * Au redimensionnement de la fenêtre
   * @internal
   */
  public onGridSizeChanged(e: any) {
    if (this.autoRowsHeight && this.mode !== DsfrGridModeConst.CLIENT) {
      // recalculer la hauteur des lignes
      this.setHeightOnRows();
    }
    // this.agGrid.api.sizeColumnsToFit(); disable  automatic resize for perfs
  }

  /**
   * Au redimensionnement d'une colonne
   * @internal
   */
  public onColumnResized() {
    if (this.autoRowsHeight && this.mode !== DsfrGridModeConst.CLIENT) {
      // recalculer la hauteur des lignes
      this.setHeightOnRows();
    }
  }
  /**
   * Premier affichage des données
   * /!\ bug connu ag-grid, firstDataRendered on infite mode is fired before data is rendered (AG-8812)
   * @internal
   */
  public onFirstDataRendered() {
    this.agGrid.api.sizeColumnsToFit();
  }

  /**
   * Initialisation de la grid
   * @param params
   * @internal
   */
  public onGridReady(params: GridReadyEvent): void {
    this.agGrid.api = params.api;
    if (this.caption) {
      this.agGrid.api.setGridAriaProperty('labelledby', this.captionId);
    }

    this.gridReady.emit(this.agGrid.api);
  }

  /**
   * Evenement clic sur une cellule
   * @internal
   */
  public onCellClicked(e: CellClickedEvent<any>): void {
    this.cellSelect.emit(e);
  }

  /**
   * Evenement à la sélection ou déselection d'une ligne
   * @internal
   */
  public onRowSelected(e: RowSelectedEvent<any>): void {
    this.rowSelect.emit({ event: e, selectedNodes: this.agGrid.api?.getSelectedNodes() });
  }

  /**
   * Evenement au changement de sélection
   * @internal
   */
  public onSelectionChanged(e: SelectionChangedEvent<any>): void {
    this.selectionChange.emit({ event: e, selectedNodes: this.agGrid.api?.getSelectedNodes() });
  }

  /**
   * Changement de page
   * @param e numéro de page
   * @internal
   */
  public onPage(e: number): void {
    this.agGrid.api.paginationGoToPage(e - 1);

    if (this.mode === DsfrGridModeConst.CLIENT) {
      this.currentPage = e;
    } else if (this.agGrid.api.paginationGetCurrentPage() === 0 && this.initialPagination > 1) {
      this.initialPagination = 0;
      this.agGrid.api.refreshInfiniteCache();
    }

    this.updateFooterPagination();
  }

  /**
   * @internal
   */
  public onClickExportCsv(): void {
    this.agGrid.api.exportDataAsCsv();
  }

  /**
   * @internal
   */
  public onChangePageSize(pageSize: number): void {
    this.pageSize = pageSize;
    this.agGrid.api.updateGridOptions({
      paginationPageSize: this.pageSize,
      cacheBlockSize: this.pageSize,
    });

    this.updateFooterPagination();
  }

  /**
   * @internal
   */
  public onGridPreDestroyed(e: GridPreDestroyedEvent) {
    if (e?.state?.pagination?.page) {
      // fix current page number base 1
      e.state.pagination.page = this.currentPage;
    }

    this.gridPreDestroyed.emit(e);
  }

  private updateFooterPagination(): void {
    this.startRow = this.agGrid.api.getFirstDisplayedRowIndex() + 1;
    this.endRow = this.agGrid.api.getLastDisplayedRowIndex() + 1;
    this.updatePagination();
  }

  private updatePagination(): void {
    this.totalElements = this.agGrid.api.paginationGetRowCount();
    this.totalPage = this.agGrid.api.paginationGetTotalPages();
    this.currentPage = this.agGrid.api.paginationGetCurrentPage() + 1;
    if (this.totalElements === 0) {
      this.agGrid.api.setGridAriaProperty('describedby', this.idNoRowsTemplate);
    } else {
      this.agGrid.api.setGridAriaProperty('describedby', null);
    }
  }

  private calculateTotalPage(): number {
    return Math.ceil(this.totalElements / this.pageSize);
  }

  /* Retourne un identifiant unique selon la propriété définie par rowId */
  private getRowNodeId(row: GetRowIdParams<any>) {
    if (!row.data || !row.data[this.rowId]) {
      throw Error(`L'identifiant rowId '${this.rowId}' n'est pas défini pour une donnée`);
    }

    return row.data[this.rowId];
  }

  /** Ajout du tri aux query params */
  private addSort(sortModel: SortModelItem[], httpParams: any): any {
    httpParams.sort = sortModel;

    return httpParams;
  }

  /**
   * Ajout des filtres aux query params
   * @internal */
  private addFilters(filterModel: any, httpParams: any): any {
    httpParams.filters = filterModel;
  }

  /**
   * Remettre manuellement les paramètres de query en cas d'initialState
   * @param params query params
   */
  private restoreInitialStateServer(params: IGetRowsParams) {
    if (this.initialState.pagination?.page) {
      params.startRow = (this.initialState.pagination.page - 1) * this.pageSize;
    }
    if (this.initialState.filter?.filterModel) {
      params.filterModel = this.initialState.filter.filterModel;
    }
    if (this.initialState.sort?.sortModel) {
      params.sortModel = this.initialState.sort.sortModel;
    }
  }

  /**
   * Détection du changement de langue depuis le service i18n (display)
   * @param code
   */
  private onLangChange(code: string) {
    // si la langue n'est pas gérée (ni anglais ni francais), pas de traitement
    if (this.localeText && this.localeText !== 'fr' && this.localeText !== 'en') return;
    // sinon mis a jour en français ou anglais
    this.localeText = code === 'fr' ? AG_GRID_LOCALE_FR : undefined;
  }

  /**
   * Permettre d'afficher sur plusieurs lignes en mode serveur (equivalent autoHeight en mode client)
   * Calculer la dimension de chaque row par rapport à son contenu et recalculer la taille de la grille
   * globale (limitation ag-grid sur le mode infinite)
   */
  private setHeightOnRows() {
    if (!this.grid || !this.grid.nativeElement) return;

    let gridHeight = 0;
    const rows = this.grid.nativeElement.getElementsByClassName('ag-row');
    this.agGrid.api.forEachNode((node, i) => {
      if (rows && rows[i]) {
        (rows[i] as HTMLElement).style.setProperty('transform', 'translateY(' + gridHeight + 'px)');
        const nodesCellValue = (rows[i] as HTMLElement).getElementsByClassName('ag-cell-value');
        // set new row height
        if (nodesCellValue) {
          let rowHeight = this.getMaxHeightOnRow(nodesCellValue) + 10;
          node.setRowHeight(rowHeight, false);
          node.rowTop = gridHeight;
          // increment grid height
          gridHeight += rowHeight;
        }
      }
    });
    // update grid height
    let elements = this.grid.nativeElement.getElementsByClassName('ag-center-cols-viewport');
    if (elements && elements[0]) {
      (elements[0] as HTMLElement).style.height = `${gridHeight}px`;
    }
  }

  /**
   * Get max cell height needed on row
   * @param nodesCell each cells on row
   * @returns max height
   */
  private getMaxHeightOnRow(nodesCell: HTMLCollectionOf<Element>): number {
    const maxHeight = Array.from(nodesCell)?.reduce((a, b) => (a.clientHeight > b.clientHeight ? a : b)).clientHeight;
    // set min height of 30px if row height is not defined
    return maxHeight && maxHeight > 31 ? maxHeight : 31;
  }

  /**
   * Redéfinir le focus pour les cellules possédant des éléments interactifs (focusables, ex. buttons)
   * Suppression de la gestion du focus par défaut de ag grid lorsque c'est le cas
   */
  private suppressCellKeyboardEvent() {
    this.defaultColDef.suppressKeyboardEvent = (params: SuppressKeyboardEventParams) => {
      const e: KeyboardEvent = params.event;

      return this.suppressDefaultKeyboardEvent(e, '.ag-cell');
    };
  }

  /**
   * Redéfinir le focus pour les cellules d'en-tête possédant des floatings filters avec éléments interactifs (focusables, ex. buttons)
   * Suppression de la gestion du focus par défaut de ag grid lorsque c'est le cas
   */
  private suppressHeaderKeyboardEvent() {
    this.defaultColDef.suppressHeaderKeyboardEvent = (params: SuppressHeaderKeyboardEventParams) => {
      const e: KeyboardEvent = params.event;
      return this.suppressDefaultKeyboardEvent(e, '.ag-floating-cell');
    };
  }

  private suppressDefaultKeyboardEvent(e: KeyboardEvent, classToSearch: string): boolean {
    if (e.code == 'Tab' || e.key == 'Tab') {
      // get interactive children of cell
      const target = e.target as HTMLElement;
      const focusableChildrenOfParent = target
        ?.closest(classToSearch)
        ?.querySelectorAll(
          'button, [href], :not(.ag-hidden) > input[tabindex]:not([tabindex="-1"], select, textarea, [tabindex]:not([tabindex="-1"])',
        );
      if (
        !focusableChildrenOfParent ||
        focusableChildrenOfParent.length == 0 ||
        (!e.shiftKey && target == focusableChildrenOfParent[focusableChildrenOfParent.length - 1]) ||
        (e.shiftKey && target == focusableChildrenOfParent[0]) ||
        (e.shiftKey && target.classList.contains('ag-cell'))
      ) {
        return false; // if no interactive elements are present do not suppress
      }
      return true; // else suppress
    }
    return false; // do not suppress by default
  }

  /* 
    Fix to force reload of ag-Grid data when no data is previously displayed (e.g., after applying a filter).
    The modelUpdated callback is not called the first time, and cacheBlockState remains in "no data" mode.
    ag-Grid bug on infite mode ? Inconvenience: Data is loaded twice in this case.
  */
  private forceReloadOnNoData(): void {
    const cacheBlock: any = this.agGrid.api.getCacheBlockState();

    if ((!cacheBlock || Object.keys(cacheBlock).length === 0) && this.totalElements > 0) {
      this.agGrid.api.refreshInfiniteCache();
    }
  }
}
