import { GridApi, IGetRowsParams, SortModelItem } from 'ag-grid-community';
import { Observable } from 'rxjs';

export type DsfrGridMode = 'client' | 'server';

export namespace DsfrGridModeConst {
  export const CLIENT = 'client';
  export const SERVER = 'server';
}

/** Données retournées par la grid */
export interface DsfrRemoteGridResult {
  data: any[];
  totalRecords: number;
}

/** Mapper le modèle de l'API par rapport aux query params utilisés par la grid  */
export interface DsfrGridParams {
  /** nombre de résultat à retourner (taille de la page) */
  limit: number;
  /**  Index du premier résultat à afficher */
  offset: number;
  /** Tri */
  sort?: SortModelItem[];
  /** Filtres */
  filters?: any[];
}

/** Mapper les résultats de la grid par rapport au résultat retournés par l'API */
export interface DsfrGridResultModel {
  /** données */
  data: string;
  /** total de résultats */
  totalRecords: string;
  /** fonction de mapping optionnelle des données */
  mappingFunction?: (e: any) => void;
}

/**
 * Datasource pour implémentation de la pagination coté backend de Ag-grid community
 * cf: https://medium.com/weekly-webtips/tech-building-reusable-server-side-pagination-for-ag-grid-in-angular-c8127e2da346
 * et https://www.ag-grid.com/javascript-data-grid/infinite-scrolling/
 *  */
export abstract class EduRemoteAgGridApi {
  getDataError?: (err: any) => void;
  gridApi?: GridApi | undefined;
  prepareUpdateData: (params: IGetRowsParams) => void;
  result$: Observable<DsfrRemoteGridResult>;
}
