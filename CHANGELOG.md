# @edugouvfr/ngx-dsfr-ext - Changelog

## 1.1.2 (2025-02-13)

- fix(autocomplete): impossible de réassigner une valeur nulle à l'input
- fix(autocomplete): en cas de `requireSelection` la valeur doit être réinitialisée uniquement après interaction clavier de l'utilisateur
- fix(datepicker): les dates ne sont pas formatés DD/MM/YYYY quand le composant reçoit une valeur initial en ISO 8601
- fix(multiselect/treeselect): affichage à tort d'un scroll horizontal avec `appendTo='body'`
- fix(multiselect/treeselect): correction du rafraîchissement en reactive form pour `enable()` et `disable()`
- fix(multiselect/treeselect): affichage d'un warning à tort sur l'absence de traduction du placeHolder
- fix(toast): la nouvelle sévérité 'valid' n'est pas gérée par le composant
- fix(toast): absence de notification sur la fermeture des toasts
- fix(treeselect): div résiduelle en l'absence de `showSearch` et `selectAll`
- fix(treeselect): erreur à l'initialisation si aucune option n'est présente
- fix(treeselect): comportement incohérent entre l'utilisation du filtre et le bouton tout sélectionner

## 1.1.1 (2025-01-30)

- fix(datepicker): le type `DsfrDateEvent` n'est pas exporté
- fix(treeselect): mauvaise propagation des états des checkboxes des noeuds parents en cas de filtre

## 1.1.0 (2025-01-23)

- break(chore): version minimale requise angular 17
- break(chore): version minimale requise ngx-dsfr 1.13
- feat(chore): utilisation du pipe `DsfrI18n` pour améliorer la gestion de la traduction
- feat(dropdownmenu): ajout d'un nouveau composant menu déroulant
- feat(tree): ajout de la méthode `getSelection` récupérant la sélection courante
- feat(tree): ajout de la méthode `getTreeState` récupérant l'état courant de l'arbre
- feat(tree/treeselect): ajout de l'attribut `hidden` pour un filtre externe des nœuds
- fix(autocomplete): la valeur initiale n'est pas positionnée
- fix(multiselect): la valeur initiale n'est pas positionnée en mode `single`
- fix(panel): utilisé à travers une modale, les sous-composants du panel persistent à la fermeture de la modale
- fix(tree): l'évènement `selectionChange` se déclenche plusieurs fois pour un même évènement
- fix(tree): en mode checkbox, incohérence de l'état de sélection des noeuds parents à l'initialisation

## 1.0.2 (2025-01-03)

- fix(datepicker): le changement de valeur de l'input n'est pas pris en compte
- fix(datepicker): correction des messages d'erreur pour les dates minimales ou maximales
- fix(multiselect): amélioration du comportement responsive avec `selectAll` combiné au filtre
- fix(multiselect/treeselect): correction du rafraîchissement pour une meilleure prise en charge des reactive forms (`setValue`/`reset`)
- fix(tree): `selectionChange` n'est pas émis sur l'évènement tout sélectionner / tout déselectionner

## 1.0.1 (2024-12-18)

- fix(toast): suppression de l'import de `BrowserAnimationsModule` au niveau du composant
- fix(tree): régression du comportement à la navigation clavier pour la touche `tab`
- fix(tree): émission de l'évènement `selectionChange` pour émettre la liste des nœuds couramment sélectionnés
- fix(tree/treeselect): en mode `single` ou sans sélection, le clic sur un nœud parent doit l'étendre/le réduire

## 1.0.0 (2024-12-16)

- break: version minimale requise ngx-dsfr 1.12.8
- break(autocomplete/multiselect/treeselect): suppression de l'attribut déprécié depuis la 0.7 `appendToBody`, utiliser `appendTo`
- break(multiselect/treeselect): suppression de l'attribut déprécié depuis la 0.6 `showCheckboxes`, utiliser `selectionMode`
- break(multiselect/treeselect): suppression de `showToolBar`, utiliser `showSearch` ou `showSelectAll`
- break(multiselect/treeselect): pas d'affichage du bouton de réinitialisation du filtre de recherche, utiliser `showClearSearch`
- break(progressbar): suppression de l'attribut `mode`, utiliser la valeur `null` pour lancer le mode "indéterminé"
- break(tree/treeselect): les options fournies sont désormais immutables et les composants en `OnPush`. Chaque nœud doit avoir un `id` défini si on utilise le `compareWith` par défaut ou si on souhaite mettre à jour le contenu de l'arbre.
- break(toast): necessité de mettre en place un `dsfr-ext-toast` à la racine de l'application
- feat(calendar): ajout du composant calendrier et de sa déclinaison en modal
- feat(date picker): révision des couleurs de la grille des jours
- feat(grid): déplacement des boutons d'actions au dessus du titre de la grille pour homogénéiser avec le DSFR 1.12
- feat(grid): exposition de `showPagination` et `showFooterResult`
- feat(autocomplete): `requireSelection` permet de forcer une valeur parmi la liste des suggestions
- feat(autocomplete): possibilité d'afficher la liste des suggestions en passant `minLength` à 0
- feat(autocomplete): la valeur de l'autocomplete est décorrélée de la valeur de l'input et correspond au type des suggestions en cas de sélection
- feat(multiselect/treeselect): ajout de `message` et `messageSeverity` pour harmoniser les messages de groupe (erreur, valide, info) avec ngx-dsfr
- feat(progressbar): ajout de `indeterminateContextMessage` pour donner plus de contexte au mode "indéterminé" avec un lecteur d'écran
- feat(tree): slot `nodeTemplate` pour personnaliser l'affichage du label des nœuds
- fix(autocomplete): accessibilité, complétion des attributs aria de la listbox et fermeture de la liste à la tabulation
- fix(autocomplete): sélection avec l'option `appendTo` non effective
- fix(grid): amélioration de la gestion du titre de la grille
- fix(grid): appliquer un outline à la sélection des cellules (identique navigation clavier)
- fix(grid): redimensionner les colonnes selon la taille de la grid après récupération des données en mode serveur
- fix(multiselect): focus et affichage incorrect de la liste avec la combinaison de `virtualScroll` et `appendTo`
- fix(grid): régression sur l'affichage des filtres actifs
- fix(multiselect/treeselect): améliorations accessibilité, ajouts de message de statut, attributs aria et prise de focus sur le filtre
- fix(multiselect/treeselect): remplacement de la case à cocher "Tout sélectionner" par un bouton d'action
- fix(multiselect/treeselect): correction du focus avec l'utilisation du virtual scroll
- fix(multiselect/treeselect): correction de la couleurs des options lors d'un message d'erreur ou de validation
- fix(panel): ajout d'un message indiquant l'état du bouton d'expansion
- fix(spinner): ajout d'un message apportant le contexte d'utilisation du composant (lecteur d'écran)
- fix(timeline): amélioration de l'accessibilité de la `timeline`
- fix(treeselect): Dans le cas du `clearableOptions`, déselectionner une option "multiple" casse le fonctionnement du composant
- fix(treeselect): mise à jour incorrect des états indéterminés des checkboxes dans le cas de la recherche

## 0.11.2 (2024-11-22)

- fix(grid): ajout de `selectionChange` pour le binding manquant au changement de sélection de ag-grid (`selectionChanged`)
- fix(grid): correction de l'affichage de la sélection en mode sans cases à cocher et avec colonnes fixées

## 0.11.1 (2024-10-25)

- fix(grid): régression 0.11 suite montée de version ag-grid, le modèle de sélection `selection` est renommé comme antérieurement `rowSelection`

## 0.11.0 (2024-10-14)

- break: version minimale requise ngx-dsfr 1.12
- break(grid): version minimale requise ag-grid 32
- break(grid): suppression des propriétés dépréciées `bordered` (utiliser `fr-table--bordered`) et `gridRender` (utiliser `gridReady`)
- feat(chore): ajout du mode haut contraste
- feat(date picker): affichage de la sélection du mois et de l'année similaire au composant Duet
- feat(editor): nouveau composant editeur riche basé sur `quill` (import depuis `@edugouvfr/ngx-dsfr-ext/editor`)
- feat(autocomplete): possibilité de virtual scrolling avec `virtualScroll` et `virtualScrollItemHeight`
- feat(autocomplete): ajout de `loading` et `delay` pour gérer le chargement en asynchrone
- feat(autocomplete): slot `suggestionTemplate` pour personnaliser l'affichage des suggestions
- feat(autocomplete): possibilité de passer des objets en tant que suggestions en précisant `suggestionValueKey`
- feat(autocomplete): exposition des évènements `suggestionSelect` et `listClose`
- feat(grid): revue du style de la grid pour homogénéiser avec la table DSFR 1.12
- feat(grid): implémentation du footer de la table `ngx-dsfr` avec ajout des slots `footerActionsTemplate` et `footerResultsTemplate`
- fix(date picker): problématiques de gestion de dates, d'affichage et d'accessibilité
- fix(multiselect/treeselect): apparence des inputs ne reflétant pas leur état `disabled`

## 0.10.0 (2024-07-25)

- break(datepicker): refonte du composant (suppression de la dépendance externe `duetds` et inclus dans `@edugouvfr/ngx-dsfr-ext`)
- feat(carousel): nouveau composant de carrousel
- feat(datepicker): possibilité de visualiser un masque de saisie
- fix(autocomplete/multiselect): ne pas cacher la liste lors de la suppression des options sous forme de tags
- fix(treeselect): correctif accessibilité pattern de navigation, cacher la liste à la touche entrée ou tabulation
- chore: migration ngx-dsfr 1.11.0

## 0.9.2 (2024-06-28)

- fix(grid): (mode serveur) pas de rechargement des données après un affichage de tableau vide (ex. après filtrage)
- fix(grid): (mode serveur) mauvaise application de la pagination passée en `initialState`
- fix(grid): (mode serveur) ajout de la propriété `rowId` pour corriger le rafraichissement de donnée avec `initialState`

## 0.9.1 (2024-06-12)

- fix(grid): exposition de la propriété ag-grid `initialState` pour permettre de passer un état initial à la grid (ex. page courante)
- fix(grid): exposition de l'évènement ag-grid `onGridPreDestroyed`
- fix(grid): dépréciation de l'évènement `gridRender` au profit de `gridReady` (lisibilité)
- fix(panel): régression bouton pour masquer le contenu non fonctionnel
- fix(panel): attribut aria-label manquant sur le bouton pour masquer le contenu

## 0.9.0 (2024-05-31)

- break(chore): version minimale requise angular 16
- feat(autocomplete): gestion filtre automatique
- feat(grid): composant `DsfrSelectFilterComponent` permettant l'utilisation du `dsfr-form-select` pour filtrer selon une liste d'options
- fix(autocomplete): correction de la gestion du focus pendant la navigation au clavier
- fix(grid): correctifs accessibilité contrastes de couleurs (couleurs de fond, de ligne, de focus et bordures)
- fix(grid): correctif accessibilité, gestion du focus pour une cellule avec éléments interactifs (implémentation par défaut de `suppressKeyboardEvent`)
- fix(grid): correctifs accessibilité sur la pagination du tableau (labels et positionnement)
- fix(grid): ajout d'une icône par défaut indiquant la possibilité de trier une colonne
- fix(grid): attributs manquants pour la gestion de l'accessibilité sur le composant `DsfrButtonCellGridComponent`
- fix(grid): dépréciation de `bordered` au profit de `customClass : 'fr-table-bordered'`
- fix(grid): hauteur minimale d'une ligne de 40px dans le cas d'utilisation de `autoRowsHeight`
- fix(grid): traductions manquantes
- fix(multiselect/treeselect): forcer la fermeture de la liste déroulante en cas de redimensionnement ou zoom de la fenêtre et utilisation de `appendTo`
- fix(spinner): le mode overlay ne couvre pas la totalité de la page
- doc: complément de la doc. du composant grid pour l'accessibilité
- chore: migration storybook 8

## 0.8.1 (2024-05-23)

- fix(toast): position des toasts

## 0.8.0 (2024-04-15)

- feat(multiselect): ajout d'un mode `single`
- feat(progressbar): nouveau composant `dsfr-ext-progressbar`
- fix(autocomplete): régression cacher la liste au clic en dehors de l'élément
- fix(chore): encapsuler les classes CSS par une classe préfixée par `dsfrx-`
- fix(multiselect): dépréciation de `showCheckboxes` au profit de `selectionMode`
- fix(multiselect, treeselect): correction de la zone d'affichage des messages d'erreur et validation
- fix(tree): correction de la couleur du noeud sélectionné
- fix(treeselect): en mode `single`, sélection uniquement des feuilles
- fix(treeselect): dans certains cas de sélections successives sur les checkboxes, l'état indéterminé affiché est incorrect
- fix(treeselect): correctif accessibilité, augmentation des espacements et zones de clic

## 0.7.2 (2024-04-03)

- fix(grid): style des checkboxes harmonisé avec le DSFR (corrige également l'utilisation avec `suppressRowClickSelection`)
- fix(grid): correctif accessibilité affichage du focus sur les cellules sélectionnées

## 0.7.1 (2024-03-26)

- fix(treeselect): correction initialisation du treeselect pour storybook

## 0.7.0 (2024-03-26)

- break(grid): version minimale requise ag-grid 31.1
- feat(date-picker): nouveau composant `dsfr-ext-datepicker`
- feat(autocomplete): possibilité de surcharger la valeur CSS z-index `zIndex`
- feat(grid): émission de l'évènement `rowSelect` lors de la sélection d'une ligne
- feat(multiselect): `clearableOptions` affiche les options sous forme de tags supprimables
- feat(multiselect): `clearSearchOnHide` vide le champ de recherche lorsque la liste est cachée
- feat(tree): nouveau composant `dsfr-ext-tree`
- feat(treeselect): nouveau composant `dsfr-ext-treeselect`
- fix(autocomplete): dépréciation de `appendToBody` au profit de `appendTo` pour pouvoir lier la liste au conteneur parent
- fix(autocomplete): correctif accessibilité navigation clavier avec `appendTo`
- fix(multiselect): correctif accessibilité navigation clavier avec `appendTo`
- fix(multiselect): correctif accessibilité container virtual scroll
- fix(multiselect): remise à zéro du filtre après modification de la fonction `searchFn`
- fix(multiselect): affichage des checkboxes en version small
- fix(toast): régression visuelle avec DSFR 1.11

## 0.6.2 (2024-02-28)

- fix(multiselect): dépréciation de `appendToBody` au profit de `appendTo` pour pouvoir lier la liste au conteneur parent
- fix(multiselect): correctif accessibilité génération automatiques des id des checkboxes
- fix(multiselect): correctif accessibilité placement du focus à la tabulation dans le cas de `appendTo`

## 0.6.1 (2024-02-14)

- fix(doc): erreur compilation storybook injection dependances I18n

## 0.6.0 (2024-02-14)

- feat(autocomplete): ajout de l'input `appendToBody` pour utiliser dans un container
- feat(multiselect): ajout possibilité de virtual scrolling (cf. `virtualScroll` et `virtualScrollItemHeight`)
- feat(multiselect): ajout des options `maxSelectedOptions`, `showCheckboxes`, `showSelectAll`
- fix(autocomplete): corrections accessibilité, attributs `aria-expanded` et `role` sur l'input
- feat(multiselect): possibilité de surcharger la valeur CSS z-index `zIndex`
- fix(autocomplete, multiselect): baser le z-index sur une valeur supérieure aux composants DSFR `lifted`
- fix(grid): correction des noms d'évènements `cellEditingStopped`, `filterChanged` et `sortChanged` pour la version côté client
- fix(grid): fix régression mode serveur sur ag-grid > v31.0.1, forcer utilisation dans ng zone pour mise à jour des données
- fix(multiselect): correction scroll horizontal intempestif avec l'option `appendToBody`
- fix(multiselect): renommage des classes CSS internes du composant (dorénavant préfixées `dsfrx-multiselect__*`)
- fix(doc): fix doc aucomplete
- chore: ajout @angular/cdk en peer dependency
- chore: migration ngx-dsfr 1.9.3
- chore(storybook): migration 7.6

## 0.5.0 (2023-12-14)

- break(chore): version minimale requise `@edugouvfr/ngx-dsfr` 1.8.0
- break(grid): L'import du module doit se faire depuis `/ngx-dsfr-ext/grid` (ajout d'un point d'entrée secondaire).
- break(grid): version minimale requise ag-grid 31.0
- feat(grid): ajout input `gridId`
- feat(toast): nouveau composant `dsfr-ext-toast`
- fix(grid): améliorations du rendu pour `autoRowsHeight` notamment si plusieurs tableaux
- fix(grid): correction des noms d'évènements `cellEditingStopped`, `filterChanged` et `sortChanged`
- chore: compatible `"strictNullChecks": true`
- chore: migration angular 15

## 0.4.3 (2023-12-04)

- fix(grid): erreur compilation type `rowData`
- fix(grid): valeur de l'attribut `fixedHeight` pris en compte

## 0.4.2 (2023-11-27)

- fix(autocomplete): améliorations accessibilité (attributs aria-controls et aria-activedescendant)
- fix(multiselect): améliorations accessibilité (interactions clavier échap/retour tab)
- chore(storybook): migration 7.5
- chore: migration ngx-dsfr 1.7.1
- chore: ajout ag-grid en peer dependencies

## 0.4.1 (2023-11-08)

- fix(grid): mise à jour de la pagination au changement d'affichage du nombre de pages successifs
- fix(multiselect): prise en compte du reset des options et de la value fait programmaticalement

## 0.4.0 (2023-11-02)

- feat(panel): mise à disposition du composant `dsfr-ext-panel`
- feat(spinner): ajout de la propriété `overlay`
- feat(timeline): nouveau composant `dsfr-ext-timeline`
- feat(multiselect): ajout d'une propriété `compareWith` pour fonction de comparaison custom
- fix(multiselect): pas de mise à jour de la value si elle n'est pas au format attendu
- fix(multiselect): correctifs affichage du champ de recherche notamment en cas de multiples multiselect

## 0.3.0 (2023-10-12)

- feat(grid) : ajout d'un attribut `autoRowsHeight` pour permettre une hauteur de ligne dynamique en mode serveur
- feat(multiselect): ajout de l'attribut `appendToBody` pour utiliser dans un conteneur
- fix(multiselect): correction mise à jour des valeurs dans un reactive form
- chore(storybook): migration 7.4.5

## 0.2.0 (2023-09-12)

- feat(autocomplete): nouveau composant `dsfr-ext-autocomplete`
- feat(multiselect) : nouveau composant `dsfr-ext-multiselect`
- feat(spinner): nouveau composant `dsfr-ext-spinner`
- chore: montée de version `@edugouvfr/ngx-dsfr@1.4.0`

## 0.1.0 (2023-07-04)

- feat(datagrid): mise à disposition du composant `dsfr-ext-ag-grid`
- feat(spinner): mise à disposition du composant `dsfr-ext-spinner`
